// Copyright 2017 The Australian National University
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Run the following after modifying this file:
// wget http://www.antlr.org/download/antlr-4.7-complete.jar -O antlr.jar
// java -jar antlr.jar -Dlanguage=Cpp -no-listener -visitor -o parser/  -lib ./ UIR.g4

grammar UIR;

ir
    :   topLevelDef* EOF
    ;

topLevelDef
    :   '.bundle' (NAME | GLOBAL_NAME)                                                           # BundleDef
    |   '.typedef' nam=globalName '=' ctor=typeConstructor                                     # TypeDef
    |   '.funcsig' nam=globalName '=' ctor=funcSigConstructor                                  # FuncSigDef
    |   '.const' nam=globalName '<' ty=type '>' '=' ctor=constConstructor                      # ConstDef
    |   '.global' nam=globalName '<' ty=type '>'                                               # GlobalDef
    |   '.funcdecl' nam=func '<' sig=funcSig '>'                                                 # FuncDecl
    |   '.expose' nam=globalName '=' func callConv cookie=constant                             # FuncExpDef
    |   '.funcdef' nam=func
            (('<' sig=funcSig '>' ('VERSION' ver=localName)?) |
             ('VERSION' ver=localName  ('<' sig=funcSig '>')?))? '{' body+=basicBlock+ '}'            # FuncDef
    ;
typeConstructor
    :   'int'    	'<' length=intParam '>'                  	# TypeInt
    |   'float'                                                 # TypeFloat
    |   'double'                                                # TypeDouble
    |   'uptr'      '<' ty=type '>'                             # TypeUPtr
    |   'ufuncptr'  '<' funcSig '>'                             # TypeUFuncPtr
    |   'struct'    '<' fieldTys+=type+ '>'                     # TypeStruct
    |   'hybrid'    '<' fieldTys+=type* varTy=type '>'          # TypeHybrid
    |   'array'     '<' ty=type length=sizeParam '>'            # TypeArray
    |   'vector'    '<' ty=type length=sizeParam '>'            # TypeVector
    |   'void'                                                  # TypeVoid
    |   'ref'	 	'<' ty=type '>'                             # TypeRef
    |   'iref' 		'<' ty=type '>'                             # TypeIRef
    |   'weakref' 	'<' ty=type '>'                             # TypeWeakRef
    |   'tagref64'                                              # TypeTagRef64
    |   'funcref' 	'<' funcSig '>'                             # TypeFuncRef
    |   'threadref'                                             # TypeThreadRef
    |   'stackref'                                              # TypeStackRef
    |   'framecursorref'                                        # TypeFrameCursorRef
    |   'irbuilderref'                                          # TypeIRBuilderRef
    ;

funcSigConstructor
    :   '(' paramTys+=type* ')' '->' '(' retTys+=type* ')'
    ;

constConstructor
    :   intLiteral                    # CtorInt
    |   floatLiteral                  # CtorFloat
    |   doubleLiteral                 # CtorDouble
    |   'NULL'                        # CtorNull
    |   '{' globalVar* '}'            # CtorList
    |   'EXTERN' symbol=stringLiteral # CtorExtern
    ;

type
    :   nam=globalName
    |   typeConstructor
    ;

funcSig
    :   nam=globalName
    |   funcSigConstructor
    ;

value
    :   nam=name
    |   constant
    ;

constant
    :   nam=globalName
    |   '<' ty=type '>' constConstructor
    ;

func
    :   nam=globalName
    ;

globalVar
    :   nam=globalName
    ;

basicBlock
    :   nam=localName '(' ('<' param_tys+=type '>' params+=localName)* ')' ('[' exc=localName ']')? ':' inst+
    ;


instName
    : ('[' nam=localName ']')?
    ;

instResult
    : result=localName '='
    ;
instResults
    : results+=localName '='
    | '(' results+=localName* ')' '='
    |
    ;
pairResult
    :'(' first=localName second=localName ')' '='
    ;
pairResults
    : first=localName '='
    |'(' first=localName second+=localName+ ')' '='
    ;

inst
    : instResult  instName binop            '<' ty=type '>'                op1=value op2=value excClause?                                                                        # InstBinOp
    | pairResults instName binop            binopStatus '<' ty=type '>'    op1=value op2=value excClause?                                                                        # InstBinOpStatus
    | instResult  instName cmpop            '<' ty=type '>'                op1=value op2=value                                                                                   # InstCmp
    | instResult  instName convop           '<' fromTy=type toTy=type '>'  opnd=value                                                                                            # InstConversion
    | instResult  instName 'SELECT'         '<' condTy=type resTy=type '>' cond=value ifTrue=value ifFalse=value                                                                 # InstSelect
    |             instName 'BRANCH'         dest=destClause                                                                                                                      # InstBranch
    |             instName 'BRANCH2'        cond=value ifTrue=destClause ifFalse=destClause                                                                                      # InstBranch2
    |             instName 'SWITCH'         '<' ty=type '>' opnd=value defDest=destClause '{' (caseVal+=constant caseDest+=destClause )* '}'                                     # InstSwitch
    | instResults instName 'CALL'           '<' funcSig '>' callee=value argList excClause? keepaliveClause?                                                                     # InstCall
    |             instName 'TAILCALL'       '<' funcSig '>' callee=value argList                                                                                                 # InstTailCall
    |             instName 'RET'            retVals                                                                                                                              # InstRet
    |             instName 'THROW'          exc=value                                                                                                                            # InstThrow
    | instResult  instName 'EXTRACTVALUE' 	'<' ty=type intParam '>'      opnd=value               		                                                      	                 # InstExtractValue
    | instResult  instName 'INSERTVALUE' 	'<' ty=type intParam '>'      opnd=value newVal=value   			                                                                 # InstInsertValue
    | instResult  instName 'EXTRACTELEMENT' '<' seqTy=type indTy=type '>'   opnd=value index=value                                                                               # InstExtractElement
    | instResult  instName 'INSERTELEMENT' 	'<' seqTy=type indTy=type '>'   opnd=value index=value newVal=value                                                                  # InstInsertElement
    | instResult  instName 'SHUFFLEVECTOR' 	'<' vecTy=type maskTy=type '>'  vec1=value vec2=value  mask=value                                                                    # InstShuffleVector
    | instResult  instName 'NEW'            '<' allocTy=type '>'            excClause?                                                                                           # InstNew
    | instResult  instName 'NEWHYBRID'      '<' allocTy=type lenTy=type '>' length=value excClause?                                                                              # InstNewHybrid
    | instResult  instName 'ALLOCA'         '<' allocTy=type '>'            excClause?                                                                                           # InstAlloca
    | instResult  instName 'ALLOCAHYBRID'   '<' allocTy=type lenTy=type '>' length=value excClause?                                                                              # InstAllocaHybrid
    | instResult  instName 'GETIREF'        '<' refTy=type '>' opnd=value                                                                                                        # InstGetIRef
    | instResult  instName 'GETFIELDIREF'   (ptr='PTR'?) '<' refTy=type index=intParam '>' opnd=value                                                                            # InstGetFieldIRef
    | instResult  instName 'GETELEMIREF'    (ptr='PTR'?) '<' refTy=type indTy=type '>' opnd=value index=value                                                                    # InstGetElemIRef
    | instResult  instName 'SHIFTIREF'      (ptr='PTR'?) '<' refTy=type offTy=type '>' opnd=value offset=value                                                                   # InstShiftIRef
    | instResult  instName 'GETVARPARTIREF' (ptr='PTR'?) '<' refTy=type '>' opnd=value                                                                                           # InstGetVarPartIRef
    | instResult  instName 'LOAD'           (ptr='PTR'?) memord? '<' type '>' loc=value excClause?                                                                               # InstLoad
    |             instName 'STORE'          (ptr='PTR'?) memord? '<' type '>' loc=value newVal=value excClause?                                                                  # InstStore
    | pairResult  instName 'CMPXCHG'        (ptr='PTR'?) (isWeak='WEAK'?) ordSucc=memord ordFail=memord '<' type '>' loc=value expected=value desired=value excClause?           # InstCmpXchg
    | instResult  instName 'ATOMICRMW'      (ptr='PTR'?) memord atomicrmwop '<' type '>' loc=value opnd=value excClause?                                                         # InstAtomicRMW
    |             instName 'FENCE'          memord                                                                                                                               # InstFence
    | instResults instName 'TRAP'           typeList excClause? keepaliveClause                                                                                                  # InstTrap
    | instResults instName 'WATCHPOINT'     wpid typeList dis=destClause ena=destClause ('WPEXC' '(' wpExc=destClause ')')? keepaliveClause?                                     # InstWatchPoint
    |             instName 'WPBRANCH'       wpid dis=destClause ena=destClause                                                                                                   # InstWPBranch
    | instResults instName 'CCALL'          callConv '<' funcTy=type funcSig '>' callee=value argList excClause? keepaliveClause?                                                # InstCCall
    | instResult  instName 'NEWTHREAD'      stack=value ('THREADLOCAL' '(' threadLocal=value ')')? newStackClause excClause?                                                       # InstNewThread
    | instResults instName 'SWAPSTACK'      swappee=value curStackClause newStackClause excClause? keepaliveClause?                                                                # InstSwapStack
    | instResults instName 'COMMINST'       commInst ('[' flags+=flag* ']')? ('<' tys+=type* '>')? ('<[' sigs+=funcSig* ']>')? ('(' args+=value* ')')? excClause? keepaliveClause? # InstCommInst
    ;

retVals
    :   '(' vals+=value* ')'
    |   vals+=value?
    ;

destClause
    :   bb argList
    ;

bb
	:	nam=localName
	;

typeList
    : '<' tys+=type* '>'
    ;

excClause
    :   'EXC' '(' nor=destClause exc=destClause ')'
    ;

keepaliveClause
    :   'KEEPALIVE' '(' value* ')'
    ;

argList
    :   '(' value* ')'
    ;

curStackClause
    :   'RET_WITH' typeList             # CurStackRetWith
    |   'KILL_OLD'                      # CurStackKillOld
    ;

newStackClause
    :   'PASS_VALUES' typeList argList      # NewStackPassValue
    |   'THROW_EXC' exc=value               # NewStackThrowExc
    ;

binop
    : 'ADD' | 'SUB' | 'MUL' | 'UDIV' | 'SDIV' | 'UREM' | 'SREM' | 'SHL' | 'LSHR' | 'ASHR' | 'AND' | 'OR' | 'XOR'
    | 'FADD'| 'FSUB' | 'FMUL' | 'FDIV' | 'FREM'
    ;

cmpop
    : 'EQ' | 'NE' | 'SGT' | 'SLT' | 'SGE' | 'SLE' | 'UGT' | 'ULT' | 'UGE' | 'ULE'
    | 'FTRUE' | 'FFALSE'
    | 'FUNO' | 'FUEQ' | 'FUNE' | 'FUGT' | 'FULT' | 'FUGE' | 'FULE'
    | 'FORD' | 'FOEQ' | 'FONE' | 'FOGT' | 'FOLT' | 'FOGE' | 'FOLE'
    ;

convop
    : 'TRUNC' | 'ZEXT' | 'SEXT' | 'FPTRUNC' | 'FPEXT'
    | 'FPTOUI' | 'FPTOSI' | 'UITOFP' | 'SITOFP'
    | 'BITCAST' | 'REFCAST' | 'PTRCAST'
    ;

memord
    : 'NOT_ATOMIC' | 'RELAXED' | 'CONSUME' | 'ACQUIRE' | 'RELEASE' | 'ACQ_REL' | 'SEQ_CST'
    ;

atomicrmwop
    : 'XCHG' | 'ADD' | 'SUB' | 'AND' | 'NAND' | 'OR' | 'XOR' | 'MAX' | 'MIN' | 'UMAX' | 'UMIN'
    ;

binopStatus
    : '#N' | '#Z' | '#C' | '#V'
    ;

callConv
    :   '#DEFAULT'
    ;

flag
    : callConv
    | binopStatus
    ;

wpid : intLiteral;
intParam : intLiteral;
sizeParam : intLiteral;

intLiteral
    : INT_DEC
    | INT_HEX
    | INT_OCT
    ;


floatLiteral
    :   FP_NUM 'f'  # FloatNumber
    |   INF 'f'     # FloatInf
    |   NAN_FP 'f'     # FloatNan
    |   'bitsf' '(' intLiteral ')'   # FloatBits
    ;

doubleLiteral
    :   FP_NUM 'd'  # DoubleNumber
    |   INF 'd'     # DoubleInf
    |   NAN_FP 'd'     # DoubleNan
    |   'bitsd' '(' intLiteral ')'   # DoubleBits
    ;

stringLiteral
    :   STRING_LITERAL
    ;

globalName // References a global (i.e. top level localName)
    : NAME
    | LOCAL_NAME
    | GLOBAL_NAME
    ;

localName
    : NAME
    | GLOBAL_NAME
    | LOCAL_NAME
    ;

name
    : NAME
    | GLOBAL_NAME
    | LOCAL_NAME
    ;

commInst: NAME | GLOBAL_NAME;

// LEXER
INT_DEC
    :   ('+'|'-')? DIGIT_NON_ZERO DIGIT*
    ;

INT_OCT
    :   ('+'|'-')? '0' OCT_DIGIT*
    ;

INT_HEX
    :   ('+'|'-')? '0x' HEX_DIGIT+
    ;

FP_NUM
    :   ('+'|'-')? DIGIT+ '.' DIGIT+ ('e' ('+'|'-')? DIGIT+)?
    ;

INF
    :   ('+'|'-') 'inf'
    ;

NAN_FP
    :   'nan'
    ;

NAME : IDCHAR+
     ;
GLOBAL_NAME
    : GLOBAL_NAME_PREFIX IDCHAR+
    ;

LOCAL_NAME
    :   LOCAL_NAME_PREFIX IDCHAR+
    ;

STRING_LITERAL
    :   '"' NON_QUOTE* '"'
    ;

fragment
DIGIT
    :   [0-9]
    ;

fragment
DIGIT_NON_ZERO
    :   [1-9]
    ;

fragment
OCT_DIGIT
    :   [0-7]
    ;

fragment
HEX_DIGIT
    :   [0-9a-fA-F]
    ;

fragment
GLOBAL_NAME_PREFIX: '@';

fragment
LOCAL_NAME_PREFIX: '%';

fragment
IDCHAR
    :   [a-z]
    |   [A-Z]
    |   [0-9]
    |   '-'
    |   '_'
    |   '.'
    |   '#' // Internal use only
    |   ':' // Internal use only
    ;

fragment
NON_QUOTE
    :   [!#-~]
    ;

WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines

LINE_COMMENT
    :   '//' ~[\r\n]* -> skip
    ;

C_COMMENT
    :   '/*' ~[*]* '*/' -> skip
    ;
