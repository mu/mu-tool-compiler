// Copyright 2017 The Australian National University
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Contains stuff common to both RuntimeVisitor and CVisitor
#pragma once

#include <map>
#include <string>
#include <set>
#include <stack>
#include <vector>
#include <utility>
#include <exception>
#include <memory>
#include <limits>
#include <iostream>
#include <type_traits>
#include <sstream>
#include <gmpxx.h>

//#include "muapi.h"
#include "parser/UIRBaseVisitor.h"
#include "parser/UIRParser.h"
#include "parser/UIRLexer.h"

using namespace std::literals;

// Returns 'v' seperated by ', '
template<typename T>
std::string list_to_string(T v) {
	if (v.size() == 0)
		return ""s;

	std::string value = std::to_string(v[0]);
	bool start = true;
	for (auto val : v) {
		if (!start)
			value += ", "s;
		else start = false;
		value += std::to_string(val);
	}
	return value;
}

// Returns 'v' seperated by ', '
template<>
std::string list_to_string<std::vector<std::string>>(std::vector<std::string> v) {
	if (v.size() == 0)
		return ""s;

	std::string value = v[0];
	for (std::size_t i = 1; i < v.size(); i++)
		value += ", "s + v[i];
	return value;
}

template<typename T>
std::string concat_strings(T v) {
	std::string result {};
	for (auto val : v)
		result += std::to_string(val);

	return result;
}

// Returns 'v' seperated by ' '
template<>
std::string concat_strings<std::vector<std::string>>(std::vector<std::string> v) {
	std::string result {};
	for (auto val : v)
		result += val;
	return result;
}


// This is converted to a string incase UINT64_MAX dosn't fit in an unsigned long
// (an mpz_class can't be constructed from a long long)
mpz_class mask_64(std::to_string(UINT64_MAX));
bool error = false; // Was there a syntax error?
std::string primordial_name;
std::stack<std::string> parent_names {}; // The names of the parent entitys
std::string last_name = ""; // The last name that was read (in global form)

std::string bundle_name = ""; // the name of the current bundle

void generate_bundle_name() {
	static std::size_t bundle_id = 0;
	::bundle_name = "__bundle__"s + std::to_string(bundle_id++);
}

// This is used to annotate the return types of visitor functions
template<typename T = void> using Any = antlrcpp::Any;

std::size_t stosz(const std::string& str, std::size_t* pos = 0, int base = 10 )
{
	static_assert(sizeof(std::size_t) <= sizeof(unsigned long long), "size_t should not be bigger than unsigned long long");
	return (std::size_t)std::stoull(str, pos, base);
}

bool is_valid_mu_name(std::string s) {
	return std::all_of(++s.cbegin(), s.cend(), [](char c){
		return std::isalnum(c) || c == '-' || c == '_' || c == '.';
	});
}