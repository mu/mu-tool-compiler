<!-- Copyright 2017 The Australian National University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# muc - A mu compiler
Translates MuIR in text form and creates a correspondinng boot image, or generates C code that does the same.

## Preqrequisites:
* You will need a C++14 compiler (tested with clang++6.0.0)
* You will need the GNU Multiple Precision Arithmetic Library (tested with GMP 6.1.0)
* You will need the Antlr4 C++ runtime, use `git clone https://github.com/antlr/antlr4.git` and follow the build instructions in `runtime/Cpp/README.md`.
  - Note that on some systems, `cmake` does not recognize the correct toolchain to use. So, we recommand (un)setting both `CC` and `CXX` environment variables, before running the `cmake ..` command.
* If you want to compile directly (as opposed to output C files) you'll need Mu, place your `libmu*` files in `./lib`,
also set the environement variable `MU_ZEBU` to the locatation of the mu-impl-fast source code.

## C Code prerequisites
To run the generated C code you will need:
* To link with Mu
  - Make sure that the environment variable `MU_ZEBU` is set to the locatation of the mu-impl-fast source code,
  - Make sure to set the environment variable `ZEBU_BUILD` to the proper value (either `release` or `debug`)
* A C compiler (tested with GCC version 5.4.0, with the default `-std=gnu11`)

## Building
Run 
```
export ANTRL_HOME=$antlr4/runtime/Cpp/run
make
``` 
where `$antlr4` is the root of the antlr4 git repo you just cloned.

If you don't have Mu run `make no_mu`. If you're getting load errors, try `make static`.

## Usage
`LD_LIBRARY_PATH=./lib muc -r [-f primordial-function-name] bundle1.mu... bootimage`

Which will compile the bundles (there must be at least one) and generate a boot image in the file `bootimage`
Mu's emmit directory (were it puts intermediate files like assemblys) will be the same folder that contains `bootimage`.
Use `-c` instead of `-r` and it won't actually do that, instead it will print C code to stdout that will have the same effect once compiled and run.
Use `-s` instead of `-r` to do syntax checking of your input files and nothing else.

## Syntax
The syntax is specified in `UIR.g4`, all the files in `./parser` are generated from this file.
The syntax is a super set of the reference implementations syntax and the syntax used in the mu-spec.

It adds the following 'syntax sugar':

* Comments, C++ style `// ... ` and C-style `/* ... */`.
* The signature in a `.funcdef` is optional, it may also be specified before the `VERSION` name. 
* If there is a signature in a `.funcdef` and no previouse corresponding `.funcdecl`, one will be created for you.
* the version name in a `.funcdef` is optional, if absent a local one (starting with a `%`) will be automatically generated 
(e.g. you can write `.funcdef @foo<@sig> { ... }`, instead of `.fundecl @foo<@sig>` and `.funcdef @foo VERSION %__1 {...}`)
* Types can be inlined (e.g. `.global @foo <int<32>>`, instead of `.typedef @int32 = int<32>`, `.global @foo <@int32>`)
* Function signatures can be inlined (e.g. `.funcdecl @foo <(int<32>)->(int<32>)>`).
* Constants can be inlined with the syntax `<type>ctor` (equivelent to `.const @__anon <type> = ctor`).
* A bundle may be given a name with the directive `.bundle @name` (the `@` is optional), it takes effect only for the following lines,
the name can be changed again with another `.bundle` directive. Muc will automatically generate a name if there isn't one.  
(note: the name is only used in forming local names (see below), it is not passed to the Mu API).
* A top level declaration may be given a local name, in which case it is prefixed with the name of the bundle followed by a `.`. 
  (e.g. `.typedef %void = void`, instead of `.typedef @__bundle_1.foo`). (Note: you can give a `.const` a name starting with `%`, however
  when refering it with `%`, if there is a local name more recently defined with a `%` (such as a block paramter) that will have priority.
* Top level declarations with a global name starting with an `_` are not passed to the whitelist of make bootimage (such as names local to unamed bundles)  
* You may ommit the `@` or `%` in front of names, where the name `foo` is interpreted according to the following rules:
    * if the name could be interpreted as a keyword/part of the syntax, it is (e.g. `.global v <void>` creates a new anonymous type with value `void`, it does not refer to `@void`)
    * if the name is used for a 'value' (an operand of an instruction, but not a constant) and `%foo` was declared, then it is interpreted as `%foo`
    * if it is used where the name of a top-level declaration is expected (such as a constant), it is interepreted as `@foo`
    * otherwise it is interpereted as `%foo`
* A local name `%foo` is interpreted according to the following rules (a superset of those in the mu-spec)
    * if the name is used for a 'value' and `@block.foo` was previously declared, then it is interpreted as that
    * if the name is used for a function version name it is interpreted as `@function.foo`
    * if it is used as the name of a basic block (in either a defintion or a destination clause) it is interpreted as `@function_version.foo`
    * if it is used as the name of an instruction or result of an instruction, it is interpreted as `@block.foo`
    * otherwise it is interpreted as `@bundle.foo`

Notes:
A new declaration will be generated for each unique inline declaration. In addition, they will not be passed to the white list of boot images.
An inline declaration will never have the same ID as a non-inline declaration.
e.g.
    
    .typedef @int32 = int<32>
    .funcsig @foo = (int<32>)->(int<32>)
Declares 2 types `@int32` and an anonymous `int<32>`.



## Limitations
Muc will only check for correct syntax, it is up to you to ensure your input is semantically valid.

There is a bug in Zebu in that it will not allow you to reference entities declared in another bundle, to overcome this
combine your bundles into 1, e.g. `./muc -r <(cat bundle1.uir bundle2.uir) out`, unfortunently this will change the behaviour
of how local top level names are interpreted, unless you put `.bundle` directives at the start of each bundle file.

The file mu-fastimpl.h was taken from 'mu/mu-impl-fast' (master branch) and muapi.h from 'mu/mu-spec' they are kept up to date on each commit of mu-tool-compiler.
If they are changed (or Zebu's behaviour or the spec changes with respect to hem), muc may not work correctly.

If you are declaring a `.const` with an integer literal, the type must be declared before the literal 
(i.e. it must be declared inline like `.const @foo <int<32>> = 3` or be declared in a previous line of the bundle or in a previous bundle).
In all other cases (except for referencing the result of an instruction or a block parameter) you may reference an entity before it is declared, as long as it is declared in the same bundle (this is the same restriction the Mu-spec places).

When using `-c`, float and double inline constants will be be considered unique (for the purposes of generating inline declarations) if there text is unique, e.g:
   `%a = FADD <float> <float>1.1f <float>1.10f` will generate two constants, whereas when using `-r` it will only generate one (since 1.1 = 1.10).

Bundle names will be generated in the form `@__bundle_#`. (so to be safe never write global names starting with a `__` in your input files).

