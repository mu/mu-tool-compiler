// Copyright 2017 The Australian National University
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once
#include "Visitor.hpp"

// Match string against 'value' if it matches returns prefix_value
#define MACRO_MATCH(string, prefix, value) ((string) == #value) ? (prefix ## _ ## value)

namespace Runtime
{
	MuID primordial_id {MU_NO_ID};

	std::map<std::string, MuID> globals {};
	std::map<MuID, int> int_sizes {};
	std::set<MuID> funcdecls {};
	std::vector<MuID> whitelist {}; // The set of all things to pass to the whitelist of nake boot image
	std::map<std::string, MuID> inline_decls {};
	std::size_t anon_id = 0; // Generate names from this (needed as Zebu dosn't work when some things don't have names)

	// These are needed as 'as' dosn't work with 'void' (since it forms a reference)
	template<typename T> struct void_if { using type = decltype(antlrcpp::Any().template as<T>()); };
	template<> struct void_if<void> { using type = void; };
	template<typename T> typename void_if<T>::type as_type(antlrcpp::Any&& a) { return a.template as<T>(); }
	template<> void_if<void>::type as_type<void>(antlrcpp::Any&& a) { return; }

	struct Visitor : UIRVisitor {
		Visitor(MuCtx* ctx) : irbuilder(ctx->new_ir_builder(ctx)) { generate_bundle_name(); }
		~Visitor() {
			if (irbuilder != nullptr) // Will only be true if 'load' hasn't been called yet
				irbuilder->abort(irbuilder); // Cleanup (in case of an
		}
		MuIRBuilder* irbuilder = nullptr;

		std::map<std::string, MuID> symbols {};

		int inline_decl = 0; // Is the 'constructor' currently being processed part of an inline declaration?

		// A table of all string literals (this keeps them alive, guarantees destruction, and prevents the underlying string buffers from moving)
		std::list<std::unique_ptr<std::string>> string_table;

		std::stack<MuID> current_ids; // This id of the objects we are constructing
		MuTypeNode current_type; // The type of the thing we are trying to construct
		// Get the id of the global name
		MuID get_id(std::string name, bool global /*if we can't find it, it must be a global*/) {
			MuID id;
			if (symbols.count(name) == 0) {
				// Must reference a top level
				if (globals.count(name) != 0)
					id = symbols[name] = globals[name];
				else {
					std::string global_name = "@"s + name;
					id = symbols[name] = irbuilder->gen_sym(irbuilder, is_valid_mu_name(global_name) ? global_name
							.c_str() : nullptr);
					if (global) {
						globals[name] = id;
						if (name[0] != '_')
							whitelist.push_back(id);
					}
				}
			} else {
                id = symbols[name];
			}

			if (name == primordial_name)
				primordial_id = id;

			return id;
		}

		// Calls 'accept' if the given tree (typically a context) is non-null and casts to 'T', otherwise returns def
		template<typename T>
		T try_accept(antlr4::tree::ParseTree* tree, T def) {
			return tree ? as_type<T>(tree->accept(this)) : def;
		}

		// Calls 'accept' on the given tree (typically a context) and casts the result to 'T'
		template<typename T = void>
		T accept(antlr4::tree::ParseTree* tree) {
			return as_type<T>(tree->accept(this));
		}

		// str: A (unique) string representation of the declaration ctor
		// id: were to return the id we should use for the ctor
		// returns: true if we should continue with the declaration, or false if 'id' is already declared
		// (NOTE: This will push to current_ids)
		bool declare_inline(std::string str, MuID& id) {
			if (inline_decl) {
				// Already generated an equivelent declaration
				if (inline_decls.count(str)) {
					current_ids.push(id = inline_decls[str]);
					return false;
				} else {
					current_ids.push(id = inline_decls[str] = irbuilder->gen_sym(irbuilder, nullptr));
					return true;
				}
			} else {
				current_ids.push(id = current_ids.top());
				return true;
			}
		}
		template<typename T, typename U>
		typename std::vector<T> accept_list(std::vector<U*> trees) {
			std::vector<T> res;
			res.reserve(trees.size());
			for (auto tree : trees) {
				auto a = tree->accept(this);
				if (a.isNotNull()) {
					res.push_back(std::move(a));
				}
			}
			return res;
		}

		/*********************************** NON TERMINALS ****************************/
		virtual Any<void> visitIr(UIRParser::IrContext *context) override {
			for (auto child : context->children)
				accept(child);

			irbuilder->load(irbuilder); // Finish compiling the bundle;
			irbuilder = nullptr;
			return Any<void>();
		}

		virtual Any<void> visitBundleDef(UIRParser::BundleDefContext *context) override {
			if (context->NAME()) // Interpret as a global name
				::bundle_name = context->NAME()->getText();
			else ::bundle_name = context->GLOBAL_NAME()->getText().substr(1, std::string::npos);

			return Any<void>();
		}

		virtual Any<void> visitTypeDef(UIRParser::TypeDefContext *context) override {
			current_ids.push(accept<MuID>(context->nam));
			accept(context->ctor);
			current_ids.pop();
			return Any<void>();
		}
		virtual Any<MuTypeNode> visitTypeInt(UIRParser::TypeIntContext *context) override {
			auto size = accept<int>(context->length);

			MuID id;
			if (declare_inline("int<"s + std::to_string(size) + ">", id)) {
				int_sizes[id] = size;
				irbuilder->new_type_int(irbuilder, id, size);
			}
			current_ids.pop();
			return (MuTypeNode)id;
		}
		virtual Any<MuTypeNode> visitTypeUFuncPtr(UIRParser::TypeUFuncPtrContext *context) override {
			auto sig = accept<MuFuncSigNode>(context->funcSig());
			MuID id;
			if (declare_inline("ufuncptr<"s + std::to_string(sig) + ">"s, id))
				irbuilder->new_type_ufuncptr(irbuilder, id, sig);
			current_ids.pop();
			return (MuTypeNode)id;
		}
		virtual Any<MuTypeNode> visitTypeFuncRef(UIRParser::TypeFuncRefContext *context) override {
			auto sig = accept<MuFuncSigNode>(context->funcSig());
			MuID id;
			if (declare_inline("funcref<"s + std::to_string(sig) + ">"s, id))
				irbuilder->new_type_funcref(irbuilder, id, sig);
			current_ids.pop();
			return (MuTypeNode)id;
		}
		virtual Any<MuTypeNode> visitTypeStruct(UIRParser::TypeStructContext *context) override {
			auto fieldtys = accept_list<MuTypeNode>(context->fieldTys);
			MuID id;
			if (declare_inline("struct<"s + list_to_string(fieldtys) + ">"s, id))
				irbuilder->new_type_struct(irbuilder, id, &fieldtys[0], fieldtys.size());
			current_ids.pop();
			return (MuTypeNode)id;
		}
		virtual Any<MuTypeNode> visitTypeHybrid(UIRParser::TypeHybridContext *context) override {
			auto fixedtys = accept_list<MuTypeNode>(context->fieldTys);
			auto varty = accept<MuTypeNode>(context->varTy);
			MuID id;
			if (declare_inline("struct<"s + list_to_string(fixedtys) + " " + std::to_string(varty) + ">"s, id))
				irbuilder->new_type_hybrid(irbuilder, id, &fixedtys[0], fixedtys.size(), varty);
			current_ids.pop();
			return (MuTypeNode)id;
		}
		virtual Any<MuTypeNode> visitTypeArray(UIRParser::TypeArrayContext *context) override {
			auto ty = accept<MuTypeNode>(context->ty);
			auto len = accept<uint64_t>(context->length);
			MuID id;
			if (declare_inline("array<"s + std::to_string(ty) + " " + std::to_string(len) + ">"s, id))
				irbuilder->new_type_array(irbuilder, id, ty, len);
			current_ids.pop();
			return (MuTypeNode)id;
		}
		virtual Any<MuTypeNode> visitTypeVector(UIRParser::TypeVectorContext *context) override {
			auto ty = accept<MuTypeNode>(context->ty);
			auto len = accept<uint64_t>(context->length);
			MuID id;
			if (declare_inline("vector<"s + std::to_string(ty) + " " + std::to_string(len) + ">"s, id))
				irbuilder->new_type_vector(irbuilder, id, ty, len);
			current_ids.pop();
			return (MuTypeNode)id;
		}
		virtual Any<MuTypeNode> visitTypeUPtr(UIRParser::TypeUPtrContext *context) override {
			auto ty = accept<MuTypeNode>(context->ty);
			MuID id;
			if (declare_inline("uptr<"s + std::to_string(ty) + ">"s, id))
				irbuilder->new_type_uptr(irbuilder, id, ty);
			current_ids.pop();
			return (MuTypeNode)id;
		}
		virtual Any<MuTypeNode> visitTypeRef(UIRParser::TypeRefContext *context) override {
			auto ty = accept<MuTypeNode>(context->ty);
			MuID id;
			if (declare_inline("ref<"s + std::to_string(ty) + ">"s, id))
				irbuilder->new_type_ref(irbuilder, id, ty);
			current_ids.pop();
			return (MuTypeNode)id;
		}
		virtual Any<MuTypeNode> visitTypeIRef(UIRParser::TypeIRefContext *context) override {
			auto ty = accept<MuTypeNode>(context->ty);
			MuID id;
			if (declare_inline("iref<"s + std::to_string(ty) + ">"s, id))
				irbuilder->new_type_iref(irbuilder, id, ty);
			current_ids.pop();
			return (MuTypeNode)id;
		}
		virtual Any<MuTypeNode> visitTypeWeakRef(UIRParser::TypeWeakRefContext *context) override {
			auto ty = accept<MuTypeNode>(context->ty);
			MuID id;
			if (declare_inline("weakref<"s + std::to_string(ty) + ">"s, id))
				irbuilder->new_type_weakref(irbuilder, id, ty);
			current_ids.pop();
			return (MuTypeNode)id;
		}
		virtual Any<MuTypeNode> visitTypeTagRef64(UIRParser::TypeTagRef64Context *context) override {
			MuID id;
			if (declare_inline("tagref64"s, id))
				irbuilder->new_type_tagref64(irbuilder, id);
			current_ids.pop();
			return (MuTypeNode)id;
		}
		virtual Any<MuTypeNode> visitTypeFloat(UIRParser::TypeFloatContext *context) override {
			MuID id;
			if (declare_inline("float"s, id))
				irbuilder->new_type_float(irbuilder, id);
			current_ids.pop();
			return (MuTypeNode)id;
		}
		virtual Any<MuTypeNode> visitTypeDouble(UIRParser::TypeDoubleContext *context) override {
			MuID id;
			if (declare_inline("double"s, id))
				irbuilder->new_type_double(irbuilder, id);
			current_ids.pop();
			return (MuTypeNode)id;
		}
		virtual Any<MuTypeNode> visitTypeThreadRef(UIRParser::TypeThreadRefContext *context) override {
			MuID id;
			if (declare_inline("threadref"s, id))
				irbuilder->new_type_threadref(irbuilder, id);
			current_ids.pop();
			return (MuTypeNode)id;
		}
		virtual Any<MuTypeNode> visitTypeVoid(UIRParser::TypeVoidContext *context) override {
			MuID id;
			if (declare_inline("void"s, id))
				irbuilder->new_type_void(irbuilder, id);
			current_ids.pop();
			return (MuTypeNode)id;
		}
		virtual Any<MuTypeNode> visitTypeStackRef(UIRParser::TypeStackRefContext *context) override {
			MuID id;
			if (declare_inline("stackref"s, id))
				irbuilder->new_type_stackref(irbuilder, id);
			current_ids.pop();
			return (MuTypeNode)id;
		}
		virtual Any<MuTypeNode> visitTypeFrameCursorRef(UIRParser::TypeFrameCursorRefContext *context) override {
			MuID id;
			if (declare_inline("framecursorref"s, id))
				irbuilder->new_type_framecursorref(irbuilder, id);
			current_ids.pop();
			return (MuTypeNode)id;
		}
		virtual Any<MuTypeNode> visitTypeIRBuilderRef(UIRParser::TypeIRBuilderRefContext *context) override {
			MuID id;
			if (declare_inline("irbuilderref"s, id))
				irbuilder->new_type_irbuilderref(irbuilder, id);
			current_ids.pop();
			return (MuTypeNode)id;
		}

		virtual Any<void> visitFuncSigDef(UIRParser::FuncSigDefContext *context) override {
			current_ids.push(accept<MuID>(context->nam));
			accept(context->ctor);
			current_ids.pop();
			return Any<void>();
		}
		virtual Any<MuTypeNode> visitFuncSigConstructor(UIRParser::FuncSigConstructorContext *context) override {
			auto paramtys = accept_list<MuTypeNode>(context->paramTys);
			auto rettys = accept_list<MuTypeNode>(context->retTys);

			MuID id;
			if (declare_inline("(" + list_to_string(paramtys) + ")->("s + list_to_string(rettys) + ")"s, id))
				irbuilder->new_funcsig(irbuilder, id, &paramtys[0], paramtys.size(), &rettys[0], rettys.size());
			current_ids.pop();
			return (MuTypeNode)id;
		}

		virtual Any<void> visitConstDef(UIRParser::ConstDefContext *context) override {
			current_ids.push(accept<MuID>(context->nam));
			current_type = accept<MuTypeNode>(context->ty);
			accept(context->ctor);
			current_ids.pop();
			return Any<void>();
		}
		virtual Any<MuVarNode> visitCtorInt(UIRParser::CtorIntContext *context) override {
			auto val = accept<std::string>(context->intLiteral());
			if (!int_sizes.count(current_type))
				throw antlr4::RuntimeException("Cannot determine size of integer constant (make sure the type declaration preceds the constant) at " +
						context->getSourceInterval().toString());
			std::size_t size = (std::size_t)int_sizes[current_type];

			MuID id;
			if (size <= 64) {
				// Can use val directly
				auto v = (uint64_t)std::stoull(val, nullptr, 0);
				if (size < 64)
					v &= ((uint64_t)1 << size) - (uint64_t)1;

				if (declare_inline("<"s + std::to_string(current_type) + ">"s + std::to_string(v), id))
					irbuilder->new_const_int(irbuilder, id, current_type, v);
			} else {
				mpz_class c {val}; // Read the integer (will auto detect base)

				if (declare_inline("<"s + std::to_string(current_type) + ">"s + c.get_str(10), id)) {
					std::size_t n = (size + 64 - 1) / 64;

					std::vector<uint64_t> vals;
					vals.reserve(n);
					for (std::size_t i = 0; i < n; i++) {
						// This 'hack' is needed as mpz_class dosn't support directly converting to a uint64_t
						std::stringstream s;
						s << (c & mask_64); // Get the lowest 64-bits
						std::uint64_t v;
						s >> v;
						vals.push_back(v);
						c >>= 64; // Shift c right by 64-bits
					}

					irbuilder->new_const_int_ex(irbuilder, id, current_type, &vals[0], vals.size());
				}
			}
			current_ids.pop();
			return (MuVarNode)id;
		}
		virtual Any<MuVarNode> visitCtorFloat(UIRParser::CtorFloatContext *context) override {
			auto f = accept<float>(context->floatLiteral());
			MuID id;
			if (declare_inline("<"s + std::to_string(current_type) + ">"s + std::to_string(f), id))
				irbuilder->new_const_float(irbuilder, id, current_type, f);
			current_ids.pop();
			return (MuVarNode)id;
		}
		virtual Any<MuVarNode> visitCtorDouble(UIRParser::CtorDoubleContext *context) override {
			auto d = accept<double>(context->doubleLiteral());
			MuID id;
			if (declare_inline("<"s + std::to_string(current_type) + ">"s + std::to_string(d), id))
				irbuilder->new_const_double(irbuilder, id, current_type, d);
			current_ids.pop();
			return (MuVarNode)id;
		}
		virtual Any<MuVarNode> visitCtorNull(UIRParser::CtorNullContext *context) override {
			MuID id;
			if (declare_inline("<"s + std::to_string(current_type) + ">NULL"s, id))
				irbuilder->new_const_null(irbuilder, id, current_type);
			current_ids.pop();
			return (MuVarNode)id;
		}
		virtual Any<MuVarNode> visitCtorList(UIRParser::CtorListContext *context) override {
			auto elems = accept_list<MuGlobalVarNode>(context->globalVar());
			MuID id;
			if (declare_inline("<"s + std::to_string(current_type) + ">{"s + list_to_string(elems) + "}", id))
				irbuilder->new_const_seq(irbuilder, id, current_type, &elems[0], elems.size());
			current_ids.pop();
			return (MuVarNode)id;
		}
		virtual Any<MuVarNode> visitCtorExtern(UIRParser::CtorExternContext *context) override {
			auto s = accept<MuCString>(context->stringLiteral());
			MuID id;
			if (declare_inline("<"s + std::to_string(current_type) + ">\""s + s + "\""s, id))
				irbuilder->new_const_extern(irbuilder, id, current_type, s);
			current_ids.pop();
			return (MuVarNode)id;
		}

		virtual Any<void> visitGlobalDef(UIRParser::GlobalDefContext *context) override {
			irbuilder->new_global_cell(irbuilder, accept<MuID >(context->nam), accept<MuID>(context->ty));
			return Any<void>();
		}

		virtual Any<void> visitFuncDecl(UIRParser::FuncDeclContext *context) override {
			auto id = accept<MuID>(context->nam);

			// Don't create a funcdecl if there is already one
			if (funcdecls.count(id) == 0) {
				funcdecls.insert(id);
				irbuilder->new_func(irbuilder, id, accept<MuFuncSigNode>(context->sig));
			}
			return Any<void>();
		}

		virtual Any<void> visitFuncExpDef(UIRParser::FuncExpDefContext *context) override {
			irbuilder->new_exp_func(irbuilder, accept<MuID>(context->nam), accept<MuFuncNode>(context->func()),
									accept<MuCallConv>(context->callConv()),
									accept<MuConstNode>(context->cookie));
			return Any<void>();
		}

		virtual Any<void> visitFuncDef(UIRParser::FuncDefContext *context) override {
			// The name of the function, function version names are relative to this
			MuID func_name = accept<MuFuncNode>(context->nam);
			::parent_names.push(::last_name); // the global name of the function

			if (context->sig && funcdecls.count(func_name) == 0) {
				funcdecls.insert(func_name);
				irbuilder->new_func(irbuilder, func_name, accept<MuFuncSigNode>(context->sig));
			}
			if (context->ver)
				current_ids.push(accept<MuID>(context->ver)); // the id of the function version we are constructing
			else
				current_ids.push(irbuilder->gen_sym(irbuilder, nullptr));

			::parent_names.push(::parent_names.top() + ".__"s + std::to_string(anon_id++)); // the global name of the function version

			auto bbs = accept_list<MuBBNode>(context->body);

			irbuilder->new_func_ver(irbuilder, current_ids.top(), func_name, &bbs[0], bbs.size());

			::parent_names.pop();
			::parent_names.pop();
			return Any<void>();
		}

		virtual Any<MuBBNode> visitBasicBlock(UIRParser::BasicBlockContext *context) override {
			auto id = accept<MuID>(context->nam);
			::parent_names.push(::last_name); // the global name of the basic block

			auto nor_param_ids = accept_list<MuID>(context->params);
                        auto exc_param_id = try_accept<MuID>(context->exc, MU_NO_ID);
			auto nor_param_types = accept_list<MuTypeNode>(context->param_tys);
			auto insts = accept_list<MuInstNode>(context->inst());
			irbuilder->new_bb(irbuilder, id,
							  &nor_param_ids[0], &nor_param_types[0], nor_param_ids.size(),
							  exc_param_id,
							  &insts[0], insts.size());

			::parent_names.pop();
			return (MuBBNode)id;
		}
		virtual Any<MuInstNode> visitInstBinOp(UIRParser::InstBinOpContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_binop(irbuilder, id,
								 accept<MuID>(context->instResult()),
								 accept<MuBinOptr>(context->binop()),
								 accept<MuTypeNode>(context->ty),
								 accept<MuVarNode>(context->op1),
								 accept<MuVarNode>(context->op2),
								 try_accept<MuExcClause>(context->excClause(), MU_NO_ID));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstBinOpStatus(UIRParser::InstBinOpStatusContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto results = accept<std::pair<MuID, std::vector<MuID>>>(context->pairResults());

			irbuilder->new_binop_with_status(irbuilder, id,
								 results.first, &results.second[0], results.second.size(),
								 accept<MuBinOptr>(context->binop()),
								 accept<MuBinOpStatus>(context->binopStatus()),
								 accept<MuTypeNode>(context->ty),
								 accept<MuVarNode>(context->op1),
								 accept<MuVarNode>(context->op2),
								 try_accept<MuExcClause>(context->excClause(), MU_NO_ID));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstCmp(UIRParser::InstCmpContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_cmp(irbuilder, id,
								 accept<MuID>(context->instResult()),
								 accept<MuCmpOptr>(context->cmpop()),
								 accept<MuTypeNode>(context->ty),
								 accept<MuVarNode>(context->op1),
								 accept<MuVarNode>(context->op2));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstConversion(UIRParser::InstConversionContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_conv(irbuilder, id,
							   accept<MuID>(context->instResult()),
							   accept<MuConvOptr>(context->convop()),
							   accept<MuTypeNode>(context->fromTy),
							   accept<MuTypeNode>(context->toTy),
							   accept<MuVarNode>(context->opnd));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstSelect(UIRParser::InstSelectContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_select(irbuilder, id,
								accept<MuID>(context->instResult()),
								accept<MuTypeNode>(context->condTy),
								accept<MuTypeNode>(context->resTy),
								accept<MuVarNode>(context->cond),
								accept<MuVarNode>(context->ifTrue),
								accept<MuVarNode>(context->ifFalse));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstBranch(UIRParser::InstBranchContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_branch(irbuilder, id, accept<MuDestClause>(context->dest));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstBranch2(UIRParser::InstBranch2Context *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_branch2(irbuilder, id, accept<MuVarNode>(context->cond),
								   accept<MuDestClause>(context->ifTrue),
								   accept<MuDestClause>(context->ifFalse));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstSwitch(UIRParser::InstSwitchContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto cases = accept_list<MuInstNode>(context->caseVal);
			auto dests = accept_list<MuInstNode>(context->caseDest);

			irbuilder->new_switch(irbuilder, id,
								   accept<MuTypeNode>(context->ty),
								   accept<MuVarNode>(context->opnd),
								   accept<MuDestClause>(context->defDest),
								   &cases[0], &dests[0], cases.size());

			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstCall(UIRParser::InstCallContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto result = accept<std::vector<MuID>>(context->instResults());
			auto args = accept<std::vector<MuVarNode>>(context->argList());
			irbuilder->new_call(irbuilder, id,
								&result[0], result.size(),
								accept<MuFuncSigNode>(context->funcSig()),
								accept<MuVarNode>(context->callee),
								&args[0], args.size(),
								try_accept<MuExcClause>(context->excClause(), MU_NO_ID),
								try_accept<MuKeepaliveClause>(context->keepaliveClause(), MU_NO_ID));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstTailCall(UIRParser::InstTailCallContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto args = accept<std::vector<MuVarNode>>(context->argList());
			irbuilder->new_tailcall(irbuilder, id,
									accept<MuFuncSigNode>(context->funcSig()),
									accept<MuVarNode>(context->callee),
									&args[0], args.size());
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstRet(UIRParser::InstRetContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto rvs = accept<std::vector<MuVarNode>>(context->retVals());;
			irbuilder->new_ret(irbuilder, id, &rvs[0], rvs.size());
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstThrow(UIRParser::InstThrowContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_throw(irbuilder, id, accept<MuVarNode>(context->exc));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstExtractValue(UIRParser::InstExtractValueContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_extractvalue(irbuilder, id,
										accept<MuID>(context->instResult()),
										accept<MuTypeNode>(context->ty),
										accept<int>(context->intParam()),
										accept<MuVarNode>(context->opnd));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstInsertValue(UIRParser::InstInsertValueContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_insertvalue(irbuilder, id,
									   accept<MuID>(context->instResult()),
									   accept<MuTypeNode>(context->ty),
									   accept<int>(context->intParam()),
									   accept<MuVarNode>(context->opnd),
									   accept<MuVarNode>(context->newVal));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstExtractElement(UIRParser::InstExtractElementContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_extractelement(irbuilder, id,
										  accept<MuID>(context->instResult()),
										  accept<MuTypeNode>(context->seqTy),
										  accept<MuTypeNode>(context->indTy),
										  accept<MuVarNode>(context->opnd),
										  accept<MuVarNode>(context->index));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstInsertElement(UIRParser::InstInsertElementContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_insertelement(irbuilder, id,
										 accept<MuID>(context->instResult()),
										 accept<MuTypeNode>(context->seqTy),
										 accept<MuTypeNode>(context->indTy),
										 accept<MuVarNode>(context->opnd),
										 accept<MuVarNode>(context->index),
										 accept<MuVarNode>(context->newVal));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstShuffleVector(UIRParser::InstShuffleVectorContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_shufflevector(irbuilder, id,
										 accept<MuID>(context->instResult()),
										 accept<MuTypeNode>(context->vecTy),
										 accept<MuTypeNode>(context->maskTy),
										 accept<MuVarNode>(context->vec1),
										 accept<MuVarNode>(context->vec2),
										 accept<MuVarNode>(context->mask));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstNew(UIRParser::InstNewContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_new(irbuilder, id,
							   accept<MuID>(context->instResult()),
							   accept<MuTypeNode>(context->allocTy),
							   try_accept<MuExcClause>(context->excClause(), MU_NO_ID));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstNewHybrid(UIRParser::InstNewHybridContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_newhybrid(irbuilder, id,
									 accept<MuID>(context->instResult()),
									 accept<MuTypeNode>(context->allocTy),
									 accept<MuTypeNode>(context->lenTy),
									 accept<MuVarNode>(context->length),
									 try_accept<MuExcClause>(context->excClause(), MU_NO_ID));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstAlloca(UIRParser::InstAllocaContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_alloca(irbuilder, id,
								  accept<MuID>(context->instResult()),
								  accept<MuTypeNode>(context->allocTy),
								  try_accept<MuExcClause>(context->excClause(), MU_NO_ID));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstAllocaHybrid(UIRParser::InstAllocaHybridContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_allocahybrid(irbuilder, id,
										accept<MuID>(context->instResult()),
										accept<MuTypeNode>(context->allocTy),
										accept<MuTypeNode>(context->lenTy),
										accept<MuVarNode>(context->length),
										try_accept<MuExcClause>(context->excClause(), MU_NO_ID));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstGetIRef(UIRParser::InstGetIRefContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_getiref(irbuilder, id,
								   accept<MuID>(context->instResult()),
								   accept<MuTypeNode>(context->refTy),
								   accept<MuVarNode>(context->opnd));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstGetFieldIRef(UIRParser::InstGetFieldIRefContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_getfieldiref(irbuilder, id,
										accept<MuID>(context->instResult()),
										(MuBool)(context->ptr != nullptr),
										accept<MuTypeNode>(context->refTy),
										accept<int>(context->index),
										accept<MuVarNode>(context->opnd));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstGetElemIRef(UIRParser::InstGetElemIRefContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_getelemiref(irbuilder, id,
									   accept<MuID>(context->instResult()),
									   (MuBool)(context->ptr != nullptr),
									   accept<MuTypeNode>(context->refTy),
									   accept<MuTypeNode>(context->indTy),
									   accept<MuVarNode>(context->opnd),
									   accept<MuVarNode>(context->index));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstShiftIRef(UIRParser::InstShiftIRefContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_shiftiref(irbuilder, id,
									 accept<MuID>(context->instResult()),
									 (MuBool)(context->ptr != nullptr),
									 accept<MuTypeNode>(context->refTy),
									 accept<MuTypeNode>(context->offTy),
									 accept<MuVarNode>(context->opnd),
									 accept<MuVarNode>(context->offset));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstGetVarPartIRef(UIRParser::InstGetVarPartIRefContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_getvarpartiref(irbuilder, id,
										  accept<MuID>(context->instResult()),
										  (MuBool)(context->ptr != nullptr),
										  accept<MuTypeNode>(context->refTy),
										  accept<MuVarNode>(context->opnd));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstLoad(UIRParser::InstLoadContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_load(irbuilder, id,
								accept<MuID>(context->instResult()),
								(MuBool)(context->ptr != nullptr),
								try_accept<MuMemOrd>(context->memord(), MU_ORD_NOT_ATOMIC),
								accept<MuTypeNode>(context->type()),
								accept<MuVarNode>(context->loc),
								try_accept<MuExcClause>(context->excClause(), MU_NO_ID));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstStore(UIRParser::InstStoreContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_store(irbuilder, id,
								 (MuBool)(context->ptr != nullptr),
								 try_accept<MuMemOrd>(context->memord(), MU_ORD_NOT_ATOMIC),
								 accept<MuTypeNode>(context->type()),
								 accept<MuVarNode>(context->loc),
								 accept<MuVarNode>(context->newVal),
								 try_accept<MuExcClause>(context->excClause(), MU_NO_ID));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstCmpXchg(UIRParser::InstCmpXchgContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto result = accept<std::pair<MuID, MuID>>(context->pairResult());
			irbuilder->new_cmpxchg(irbuilder, id,
								   result.first, result.second,
								   (MuBool)(context->ptr != nullptr),
								   (MuBool)(context->isWeak != nullptr),
								   accept<MuMemOrd>(context->ordSucc),
								   accept<MuMemOrd>(context->ordFail),
								   accept<MuTypeNode>(context->type()),
								   accept<MuVarNode>(context->loc),
								   accept<MuVarNode>(context->expected),
								   accept<MuVarNode>(context->desired),
								   try_accept<MuExcClause>(context->excClause(), MU_NO_ID));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstAtomicRMW(UIRParser::InstAtomicRMWContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_atomicrmw(irbuilder, id,
									 accept<MuID>(context->instResult()),
									 (MuBool)(context->ptr != nullptr),
									 accept<MuMemOrd>(context->memord()),
									 accept<MuAtomicRMWOptr>(context->atomicrmwop()),
									 accept<MuTypeNode>(context->type()),
									 accept<MuVarNode>(context->loc),
									 accept<MuVarNode>(context->opnd),
									 try_accept<MuExcClause>(context->excClause(), MU_NO_ID));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstFence(UIRParser::InstFenceContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_fence(irbuilder, id, accept<MuMemOrd>(context->memord()));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstTrap(UIRParser::InstTrapContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto result_ids = accept<std::vector<MuID>>(context->instResults());
			auto rettys = accept<std::vector<MuTypeNode>>(context->typeList());

			irbuilder->new_trap(irbuilder, id,
								&result_ids[0], &rettys[0], result_ids.size(),
								try_accept<MuExcClause>(context->excClause(), MU_NO_ID),
								try_accept<MuKeepaliveClause>(context->keepaliveClause(), MU_NO_ID));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstWatchPoint(UIRParser::InstWatchPointContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto result_ids = accept<std::vector<MuID>>(context->instResults());
			auto rettys = accept<std::vector<MuTypeNode>>(context->typeList());

			irbuilder->new_watchpoint(irbuilder, id,
									  accept<MuWPID>(context->wpid()),
									  &result_ids[0], &rettys[0], result_ids.size(),
									  accept<MuDestClause>(context->dis),
									  accept<MuDestClause>(context->ena),
									  try_accept<MuDestClause >(context->wpExc, MU_NO_ID),
									  try_accept<MuKeepaliveClause>(context->keepaliveClause(), MU_NO_ID));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstWPBranch(UIRParser::InstWPBranchContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_wpbranch(irbuilder, id,
									accept<MuWPID>(context->wpid()),
									accept<MuDestClause>(context->dis),
									accept<MuDestClause>(context->ena));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstCCall(UIRParser::InstCCallContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto result = accept<std::vector<MuID>>(context->instResults());
			auto args = accept<std::vector<MuVarNode>>(context->argList());

			irbuilder->new_ccall(irbuilder, id,
								 &result[0], result.size(),
								 accept<MuCallConv>(context->callConv()),
								 accept<MuTypeNode>(context->funcTy),
								 accept<MuFuncSigNode>(context->funcSig()),
								 accept<MuVarNode>(context->callee),
								 &args[0], args.size(),
								 try_accept<MuExcClause>(context->excClause(), MU_NO_ID),
								 try_accept<MuKeepaliveClause>(context->keepaliveClause(), MU_NO_ID));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstNewThread(UIRParser::InstNewThreadContext *context) override {
			auto id = accept<MuID>(context->instName());

			irbuilder->new_newthread(irbuilder, id,
									 accept<MuID>(context->instResult()),
									 accept<MuVarNode>(context->stack),
									 try_accept<MuVarNode>(context->threadLocal, MU_NO_ID),
									 accept<MuNewStackClause>(context->newStackClause()),
									 try_accept<MuExcClause>(context->excClause(), MU_NO_ID));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstSwapStack(UIRParser::InstSwapStackContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto result_ids = accept<std::vector<MuID>>(context->instResults());
			irbuilder->new_swapstack(irbuilder, id,
									 &result_ids[0], result_ids.size(),
									 accept<MuVarNode>(context->swappee),
									 accept<MuCurStackClause>(context->curStackClause()),
									 accept<MuNewStackClause>(context->newStackClause()),
									 try_accept<MuExcClause>(context->excClause(), MU_NO_ID),
									 try_accept<MuKeepaliveClause>(context->keepaliveClause(), MU_NO_ID));
			return (MuInstNode)id;
		}
		virtual Any<MuInstNode> visitInstCommInst(UIRParser::InstCommInstContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto result_ids = accept<std::vector<MuID>>(context->instResults());

			auto flags = accept_list<MuFlag>(context->flags);
			auto tys = accept_list<MuTypeNode>(context->tys);
			auto sigs = accept_list<MuFuncSigNode>(context->sigs);
			auto args = accept_list<MuVarNode>(context->args);


			irbuilder->new_comminst(irbuilder, id,
									&result_ids[0], result_ids.size(),
									accept<MuCommInst>(context->commInst()),

									&flags[0], flags.size(),
									&tys[0], tys.size(),
									&sigs[0], sigs.size(),
									&args[0], args.size(),

									try_accept<MuExcClause>(context->excClause(), MU_NO_ID),
									try_accept<MuKeepaliveClause>(context->keepaliveClause(), MU_NO_ID));

			return (MuInstNode)id;
		}

		/***************** Used by instructions **********************/
		virtual Any<MuID> visitInstName(UIRParser::InstNameContext *context) override {
			return context->nam ? accept<MuID>(context->nam) : irbuilder->gen_sym(irbuilder, nullptr);
		}
		virtual Any<std::vector<MuID>> visitInstResults(UIRParser::InstResultsContext *context) override {
			return accept_list<MuVarNode>(context->results);

		}
		virtual Any<std::pair<MuID, MuID>> visitPairResult(UIRParser::PairResultContext *context) override {
			return std::make_pair(accept<MuID>(context->first), accept<MuID>(context->second));
		}
		virtual Any<std::pair<MuID, std::vector<MuID>>> visitPairResults(UIRParser::PairResultsContext *context) override {
			return std::make_pair(accept<MuID>(context->first), accept_list<MuID>(context->second));
		}
		virtual Any<std::vector<MuTypeNode>> visitTypeList(UIRParser::TypeListContext *context) override {
			return accept_list<MuTypeNode>(context->tys);
		}

		virtual Any<MuExcClause> visitExcClause(UIRParser::ExcClauseContext *context) override {
			MuID id = irbuilder->gen_sym(irbuilder, nullptr);

			irbuilder->new_exc_clause(irbuilder, id, accept<MuDestClause>(context->nor), accept<MuDestClause>(context->exc));
			return (MuExcClause)id;
		}
		virtual Any<MuDestClause> visitDestClause(UIRParser::DestClauseContext *context) override {
			MuID id = irbuilder->gen_sym(irbuilder, nullptr);
			auto vars = accept<std::vector<MuVarNode>>(context->argList());

            std::string last_parent = ::parent_names.top();
            ::parent_names.pop();
            // local BB names are relative to the enclosing function (not the enclosing block)
            auto bb = accept<MuBBNode>(context->bb());
            ::parent_names.push(last_parent);

            irbuilder->new_dest_clause(irbuilder, id, bb, &vars[0], vars.size());
			return (MuDestClause)id;
		}
		virtual Any<MuKeepaliveClause> visitKeepaliveClause(UIRParser::KeepaliveClauseContext *context) override {
			MuID id = irbuilder->gen_sym(irbuilder, nullptr);
			auto vars = accept_list<MuLocalVarNode>(context->value());
			irbuilder->new_keepalive_clause(irbuilder, id, &vars[0], vars.size());
			return (MuKeepaliveClause)id;
		}
		virtual Any<MuNewStackClause> visitNewStackThrowExc(UIRParser::NewStackThrowExcContext *context) override {
			MuID id = irbuilder->gen_sym(irbuilder, nullptr);
			irbuilder->new_nsc_throw_exc(irbuilder, id, accept<MuVarNode>(context->exc));
			return (MuNewStackClause)id;
		}
		virtual Any<MuNewStackClause> visitNewStackPassValue(UIRParser::NewStackPassValueContext *context) override {
			MuID id = irbuilder->gen_sym(irbuilder, nullptr);
			auto tys = accept<std::vector<MuTypeNode>>(context->typeList());
			auto vars = accept<std::vector<MuVarNode>>(context->argList());
			irbuilder->new_nsc_pass_values(irbuilder, id, &tys[0], &vars[0], vars.size());
			return (MuCurStackClause)id;
		}
		virtual Any<MuCurStackClause> visitCurStackKillOld(UIRParser::CurStackKillOldContext *context) override {
			MuID id = irbuilder->gen_sym(irbuilder, nullptr);
			irbuilder->new_csc_kill_old(irbuilder, id);
			return (MuCurStackClause)id;
		}
		virtual Any<MuCurStackClause> visitCurStackRetWith(UIRParser::CurStackRetWithContext *context) override {
			MuID id = irbuilder->gen_sym(irbuilder, nullptr);
			auto rettys = accept<std::vector<MuTypeNode>>(context->typeList());
			irbuilder->new_csc_ret_with(irbuilder, id, &rettys[0], rettys.size());
			return (MuCurStackClause)id;
		}

		virtual Any<std::vector<MuVarNode>> visitArgList(UIRParser::ArgListContext *context) override {
			return accept_list<MuVarNode>(context->value());
		}
		virtual Any<std::vector<MuVarNode>> visitRetVals(UIRParser::RetValsContext *context) override {
			return accept_list<MuVarNode>(context->vals);
		}

		// Inline declarations or a name
		virtual Any<MuTypeNode> visitType(UIRParser::TypeContext *context) override {
			if (context->nam)
				return (MuTypeNode)accept<MuID>(context->nam);
			else {
				inline_decl++;

				auto id = accept<MuTypeNode>(context->typeConstructor());

				inline_decl--;
				return id;
			}
		}

		virtual Any<MuFuncSigNode> visitFuncSig(UIRParser::FuncSigContext *context) override {
			if (context->nam)
				return (MuFuncSigNode)accept<MuID>(context->nam);
			else {
				inline_decl++;

				auto id = accept<MuFuncSigNode>(context->funcSigConstructor());

				inline_decl--;
				return id;
			}
		}
		virtual Any<MuConstNode> visitConstant(UIRParser::ConstantContext *context) override {
			if (context->nam)
				return (MuConstNode)accept<MuID>(context->nam);
			else {
				MuID old_type = current_type;
				inline_decl++;

				current_type = accept<MuTypeNode>(context->ty);
				auto id = accept<MuConstNode>(context->constConstructor());

				inline_decl--;
				current_type = old_type;
				return id;
			}
		}

		// ******************************* TERMINALS *****************************

		// Names
        virtual Any<MuID> visitGlobalName(UIRParser::GlobalNameContext *context) override {
            // References a top level entity, interpret as a global name
            if (context->NAME())
                return get_id(::last_name = context->NAME()->getText(), true);
            else if (context->LOCAL_NAME())
                return get_id(::last_name = ::bundle_name + "."s + context->LOCAL_NAME()->getText().substr(1, std::string::npos), true);
            else // if (context->GLOBAL_NAME())
                return get_id(::last_name = context->GLOBAL_NAME()->getText().substr(1, std::string::npos), true);
        }
        virtual Any<MuID> visitLocalName(UIRParser::LocalNameContext *context) override {
            if (context->NAME()) // Interpret as a local name
                return get_id(::last_name = ::parent_names.top() + "."s + context->NAME()->getText(), false);
            else if (context->LOCAL_NAME())
                return get_id(::last_name = ::parent_names.top() + "."s + context->LOCAL_NAME()->getText().substr(1, std::string::npos), false);
            else // if (context->GLOBAL_NAME())
                return get_id(::last_name = context->GLOBAL_NAME()->getText().substr(1, std::string::npos), false);
        }
		virtual Any<MuID> visitName(UIRParser::NameContext *context) override {
            // Interpret as a local name if previously defined, otherwise global

            std::string suffix =
                    context->NAME()       ? context->NAME()->getText()
                                          : context->LOCAL_NAME() ? context->LOCAL_NAME()->getText().substr(1, std::string::npos)
                                                                  : context->GLOBAL_NAME()->getText().substr(1, std::string::npos);

            ::last_name = context->GLOBAL_NAME() ? suffix : ::parent_names.top() + "."s + suffix;

            if (symbols.count(::last_name) != 0)
                return get_id(::last_name, false);

            // Must be a global name
            ::last_name = context->LOCAL_NAME() ? ::bundle_name + "."s + suffix : suffix;

            return get_id(::last_name, true);
		}

		// Literals
		virtual Any<MuWPID> visitWpid(UIRParser::WpidContext *context) override {
			// MuWPID is an alias for uint32_t and 'unsigned long' is required to be at least 32 bits
			return (MuWPID)std::stoul(accept<std::string>(context->intLiteral()), nullptr, 0 /* auto detect base */);
		}
		virtual Any<int> visitIntParam(UIRParser::IntParamContext *context) override {
			return std::stoi(accept<std::string>(context->intLiteral()), nullptr, 0 /* auto detect base */);
		}
		virtual Any<uint64_t> visitSizeParam(UIRParser::SizeParamContext *context) override {
			// Unsigned long long is required to be at least 64 bits
			return (uint64_t)std::stoull(accept<std::string>(context->intLiteral()), nullptr, 0 /* auto detect base */);
		}
		virtual Any<std::string> visitIntLiteral(UIRParser::IntLiteralContext *context) override {
			return context->getText();
		}

		virtual Any<float> visitFloatNumber(UIRParser::FloatNumberContext *context) override {
			return std::stof(context->FP_NUM()->getText(), nullptr);
		}
		virtual Any<float> visitFloatInf(UIRParser::FloatInfContext *context) override {
			if (context->INF()->getText()[0] == '-')
				return -std::numeric_limits<float>::infinity();
			else return std::numeric_limits<float>::infinity();
		}
		virtual Any<float> visitFloatNan(UIRParser::FloatNanContext *context) override {
			return std::numeric_limits<float>::quiet_NaN();
		}
		virtual Any<float> visitFloatBits(UIRParser::FloatBitsContext *context) override {
			// An unsigned long is at least 32 bits, so cast that to uint32_t and reinterpret that as a float
			union my_union { std::uint32_t i; float f; };
			my_union u;
			u.i = (std::uint32_t)std::stoull(accept<std::string>(context->intLiteral()), nullptr, 0);
			return u.f;
		}

		virtual Any<double> visitDoubleNumber(UIRParser::DoubleNumberContext *context) override {
			return std::stod(context->FP_NUM()->getText(), nullptr);
		}
		virtual Any<double> visitDoubleInf(UIRParser::DoubleInfContext *context) override {
			if (context->INF()->getText()[0] == '-')
				return -std::numeric_limits<double>::infinity();
			else return std::numeric_limits<double>::infinity();
		}
		virtual Any<double> visitDoubleNan(UIRParser::DoubleNanContext *context) override {
			return std::numeric_limits<double>::quiet_NaN();
		}
		virtual Any<double> visitDoubleBits(UIRParser::DoubleBitsContext *context) override {
			// An unsigned long is at least 64-bits, so cast that to to a uint64_t and use a union to get the double with the same representation
			// (Note: there are other ways of doing this, such as by casting pointers/references but the compiler warns against this due to aliasing rules)
			union my_union { std::uint64_t i; double d; };
			my_union u;
			u.i = (std::uint64_t)std::stoull(accept<std::string>(context->intLiteral()), nullptr, 0);
			return u.d;
		}

		virtual Any<MuCString> visitStringLiteral(UIRParser::StringLiteralContext *context) override {
			std::string string = context->STRING_LITERAL()->getText();
			// Skip the leading and trailing "
			string_table.push_back(std::make_unique<std::string>(string.substr(1, string.size() - 2)));
			return string_table.back()->c_str();
		}

		// Flags
		virtual Any<MuCallConv> visitCallConv(UIRParser::CallConvContext *context) override {
			return MU_CC_DEFAULT; // Only one flag is supported
		}
		virtual Any<MuBinOpStatus> visitBinopStatus(UIRParser::BinopStatusContext *context) override {
			std::string flag = context->getText().substr(1, std::string::npos); // skip the leading '#'
			return MACRO_MATCH(flag, MU_BOS, N)
				: MACRO_MATCH(flag, MU_BOS, Z)
				: MACRO_MATCH(flag, MU_BOS, C)
				: MU_BOS_V;
		}

		// Operators
		virtual Any<MuBinOptr> visitBinop(UIRParser::BinopContext *context) override {
			std::string op = context->getText();
			return MACRO_MATCH(op, MU_BINOP, ADD)
				: MACRO_MATCH(op, MU_BINOP, SUB)
				: MACRO_MATCH(op, MU_BINOP, MUL)
				: MACRO_MATCH(op, MU_BINOP, SDIV)
				: MACRO_MATCH(op, MU_BINOP, SREM)
				: MACRO_MATCH(op, MU_BINOP, UDIV)
				: MACRO_MATCH(op, MU_BINOP, UREM)
				: MACRO_MATCH(op, MU_BINOP, SHL)
				: MACRO_MATCH(op, MU_BINOP, LSHR)
				: MACRO_MATCH(op, MU_BINOP, ASHR)
				: MACRO_MATCH(op, MU_BINOP, AND)
				: MACRO_MATCH(op, MU_BINOP, OR)
				: MACRO_MATCH(op, MU_BINOP, XOR)
				: MACRO_MATCH(op, MU_BINOP, FADD)
				: MACRO_MATCH(op, MU_BINOP, FSUB)
				: MACRO_MATCH(op, MU_BINOP, FMUL)
				: MACRO_MATCH(op, MU_BINOP, FDIV)
				: MU_BINOP_FREM;
		}
		virtual Any<MuCmpOptr> visitCmpop(UIRParser::CmpopContext *context) override {
			std::string op = context->getText();
			return MACRO_MATCH(op, MU_CMP, EQ)
				: MACRO_MATCH(op, MU_CMP, NE)
				: MACRO_MATCH(op, MU_CMP, SGE)
				: MACRO_MATCH(op, MU_CMP, SGT)
				: MACRO_MATCH(op, MU_CMP, SLE)
				: MACRO_MATCH(op, MU_CMP, SLT)
				: MACRO_MATCH(op, MU_CMP, UGE)
				: MACRO_MATCH(op, MU_CMP, UGT)
				: MACRO_MATCH(op, MU_CMP, ULE)
				: MACRO_MATCH(op, MU_CMP, ULT)
				: MACRO_MATCH(op, MU_CMP, FFALSE)
				: MACRO_MATCH(op, MU_CMP, FTRUE)
				: MACRO_MATCH(op, MU_CMP, FUNO)
				: MACRO_MATCH(op, MU_CMP, FUEQ)
				: MACRO_MATCH(op, MU_CMP, FUNE)
				: MACRO_MATCH(op, MU_CMP, FUGT)
				: MACRO_MATCH(op, MU_CMP, FUGE)
				: MACRO_MATCH(op, MU_CMP, FULT)
				: MACRO_MATCH(op, MU_CMP, FULE)
				: MACRO_MATCH(op, MU_CMP, FORD)
				: MACRO_MATCH(op, MU_CMP, FOEQ)
				: MACRO_MATCH(op, MU_CMP, FONE)
				: MACRO_MATCH(op, MU_CMP, FOGT)
				: MACRO_MATCH(op, MU_CMP, FOGE)
				: MACRO_MATCH(op, MU_CMP, FOLT)
				: MU_CMP_FOLE;
		}
		virtual Any<MuConvOptr> visitConvop(UIRParser::ConvopContext *context) override {
			std::string op = context->getText();
			return MACRO_MATCH(op, MU_CONV, TRUNC)
				: MACRO_MATCH(op, MU_CONV, ZEXT)
				: MACRO_MATCH(op, MU_CONV, SEXT)
				: MACRO_MATCH(op, MU_CONV, FPTRUNC)
				: MACRO_MATCH(op, MU_CONV, FPEXT)
				: MACRO_MATCH(op, MU_CONV, FPTOUI)
				: MACRO_MATCH(op, MU_CONV, FPTOSI)
				: MACRO_MATCH(op, MU_CONV, UITOFP)
				: MACRO_MATCH(op, MU_CONV, SITOFP)
				: MACRO_MATCH(op, MU_CONV, BITCAST)
				: MACRO_MATCH(op, MU_CONV, REFCAST)
				: MU_CONV_PTRCAST;
		}
		virtual Any<MuMemOrd> visitMemord(UIRParser::MemordContext *context) override {
			std::string op = context->getText();
			return MACRO_MATCH(op, MU_ORD, NOT_ATOMIC)
				: MACRO_MATCH(op, MU_ORD, RELAXED)
				: MACRO_MATCH(op, MU_ORD, CONSUME)
				: MACRO_MATCH(op, MU_ORD, ACQUIRE)
				: MACRO_MATCH(op, MU_ORD, RELEASE)
				: MACRO_MATCH(op, MU_ORD, ACQ_REL)
				: MU_ORD_SEQ_CST;
		}
		virtual Any<MuAtomicRMWOptr> visitAtomicrmwop(UIRParser::AtomicrmwopContext *context) override {
			std::string op = context->getText();
			return MACRO_MATCH(op, MU_ARMW, XCHG)
				: MACRO_MATCH(op, MU_ARMW, ADD)
				: MACRO_MATCH(op, MU_ARMW, SUB)
				: MACRO_MATCH(op, MU_ARMW, AND)
				: MACRO_MATCH(op, MU_ARMW, NAND)
				: MACRO_MATCH(op, MU_ARMW, OR)
				: MACRO_MATCH(op, MU_ARMW, XOR)
				: MACRO_MATCH(op, MU_ARMW, MAX)
				: MACRO_MATCH(op, MU_ARMW, MIN)
				: MACRO_MATCH(op, MU_ARMW, UMAX)
				: MU_ARMW_UMIN;
		}
		virtual Any<MuCommInst> visitCommInst(UIRParser::CommInstContext *context) override {
			std::string op = context->NAME() ? context->NAME()->getText()
											 : context->GLOBAL_NAME()->getText().substr(1, std::string::npos);
			// Use the following regex on the MU_CI... definition section in muapi.h to generate this code:
            //	search		#[^ ]* ([A-Z_0-9]*)[^@]*@([a-z0-9.-_]*)
            //	replace		: op == "$2" ? $1
			return op == "uvm.new_stack" ? MU_CI_UVM_NEW_STACK
				 : op == "uvm.kill_stack" ? MU_CI_UVM_KILL_STACK
				 : op == "uvm.thread_exit" ? MU_CI_UVM_THREAD_EXIT
				 : op == "uvm.current_stack" ? MU_CI_UVM_CURRENT_STACK
				 : op == "uvm.set_threadlocal" ? MU_CI_UVM_SET_THREADLOCAL
				 : op == "uvm.get_threadlocal" ? MU_CI_UVM_GET_THREADLOCAL
				 : op == "uvm.tr64.is_fp" ? MU_CI_UVM_TR64_IS_FP
				 : op == "uvm.tr64.is_int" ? MU_CI_UVM_TR64_IS_INT
				 : op == "uvm.tr64.is_ref" ? MU_CI_UVM_TR64_IS_REF
				 : op == "uvm.tr64.from_fp" ? MU_CI_UVM_TR64_FROM_FP
				 : op == "uvm.tr64.from_int" ? MU_CI_UVM_TR64_FROM_INT
				 : op == "uvm.tr64.from_ref" ? MU_CI_UVM_TR64_FROM_REF
				 : op == "uvm.tr64.to_fp" ? MU_CI_UVM_TR64_TO_FP
				 : op == "uvm.tr64.to_int" ? MU_CI_UVM_TR64_TO_INT
				 : op == "uvm.tr64.to_ref" ? MU_CI_UVM_TR64_TO_REF
				 : op == "uvm.tr64.to_tag" ? MU_CI_UVM_TR64_TO_TAG
				 : op == "uvm.futex.wait" ? MU_CI_UVM_FUTEX_WAIT
				 : op == "uvm.futex.wait_timeout" ? MU_CI_UVM_FUTEX_WAIT_TIMEOUT
				 : op == "uvm.futex.wake" ? MU_CI_UVM_FUTEX_WAKE
				 : op == "uvm.futex.cmp_requeue" ? MU_CI_UVM_FUTEX_CMP_REQUEUE
				 : op == "uvm.kill_dependency" ? MU_CI_UVM_KILL_DEPENDENCY
				 : op == "uvm.native.pin" ? MU_CI_UVM_NATIVE_PIN
				 : op == "uvm.native.unpin" ? MU_CI_UVM_NATIVE_UNPIN
				 : op == "uvm.native.get_addr" ? MU_CI_UVM_NATIVE_GET_ADDR
				 : op == "uvm.native.expose" ? MU_CI_UVM_NATIVE_EXPOSE
				 : op == "uvm.native.unexpose" ? MU_CI_UVM_NATIVE_UNEXPOSE
				 : op == "uvm.native.get_cookie" ? MU_CI_UVM_NATIVE_GET_COOKIE
				 : op == "uvm.meta.id_of" ? MU_CI_UVM_META_ID_OF
				 : op == "uvm.meta.name_of" ? MU_CI_UVM_META_NAME_OF
				 : op == "uvm.meta.load_bundle" ? MU_CI_UVM_META_LOAD_BUNDLE
				 : op == "uvm.meta.load_hail" ? MU_CI_UVM_META_LOAD_HAIL
				 : op == "uvm.meta.new_cursor" ? MU_CI_UVM_META_NEW_CURSOR
				 : op == "uvm.meta.next_frame" ? MU_CI_UVM_META_NEXT_FRAME
				 : op == "uvm.meta.copy_cursor" ? MU_CI_UVM_META_COPY_CURSOR
				 : op == "uvm.meta.close_cursor" ? MU_CI_UVM_META_CLOSE_CURSOR
				 : op == "uvm.meta.cur_func" ? MU_CI_UVM_META_CUR_FUNC
				 : op == "uvm.meta.cur_func_Ver" ? MU_CI_UVM_META_CUR_FUNC_VER
				 : op == "uvm.meta.cur_inst" ? MU_CI_UVM_META_CUR_INST
				 : op == "uvm.meta.dump_keepalives" ? MU_CI_UVM_META_DUMP_KEEPALIVES
				 : op == "uvm.meta.pop_frames_to" ? MU_CI_UVM_META_POP_FRAMES_TO
				 : op == "uvm.meta.push_frame" ? MU_CI_UVM_META_PUSH_FRAME
				 : op == "uvm.meta.enable_watchpoint" ? MU_CI_UVM_META_ENABLE_WATCHPOINT
				 : op == "uvm.meta.disable_watchpoint" ? MU_CI_UVM_META_DISABLE_WATCHPOINT
				 : op == "uvm.meta.set_trap_handler" ? MU_CI_UVM_META_SET_TRAP_HANDLER
				 : op == "uvm.meta.constant_by_id" ? MU_CI_UVM_META_CONSTANT_BY_ID
                 : op == "uvm.meta.global_by_id" ? MU_CI_UVM_META_GLOBAL_BY_ID
                 : op == "uvm.meta.func_by_id" ? MU_CI_UVM_META_FUNC_BY_ID
                 : op == "uvm.meta.expfunc_by_id" ? MU_CI_UVM_META_EXPFUNC_BY_ID
				 : op == "uvm.irbuilder.new_ir_builder" ? MU_CI_UVM_IRBUILDER_NEW_IR_BUILDER
				 : op == "uvm.irbuilder.load" ? MU_CI_UVM_IRBUILDER_LOAD
				 : op == "uvm.irbuilder.abort" ? MU_CI_UVM_IRBUILDER_ABORT
				 : op == "uvm.irbuilder.gen_sym" ? MU_CI_UVM_IRBUILDER_GEN_SYM
				 : op == "uvm.irbuilder.new_type_int" ? MU_CI_UVM_IRBUILDER_NEW_TYPE_INT
				 : op == "uvm.irbuilder.new_type_float" ? MU_CI_UVM_IRBUILDER_NEW_TYPE_FLOAT
				 : op == "uvm.irbuilder.new_type_double" ? MU_CI_UVM_IRBUILDER_NEW_TYPE_DOUBLE
				 : op == "uvm.irbuilder.new_type_uptr" ? MU_CI_UVM_IRBUILDER_NEW_TYPE_UPTR
				 : op == "uvm.irbuilder.new_type_ufuncptr" ? MU_CI_UVM_IRBUILDER_NEW_TYPE_UFUNCPTR
				 : op == "uvm.irbuilder.new_type_struct" ? MU_CI_UVM_IRBUILDER_NEW_TYPE_STRUCT
				 : op == "uvm.irbuilder.new_type_hybrid" ? MU_CI_UVM_IRBUILDER_NEW_TYPE_HYBRID
				 : op == "uvm.irbuilder.new_type_array" ? MU_CI_UVM_IRBUILDER_NEW_TYPE_ARRAY
				 : op == "uvm.irbuilder.new_type_vector" ? MU_CI_UVM_IRBUILDER_NEW_TYPE_VECTOR
				 : op == "uvm.irbuilder.new_type_void" ? MU_CI_UVM_IRBUILDER_NEW_TYPE_VOID
				 : op == "uvm.irbuilder.new_type_ref" ? MU_CI_UVM_IRBUILDER_NEW_TYPE_REF
				 : op == "uvm.irbuilder.new_type_iref" ? MU_CI_UVM_IRBUILDER_NEW_TYPE_IREF
				 : op == "uvm.irbuilder.new_type_weakref" ? MU_CI_UVM_IRBUILDER_NEW_TYPE_WEAKREF
				 : op == "uvm.irbuilder.new_type_funcref" ? MU_CI_UVM_IRBUILDER_NEW_TYPE_FUNCREF
				 : op == "uvm.irbuilder.new_type_tagref64" ? MU_CI_UVM_IRBUILDER_NEW_TYPE_TAGREF64
				 : op == "uvm.irbuilder.new_type_threadref" ? MU_CI_UVM_IRBUILDER_NEW_TYPE_THREADREF
				 : op == "uvm.irbuilder.new_type_stackref" ? MU_CI_UVM_IRBUILDER_NEW_TYPE_STACKREF
				 : op == "uvm.irbuilder.new_type_framecursorref" ? MU_CI_UVM_IRBUILDER_NEW_TYPE_FRAMECURSORREF
				 : op == "uvm.irbuilder.new_type_irbuilderref" ? MU_CI_UVM_IRBUILDER_NEW_TYPE_IRBUILDERREF
				 : op == "uvm.irbuilder.new_funcsig" ? MU_CI_UVM_IRBUILDER_NEW_FUNCSIG
				 : op == "uvm.irbuilder.new_const_int" ? MU_CI_UVM_IRBUILDER_NEW_CONST_INT
				 : op == "uvm.irbuilder.new_const_int_ex" ? MU_CI_UVM_IRBUILDER_NEW_CONST_INT_EX
				 : op == "uvm.irbuilder.new_const_float" ? MU_CI_UVM_IRBUILDER_NEW_CONST_FLOAT
				 : op == "uvm.irbuilder.new_const_double" ? MU_CI_UVM_IRBUILDER_NEW_CONST_DOUBLE
				 : op == "uvm.irbuilder.new_const_null" ? MU_CI_UVM_IRBUILDER_NEW_CONST_NULL
				 : op == "uvm.irbuilder.new_const_seq" ? MU_CI_UVM_IRBUILDER_NEW_CONST_SEQ
				 : op == "uvm.irbuilder.new_const_extern" ? MU_CI_UVM_IRBUILDER_NEW_CONST_EXTERN
				 : op == "uvm.irbuilder.new_global_cell" ? MU_CI_UVM_IRBUILDER_NEW_GLOBAL_CELL
				 : op == "uvm.irbuilder.new_func" ? MU_CI_UVM_IRBUILDER_NEW_FUNC
				 : op == "uvm.irbuilder.new_exp_func" ? MU_CI_UVM_IRBUILDER_NEW_EXP_FUNC
				 : op == "uvm.irbuilder.new_func_ver" ? MU_CI_UVM_IRBUILDER_NEW_FUNC_VER
				 : op == "uvm.irbuilder.new_bb" ? MU_CI_UVM_IRBUILDER_NEW_BB
				 : op == "uvm.irbuilder.new_dest_clause" ? MU_CI_UVM_IRBUILDER_NEW_DEST_CLAUSE
				 : op == "uvm.irbuilder.new_exc_clause" ? MU_CI_UVM_IRBUILDER_NEW_EXC_CLAUSE
				 : op == "uvm.irbuilder.new_keepalive_clause" ? MU_CI_UVM_IRBUILDER_NEW_KEEPALIVE_CLAUSE
				 : op == "uvm.irbuilder.new_csc_ret_with" ? MU_CI_UVM_IRBUILDER_NEW_CSC_RET_WITH
				 : op == "uvm.irbuilder.new_csc_kill_old" ? MU_CI_UVM_IRBUILDER_NEW_CSC_KILL_OLD
				 : op == "uvm.irbuilder.new_nsc_pass_values" ? MU_CI_UVM_IRBUILDER_NEW_NSC_PASS_VALUES
				 : op == "uvm.irbuilder.new_nsc_throw_exc" ? MU_CI_UVM_IRBUILDER_NEW_NSC_THROW_EXC
				 : op == "uvm.irbuilder.new_binop" ? MU_CI_UVM_IRBUILDER_NEW_BINOP
				 : op == "uvm.irbuilder.new_binop_with_status" ? MU_CI_UVM_IRBUILDER_NEW_BINOP_WITH_STATUS
				 : op == "uvm.irbuilder.new_cmp" ? MU_CI_UVM_IRBUILDER_NEW_CMP
				 : op == "uvm.irbuilder.new_conv" ? MU_CI_UVM_IRBUILDER_NEW_CONV
				 : op == "uvm.irbuilder.new_select" ? MU_CI_UVM_IRBUILDER_NEW_SELECT
				 : op == "uvm.irbuilder.new_branch" ? MU_CI_UVM_IRBUILDER_NEW_BRANCH
				 : op == "uvm.irbuilder.new_branch2" ? MU_CI_UVM_IRBUILDER_NEW_BRANCH2
				 : op == "uvm.irbuilder.new_switch" ? MU_CI_UVM_IRBUILDER_NEW_SWITCH
				 : op == "uvm.irbuilder.new_call" ? MU_CI_UVM_IRBUILDER_NEW_CALL
				 : op == "uvm.irbuilder.new_tailcall" ? MU_CI_UVM_IRBUILDER_NEW_TAILCALL
				 : op == "uvm.irbuilder.new_ret" ? MU_CI_UVM_IRBUILDER_NEW_RET
				 : op == "uvm.irbuilder.new_throw" ? MU_CI_UVM_IRBUILDER_NEW_THROW
				 : op == "uvm.irbuilder.new_extractvalue" ? MU_CI_UVM_IRBUILDER_NEW_EXTRACTVALUE
				 : op == "uvm.irbuilder.new_insertvalue" ? MU_CI_UVM_IRBUILDER_NEW_INSERTVALUE
				 : op == "uvm.irbuilder.new_extractelement" ? MU_CI_UVM_IRBUILDER_NEW_EXTRACTELEMENT
				 : op == "uvm.irbuilder.new_insertelement" ? MU_CI_UVM_IRBUILDER_NEW_INSERTELEMENT
				 : op == "uvm.irbuilder.new_shufflevector" ? MU_CI_UVM_IRBUILDER_NEW_SHUFFLEVECTOR
				 : op == "uvm.irbuilder.new_new" ? MU_CI_UVM_IRBUILDER_NEW_NEW
				 : op == "uvm.irbuilder.new_newhybrid" ? MU_CI_UVM_IRBUILDER_NEW_NEWHYBRID
				 : op == "uvm.irbuilder.new_alloca" ? MU_CI_UVM_IRBUILDER_NEW_ALLOCA
				 : op == "uvm.irbuilder.new_allocahybrid" ? MU_CI_UVM_IRBUILDER_NEW_ALLOCAHYBRID
				 : op == "uvm.irbuilder.new_getiref" ? MU_CI_UVM_IRBUILDER_NEW_GETIREF
				 : op == "uvm.irbuilder.new_getfieldiref" ? MU_CI_UVM_IRBUILDER_NEW_GETFIELDIREF
				 : op == "uvm.irbuilder.new_getelemiref" ? MU_CI_UVM_IRBUILDER_NEW_GETELEMIREF
				 : op == "uvm.irbuilder.new_shiftiref" ? MU_CI_UVM_IRBUILDER_NEW_SHIFTIREF
				 : op == "uvm.irbuilder.new_getvarpartiref" ? MU_CI_UVM_IRBUILDER_NEW_GETVARPARTIREF
				 : op == "uvm.irbuilder.new_load" ? MU_CI_UVM_IRBUILDER_NEW_LOAD
				 : op == "uvm.irbuilder.new_store" ? MU_CI_UVM_IRBUILDER_NEW_STORE
				 : op == "uvm.irbuilder.new_cmpxchg" ? MU_CI_UVM_IRBUILDER_NEW_CMPXCHG
				 : op == "uvm.irbuilder.new_atomicrmw" ? MU_CI_UVM_IRBUILDER_NEW_ATOMICRMW
				 : op == "uvm.irbuilder.new_fence" ? MU_CI_UVM_IRBUILDER_NEW_FENCE
				 : op == "uvm.irbuilder.new_trap" ? MU_CI_UVM_IRBUILDER_NEW_TRAP
				 : op == "uvm.irbuilder.new_watchpoint" ? MU_CI_UVM_IRBUILDER_NEW_WATCHPOINT
				 : op == "uvm.irbuilder.new_wpbranch" ? MU_CI_UVM_IRBUILDER_NEW_WPBRANCH
				 : op == "uvm.irbuilder.new_ccall" ? MU_CI_UVM_IRBUILDER_NEW_CCALL
				 : op == "uvm.irbuilder.new_newthread" ? MU_CI_UVM_IRBUILDER_NEW_NEWTHREAD
				 : op == "uvm.irbuilder.new_swapstack" ? MU_CI_UVM_IRBUILDER_NEW_SWAPSTACK
				 : op == "uvm.irbuilder.new_comminst" ? MU_CI_UVM_IRBUILDER_NEW_COMMINST
				 : throw antlr4::RuntimeException("Unrecognised common instruction \"@"s + op + "\""s);
		}

		// ******************************* Trivials, mearly accept ther child *****************************
		virtual Any<MuFuncNode> visitFunc(UIRParser::FuncContext *context) override { return (MuFuncNode)accept<MuID>(context->nam); }
		virtual Any<MuGlobalVarNode> visitGlobalVar(UIRParser::GlobalVarContext *context) override { return (MuGlobalVarNode)accept<MuID>(context->nam); }
		virtual Any<MuBBNode> visitBb(UIRParser::BbContext *context) override { return (MuBBNode)accept<MuID>(context->nam); }
		virtual Any<MuID> visitInstResult(UIRParser::InstResultContext *context) override { return accept<MuID>(context->result); }
		virtual Any<MuFlag> visitFlag(UIRParser::FlagContext *context) override { return context->callConv() ? (MuFlag)accept<MuCallConv>(context->callConv()) : (MuFlag)accept<MuBinOpStatus>(context->binopStatus()); }
        virtual Any<MuVarNode> visitValue(UIRParser::ValueContext *context) override { return context->nam ? (MuVarNode)accept<MuID>(context->nam) : (MuVarNode)accept<MuConstNode>(context->constant()); }
	};
}

void runtime_compile(MuCtx* ctx, std::string filename) {
    std::ifstream input_file (filename);
	if (input_file.fail()) {
		throw antlr4::RuntimeException("Couldn't oppen file \""s + filename + "\""s);
	}

    antlr4::ANTLRInputStream input(input_file);

    UIRLexer lexer(&input);
    antlr4::CommonTokenStream tokens(&lexer);

    tokens.fill();
    UIRParser parser(&tokens);

	auto ir = parser.ir();
	if (lexer.getNumberOfSyntaxErrors() != 0 || parser.getNumberOfSyntaxErrors() != 0)
		::error = true;
    else
        Runtime::Visitor(ctx).accept(ir);
}
