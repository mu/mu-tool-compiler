
// Generated from UIR.g4 by ANTLR 4.7


#include "UIRVisitor.h"

#include "UIRParser.h"


using namespace antlrcpp;
using namespace antlr4;

UIRParser::UIRParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

UIRParser::~UIRParser() {
  delete _interpreter;
}

std::string UIRParser::getGrammarFileName() const {
  return "UIR.g4";
}

const std::vector<std::string>& UIRParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& UIRParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- IrContext ------------------------------------------------------------------

UIRParser::IrContext::IrContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* UIRParser::IrContext::EOF() {
  return getToken(UIRParser::EOF, 0);
}

std::vector<UIRParser::TopLevelDefContext *> UIRParser::IrContext::topLevelDef() {
  return getRuleContexts<UIRParser::TopLevelDefContext>();
}

UIRParser::TopLevelDefContext* UIRParser::IrContext::topLevelDef(size_t i) {
  return getRuleContext<UIRParser::TopLevelDefContext>(i);
}


size_t UIRParser::IrContext::getRuleIndex() const {
  return UIRParser::RuleIr;
}

antlrcpp::Any UIRParser::IrContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitIr(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::IrContext* UIRParser::ir() {
  IrContext *_localctx = _tracker.createInstance<IrContext>(_ctx, getState());
  enterRule(_localctx, 0, UIRParser::RuleIr);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(95);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << UIRParser::T__0)
      | (1ULL << UIRParser::T__1)
      | (1ULL << UIRParser::T__3)
      | (1ULL << UIRParser::T__4)
      | (1ULL << UIRParser::T__7)
      | (1ULL << UIRParser::T__8)
      | (1ULL << UIRParser::T__9)
      | (1ULL << UIRParser::T__10))) != 0)) {
      setState(92);
      topLevelDef();
      setState(97);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(98);
    match(UIRParser::EOF);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TopLevelDefContext ------------------------------------------------------------------

UIRParser::TopLevelDefContext::TopLevelDefContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t UIRParser::TopLevelDefContext::getRuleIndex() const {
  return UIRParser::RuleTopLevelDef;
}

void UIRParser::TopLevelDefContext::copyFrom(TopLevelDefContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- BundleDefContext ------------------------------------------------------------------

tree::TerminalNode* UIRParser::BundleDefContext::NAME() {
  return getToken(UIRParser::NAME, 0);
}

tree::TerminalNode* UIRParser::BundleDefContext::GLOBAL_NAME() {
  return getToken(UIRParser::GLOBAL_NAME, 0);
}

UIRParser::BundleDefContext::BundleDefContext(TopLevelDefContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::BundleDefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitBundleDef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ConstDefContext ------------------------------------------------------------------

UIRParser::GlobalNameContext* UIRParser::ConstDefContext::globalName() {
  return getRuleContext<UIRParser::GlobalNameContext>(0);
}

UIRParser::TypeContext* UIRParser::ConstDefContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

UIRParser::ConstConstructorContext* UIRParser::ConstDefContext::constConstructor() {
  return getRuleContext<UIRParser::ConstConstructorContext>(0);
}

UIRParser::ConstDefContext::ConstDefContext(TopLevelDefContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::ConstDefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitConstDef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- GlobalDefContext ------------------------------------------------------------------

UIRParser::GlobalNameContext* UIRParser::GlobalDefContext::globalName() {
  return getRuleContext<UIRParser::GlobalNameContext>(0);
}

UIRParser::TypeContext* UIRParser::GlobalDefContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

UIRParser::GlobalDefContext::GlobalDefContext(TopLevelDefContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::GlobalDefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitGlobalDef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeDefContext ------------------------------------------------------------------

UIRParser::GlobalNameContext* UIRParser::TypeDefContext::globalName() {
  return getRuleContext<UIRParser::GlobalNameContext>(0);
}

UIRParser::TypeConstructorContext* UIRParser::TypeDefContext::typeConstructor() {
  return getRuleContext<UIRParser::TypeConstructorContext>(0);
}

UIRParser::TypeDefContext::TypeDefContext(TopLevelDefContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeDefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeDef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- FuncDefContext ------------------------------------------------------------------

UIRParser::FuncContext* UIRParser::FuncDefContext::func() {
  return getRuleContext<UIRParser::FuncContext>(0);
}

std::vector<UIRParser::BasicBlockContext *> UIRParser::FuncDefContext::basicBlock() {
  return getRuleContexts<UIRParser::BasicBlockContext>();
}

UIRParser::BasicBlockContext* UIRParser::FuncDefContext::basicBlock(size_t i) {
  return getRuleContext<UIRParser::BasicBlockContext>(i);
}

UIRParser::FuncSigContext* UIRParser::FuncDefContext::funcSig() {
  return getRuleContext<UIRParser::FuncSigContext>(0);
}

UIRParser::LocalNameContext* UIRParser::FuncDefContext::localName() {
  return getRuleContext<UIRParser::LocalNameContext>(0);
}

UIRParser::FuncDefContext::FuncDefContext(TopLevelDefContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::FuncDefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitFuncDef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- FuncSigDefContext ------------------------------------------------------------------

UIRParser::GlobalNameContext* UIRParser::FuncSigDefContext::globalName() {
  return getRuleContext<UIRParser::GlobalNameContext>(0);
}

UIRParser::FuncSigConstructorContext* UIRParser::FuncSigDefContext::funcSigConstructor() {
  return getRuleContext<UIRParser::FuncSigConstructorContext>(0);
}

UIRParser::FuncSigDefContext::FuncSigDefContext(TopLevelDefContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::FuncSigDefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitFuncSigDef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- FuncDeclContext ------------------------------------------------------------------

UIRParser::FuncContext* UIRParser::FuncDeclContext::func() {
  return getRuleContext<UIRParser::FuncContext>(0);
}

UIRParser::FuncSigContext* UIRParser::FuncDeclContext::funcSig() {
  return getRuleContext<UIRParser::FuncSigContext>(0);
}

UIRParser::FuncDeclContext::FuncDeclContext(TopLevelDefContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::FuncDeclContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitFuncDecl(this);
  else
    return visitor->visitChildren(this);
}
//----------------- FuncExpDefContext ------------------------------------------------------------------

UIRParser::FuncContext* UIRParser::FuncExpDefContext::func() {
  return getRuleContext<UIRParser::FuncContext>(0);
}

UIRParser::CallConvContext* UIRParser::FuncExpDefContext::callConv() {
  return getRuleContext<UIRParser::CallConvContext>(0);
}

UIRParser::GlobalNameContext* UIRParser::FuncExpDefContext::globalName() {
  return getRuleContext<UIRParser::GlobalNameContext>(0);
}

UIRParser::ConstantContext* UIRParser::FuncExpDefContext::constant() {
  return getRuleContext<UIRParser::ConstantContext>(0);
}

UIRParser::FuncExpDefContext::FuncExpDefContext(TopLevelDefContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::FuncExpDefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitFuncExpDef(this);
  else
    return visitor->visitChildren(this);
}
UIRParser::TopLevelDefContext* UIRParser::topLevelDef() {
  TopLevelDefContext *_localctx = _tracker.createInstance<TopLevelDefContext>(_ctx, getState());
  enterRule(_localctx, 2, UIRParser::RuleTopLevelDef);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(166);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case UIRParser::T__0: {
        _localctx = dynamic_cast<TopLevelDefContext *>(_tracker.createInstance<UIRParser::BundleDefContext>(_localctx));
        enterOuterAlt(_localctx, 1);
        setState(100);
        match(UIRParser::T__0);
        setState(101);
        _la = _input->LA(1);
        if (!(_la == UIRParser::NAME

        || _la == UIRParser::GLOBAL_NAME)) {
        _errHandler->recoverInline(this);
        }
        else {
          _errHandler->reportMatch(this);
          consume();
        }
        break;
      }

      case UIRParser::T__1: {
        _localctx = dynamic_cast<TopLevelDefContext *>(_tracker.createInstance<UIRParser::TypeDefContext>(_localctx));
        enterOuterAlt(_localctx, 2);
        setState(102);
        match(UIRParser::T__1);
        setState(103);
        dynamic_cast<TypeDefContext *>(_localctx)->nam = globalName();
        setState(104);
        match(UIRParser::T__2);
        setState(105);
        dynamic_cast<TypeDefContext *>(_localctx)->ctor = typeConstructor();
        break;
      }

      case UIRParser::T__3: {
        _localctx = dynamic_cast<TopLevelDefContext *>(_tracker.createInstance<UIRParser::FuncSigDefContext>(_localctx));
        enterOuterAlt(_localctx, 3);
        setState(107);
        match(UIRParser::T__3);
        setState(108);
        dynamic_cast<FuncSigDefContext *>(_localctx)->nam = globalName();
        setState(109);
        match(UIRParser::T__2);
        setState(110);
        dynamic_cast<FuncSigDefContext *>(_localctx)->ctor = funcSigConstructor();
        break;
      }

      case UIRParser::T__4: {
        _localctx = dynamic_cast<TopLevelDefContext *>(_tracker.createInstance<UIRParser::ConstDefContext>(_localctx));
        enterOuterAlt(_localctx, 4);
        setState(112);
        match(UIRParser::T__4);
        setState(113);
        dynamic_cast<ConstDefContext *>(_localctx)->nam = globalName();
        setState(114);
        match(UIRParser::T__5);
        setState(115);
        dynamic_cast<ConstDefContext *>(_localctx)->ty = type();
        setState(116);
        match(UIRParser::T__6);
        setState(117);
        match(UIRParser::T__2);
        setState(118);
        dynamic_cast<ConstDefContext *>(_localctx)->ctor = constConstructor();
        break;
      }

      case UIRParser::T__7: {
        _localctx = dynamic_cast<TopLevelDefContext *>(_tracker.createInstance<UIRParser::GlobalDefContext>(_localctx));
        enterOuterAlt(_localctx, 5);
        setState(120);
        match(UIRParser::T__7);
        setState(121);
        dynamic_cast<GlobalDefContext *>(_localctx)->nam = globalName();
        setState(122);
        match(UIRParser::T__5);
        setState(123);
        dynamic_cast<GlobalDefContext *>(_localctx)->ty = type();
        setState(124);
        match(UIRParser::T__6);
        break;
      }

      case UIRParser::T__8: {
        _localctx = dynamic_cast<TopLevelDefContext *>(_tracker.createInstance<UIRParser::FuncDeclContext>(_localctx));
        enterOuterAlt(_localctx, 6);
        setState(126);
        match(UIRParser::T__8);
        setState(127);
        dynamic_cast<FuncDeclContext *>(_localctx)->nam = func();
        setState(128);
        match(UIRParser::T__5);
        setState(129);
        dynamic_cast<FuncDeclContext *>(_localctx)->sig = funcSig();
        setState(130);
        match(UIRParser::T__6);
        break;
      }

      case UIRParser::T__9: {
        _localctx = dynamic_cast<TopLevelDefContext *>(_tracker.createInstance<UIRParser::FuncExpDefContext>(_localctx));
        enterOuterAlt(_localctx, 7);
        setState(132);
        match(UIRParser::T__9);
        setState(133);
        dynamic_cast<FuncExpDefContext *>(_localctx)->nam = globalName();
        setState(134);
        match(UIRParser::T__2);
        setState(135);
        func();
        setState(136);
        callConv();
        setState(137);
        dynamic_cast<FuncExpDefContext *>(_localctx)->cookie = constant();
        break;
      }

      case UIRParser::T__10: {
        _localctx = dynamic_cast<TopLevelDefContext *>(_tracker.createInstance<UIRParser::FuncDefContext>(_localctx));
        enterOuterAlt(_localctx, 8);
        setState(139);
        match(UIRParser::T__10);
        setState(140);
        dynamic_cast<FuncDefContext *>(_localctx)->nam = func();
        setState(156);
        _errHandler->sync(this);
        switch (_input->LA(1)) {
          case UIRParser::T__5: {
            setState(141);
            match(UIRParser::T__5);
            setState(142);
            dynamic_cast<FuncDefContext *>(_localctx)->sig = funcSig();
            setState(143);
            match(UIRParser::T__6);
            setState(146);
            _errHandler->sync(this);

            _la = _input->LA(1);
            if (_la == UIRParser::T__11) {
              setState(144);
              match(UIRParser::T__11);
              setState(145);
              dynamic_cast<FuncDefContext *>(_localctx)->ver = localName();
            }
            break;
          }

          case UIRParser::T__11: {
            setState(148);
            match(UIRParser::T__11);
            setState(149);
            dynamic_cast<FuncDefContext *>(_localctx)->ver = localName();
            setState(154);
            _errHandler->sync(this);

            _la = _input->LA(1);
            if (_la == UIRParser::T__5) {
              setState(150);
              match(UIRParser::T__5);
              setState(151);
              dynamic_cast<FuncDefContext *>(_localctx)->sig = funcSig();
              setState(152);
              match(UIRParser::T__6);
            }
            break;
          }

          case UIRParser::T__12: {
            break;
          }

        default:
          break;
        }
        setState(158);
        match(UIRParser::T__12);
        setState(160); 
        _errHandler->sync(this);
        _la = _input->LA(1);
        do {
          setState(159);
          dynamic_cast<FuncDefContext *>(_localctx)->basicBlockContext = basicBlock();
          dynamic_cast<FuncDefContext *>(_localctx)->body.push_back(dynamic_cast<FuncDefContext *>(_localctx)->basicBlockContext);
          setState(162); 
          _errHandler->sync(this);
          _la = _input->LA(1);
        } while (((((_la - 172) & ~ 0x3fULL) == 0) &&
          ((1ULL << (_la - 172)) & ((1ULL << (UIRParser::NAME - 172))
          | (1ULL << (UIRParser::GLOBAL_NAME - 172))
          | (1ULL << (UIRParser::LOCAL_NAME - 172)))) != 0));
        setState(164);
        match(UIRParser::T__13);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TypeConstructorContext ------------------------------------------------------------------

UIRParser::TypeConstructorContext::TypeConstructorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t UIRParser::TypeConstructorContext::getRuleIndex() const {
  return UIRParser::RuleTypeConstructor;
}

void UIRParser::TypeConstructorContext::copyFrom(TypeConstructorContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- TypeArrayContext ------------------------------------------------------------------

UIRParser::TypeContext* UIRParser::TypeArrayContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

UIRParser::SizeParamContext* UIRParser::TypeArrayContext::sizeParam() {
  return getRuleContext<UIRParser::SizeParamContext>(0);
}

UIRParser::TypeArrayContext::TypeArrayContext(TypeConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeArrayContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeArray(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeFloatContext ------------------------------------------------------------------

UIRParser::TypeFloatContext::TypeFloatContext(TypeConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeFloatContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeFloat(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeIRefContext ------------------------------------------------------------------

UIRParser::TypeContext* UIRParser::TypeIRefContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

UIRParser::TypeIRefContext::TypeIRefContext(TypeConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeIRefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeIRef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeVoidContext ------------------------------------------------------------------

UIRParser::TypeVoidContext::TypeVoidContext(TypeConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeVoidContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeVoid(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeRefContext ------------------------------------------------------------------

UIRParser::TypeContext* UIRParser::TypeRefContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

UIRParser::TypeRefContext::TypeRefContext(TypeConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeRefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeRef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeStackRefContext ------------------------------------------------------------------

UIRParser::TypeStackRefContext::TypeStackRefContext(TypeConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeStackRefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeStackRef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeHybridContext ------------------------------------------------------------------

std::vector<UIRParser::TypeContext *> UIRParser::TypeHybridContext::type() {
  return getRuleContexts<UIRParser::TypeContext>();
}

UIRParser::TypeContext* UIRParser::TypeHybridContext::type(size_t i) {
  return getRuleContext<UIRParser::TypeContext>(i);
}

UIRParser::TypeHybridContext::TypeHybridContext(TypeConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeHybridContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeHybrid(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeTagRef64Context ------------------------------------------------------------------

UIRParser::TypeTagRef64Context::TypeTagRef64Context(TypeConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeTagRef64Context::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeTagRef64(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeWeakRefContext ------------------------------------------------------------------

UIRParser::TypeContext* UIRParser::TypeWeakRefContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

UIRParser::TypeWeakRefContext::TypeWeakRefContext(TypeConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeWeakRefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeWeakRef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeIntContext ------------------------------------------------------------------

UIRParser::IntParamContext* UIRParser::TypeIntContext::intParam() {
  return getRuleContext<UIRParser::IntParamContext>(0);
}

UIRParser::TypeIntContext::TypeIntContext(TypeConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeIntContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeInt(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeStructContext ------------------------------------------------------------------

std::vector<UIRParser::TypeContext *> UIRParser::TypeStructContext::type() {
  return getRuleContexts<UIRParser::TypeContext>();
}

UIRParser::TypeContext* UIRParser::TypeStructContext::type(size_t i) {
  return getRuleContext<UIRParser::TypeContext>(i);
}

UIRParser::TypeStructContext::TypeStructContext(TypeConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeStructContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeStruct(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeFuncRefContext ------------------------------------------------------------------

UIRParser::FuncSigContext* UIRParser::TypeFuncRefContext::funcSig() {
  return getRuleContext<UIRParser::FuncSigContext>(0);
}

UIRParser::TypeFuncRefContext::TypeFuncRefContext(TypeConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeFuncRefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeFuncRef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeIRBuilderRefContext ------------------------------------------------------------------

UIRParser::TypeIRBuilderRefContext::TypeIRBuilderRefContext(TypeConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeIRBuilderRefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeIRBuilderRef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeDoubleContext ------------------------------------------------------------------

UIRParser::TypeDoubleContext::TypeDoubleContext(TypeConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeDoubleContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeDouble(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeUFuncPtrContext ------------------------------------------------------------------

UIRParser::FuncSigContext* UIRParser::TypeUFuncPtrContext::funcSig() {
  return getRuleContext<UIRParser::FuncSigContext>(0);
}

UIRParser::TypeUFuncPtrContext::TypeUFuncPtrContext(TypeConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeUFuncPtrContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeUFuncPtr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeFrameCursorRefContext ------------------------------------------------------------------

UIRParser::TypeFrameCursorRefContext::TypeFrameCursorRefContext(TypeConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeFrameCursorRefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeFrameCursorRef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeVectorContext ------------------------------------------------------------------

UIRParser::TypeContext* UIRParser::TypeVectorContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

UIRParser::SizeParamContext* UIRParser::TypeVectorContext::sizeParam() {
  return getRuleContext<UIRParser::SizeParamContext>(0);
}

UIRParser::TypeVectorContext::TypeVectorContext(TypeConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeVectorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeVector(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeUPtrContext ------------------------------------------------------------------

UIRParser::TypeContext* UIRParser::TypeUPtrContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

UIRParser::TypeUPtrContext::TypeUPtrContext(TypeConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeUPtrContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeUPtr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeThreadRefContext ------------------------------------------------------------------

UIRParser::TypeThreadRefContext::TypeThreadRefContext(TypeConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::TypeThreadRefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeThreadRef(this);
  else
    return visitor->visitChildren(this);
}
UIRParser::TypeConstructorContext* UIRParser::typeConstructor() {
  TypeConstructorContext *_localctx = _tracker.createInstance<TypeConstructorContext>(_ctx, getState());
  enterRule(_localctx, 4, UIRParser::RuleTypeConstructor);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    setState(243);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case UIRParser::T__14: {
        _localctx = dynamic_cast<TypeConstructorContext *>(_tracker.createInstance<UIRParser::TypeIntContext>(_localctx));
        enterOuterAlt(_localctx, 1);
        setState(168);
        match(UIRParser::T__14);
        setState(169);
        match(UIRParser::T__5);
        setState(170);
        dynamic_cast<TypeIntContext *>(_localctx)->length = intParam();
        setState(171);
        match(UIRParser::T__6);
        break;
      }

      case UIRParser::T__15: {
        _localctx = dynamic_cast<TypeConstructorContext *>(_tracker.createInstance<UIRParser::TypeFloatContext>(_localctx));
        enterOuterAlt(_localctx, 2);
        setState(173);
        match(UIRParser::T__15);
        break;
      }

      case UIRParser::T__16: {
        _localctx = dynamic_cast<TypeConstructorContext *>(_tracker.createInstance<UIRParser::TypeDoubleContext>(_localctx));
        enterOuterAlt(_localctx, 3);
        setState(174);
        match(UIRParser::T__16);
        break;
      }

      case UIRParser::T__17: {
        _localctx = dynamic_cast<TypeConstructorContext *>(_tracker.createInstance<UIRParser::TypeUPtrContext>(_localctx));
        enterOuterAlt(_localctx, 4);
        setState(175);
        match(UIRParser::T__17);
        setState(176);
        match(UIRParser::T__5);
        setState(177);
        dynamic_cast<TypeUPtrContext *>(_localctx)->ty = type();
        setState(178);
        match(UIRParser::T__6);
        break;
      }

      case UIRParser::T__18: {
        _localctx = dynamic_cast<TypeConstructorContext *>(_tracker.createInstance<UIRParser::TypeUFuncPtrContext>(_localctx));
        enterOuterAlt(_localctx, 5);
        setState(180);
        match(UIRParser::T__18);
        setState(181);
        match(UIRParser::T__5);
        setState(182);
        funcSig();
        setState(183);
        match(UIRParser::T__6);
        break;
      }

      case UIRParser::T__19: {
        _localctx = dynamic_cast<TypeConstructorContext *>(_tracker.createInstance<UIRParser::TypeStructContext>(_localctx));
        enterOuterAlt(_localctx, 6);
        setState(185);
        match(UIRParser::T__19);
        setState(186);
        match(UIRParser::T__5);
        setState(188); 
        _errHandler->sync(this);
        _la = _input->LA(1);
        do {
          setState(187);
          dynamic_cast<TypeStructContext *>(_localctx)->typeContext = type();
          dynamic_cast<TypeStructContext *>(_localctx)->fieldTys.push_back(dynamic_cast<TypeStructContext *>(_localctx)->typeContext);
          setState(190); 
          _errHandler->sync(this);
          _la = _input->LA(1);
        } while ((((_la & ~ 0x3fULL) == 0) &&
          ((1ULL << _la) & ((1ULL << UIRParser::T__14)
          | (1ULL << UIRParser::T__15)
          | (1ULL << UIRParser::T__16)
          | (1ULL << UIRParser::T__17)
          | (1ULL << UIRParser::T__18)
          | (1ULL << UIRParser::T__19)
          | (1ULL << UIRParser::T__20)
          | (1ULL << UIRParser::T__21)
          | (1ULL << UIRParser::T__22)
          | (1ULL << UIRParser::T__23)
          | (1ULL << UIRParser::T__24)
          | (1ULL << UIRParser::T__25)
          | (1ULL << UIRParser::T__26)
          | (1ULL << UIRParser::T__27)
          | (1ULL << UIRParser::T__28)
          | (1ULL << UIRParser::T__29)
          | (1ULL << UIRParser::T__30)
          | (1ULL << UIRParser::T__31)
          | (1ULL << UIRParser::T__32))) != 0) || ((((_la - 172) & ~ 0x3fULL) == 0) &&
          ((1ULL << (_la - 172)) & ((1ULL << (UIRParser::NAME - 172))
          | (1ULL << (UIRParser::GLOBAL_NAME - 172))
          | (1ULL << (UIRParser::LOCAL_NAME - 172)))) != 0));
        setState(192);
        match(UIRParser::T__6);
        break;
      }

      case UIRParser::T__20: {
        _localctx = dynamic_cast<TypeConstructorContext *>(_tracker.createInstance<UIRParser::TypeHybridContext>(_localctx));
        enterOuterAlt(_localctx, 7);
        setState(194);
        match(UIRParser::T__20);
        setState(195);
        match(UIRParser::T__5);
        setState(199);
        _errHandler->sync(this);
        alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 7, _ctx);
        while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
          if (alt == 1) {
            setState(196);
            dynamic_cast<TypeHybridContext *>(_localctx)->typeContext = type();
            dynamic_cast<TypeHybridContext *>(_localctx)->fieldTys.push_back(dynamic_cast<TypeHybridContext *>(_localctx)->typeContext); 
          }
          setState(201);
          _errHandler->sync(this);
          alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 7, _ctx);
        }
        setState(202);
        dynamic_cast<TypeHybridContext *>(_localctx)->varTy = type();
        setState(203);
        match(UIRParser::T__6);
        break;
      }

      case UIRParser::T__21: {
        _localctx = dynamic_cast<TypeConstructorContext *>(_tracker.createInstance<UIRParser::TypeArrayContext>(_localctx));
        enterOuterAlt(_localctx, 8);
        setState(205);
        match(UIRParser::T__21);
        setState(206);
        match(UIRParser::T__5);
        setState(207);
        dynamic_cast<TypeArrayContext *>(_localctx)->ty = type();
        setState(208);
        dynamic_cast<TypeArrayContext *>(_localctx)->length = sizeParam();
        setState(209);
        match(UIRParser::T__6);
        break;
      }

      case UIRParser::T__22: {
        _localctx = dynamic_cast<TypeConstructorContext *>(_tracker.createInstance<UIRParser::TypeVectorContext>(_localctx));
        enterOuterAlt(_localctx, 9);
        setState(211);
        match(UIRParser::T__22);
        setState(212);
        match(UIRParser::T__5);
        setState(213);
        dynamic_cast<TypeVectorContext *>(_localctx)->ty = type();
        setState(214);
        dynamic_cast<TypeVectorContext *>(_localctx)->length = sizeParam();
        setState(215);
        match(UIRParser::T__6);
        break;
      }

      case UIRParser::T__23: {
        _localctx = dynamic_cast<TypeConstructorContext *>(_tracker.createInstance<UIRParser::TypeVoidContext>(_localctx));
        enterOuterAlt(_localctx, 10);
        setState(217);
        match(UIRParser::T__23);
        break;
      }

      case UIRParser::T__24: {
        _localctx = dynamic_cast<TypeConstructorContext *>(_tracker.createInstance<UIRParser::TypeRefContext>(_localctx));
        enterOuterAlt(_localctx, 11);
        setState(218);
        match(UIRParser::T__24);
        setState(219);
        match(UIRParser::T__5);
        setState(220);
        dynamic_cast<TypeRefContext *>(_localctx)->ty = type();
        setState(221);
        match(UIRParser::T__6);
        break;
      }

      case UIRParser::T__25: {
        _localctx = dynamic_cast<TypeConstructorContext *>(_tracker.createInstance<UIRParser::TypeIRefContext>(_localctx));
        enterOuterAlt(_localctx, 12);
        setState(223);
        match(UIRParser::T__25);
        setState(224);
        match(UIRParser::T__5);
        setState(225);
        dynamic_cast<TypeIRefContext *>(_localctx)->ty = type();
        setState(226);
        match(UIRParser::T__6);
        break;
      }

      case UIRParser::T__26: {
        _localctx = dynamic_cast<TypeConstructorContext *>(_tracker.createInstance<UIRParser::TypeWeakRefContext>(_localctx));
        enterOuterAlt(_localctx, 13);
        setState(228);
        match(UIRParser::T__26);
        setState(229);
        match(UIRParser::T__5);
        setState(230);
        dynamic_cast<TypeWeakRefContext *>(_localctx)->ty = type();
        setState(231);
        match(UIRParser::T__6);
        break;
      }

      case UIRParser::T__27: {
        _localctx = dynamic_cast<TypeConstructorContext *>(_tracker.createInstance<UIRParser::TypeTagRef64Context>(_localctx));
        enterOuterAlt(_localctx, 14);
        setState(233);
        match(UIRParser::T__27);
        break;
      }

      case UIRParser::T__28: {
        _localctx = dynamic_cast<TypeConstructorContext *>(_tracker.createInstance<UIRParser::TypeFuncRefContext>(_localctx));
        enterOuterAlt(_localctx, 15);
        setState(234);
        match(UIRParser::T__28);
        setState(235);
        match(UIRParser::T__5);
        setState(236);
        funcSig();
        setState(237);
        match(UIRParser::T__6);
        break;
      }

      case UIRParser::T__29: {
        _localctx = dynamic_cast<TypeConstructorContext *>(_tracker.createInstance<UIRParser::TypeThreadRefContext>(_localctx));
        enterOuterAlt(_localctx, 16);
        setState(239);
        match(UIRParser::T__29);
        break;
      }

      case UIRParser::T__30: {
        _localctx = dynamic_cast<TypeConstructorContext *>(_tracker.createInstance<UIRParser::TypeStackRefContext>(_localctx));
        enterOuterAlt(_localctx, 17);
        setState(240);
        match(UIRParser::T__30);
        break;
      }

      case UIRParser::T__31: {
        _localctx = dynamic_cast<TypeConstructorContext *>(_tracker.createInstance<UIRParser::TypeFrameCursorRefContext>(_localctx));
        enterOuterAlt(_localctx, 18);
        setState(241);
        match(UIRParser::T__31);
        break;
      }

      case UIRParser::T__32: {
        _localctx = dynamic_cast<TypeConstructorContext *>(_tracker.createInstance<UIRParser::TypeIRBuilderRefContext>(_localctx));
        enterOuterAlt(_localctx, 19);
        setState(242);
        match(UIRParser::T__32);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FuncSigConstructorContext ------------------------------------------------------------------

UIRParser::FuncSigConstructorContext::FuncSigConstructorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<UIRParser::TypeContext *> UIRParser::FuncSigConstructorContext::type() {
  return getRuleContexts<UIRParser::TypeContext>();
}

UIRParser::TypeContext* UIRParser::FuncSigConstructorContext::type(size_t i) {
  return getRuleContext<UIRParser::TypeContext>(i);
}


size_t UIRParser::FuncSigConstructorContext::getRuleIndex() const {
  return UIRParser::RuleFuncSigConstructor;
}

antlrcpp::Any UIRParser::FuncSigConstructorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitFuncSigConstructor(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::FuncSigConstructorContext* UIRParser::funcSigConstructor() {
  FuncSigConstructorContext *_localctx = _tracker.createInstance<FuncSigConstructorContext>(_ctx, getState());
  enterRule(_localctx, 6, UIRParser::RuleFuncSigConstructor);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(245);
    match(UIRParser::T__33);
    setState(249);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << UIRParser::T__14)
      | (1ULL << UIRParser::T__15)
      | (1ULL << UIRParser::T__16)
      | (1ULL << UIRParser::T__17)
      | (1ULL << UIRParser::T__18)
      | (1ULL << UIRParser::T__19)
      | (1ULL << UIRParser::T__20)
      | (1ULL << UIRParser::T__21)
      | (1ULL << UIRParser::T__22)
      | (1ULL << UIRParser::T__23)
      | (1ULL << UIRParser::T__24)
      | (1ULL << UIRParser::T__25)
      | (1ULL << UIRParser::T__26)
      | (1ULL << UIRParser::T__27)
      | (1ULL << UIRParser::T__28)
      | (1ULL << UIRParser::T__29)
      | (1ULL << UIRParser::T__30)
      | (1ULL << UIRParser::T__31)
      | (1ULL << UIRParser::T__32))) != 0) || ((((_la - 172) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 172)) & ((1ULL << (UIRParser::NAME - 172))
      | (1ULL << (UIRParser::GLOBAL_NAME - 172))
      | (1ULL << (UIRParser::LOCAL_NAME - 172)))) != 0)) {
      setState(246);
      dynamic_cast<FuncSigConstructorContext *>(_localctx)->typeContext = type();
      dynamic_cast<FuncSigConstructorContext *>(_localctx)->paramTys.push_back(dynamic_cast<FuncSigConstructorContext *>(_localctx)->typeContext);
      setState(251);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(252);
    match(UIRParser::T__34);
    setState(253);
    match(UIRParser::T__35);
    setState(254);
    match(UIRParser::T__33);
    setState(258);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << UIRParser::T__14)
      | (1ULL << UIRParser::T__15)
      | (1ULL << UIRParser::T__16)
      | (1ULL << UIRParser::T__17)
      | (1ULL << UIRParser::T__18)
      | (1ULL << UIRParser::T__19)
      | (1ULL << UIRParser::T__20)
      | (1ULL << UIRParser::T__21)
      | (1ULL << UIRParser::T__22)
      | (1ULL << UIRParser::T__23)
      | (1ULL << UIRParser::T__24)
      | (1ULL << UIRParser::T__25)
      | (1ULL << UIRParser::T__26)
      | (1ULL << UIRParser::T__27)
      | (1ULL << UIRParser::T__28)
      | (1ULL << UIRParser::T__29)
      | (1ULL << UIRParser::T__30)
      | (1ULL << UIRParser::T__31)
      | (1ULL << UIRParser::T__32))) != 0) || ((((_la - 172) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 172)) & ((1ULL << (UIRParser::NAME - 172))
      | (1ULL << (UIRParser::GLOBAL_NAME - 172))
      | (1ULL << (UIRParser::LOCAL_NAME - 172)))) != 0)) {
      setState(255);
      dynamic_cast<FuncSigConstructorContext *>(_localctx)->typeContext = type();
      dynamic_cast<FuncSigConstructorContext *>(_localctx)->retTys.push_back(dynamic_cast<FuncSigConstructorContext *>(_localctx)->typeContext);
      setState(260);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(261);
    match(UIRParser::T__34);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ConstConstructorContext ------------------------------------------------------------------

UIRParser::ConstConstructorContext::ConstConstructorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t UIRParser::ConstConstructorContext::getRuleIndex() const {
  return UIRParser::RuleConstConstructor;
}

void UIRParser::ConstConstructorContext::copyFrom(ConstConstructorContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- CtorFloatContext ------------------------------------------------------------------

UIRParser::FloatLiteralContext* UIRParser::CtorFloatContext::floatLiteral() {
  return getRuleContext<UIRParser::FloatLiteralContext>(0);
}

UIRParser::CtorFloatContext::CtorFloatContext(ConstConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::CtorFloatContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitCtorFloat(this);
  else
    return visitor->visitChildren(this);
}
//----------------- CtorListContext ------------------------------------------------------------------

std::vector<UIRParser::GlobalVarContext *> UIRParser::CtorListContext::globalVar() {
  return getRuleContexts<UIRParser::GlobalVarContext>();
}

UIRParser::GlobalVarContext* UIRParser::CtorListContext::globalVar(size_t i) {
  return getRuleContext<UIRParser::GlobalVarContext>(i);
}

UIRParser::CtorListContext::CtorListContext(ConstConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::CtorListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitCtorList(this);
  else
    return visitor->visitChildren(this);
}
//----------------- CtorDoubleContext ------------------------------------------------------------------

UIRParser::DoubleLiteralContext* UIRParser::CtorDoubleContext::doubleLiteral() {
  return getRuleContext<UIRParser::DoubleLiteralContext>(0);
}

UIRParser::CtorDoubleContext::CtorDoubleContext(ConstConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::CtorDoubleContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitCtorDouble(this);
  else
    return visitor->visitChildren(this);
}
//----------------- CtorExternContext ------------------------------------------------------------------

UIRParser::StringLiteralContext* UIRParser::CtorExternContext::stringLiteral() {
  return getRuleContext<UIRParser::StringLiteralContext>(0);
}

UIRParser::CtorExternContext::CtorExternContext(ConstConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::CtorExternContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitCtorExtern(this);
  else
    return visitor->visitChildren(this);
}
//----------------- CtorNullContext ------------------------------------------------------------------

UIRParser::CtorNullContext::CtorNullContext(ConstConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::CtorNullContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitCtorNull(this);
  else
    return visitor->visitChildren(this);
}
//----------------- CtorIntContext ------------------------------------------------------------------

UIRParser::IntLiteralContext* UIRParser::CtorIntContext::intLiteral() {
  return getRuleContext<UIRParser::IntLiteralContext>(0);
}

UIRParser::CtorIntContext::CtorIntContext(ConstConstructorContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::CtorIntContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitCtorInt(this);
  else
    return visitor->visitChildren(this);
}
UIRParser::ConstConstructorContext* UIRParser::constConstructor() {
  ConstConstructorContext *_localctx = _tracker.createInstance<ConstConstructorContext>(_ctx, getState());
  enterRule(_localctx, 8, UIRParser::RuleConstConstructor);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(277);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 12, _ctx)) {
    case 1: {
      _localctx = dynamic_cast<ConstConstructorContext *>(_tracker.createInstance<UIRParser::CtorIntContext>(_localctx));
      enterOuterAlt(_localctx, 1);
      setState(263);
      intLiteral();
      break;
    }

    case 2: {
      _localctx = dynamic_cast<ConstConstructorContext *>(_tracker.createInstance<UIRParser::CtorFloatContext>(_localctx));
      enterOuterAlt(_localctx, 2);
      setState(264);
      floatLiteral();
      break;
    }

    case 3: {
      _localctx = dynamic_cast<ConstConstructorContext *>(_tracker.createInstance<UIRParser::CtorDoubleContext>(_localctx));
      enterOuterAlt(_localctx, 3);
      setState(265);
      doubleLiteral();
      break;
    }

    case 4: {
      _localctx = dynamic_cast<ConstConstructorContext *>(_tracker.createInstance<UIRParser::CtorNullContext>(_localctx));
      enterOuterAlt(_localctx, 4);
      setState(266);
      match(UIRParser::T__36);
      break;
    }

    case 5: {
      _localctx = dynamic_cast<ConstConstructorContext *>(_tracker.createInstance<UIRParser::CtorListContext>(_localctx));
      enterOuterAlt(_localctx, 5);
      setState(267);
      match(UIRParser::T__12);
      setState(271);
      _errHandler->sync(this);
      _la = _input->LA(1);
      while (((((_la - 172) & ~ 0x3fULL) == 0) &&
        ((1ULL << (_la - 172)) & ((1ULL << (UIRParser::NAME - 172))
        | (1ULL << (UIRParser::GLOBAL_NAME - 172))
        | (1ULL << (UIRParser::LOCAL_NAME - 172)))) != 0)) {
        setState(268);
        globalVar();
        setState(273);
        _errHandler->sync(this);
        _la = _input->LA(1);
      }
      setState(274);
      match(UIRParser::T__13);
      break;
    }

    case 6: {
      _localctx = dynamic_cast<ConstConstructorContext *>(_tracker.createInstance<UIRParser::CtorExternContext>(_localctx));
      enterOuterAlt(_localctx, 6);
      setState(275);
      match(UIRParser::T__37);
      setState(276);
      dynamic_cast<CtorExternContext *>(_localctx)->symbol = stringLiteral();
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TypeContext ------------------------------------------------------------------

UIRParser::TypeContext::TypeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

UIRParser::GlobalNameContext* UIRParser::TypeContext::globalName() {
  return getRuleContext<UIRParser::GlobalNameContext>(0);
}

UIRParser::TypeConstructorContext* UIRParser::TypeContext::typeConstructor() {
  return getRuleContext<UIRParser::TypeConstructorContext>(0);
}


size_t UIRParser::TypeContext::getRuleIndex() const {
  return UIRParser::RuleType;
}

antlrcpp::Any UIRParser::TypeContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitType(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::TypeContext* UIRParser::type() {
  TypeContext *_localctx = _tracker.createInstance<TypeContext>(_ctx, getState());
  enterRule(_localctx, 10, UIRParser::RuleType);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(281);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case UIRParser::NAME:
      case UIRParser::GLOBAL_NAME:
      case UIRParser::LOCAL_NAME: {
        enterOuterAlt(_localctx, 1);
        setState(279);
        dynamic_cast<TypeContext *>(_localctx)->nam = globalName();
        break;
      }

      case UIRParser::T__14:
      case UIRParser::T__15:
      case UIRParser::T__16:
      case UIRParser::T__17:
      case UIRParser::T__18:
      case UIRParser::T__19:
      case UIRParser::T__20:
      case UIRParser::T__21:
      case UIRParser::T__22:
      case UIRParser::T__23:
      case UIRParser::T__24:
      case UIRParser::T__25:
      case UIRParser::T__26:
      case UIRParser::T__27:
      case UIRParser::T__28:
      case UIRParser::T__29:
      case UIRParser::T__30:
      case UIRParser::T__31:
      case UIRParser::T__32: {
        enterOuterAlt(_localctx, 2);
        setState(280);
        typeConstructor();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FuncSigContext ------------------------------------------------------------------

UIRParser::FuncSigContext::FuncSigContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

UIRParser::GlobalNameContext* UIRParser::FuncSigContext::globalName() {
  return getRuleContext<UIRParser::GlobalNameContext>(0);
}

UIRParser::FuncSigConstructorContext* UIRParser::FuncSigContext::funcSigConstructor() {
  return getRuleContext<UIRParser::FuncSigConstructorContext>(0);
}


size_t UIRParser::FuncSigContext::getRuleIndex() const {
  return UIRParser::RuleFuncSig;
}

antlrcpp::Any UIRParser::FuncSigContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitFuncSig(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::FuncSigContext* UIRParser::funcSig() {
  FuncSigContext *_localctx = _tracker.createInstance<FuncSigContext>(_ctx, getState());
  enterRule(_localctx, 12, UIRParser::RuleFuncSig);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(285);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case UIRParser::NAME:
      case UIRParser::GLOBAL_NAME:
      case UIRParser::LOCAL_NAME: {
        enterOuterAlt(_localctx, 1);
        setState(283);
        dynamic_cast<FuncSigContext *>(_localctx)->nam = globalName();
        break;
      }

      case UIRParser::T__33: {
        enterOuterAlt(_localctx, 2);
        setState(284);
        funcSigConstructor();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ValueContext ------------------------------------------------------------------

UIRParser::ValueContext::ValueContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

UIRParser::NameContext* UIRParser::ValueContext::name() {
  return getRuleContext<UIRParser::NameContext>(0);
}

UIRParser::ConstantContext* UIRParser::ValueContext::constant() {
  return getRuleContext<UIRParser::ConstantContext>(0);
}


size_t UIRParser::ValueContext::getRuleIndex() const {
  return UIRParser::RuleValue;
}

antlrcpp::Any UIRParser::ValueContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitValue(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::ValueContext* UIRParser::value() {
  ValueContext *_localctx = _tracker.createInstance<ValueContext>(_ctx, getState());
  enterRule(_localctx, 14, UIRParser::RuleValue);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(289);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 15, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(287);
      dynamic_cast<ValueContext *>(_localctx)->nam = name();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(288);
      constant();
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ConstantContext ------------------------------------------------------------------

UIRParser::ConstantContext::ConstantContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

UIRParser::GlobalNameContext* UIRParser::ConstantContext::globalName() {
  return getRuleContext<UIRParser::GlobalNameContext>(0);
}

UIRParser::ConstConstructorContext* UIRParser::ConstantContext::constConstructor() {
  return getRuleContext<UIRParser::ConstConstructorContext>(0);
}

UIRParser::TypeContext* UIRParser::ConstantContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}


size_t UIRParser::ConstantContext::getRuleIndex() const {
  return UIRParser::RuleConstant;
}

antlrcpp::Any UIRParser::ConstantContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitConstant(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::ConstantContext* UIRParser::constant() {
  ConstantContext *_localctx = _tracker.createInstance<ConstantContext>(_ctx, getState());
  enterRule(_localctx, 16, UIRParser::RuleConstant);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(297);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case UIRParser::NAME:
      case UIRParser::GLOBAL_NAME:
      case UIRParser::LOCAL_NAME: {
        enterOuterAlt(_localctx, 1);
        setState(291);
        dynamic_cast<ConstantContext *>(_localctx)->nam = globalName();
        break;
      }

      case UIRParser::T__5: {
        enterOuterAlt(_localctx, 2);
        setState(292);
        match(UIRParser::T__5);
        setState(293);
        dynamic_cast<ConstantContext *>(_localctx)->ty = type();
        setState(294);
        match(UIRParser::T__6);
        setState(295);
        constConstructor();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FuncContext ------------------------------------------------------------------

UIRParser::FuncContext::FuncContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

UIRParser::GlobalNameContext* UIRParser::FuncContext::globalName() {
  return getRuleContext<UIRParser::GlobalNameContext>(0);
}


size_t UIRParser::FuncContext::getRuleIndex() const {
  return UIRParser::RuleFunc;
}

antlrcpp::Any UIRParser::FuncContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitFunc(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::FuncContext* UIRParser::func() {
  FuncContext *_localctx = _tracker.createInstance<FuncContext>(_ctx, getState());
  enterRule(_localctx, 18, UIRParser::RuleFunc);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(299);
    dynamic_cast<FuncContext *>(_localctx)->nam = globalName();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- GlobalVarContext ------------------------------------------------------------------

UIRParser::GlobalVarContext::GlobalVarContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

UIRParser::GlobalNameContext* UIRParser::GlobalVarContext::globalName() {
  return getRuleContext<UIRParser::GlobalNameContext>(0);
}


size_t UIRParser::GlobalVarContext::getRuleIndex() const {
  return UIRParser::RuleGlobalVar;
}

antlrcpp::Any UIRParser::GlobalVarContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitGlobalVar(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::GlobalVarContext* UIRParser::globalVar() {
  GlobalVarContext *_localctx = _tracker.createInstance<GlobalVarContext>(_ctx, getState());
  enterRule(_localctx, 20, UIRParser::RuleGlobalVar);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(301);
    dynamic_cast<GlobalVarContext *>(_localctx)->nam = globalName();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BasicBlockContext ------------------------------------------------------------------

UIRParser::BasicBlockContext::BasicBlockContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<UIRParser::LocalNameContext *> UIRParser::BasicBlockContext::localName() {
  return getRuleContexts<UIRParser::LocalNameContext>();
}

UIRParser::LocalNameContext* UIRParser::BasicBlockContext::localName(size_t i) {
  return getRuleContext<UIRParser::LocalNameContext>(i);
}

std::vector<UIRParser::InstContext *> UIRParser::BasicBlockContext::inst() {
  return getRuleContexts<UIRParser::InstContext>();
}

UIRParser::InstContext* UIRParser::BasicBlockContext::inst(size_t i) {
  return getRuleContext<UIRParser::InstContext>(i);
}

std::vector<UIRParser::TypeContext *> UIRParser::BasicBlockContext::type() {
  return getRuleContexts<UIRParser::TypeContext>();
}

UIRParser::TypeContext* UIRParser::BasicBlockContext::type(size_t i) {
  return getRuleContext<UIRParser::TypeContext>(i);
}


size_t UIRParser::BasicBlockContext::getRuleIndex() const {
  return UIRParser::RuleBasicBlock;
}

antlrcpp::Any UIRParser::BasicBlockContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitBasicBlock(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::BasicBlockContext* UIRParser::basicBlock() {
  BasicBlockContext *_localctx = _tracker.createInstance<BasicBlockContext>(_ctx, getState());
  enterRule(_localctx, 22, UIRParser::RuleBasicBlock);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(303);
    dynamic_cast<BasicBlockContext *>(_localctx)->nam = localName();
    setState(304);
    match(UIRParser::T__33);
    setState(312);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == UIRParser::T__5) {
      setState(305);
      match(UIRParser::T__5);
      setState(306);
      dynamic_cast<BasicBlockContext *>(_localctx)->typeContext = type();
      dynamic_cast<BasicBlockContext *>(_localctx)->param_tys.push_back(dynamic_cast<BasicBlockContext *>(_localctx)->typeContext);
      setState(307);
      match(UIRParser::T__6);
      setState(308);
      dynamic_cast<BasicBlockContext *>(_localctx)->localNameContext = localName();
      dynamic_cast<BasicBlockContext *>(_localctx)->params.push_back(dynamic_cast<BasicBlockContext *>(_localctx)->localNameContext);
      setState(314);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(315);
    match(UIRParser::T__34);
    setState(320);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == UIRParser::T__38) {
      setState(316);
      match(UIRParser::T__38);
      setState(317);
      dynamic_cast<BasicBlockContext *>(_localctx)->exc = localName();
      setState(318);
      match(UIRParser::T__39);
    }
    setState(322);
    match(UIRParser::T__40);
    setState(324); 
    _errHandler->sync(this);
    alt = 1;
    do {
      switch (alt) {
        case 1: {
              setState(323);
              inst();
              break;
            }

      default:
        throw NoViableAltException(this);
      }
      setState(326); 
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 19, _ctx);
    } while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- InstNameContext ------------------------------------------------------------------

UIRParser::InstNameContext::InstNameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

UIRParser::LocalNameContext* UIRParser::InstNameContext::localName() {
  return getRuleContext<UIRParser::LocalNameContext>(0);
}


size_t UIRParser::InstNameContext::getRuleIndex() const {
  return UIRParser::RuleInstName;
}

antlrcpp::Any UIRParser::InstNameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstName(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::InstNameContext* UIRParser::instName() {
  InstNameContext *_localctx = _tracker.createInstance<InstNameContext>(_ctx, getState());
  enterRule(_localctx, 24, UIRParser::RuleInstName);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(332);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == UIRParser::T__38) {
      setState(328);
      match(UIRParser::T__38);
      setState(329);
      dynamic_cast<InstNameContext *>(_localctx)->nam = localName();
      setState(330);
      match(UIRParser::T__39);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- InstResultContext ------------------------------------------------------------------

UIRParser::InstResultContext::InstResultContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

UIRParser::LocalNameContext* UIRParser::InstResultContext::localName() {
  return getRuleContext<UIRParser::LocalNameContext>(0);
}


size_t UIRParser::InstResultContext::getRuleIndex() const {
  return UIRParser::RuleInstResult;
}

antlrcpp::Any UIRParser::InstResultContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstResult(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::InstResultContext* UIRParser::instResult() {
  InstResultContext *_localctx = _tracker.createInstance<InstResultContext>(_ctx, getState());
  enterRule(_localctx, 26, UIRParser::RuleInstResult);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(334);
    dynamic_cast<InstResultContext *>(_localctx)->result = localName();
    setState(335);
    match(UIRParser::T__2);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- InstResultsContext ------------------------------------------------------------------

UIRParser::InstResultsContext::InstResultsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<UIRParser::LocalNameContext *> UIRParser::InstResultsContext::localName() {
  return getRuleContexts<UIRParser::LocalNameContext>();
}

UIRParser::LocalNameContext* UIRParser::InstResultsContext::localName(size_t i) {
  return getRuleContext<UIRParser::LocalNameContext>(i);
}


size_t UIRParser::InstResultsContext::getRuleIndex() const {
  return UIRParser::RuleInstResults;
}

antlrcpp::Any UIRParser::InstResultsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstResults(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::InstResultsContext* UIRParser::instResults() {
  InstResultsContext *_localctx = _tracker.createInstance<InstResultsContext>(_ctx, getState());
  enterRule(_localctx, 28, UIRParser::RuleInstResults);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(350);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case UIRParser::NAME:
      case UIRParser::GLOBAL_NAME:
      case UIRParser::LOCAL_NAME: {
        enterOuterAlt(_localctx, 1);
        setState(337);
        dynamic_cast<InstResultsContext *>(_localctx)->localNameContext = localName();
        dynamic_cast<InstResultsContext *>(_localctx)->results.push_back(dynamic_cast<InstResultsContext *>(_localctx)->localNameContext);
        setState(338);
        match(UIRParser::T__2);
        break;
      }

      case UIRParser::T__33: {
        enterOuterAlt(_localctx, 2);
        setState(340);
        match(UIRParser::T__33);
        setState(344);
        _errHandler->sync(this);
        _la = _input->LA(1);
        while (((((_la - 172) & ~ 0x3fULL) == 0) &&
          ((1ULL << (_la - 172)) & ((1ULL << (UIRParser::NAME - 172))
          | (1ULL << (UIRParser::GLOBAL_NAME - 172))
          | (1ULL << (UIRParser::LOCAL_NAME - 172)))) != 0)) {
          setState(341);
          dynamic_cast<InstResultsContext *>(_localctx)->localNameContext = localName();
          dynamic_cast<InstResultsContext *>(_localctx)->results.push_back(dynamic_cast<InstResultsContext *>(_localctx)->localNameContext);
          setState(346);
          _errHandler->sync(this);
          _la = _input->LA(1);
        }
        setState(347);
        match(UIRParser::T__34);
        setState(348);
        match(UIRParser::T__2);
        break;
      }

      case UIRParser::T__38:
      case UIRParser::T__45:
      case UIRParser::T__70:
      case UIRParser::T__71:
      case UIRParser::T__74:
      case UIRParser::T__77:
      case UIRParser::T__78: {
        enterOuterAlt(_localctx, 3);

        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PairResultContext ------------------------------------------------------------------

UIRParser::PairResultContext::PairResultContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<UIRParser::LocalNameContext *> UIRParser::PairResultContext::localName() {
  return getRuleContexts<UIRParser::LocalNameContext>();
}

UIRParser::LocalNameContext* UIRParser::PairResultContext::localName(size_t i) {
  return getRuleContext<UIRParser::LocalNameContext>(i);
}


size_t UIRParser::PairResultContext::getRuleIndex() const {
  return UIRParser::RulePairResult;
}

antlrcpp::Any UIRParser::PairResultContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitPairResult(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::PairResultContext* UIRParser::pairResult() {
  PairResultContext *_localctx = _tracker.createInstance<PairResultContext>(_ctx, getState());
  enterRule(_localctx, 30, UIRParser::RulePairResult);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(352);
    match(UIRParser::T__33);
    setState(353);
    dynamic_cast<PairResultContext *>(_localctx)->first = localName();
    setState(354);
    dynamic_cast<PairResultContext *>(_localctx)->second = localName();
    setState(355);
    match(UIRParser::T__34);
    setState(356);
    match(UIRParser::T__2);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PairResultsContext ------------------------------------------------------------------

UIRParser::PairResultsContext::PairResultsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<UIRParser::LocalNameContext *> UIRParser::PairResultsContext::localName() {
  return getRuleContexts<UIRParser::LocalNameContext>();
}

UIRParser::LocalNameContext* UIRParser::PairResultsContext::localName(size_t i) {
  return getRuleContext<UIRParser::LocalNameContext>(i);
}


size_t UIRParser::PairResultsContext::getRuleIndex() const {
  return UIRParser::RulePairResults;
}

antlrcpp::Any UIRParser::PairResultsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitPairResults(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::PairResultsContext* UIRParser::pairResults() {
  PairResultsContext *_localctx = _tracker.createInstance<PairResultsContext>(_ctx, getState());
  enterRule(_localctx, 32, UIRParser::RulePairResults);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(371);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case UIRParser::NAME:
      case UIRParser::GLOBAL_NAME:
      case UIRParser::LOCAL_NAME: {
        enterOuterAlt(_localctx, 1);
        setState(358);
        dynamic_cast<PairResultsContext *>(_localctx)->first = localName();
        setState(359);
        match(UIRParser::T__2);
        break;
      }

      case UIRParser::T__33: {
        enterOuterAlt(_localctx, 2);
        setState(361);
        match(UIRParser::T__33);
        setState(362);
        dynamic_cast<PairResultsContext *>(_localctx)->first = localName();
        setState(364); 
        _errHandler->sync(this);
        _la = _input->LA(1);
        do {
          setState(363);
          dynamic_cast<PairResultsContext *>(_localctx)->localNameContext = localName();
          dynamic_cast<PairResultsContext *>(_localctx)->second.push_back(dynamic_cast<PairResultsContext *>(_localctx)->localNameContext);
          setState(366); 
          _errHandler->sync(this);
          _la = _input->LA(1);
        } while (((((_la - 172) & ~ 0x3fULL) == 0) &&
          ((1ULL << (_la - 172)) & ((1ULL << (UIRParser::NAME - 172))
          | (1ULL << (UIRParser::GLOBAL_NAME - 172))
          | (1ULL << (UIRParser::LOCAL_NAME - 172)))) != 0));
        setState(368);
        match(UIRParser::T__34);
        setState(369);
        match(UIRParser::T__2);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- InstContext ------------------------------------------------------------------

UIRParser::InstContext::InstContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t UIRParser::InstContext::getRuleIndex() const {
  return UIRParser::RuleInst;
}

void UIRParser::InstContext::copyFrom(InstContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- InstExtractElementContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstExtractElementContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstExtractElementContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

std::vector<UIRParser::TypeContext *> UIRParser::InstExtractElementContext::type() {
  return getRuleContexts<UIRParser::TypeContext>();
}

UIRParser::TypeContext* UIRParser::InstExtractElementContext::type(size_t i) {
  return getRuleContext<UIRParser::TypeContext>(i);
}

std::vector<UIRParser::ValueContext *> UIRParser::InstExtractElementContext::value() {
  return getRuleContexts<UIRParser::ValueContext>();
}

UIRParser::ValueContext* UIRParser::InstExtractElementContext::value(size_t i) {
  return getRuleContext<UIRParser::ValueContext>(i);
}

UIRParser::InstExtractElementContext::InstExtractElementContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstExtractElementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstExtractElement(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstFenceContext ------------------------------------------------------------------

UIRParser::InstNameContext* UIRParser::InstFenceContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::MemordContext* UIRParser::InstFenceContext::memord() {
  return getRuleContext<UIRParser::MemordContext>(0);
}

UIRParser::InstFenceContext::InstFenceContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstFenceContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstFence(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstWatchPointContext ------------------------------------------------------------------

UIRParser::InstResultsContext* UIRParser::InstWatchPointContext::instResults() {
  return getRuleContext<UIRParser::InstResultsContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstWatchPointContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::WpidContext* UIRParser::InstWatchPointContext::wpid() {
  return getRuleContext<UIRParser::WpidContext>(0);
}

UIRParser::TypeListContext* UIRParser::InstWatchPointContext::typeList() {
  return getRuleContext<UIRParser::TypeListContext>(0);
}

std::vector<UIRParser::DestClauseContext *> UIRParser::InstWatchPointContext::destClause() {
  return getRuleContexts<UIRParser::DestClauseContext>();
}

UIRParser::DestClauseContext* UIRParser::InstWatchPointContext::destClause(size_t i) {
  return getRuleContext<UIRParser::DestClauseContext>(i);
}

UIRParser::KeepaliveClauseContext* UIRParser::InstWatchPointContext::keepaliveClause() {
  return getRuleContext<UIRParser::KeepaliveClauseContext>(0);
}

UIRParser::InstWatchPointContext::InstWatchPointContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstWatchPointContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstWatchPoint(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstCallContext ------------------------------------------------------------------

UIRParser::InstResultsContext* UIRParser::InstCallContext::instResults() {
  return getRuleContext<UIRParser::InstResultsContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstCallContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::FuncSigContext* UIRParser::InstCallContext::funcSig() {
  return getRuleContext<UIRParser::FuncSigContext>(0);
}

UIRParser::ArgListContext* UIRParser::InstCallContext::argList() {
  return getRuleContext<UIRParser::ArgListContext>(0);
}

UIRParser::ValueContext* UIRParser::InstCallContext::value() {
  return getRuleContext<UIRParser::ValueContext>(0);
}

UIRParser::ExcClauseContext* UIRParser::InstCallContext::excClause() {
  return getRuleContext<UIRParser::ExcClauseContext>(0);
}

UIRParser::KeepaliveClauseContext* UIRParser::InstCallContext::keepaliveClause() {
  return getRuleContext<UIRParser::KeepaliveClauseContext>(0);
}

UIRParser::InstCallContext::InstCallContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstCallContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstCall(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstInsertValueContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstInsertValueContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstInsertValueContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::IntParamContext* UIRParser::InstInsertValueContext::intParam() {
  return getRuleContext<UIRParser::IntParamContext>(0);
}

UIRParser::TypeContext* UIRParser::InstInsertValueContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

std::vector<UIRParser::ValueContext *> UIRParser::InstInsertValueContext::value() {
  return getRuleContexts<UIRParser::ValueContext>();
}

UIRParser::ValueContext* UIRParser::InstInsertValueContext::value(size_t i) {
  return getRuleContext<UIRParser::ValueContext>(i);
}

UIRParser::InstInsertValueContext::InstInsertValueContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstInsertValueContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstInsertValue(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstConversionContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstConversionContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstConversionContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::ConvopContext* UIRParser::InstConversionContext::convop() {
  return getRuleContext<UIRParser::ConvopContext>(0);
}

std::vector<UIRParser::TypeContext *> UIRParser::InstConversionContext::type() {
  return getRuleContexts<UIRParser::TypeContext>();
}

UIRParser::TypeContext* UIRParser::InstConversionContext::type(size_t i) {
  return getRuleContext<UIRParser::TypeContext>(i);
}

UIRParser::ValueContext* UIRParser::InstConversionContext::value() {
  return getRuleContext<UIRParser::ValueContext>(0);
}

UIRParser::InstConversionContext::InstConversionContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstConversionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstConversion(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstSwitchContext ------------------------------------------------------------------

UIRParser::InstNameContext* UIRParser::InstSwitchContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::TypeContext* UIRParser::InstSwitchContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

UIRParser::ValueContext* UIRParser::InstSwitchContext::value() {
  return getRuleContext<UIRParser::ValueContext>(0);
}

std::vector<UIRParser::DestClauseContext *> UIRParser::InstSwitchContext::destClause() {
  return getRuleContexts<UIRParser::DestClauseContext>();
}

UIRParser::DestClauseContext* UIRParser::InstSwitchContext::destClause(size_t i) {
  return getRuleContext<UIRParser::DestClauseContext>(i);
}

std::vector<UIRParser::ConstantContext *> UIRParser::InstSwitchContext::constant() {
  return getRuleContexts<UIRParser::ConstantContext>();
}

UIRParser::ConstantContext* UIRParser::InstSwitchContext::constant(size_t i) {
  return getRuleContext<UIRParser::ConstantContext>(i);
}

UIRParser::InstSwitchContext::InstSwitchContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstSwitchContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstSwitch(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstBinOpStatusContext ------------------------------------------------------------------

UIRParser::PairResultsContext* UIRParser::InstBinOpStatusContext::pairResults() {
  return getRuleContext<UIRParser::PairResultsContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstBinOpStatusContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::BinopContext* UIRParser::InstBinOpStatusContext::binop() {
  return getRuleContext<UIRParser::BinopContext>(0);
}

UIRParser::BinopStatusContext* UIRParser::InstBinOpStatusContext::binopStatus() {
  return getRuleContext<UIRParser::BinopStatusContext>(0);
}

UIRParser::TypeContext* UIRParser::InstBinOpStatusContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

std::vector<UIRParser::ValueContext *> UIRParser::InstBinOpStatusContext::value() {
  return getRuleContexts<UIRParser::ValueContext>();
}

UIRParser::ValueContext* UIRParser::InstBinOpStatusContext::value(size_t i) {
  return getRuleContext<UIRParser::ValueContext>(i);
}

UIRParser::ExcClauseContext* UIRParser::InstBinOpStatusContext::excClause() {
  return getRuleContext<UIRParser::ExcClauseContext>(0);
}

UIRParser::InstBinOpStatusContext::InstBinOpStatusContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstBinOpStatusContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstBinOpStatus(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstLoadContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstLoadContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstLoadContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::TypeContext* UIRParser::InstLoadContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

UIRParser::ValueContext* UIRParser::InstLoadContext::value() {
  return getRuleContext<UIRParser::ValueContext>(0);
}

UIRParser::MemordContext* UIRParser::InstLoadContext::memord() {
  return getRuleContext<UIRParser::MemordContext>(0);
}

UIRParser::ExcClauseContext* UIRParser::InstLoadContext::excClause() {
  return getRuleContext<UIRParser::ExcClauseContext>(0);
}

UIRParser::InstLoadContext::InstLoadContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstLoadContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstLoad(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstAtomicRMWContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstAtomicRMWContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstAtomicRMWContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::MemordContext* UIRParser::InstAtomicRMWContext::memord() {
  return getRuleContext<UIRParser::MemordContext>(0);
}

UIRParser::AtomicrmwopContext* UIRParser::InstAtomicRMWContext::atomicrmwop() {
  return getRuleContext<UIRParser::AtomicrmwopContext>(0);
}

UIRParser::TypeContext* UIRParser::InstAtomicRMWContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

std::vector<UIRParser::ValueContext *> UIRParser::InstAtomicRMWContext::value() {
  return getRuleContexts<UIRParser::ValueContext>();
}

UIRParser::ValueContext* UIRParser::InstAtomicRMWContext::value(size_t i) {
  return getRuleContext<UIRParser::ValueContext>(i);
}

UIRParser::ExcClauseContext* UIRParser::InstAtomicRMWContext::excClause() {
  return getRuleContext<UIRParser::ExcClauseContext>(0);
}

UIRParser::InstAtomicRMWContext::InstAtomicRMWContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstAtomicRMWContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstAtomicRMW(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstAllocaHybridContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstAllocaHybridContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstAllocaHybridContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

std::vector<UIRParser::TypeContext *> UIRParser::InstAllocaHybridContext::type() {
  return getRuleContexts<UIRParser::TypeContext>();
}

UIRParser::TypeContext* UIRParser::InstAllocaHybridContext::type(size_t i) {
  return getRuleContext<UIRParser::TypeContext>(i);
}

UIRParser::ValueContext* UIRParser::InstAllocaHybridContext::value() {
  return getRuleContext<UIRParser::ValueContext>(0);
}

UIRParser::ExcClauseContext* UIRParser::InstAllocaHybridContext::excClause() {
  return getRuleContext<UIRParser::ExcClauseContext>(0);
}

UIRParser::InstAllocaHybridContext::InstAllocaHybridContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstAllocaHybridContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstAllocaHybrid(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstInsertElementContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstInsertElementContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstInsertElementContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

std::vector<UIRParser::TypeContext *> UIRParser::InstInsertElementContext::type() {
  return getRuleContexts<UIRParser::TypeContext>();
}

UIRParser::TypeContext* UIRParser::InstInsertElementContext::type(size_t i) {
  return getRuleContext<UIRParser::TypeContext>(i);
}

std::vector<UIRParser::ValueContext *> UIRParser::InstInsertElementContext::value() {
  return getRuleContexts<UIRParser::ValueContext>();
}

UIRParser::ValueContext* UIRParser::InstInsertElementContext::value(size_t i) {
  return getRuleContext<UIRParser::ValueContext>(i);
}

UIRParser::InstInsertElementContext::InstInsertElementContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstInsertElementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstInsertElement(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstBranchContext ------------------------------------------------------------------

UIRParser::InstNameContext* UIRParser::InstBranchContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::DestClauseContext* UIRParser::InstBranchContext::destClause() {
  return getRuleContext<UIRParser::DestClauseContext>(0);
}

UIRParser::InstBranchContext::InstBranchContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstBranchContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstBranch(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstGetVarPartIRefContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstGetVarPartIRefContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstGetVarPartIRefContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::TypeContext* UIRParser::InstGetVarPartIRefContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

UIRParser::ValueContext* UIRParser::InstGetVarPartIRefContext::value() {
  return getRuleContext<UIRParser::ValueContext>(0);
}

UIRParser::InstGetVarPartIRefContext::InstGetVarPartIRefContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstGetVarPartIRefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstGetVarPartIRef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstThrowContext ------------------------------------------------------------------

UIRParser::InstNameContext* UIRParser::InstThrowContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::ValueContext* UIRParser::InstThrowContext::value() {
  return getRuleContext<UIRParser::ValueContext>(0);
}

UIRParser::InstThrowContext::InstThrowContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstThrowContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstThrow(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstTrapContext ------------------------------------------------------------------

UIRParser::InstResultsContext* UIRParser::InstTrapContext::instResults() {
  return getRuleContext<UIRParser::InstResultsContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstTrapContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::TypeListContext* UIRParser::InstTrapContext::typeList() {
  return getRuleContext<UIRParser::TypeListContext>(0);
}

UIRParser::KeepaliveClauseContext* UIRParser::InstTrapContext::keepaliveClause() {
  return getRuleContext<UIRParser::KeepaliveClauseContext>(0);
}

UIRParser::ExcClauseContext* UIRParser::InstTrapContext::excClause() {
  return getRuleContext<UIRParser::ExcClauseContext>(0);
}

UIRParser::InstTrapContext::InstTrapContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstTrapContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstTrap(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstExtractValueContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstExtractValueContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstExtractValueContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::IntParamContext* UIRParser::InstExtractValueContext::intParam() {
  return getRuleContext<UIRParser::IntParamContext>(0);
}

UIRParser::TypeContext* UIRParser::InstExtractValueContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

UIRParser::ValueContext* UIRParser::InstExtractValueContext::value() {
  return getRuleContext<UIRParser::ValueContext>(0);
}

UIRParser::InstExtractValueContext::InstExtractValueContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstExtractValueContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstExtractValue(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstSwapStackContext ------------------------------------------------------------------

UIRParser::InstResultsContext* UIRParser::InstSwapStackContext::instResults() {
  return getRuleContext<UIRParser::InstResultsContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstSwapStackContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::CurStackClauseContext* UIRParser::InstSwapStackContext::curStackClause() {
  return getRuleContext<UIRParser::CurStackClauseContext>(0);
}

UIRParser::NewStackClauseContext* UIRParser::InstSwapStackContext::newStackClause() {
  return getRuleContext<UIRParser::NewStackClauseContext>(0);
}

UIRParser::ValueContext* UIRParser::InstSwapStackContext::value() {
  return getRuleContext<UIRParser::ValueContext>(0);
}

UIRParser::ExcClauseContext* UIRParser::InstSwapStackContext::excClause() {
  return getRuleContext<UIRParser::ExcClauseContext>(0);
}

UIRParser::KeepaliveClauseContext* UIRParser::InstSwapStackContext::keepaliveClause() {
  return getRuleContext<UIRParser::KeepaliveClauseContext>(0);
}

UIRParser::InstSwapStackContext::InstSwapStackContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstSwapStackContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstSwapStack(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstTailCallContext ------------------------------------------------------------------

UIRParser::InstNameContext* UIRParser::InstTailCallContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::FuncSigContext* UIRParser::InstTailCallContext::funcSig() {
  return getRuleContext<UIRParser::FuncSigContext>(0);
}

UIRParser::ArgListContext* UIRParser::InstTailCallContext::argList() {
  return getRuleContext<UIRParser::ArgListContext>(0);
}

UIRParser::ValueContext* UIRParser::InstTailCallContext::value() {
  return getRuleContext<UIRParser::ValueContext>(0);
}

UIRParser::InstTailCallContext::InstTailCallContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstTailCallContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstTailCall(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstGetIRefContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstGetIRefContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstGetIRefContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::TypeContext* UIRParser::InstGetIRefContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

UIRParser::ValueContext* UIRParser::InstGetIRefContext::value() {
  return getRuleContext<UIRParser::ValueContext>(0);
}

UIRParser::InstGetIRefContext::InstGetIRefContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstGetIRefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstGetIRef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstShuffleVectorContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstShuffleVectorContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstShuffleVectorContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

std::vector<UIRParser::TypeContext *> UIRParser::InstShuffleVectorContext::type() {
  return getRuleContexts<UIRParser::TypeContext>();
}

UIRParser::TypeContext* UIRParser::InstShuffleVectorContext::type(size_t i) {
  return getRuleContext<UIRParser::TypeContext>(i);
}

std::vector<UIRParser::ValueContext *> UIRParser::InstShuffleVectorContext::value() {
  return getRuleContexts<UIRParser::ValueContext>();
}

UIRParser::ValueContext* UIRParser::InstShuffleVectorContext::value(size_t i) {
  return getRuleContext<UIRParser::ValueContext>(i);
}

UIRParser::InstShuffleVectorContext::InstShuffleVectorContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstShuffleVectorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstShuffleVector(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstCmpContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstCmpContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstCmpContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::CmpopContext* UIRParser::InstCmpContext::cmpop() {
  return getRuleContext<UIRParser::CmpopContext>(0);
}

UIRParser::TypeContext* UIRParser::InstCmpContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

std::vector<UIRParser::ValueContext *> UIRParser::InstCmpContext::value() {
  return getRuleContexts<UIRParser::ValueContext>();
}

UIRParser::ValueContext* UIRParser::InstCmpContext::value(size_t i) {
  return getRuleContext<UIRParser::ValueContext>(i);
}

UIRParser::InstCmpContext::InstCmpContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstCmpContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstCmp(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstShiftIRefContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstShiftIRefContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstShiftIRefContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

std::vector<UIRParser::TypeContext *> UIRParser::InstShiftIRefContext::type() {
  return getRuleContexts<UIRParser::TypeContext>();
}

UIRParser::TypeContext* UIRParser::InstShiftIRefContext::type(size_t i) {
  return getRuleContext<UIRParser::TypeContext>(i);
}

std::vector<UIRParser::ValueContext *> UIRParser::InstShiftIRefContext::value() {
  return getRuleContexts<UIRParser::ValueContext>();
}

UIRParser::ValueContext* UIRParser::InstShiftIRefContext::value(size_t i) {
  return getRuleContext<UIRParser::ValueContext>(i);
}

UIRParser::InstShiftIRefContext::InstShiftIRefContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstShiftIRefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstShiftIRef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstCCallContext ------------------------------------------------------------------

UIRParser::InstResultsContext* UIRParser::InstCCallContext::instResults() {
  return getRuleContext<UIRParser::InstResultsContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstCCallContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::CallConvContext* UIRParser::InstCCallContext::callConv() {
  return getRuleContext<UIRParser::CallConvContext>(0);
}

UIRParser::FuncSigContext* UIRParser::InstCCallContext::funcSig() {
  return getRuleContext<UIRParser::FuncSigContext>(0);
}

UIRParser::ArgListContext* UIRParser::InstCCallContext::argList() {
  return getRuleContext<UIRParser::ArgListContext>(0);
}

UIRParser::TypeContext* UIRParser::InstCCallContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

UIRParser::ValueContext* UIRParser::InstCCallContext::value() {
  return getRuleContext<UIRParser::ValueContext>(0);
}

UIRParser::ExcClauseContext* UIRParser::InstCCallContext::excClause() {
  return getRuleContext<UIRParser::ExcClauseContext>(0);
}

UIRParser::KeepaliveClauseContext* UIRParser::InstCCallContext::keepaliveClause() {
  return getRuleContext<UIRParser::KeepaliveClauseContext>(0);
}

UIRParser::InstCCallContext::InstCCallContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstCCallContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstCCall(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstNewHybridContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstNewHybridContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstNewHybridContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

std::vector<UIRParser::TypeContext *> UIRParser::InstNewHybridContext::type() {
  return getRuleContexts<UIRParser::TypeContext>();
}

UIRParser::TypeContext* UIRParser::InstNewHybridContext::type(size_t i) {
  return getRuleContext<UIRParser::TypeContext>(i);
}

UIRParser::ValueContext* UIRParser::InstNewHybridContext::value() {
  return getRuleContext<UIRParser::ValueContext>(0);
}

UIRParser::ExcClauseContext* UIRParser::InstNewHybridContext::excClause() {
  return getRuleContext<UIRParser::ExcClauseContext>(0);
}

UIRParser::InstNewHybridContext::InstNewHybridContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstNewHybridContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstNewHybrid(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstGetFieldIRefContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstGetFieldIRefContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstGetFieldIRefContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::TypeContext* UIRParser::InstGetFieldIRefContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

UIRParser::IntParamContext* UIRParser::InstGetFieldIRefContext::intParam() {
  return getRuleContext<UIRParser::IntParamContext>(0);
}

UIRParser::ValueContext* UIRParser::InstGetFieldIRefContext::value() {
  return getRuleContext<UIRParser::ValueContext>(0);
}

UIRParser::InstGetFieldIRefContext::InstGetFieldIRefContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstGetFieldIRefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstGetFieldIRef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstCommInstContext ------------------------------------------------------------------

UIRParser::InstResultsContext* UIRParser::InstCommInstContext::instResults() {
  return getRuleContext<UIRParser::InstResultsContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstCommInstContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::CommInstContext* UIRParser::InstCommInstContext::commInst() {
  return getRuleContext<UIRParser::CommInstContext>(0);
}

UIRParser::ExcClauseContext* UIRParser::InstCommInstContext::excClause() {
  return getRuleContext<UIRParser::ExcClauseContext>(0);
}

UIRParser::KeepaliveClauseContext* UIRParser::InstCommInstContext::keepaliveClause() {
  return getRuleContext<UIRParser::KeepaliveClauseContext>(0);
}

std::vector<UIRParser::FlagContext *> UIRParser::InstCommInstContext::flag() {
  return getRuleContexts<UIRParser::FlagContext>();
}

UIRParser::FlagContext* UIRParser::InstCommInstContext::flag(size_t i) {
  return getRuleContext<UIRParser::FlagContext>(i);
}

std::vector<UIRParser::TypeContext *> UIRParser::InstCommInstContext::type() {
  return getRuleContexts<UIRParser::TypeContext>();
}

UIRParser::TypeContext* UIRParser::InstCommInstContext::type(size_t i) {
  return getRuleContext<UIRParser::TypeContext>(i);
}

std::vector<UIRParser::FuncSigContext *> UIRParser::InstCommInstContext::funcSig() {
  return getRuleContexts<UIRParser::FuncSigContext>();
}

UIRParser::FuncSigContext* UIRParser::InstCommInstContext::funcSig(size_t i) {
  return getRuleContext<UIRParser::FuncSigContext>(i);
}

std::vector<UIRParser::ValueContext *> UIRParser::InstCommInstContext::value() {
  return getRuleContexts<UIRParser::ValueContext>();
}

UIRParser::ValueContext* UIRParser::InstCommInstContext::value(size_t i) {
  return getRuleContext<UIRParser::ValueContext>(i);
}

UIRParser::InstCommInstContext::InstCommInstContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstCommInstContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstCommInst(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstCmpXchgContext ------------------------------------------------------------------

UIRParser::PairResultContext* UIRParser::InstCmpXchgContext::pairResult() {
  return getRuleContext<UIRParser::PairResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstCmpXchgContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::TypeContext* UIRParser::InstCmpXchgContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

std::vector<UIRParser::MemordContext *> UIRParser::InstCmpXchgContext::memord() {
  return getRuleContexts<UIRParser::MemordContext>();
}

UIRParser::MemordContext* UIRParser::InstCmpXchgContext::memord(size_t i) {
  return getRuleContext<UIRParser::MemordContext>(i);
}

std::vector<UIRParser::ValueContext *> UIRParser::InstCmpXchgContext::value() {
  return getRuleContexts<UIRParser::ValueContext>();
}

UIRParser::ValueContext* UIRParser::InstCmpXchgContext::value(size_t i) {
  return getRuleContext<UIRParser::ValueContext>(i);
}

UIRParser::ExcClauseContext* UIRParser::InstCmpXchgContext::excClause() {
  return getRuleContext<UIRParser::ExcClauseContext>(0);
}

UIRParser::InstCmpXchgContext::InstCmpXchgContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstCmpXchgContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstCmpXchg(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstAllocaContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstAllocaContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstAllocaContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::TypeContext* UIRParser::InstAllocaContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

UIRParser::ExcClauseContext* UIRParser::InstAllocaContext::excClause() {
  return getRuleContext<UIRParser::ExcClauseContext>(0);
}

UIRParser::InstAllocaContext::InstAllocaContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstAllocaContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstAlloca(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstStoreContext ------------------------------------------------------------------

UIRParser::InstNameContext* UIRParser::InstStoreContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::TypeContext* UIRParser::InstStoreContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

std::vector<UIRParser::ValueContext *> UIRParser::InstStoreContext::value() {
  return getRuleContexts<UIRParser::ValueContext>();
}

UIRParser::ValueContext* UIRParser::InstStoreContext::value(size_t i) {
  return getRuleContext<UIRParser::ValueContext>(i);
}

UIRParser::MemordContext* UIRParser::InstStoreContext::memord() {
  return getRuleContext<UIRParser::MemordContext>(0);
}

UIRParser::ExcClauseContext* UIRParser::InstStoreContext::excClause() {
  return getRuleContext<UIRParser::ExcClauseContext>(0);
}

UIRParser::InstStoreContext::InstStoreContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstStoreContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstStore(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstBranch2Context ------------------------------------------------------------------

UIRParser::InstNameContext* UIRParser::InstBranch2Context::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::ValueContext* UIRParser::InstBranch2Context::value() {
  return getRuleContext<UIRParser::ValueContext>(0);
}

std::vector<UIRParser::DestClauseContext *> UIRParser::InstBranch2Context::destClause() {
  return getRuleContexts<UIRParser::DestClauseContext>();
}

UIRParser::DestClauseContext* UIRParser::InstBranch2Context::destClause(size_t i) {
  return getRuleContext<UIRParser::DestClauseContext>(i);
}

UIRParser::InstBranch2Context::InstBranch2Context(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstBranch2Context::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstBranch2(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstBinOpContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstBinOpContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstBinOpContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::BinopContext* UIRParser::InstBinOpContext::binop() {
  return getRuleContext<UIRParser::BinopContext>(0);
}

UIRParser::TypeContext* UIRParser::InstBinOpContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

std::vector<UIRParser::ValueContext *> UIRParser::InstBinOpContext::value() {
  return getRuleContexts<UIRParser::ValueContext>();
}

UIRParser::ValueContext* UIRParser::InstBinOpContext::value(size_t i) {
  return getRuleContext<UIRParser::ValueContext>(i);
}

UIRParser::ExcClauseContext* UIRParser::InstBinOpContext::excClause() {
  return getRuleContext<UIRParser::ExcClauseContext>(0);
}

UIRParser::InstBinOpContext::InstBinOpContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstBinOpContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstBinOp(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstSelectContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstSelectContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstSelectContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

std::vector<UIRParser::TypeContext *> UIRParser::InstSelectContext::type() {
  return getRuleContexts<UIRParser::TypeContext>();
}

UIRParser::TypeContext* UIRParser::InstSelectContext::type(size_t i) {
  return getRuleContext<UIRParser::TypeContext>(i);
}

std::vector<UIRParser::ValueContext *> UIRParser::InstSelectContext::value() {
  return getRuleContexts<UIRParser::ValueContext>();
}

UIRParser::ValueContext* UIRParser::InstSelectContext::value(size_t i) {
  return getRuleContext<UIRParser::ValueContext>(i);
}

UIRParser::InstSelectContext::InstSelectContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstSelectContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstSelect(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstNewThreadContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstNewThreadContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstNewThreadContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::NewStackClauseContext* UIRParser::InstNewThreadContext::newStackClause() {
  return getRuleContext<UIRParser::NewStackClauseContext>(0);
}

std::vector<UIRParser::ValueContext *> UIRParser::InstNewThreadContext::value() {
  return getRuleContexts<UIRParser::ValueContext>();
}

UIRParser::ValueContext* UIRParser::InstNewThreadContext::value(size_t i) {
  return getRuleContext<UIRParser::ValueContext>(i);
}

UIRParser::ExcClauseContext* UIRParser::InstNewThreadContext::excClause() {
  return getRuleContext<UIRParser::ExcClauseContext>(0);
}

UIRParser::InstNewThreadContext::InstNewThreadContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstNewThreadContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstNewThread(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstWPBranchContext ------------------------------------------------------------------

UIRParser::InstNameContext* UIRParser::InstWPBranchContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::WpidContext* UIRParser::InstWPBranchContext::wpid() {
  return getRuleContext<UIRParser::WpidContext>(0);
}

std::vector<UIRParser::DestClauseContext *> UIRParser::InstWPBranchContext::destClause() {
  return getRuleContexts<UIRParser::DestClauseContext>();
}

UIRParser::DestClauseContext* UIRParser::InstWPBranchContext::destClause(size_t i) {
  return getRuleContext<UIRParser::DestClauseContext>(i);
}

UIRParser::InstWPBranchContext::InstWPBranchContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstWPBranchContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstWPBranch(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstGetElemIRefContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstGetElemIRefContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstGetElemIRefContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

std::vector<UIRParser::TypeContext *> UIRParser::InstGetElemIRefContext::type() {
  return getRuleContexts<UIRParser::TypeContext>();
}

UIRParser::TypeContext* UIRParser::InstGetElemIRefContext::type(size_t i) {
  return getRuleContext<UIRParser::TypeContext>(i);
}

std::vector<UIRParser::ValueContext *> UIRParser::InstGetElemIRefContext::value() {
  return getRuleContexts<UIRParser::ValueContext>();
}

UIRParser::ValueContext* UIRParser::InstGetElemIRefContext::value(size_t i) {
  return getRuleContext<UIRParser::ValueContext>(i);
}

UIRParser::InstGetElemIRefContext::InstGetElemIRefContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstGetElemIRefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstGetElemIRef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstNewContext ------------------------------------------------------------------

UIRParser::InstResultContext* UIRParser::InstNewContext::instResult() {
  return getRuleContext<UIRParser::InstResultContext>(0);
}

UIRParser::InstNameContext* UIRParser::InstNewContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::TypeContext* UIRParser::InstNewContext::type() {
  return getRuleContext<UIRParser::TypeContext>(0);
}

UIRParser::ExcClauseContext* UIRParser::InstNewContext::excClause() {
  return getRuleContext<UIRParser::ExcClauseContext>(0);
}

UIRParser::InstNewContext::InstNewContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstNewContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstNew(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstRetContext ------------------------------------------------------------------

UIRParser::InstNameContext* UIRParser::InstRetContext::instName() {
  return getRuleContext<UIRParser::InstNameContext>(0);
}

UIRParser::RetValsContext* UIRParser::InstRetContext::retVals() {
  return getRuleContext<UIRParser::RetValsContext>(0);
}

UIRParser::InstRetContext::InstRetContext(InstContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::InstRetContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitInstRet(this);
  else
    return visitor->visitChildren(this);
}
UIRParser::InstContext* UIRParser::inst() {
  InstContext *_localctx = _tracker.createInstance<InstContext>(_ctx, getState());
  enterRule(_localctx, 34, UIRParser::RuleInst);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(828);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 68, _ctx)) {
    case 1: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstBinOpContext>(_localctx));
      enterOuterAlt(_localctx, 1);
      setState(373);
      instResult();
      setState(374);
      instName();
      setState(375);
      binop();
      setState(376);
      match(UIRParser::T__5);
      setState(377);
      dynamic_cast<InstBinOpContext *>(_localctx)->ty = type();
      setState(378);
      match(UIRParser::T__6);
      setState(379);
      dynamic_cast<InstBinOpContext *>(_localctx)->op1 = value();
      setState(380);
      dynamic_cast<InstBinOpContext *>(_localctx)->op2 = value();
      setState(382);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__81) {
        setState(381);
        excClause();
      }
      break;
    }

    case 2: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstBinOpStatusContext>(_localctx));
      enterOuterAlt(_localctx, 2);
      setState(384);
      pairResults();
      setState(385);
      instName();
      setState(386);
      binop();
      setState(387);
      binopStatus();
      setState(388);
      match(UIRParser::T__5);
      setState(389);
      dynamic_cast<InstBinOpStatusContext *>(_localctx)->ty = type();
      setState(390);
      match(UIRParser::T__6);
      setState(391);
      dynamic_cast<InstBinOpStatusContext *>(_localctx)->op1 = value();
      setState(392);
      dynamic_cast<InstBinOpStatusContext *>(_localctx)->op2 = value();
      setState(394);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__81) {
        setState(393);
        excClause();
      }
      break;
    }

    case 3: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstCmpContext>(_localctx));
      enterOuterAlt(_localctx, 3);
      setState(396);
      instResult();
      setState(397);
      instName();
      setState(398);
      cmpop();
      setState(399);
      match(UIRParser::T__5);
      setState(400);
      dynamic_cast<InstCmpContext *>(_localctx)->ty = type();
      setState(401);
      match(UIRParser::T__6);
      setState(402);
      dynamic_cast<InstCmpContext *>(_localctx)->op1 = value();
      setState(403);
      dynamic_cast<InstCmpContext *>(_localctx)->op2 = value();
      break;
    }

    case 4: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstConversionContext>(_localctx));
      enterOuterAlt(_localctx, 4);
      setState(405);
      instResult();
      setState(406);
      instName();
      setState(407);
      convop();
      setState(408);
      match(UIRParser::T__5);
      setState(409);
      dynamic_cast<InstConversionContext *>(_localctx)->fromTy = type();
      setState(410);
      dynamic_cast<InstConversionContext *>(_localctx)->toTy = type();
      setState(411);
      match(UIRParser::T__6);
      setState(412);
      dynamic_cast<InstConversionContext *>(_localctx)->opnd = value();
      break;
    }

    case 5: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstSelectContext>(_localctx));
      enterOuterAlt(_localctx, 5);
      setState(414);
      instResult();
      setState(415);
      instName();
      setState(416);
      match(UIRParser::T__41);
      setState(417);
      match(UIRParser::T__5);
      setState(418);
      dynamic_cast<InstSelectContext *>(_localctx)->condTy = type();
      setState(419);
      dynamic_cast<InstSelectContext *>(_localctx)->resTy = type();
      setState(420);
      match(UIRParser::T__6);
      setState(421);
      dynamic_cast<InstSelectContext *>(_localctx)->cond = value();
      setState(422);
      dynamic_cast<InstSelectContext *>(_localctx)->ifTrue = value();
      setState(423);
      dynamic_cast<InstSelectContext *>(_localctx)->ifFalse = value();
      break;
    }

    case 6: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstBranchContext>(_localctx));
      enterOuterAlt(_localctx, 6);
      setState(425);
      instName();
      setState(426);
      match(UIRParser::T__42);
      setState(427);
      dynamic_cast<InstBranchContext *>(_localctx)->dest = destClause();
      break;
    }

    case 7: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstBranch2Context>(_localctx));
      enterOuterAlt(_localctx, 7);
      setState(429);
      instName();
      setState(430);
      match(UIRParser::T__43);
      setState(431);
      dynamic_cast<InstBranch2Context *>(_localctx)->cond = value();
      setState(432);
      dynamic_cast<InstBranch2Context *>(_localctx)->ifTrue = destClause();
      setState(433);
      dynamic_cast<InstBranch2Context *>(_localctx)->ifFalse = destClause();
      break;
    }

    case 8: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstSwitchContext>(_localctx));
      enterOuterAlt(_localctx, 8);
      setState(435);
      instName();
      setState(436);
      match(UIRParser::T__44);
      setState(437);
      match(UIRParser::T__5);
      setState(438);
      dynamic_cast<InstSwitchContext *>(_localctx)->ty = type();
      setState(439);
      match(UIRParser::T__6);
      setState(440);
      dynamic_cast<InstSwitchContext *>(_localctx)->opnd = value();
      setState(441);
      dynamic_cast<InstSwitchContext *>(_localctx)->defDest = destClause();
      setState(442);
      match(UIRParser::T__12);
      setState(448);
      _errHandler->sync(this);
      _la = _input->LA(1);
      while (_la == UIRParser::T__5 || ((((_la - 172) & ~ 0x3fULL) == 0) &&
        ((1ULL << (_la - 172)) & ((1ULL << (UIRParser::NAME - 172))
        | (1ULL << (UIRParser::GLOBAL_NAME - 172))
        | (1ULL << (UIRParser::LOCAL_NAME - 172)))) != 0)) {
        setState(443);
        dynamic_cast<InstSwitchContext *>(_localctx)->constantContext = constant();
        dynamic_cast<InstSwitchContext *>(_localctx)->caseVal.push_back(dynamic_cast<InstSwitchContext *>(_localctx)->constantContext);
        setState(444);
        dynamic_cast<InstSwitchContext *>(_localctx)->destClauseContext = destClause();
        dynamic_cast<InstSwitchContext *>(_localctx)->caseDest.push_back(dynamic_cast<InstSwitchContext *>(_localctx)->destClauseContext);
        setState(450);
        _errHandler->sync(this);
        _la = _input->LA(1);
      }
      setState(451);
      match(UIRParser::T__13);
      break;
    }

    case 9: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstCallContext>(_localctx));
      enterOuterAlt(_localctx, 9);
      setState(453);
      instResults();
      setState(454);
      instName();
      setState(455);
      match(UIRParser::T__45);
      setState(456);
      match(UIRParser::T__5);
      setState(457);
      funcSig();
      setState(458);
      match(UIRParser::T__6);
      setState(459);
      dynamic_cast<InstCallContext *>(_localctx)->callee = value();
      setState(460);
      argList();
      setState(462);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__81) {
        setState(461);
        excClause();
      }
      setState(465);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__82) {
        setState(464);
        keepaliveClause();
      }
      break;
    }

    case 10: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstTailCallContext>(_localctx));
      enterOuterAlt(_localctx, 10);
      setState(467);
      instName();
      setState(468);
      match(UIRParser::T__46);
      setState(469);
      match(UIRParser::T__5);
      setState(470);
      funcSig();
      setState(471);
      match(UIRParser::T__6);
      setState(472);
      dynamic_cast<InstTailCallContext *>(_localctx)->callee = value();
      setState(473);
      argList();
      break;
    }

    case 11: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstRetContext>(_localctx));
      enterOuterAlt(_localctx, 11);
      setState(475);
      instName();
      setState(476);
      match(UIRParser::T__47);
      setState(477);
      retVals();
      break;
    }

    case 12: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstThrowContext>(_localctx));
      enterOuterAlt(_localctx, 12);
      setState(479);
      instName();
      setState(480);
      match(UIRParser::T__48);
      setState(481);
      dynamic_cast<InstThrowContext *>(_localctx)->exc = value();
      break;
    }

    case 13: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstExtractValueContext>(_localctx));
      enterOuterAlt(_localctx, 13);
      setState(483);
      instResult();
      setState(484);
      instName();
      setState(485);
      match(UIRParser::T__49);
      setState(486);
      match(UIRParser::T__5);
      setState(487);
      dynamic_cast<InstExtractValueContext *>(_localctx)->ty = type();
      setState(488);
      intParam();
      setState(489);
      match(UIRParser::T__6);
      setState(490);
      dynamic_cast<InstExtractValueContext *>(_localctx)->opnd = value();
      break;
    }

    case 14: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstInsertValueContext>(_localctx));
      enterOuterAlt(_localctx, 14);
      setState(492);
      instResult();
      setState(493);
      instName();
      setState(494);
      match(UIRParser::T__50);
      setState(495);
      match(UIRParser::T__5);
      setState(496);
      dynamic_cast<InstInsertValueContext *>(_localctx)->ty = type();
      setState(497);
      intParam();
      setState(498);
      match(UIRParser::T__6);
      setState(499);
      dynamic_cast<InstInsertValueContext *>(_localctx)->opnd = value();
      setState(500);
      dynamic_cast<InstInsertValueContext *>(_localctx)->newVal = value();
      break;
    }

    case 15: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstExtractElementContext>(_localctx));
      enterOuterAlt(_localctx, 15);
      setState(502);
      instResult();
      setState(503);
      instName();
      setState(504);
      match(UIRParser::T__51);
      setState(505);
      match(UIRParser::T__5);
      setState(506);
      dynamic_cast<InstExtractElementContext *>(_localctx)->seqTy = type();
      setState(507);
      dynamic_cast<InstExtractElementContext *>(_localctx)->indTy = type();
      setState(508);
      match(UIRParser::T__6);
      setState(509);
      dynamic_cast<InstExtractElementContext *>(_localctx)->opnd = value();
      setState(510);
      dynamic_cast<InstExtractElementContext *>(_localctx)->index = value();
      break;
    }

    case 16: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstInsertElementContext>(_localctx));
      enterOuterAlt(_localctx, 16);
      setState(512);
      instResult();
      setState(513);
      instName();
      setState(514);
      match(UIRParser::T__52);
      setState(515);
      match(UIRParser::T__5);
      setState(516);
      dynamic_cast<InstInsertElementContext *>(_localctx)->seqTy = type();
      setState(517);
      dynamic_cast<InstInsertElementContext *>(_localctx)->indTy = type();
      setState(518);
      match(UIRParser::T__6);
      setState(519);
      dynamic_cast<InstInsertElementContext *>(_localctx)->opnd = value();
      setState(520);
      dynamic_cast<InstInsertElementContext *>(_localctx)->index = value();
      setState(521);
      dynamic_cast<InstInsertElementContext *>(_localctx)->newVal = value();
      break;
    }

    case 17: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstShuffleVectorContext>(_localctx));
      enterOuterAlt(_localctx, 17);
      setState(523);
      instResult();
      setState(524);
      instName();
      setState(525);
      match(UIRParser::T__53);
      setState(526);
      match(UIRParser::T__5);
      setState(527);
      dynamic_cast<InstShuffleVectorContext *>(_localctx)->vecTy = type();
      setState(528);
      dynamic_cast<InstShuffleVectorContext *>(_localctx)->maskTy = type();
      setState(529);
      match(UIRParser::T__6);
      setState(530);
      dynamic_cast<InstShuffleVectorContext *>(_localctx)->vec1 = value();
      setState(531);
      dynamic_cast<InstShuffleVectorContext *>(_localctx)->vec2 = value();
      setState(532);
      dynamic_cast<InstShuffleVectorContext *>(_localctx)->mask = value();
      break;
    }

    case 18: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstNewContext>(_localctx));
      enterOuterAlt(_localctx, 18);
      setState(534);
      instResult();
      setState(535);
      instName();
      setState(536);
      match(UIRParser::T__54);
      setState(537);
      match(UIRParser::T__5);
      setState(538);
      dynamic_cast<InstNewContext *>(_localctx)->allocTy = type();
      setState(539);
      match(UIRParser::T__6);
      setState(541);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__81) {
        setState(540);
        excClause();
      }
      break;
    }

    case 19: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstNewHybridContext>(_localctx));
      enterOuterAlt(_localctx, 19);
      setState(543);
      instResult();
      setState(544);
      instName();
      setState(545);
      match(UIRParser::T__55);
      setState(546);
      match(UIRParser::T__5);
      setState(547);
      dynamic_cast<InstNewHybridContext *>(_localctx)->allocTy = type();
      setState(548);
      dynamic_cast<InstNewHybridContext *>(_localctx)->lenTy = type();
      setState(549);
      match(UIRParser::T__6);
      setState(550);
      dynamic_cast<InstNewHybridContext *>(_localctx)->length = value();
      setState(552);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__81) {
        setState(551);
        excClause();
      }
      break;
    }

    case 20: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstAllocaContext>(_localctx));
      enterOuterAlt(_localctx, 20);
      setState(554);
      instResult();
      setState(555);
      instName();
      setState(556);
      match(UIRParser::T__56);
      setState(557);
      match(UIRParser::T__5);
      setState(558);
      dynamic_cast<InstAllocaContext *>(_localctx)->allocTy = type();
      setState(559);
      match(UIRParser::T__6);
      setState(561);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__81) {
        setState(560);
        excClause();
      }
      break;
    }

    case 21: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstAllocaHybridContext>(_localctx));
      enterOuterAlt(_localctx, 21);
      setState(563);
      instResult();
      setState(564);
      instName();
      setState(565);
      match(UIRParser::T__57);
      setState(566);
      match(UIRParser::T__5);
      setState(567);
      dynamic_cast<InstAllocaHybridContext *>(_localctx)->allocTy = type();
      setState(568);
      dynamic_cast<InstAllocaHybridContext *>(_localctx)->lenTy = type();
      setState(569);
      match(UIRParser::T__6);
      setState(570);
      dynamic_cast<InstAllocaHybridContext *>(_localctx)->length = value();
      setState(572);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__81) {
        setState(571);
        excClause();
      }
      break;
    }

    case 22: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstGetIRefContext>(_localctx));
      enterOuterAlt(_localctx, 22);
      setState(574);
      instResult();
      setState(575);
      instName();
      setState(576);
      match(UIRParser::T__58);
      setState(577);
      match(UIRParser::T__5);
      setState(578);
      dynamic_cast<InstGetIRefContext *>(_localctx)->refTy = type();
      setState(579);
      match(UIRParser::T__6);
      setState(580);
      dynamic_cast<InstGetIRefContext *>(_localctx)->opnd = value();
      break;
    }

    case 23: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstGetFieldIRefContext>(_localctx));
      enterOuterAlt(_localctx, 23);
      setState(582);
      instResult();
      setState(583);
      instName();
      setState(584);
      match(UIRParser::T__59);

      setState(586);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__60) {
        setState(585);
        dynamic_cast<InstGetFieldIRefContext *>(_localctx)->ptr = match(UIRParser::T__60);
      }
      setState(588);
      match(UIRParser::T__5);
      setState(589);
      dynamic_cast<InstGetFieldIRefContext *>(_localctx)->refTy = type();
      setState(590);
      dynamic_cast<InstGetFieldIRefContext *>(_localctx)->index = intParam();
      setState(591);
      match(UIRParser::T__6);
      setState(592);
      dynamic_cast<InstGetFieldIRefContext *>(_localctx)->opnd = value();
      break;
    }

    case 24: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstGetElemIRefContext>(_localctx));
      enterOuterAlt(_localctx, 24);
      setState(594);
      instResult();
      setState(595);
      instName();
      setState(596);
      match(UIRParser::T__61);

      setState(598);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__60) {
        setState(597);
        dynamic_cast<InstGetElemIRefContext *>(_localctx)->ptr = match(UIRParser::T__60);
      }
      setState(600);
      match(UIRParser::T__5);
      setState(601);
      dynamic_cast<InstGetElemIRefContext *>(_localctx)->refTy = type();
      setState(602);
      dynamic_cast<InstGetElemIRefContext *>(_localctx)->indTy = type();
      setState(603);
      match(UIRParser::T__6);
      setState(604);
      dynamic_cast<InstGetElemIRefContext *>(_localctx)->opnd = value();
      setState(605);
      dynamic_cast<InstGetElemIRefContext *>(_localctx)->index = value();
      break;
    }

    case 25: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstShiftIRefContext>(_localctx));
      enterOuterAlt(_localctx, 25);
      setState(607);
      instResult();
      setState(608);
      instName();
      setState(609);
      match(UIRParser::T__62);

      setState(611);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__60) {
        setState(610);
        dynamic_cast<InstShiftIRefContext *>(_localctx)->ptr = match(UIRParser::T__60);
      }
      setState(613);
      match(UIRParser::T__5);
      setState(614);
      dynamic_cast<InstShiftIRefContext *>(_localctx)->refTy = type();
      setState(615);
      dynamic_cast<InstShiftIRefContext *>(_localctx)->offTy = type();
      setState(616);
      match(UIRParser::T__6);
      setState(617);
      dynamic_cast<InstShiftIRefContext *>(_localctx)->opnd = value();
      setState(618);
      dynamic_cast<InstShiftIRefContext *>(_localctx)->offset = value();
      break;
    }

    case 26: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstGetVarPartIRefContext>(_localctx));
      enterOuterAlt(_localctx, 26);
      setState(620);
      instResult();
      setState(621);
      instName();
      setState(622);
      match(UIRParser::T__63);

      setState(624);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__60) {
        setState(623);
        dynamic_cast<InstGetVarPartIRefContext *>(_localctx)->ptr = match(UIRParser::T__60);
      }
      setState(626);
      match(UIRParser::T__5);
      setState(627);
      dynamic_cast<InstGetVarPartIRefContext *>(_localctx)->refTy = type();
      setState(628);
      match(UIRParser::T__6);
      setState(629);
      dynamic_cast<InstGetVarPartIRefContext *>(_localctx)->opnd = value();
      break;
    }

    case 27: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstLoadContext>(_localctx));
      enterOuterAlt(_localctx, 27);
      setState(631);
      instResult();
      setState(632);
      instName();
      setState(633);
      match(UIRParser::T__64);

      setState(635);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__60) {
        setState(634);
        dynamic_cast<InstLoadContext *>(_localctx)->ptr = match(UIRParser::T__60);
      }
      setState(638);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (((((_la - 144) & ~ 0x3fULL) == 0) &&
        ((1ULL << (_la - 144)) & ((1ULL << (UIRParser::T__143 - 144))
        | (1ULL << (UIRParser::T__144 - 144))
        | (1ULL << (UIRParser::T__145 - 144))
        | (1ULL << (UIRParser::T__146 - 144))
        | (1ULL << (UIRParser::T__147 - 144))
        | (1ULL << (UIRParser::T__148 - 144))
        | (1ULL << (UIRParser::T__149 - 144)))) != 0)) {
        setState(637);
        memord();
      }
      setState(640);
      match(UIRParser::T__5);
      setState(641);
      type();
      setState(642);
      match(UIRParser::T__6);
      setState(643);
      dynamic_cast<InstLoadContext *>(_localctx)->loc = value();
      setState(645);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__81) {
        setState(644);
        excClause();
      }
      break;
    }

    case 28: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstStoreContext>(_localctx));
      enterOuterAlt(_localctx, 28);
      setState(647);
      instName();
      setState(648);
      match(UIRParser::T__65);

      setState(650);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__60) {
        setState(649);
        dynamic_cast<InstStoreContext *>(_localctx)->ptr = match(UIRParser::T__60);
      }
      setState(653);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (((((_la - 144) & ~ 0x3fULL) == 0) &&
        ((1ULL << (_la - 144)) & ((1ULL << (UIRParser::T__143 - 144))
        | (1ULL << (UIRParser::T__144 - 144))
        | (1ULL << (UIRParser::T__145 - 144))
        | (1ULL << (UIRParser::T__146 - 144))
        | (1ULL << (UIRParser::T__147 - 144))
        | (1ULL << (UIRParser::T__148 - 144))
        | (1ULL << (UIRParser::T__149 - 144)))) != 0)) {
        setState(652);
        memord();
      }
      setState(655);
      match(UIRParser::T__5);
      setState(656);
      type();
      setState(657);
      match(UIRParser::T__6);
      setState(658);
      dynamic_cast<InstStoreContext *>(_localctx)->loc = value();
      setState(659);
      dynamic_cast<InstStoreContext *>(_localctx)->newVal = value();
      setState(661);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__81) {
        setState(660);
        excClause();
      }
      break;
    }

    case 29: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstCmpXchgContext>(_localctx));
      enterOuterAlt(_localctx, 29);
      setState(663);
      pairResult();
      setState(664);
      instName();
      setState(665);
      match(UIRParser::T__66);

      setState(667);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__60) {
        setState(666);
        dynamic_cast<InstCmpXchgContext *>(_localctx)->ptr = match(UIRParser::T__60);
      }

      setState(670);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__67) {
        setState(669);
        dynamic_cast<InstCmpXchgContext *>(_localctx)->isWeak = match(UIRParser::T__67);
      }
      setState(672);
      dynamic_cast<InstCmpXchgContext *>(_localctx)->ordSucc = memord();
      setState(673);
      dynamic_cast<InstCmpXchgContext *>(_localctx)->ordFail = memord();
      setState(674);
      match(UIRParser::T__5);
      setState(675);
      type();
      setState(676);
      match(UIRParser::T__6);
      setState(677);
      dynamic_cast<InstCmpXchgContext *>(_localctx)->loc = value();
      setState(678);
      dynamic_cast<InstCmpXchgContext *>(_localctx)->expected = value();
      setState(679);
      dynamic_cast<InstCmpXchgContext *>(_localctx)->desired = value();
      setState(681);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__81) {
        setState(680);
        excClause();
      }
      break;
    }

    case 30: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstAtomicRMWContext>(_localctx));
      enterOuterAlt(_localctx, 30);
      setState(683);
      instResult();
      setState(684);
      instName();
      setState(685);
      match(UIRParser::T__68);

      setState(687);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__60) {
        setState(686);
        dynamic_cast<InstAtomicRMWContext *>(_localctx)->ptr = match(UIRParser::T__60);
      }
      setState(689);
      memord();
      setState(690);
      atomicrmwop();
      setState(691);
      match(UIRParser::T__5);
      setState(692);
      type();
      setState(693);
      match(UIRParser::T__6);
      setState(694);
      dynamic_cast<InstAtomicRMWContext *>(_localctx)->loc = value();
      setState(695);
      dynamic_cast<InstAtomicRMWContext *>(_localctx)->opnd = value();
      setState(697);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__81) {
        setState(696);
        excClause();
      }
      break;
    }

    case 31: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstFenceContext>(_localctx));
      enterOuterAlt(_localctx, 31);
      setState(699);
      instName();
      setState(700);
      match(UIRParser::T__69);
      setState(701);
      memord();
      break;
    }

    case 32: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstTrapContext>(_localctx));
      enterOuterAlt(_localctx, 32);
      setState(703);
      instResults();
      setState(704);
      instName();
      setState(705);
      match(UIRParser::T__70);
      setState(706);
      typeList();
      setState(708);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__81) {
        setState(707);
        excClause();
      }
      setState(710);
      keepaliveClause();
      break;
    }

    case 33: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstWatchPointContext>(_localctx));
      enterOuterAlt(_localctx, 33);
      setState(712);
      instResults();
      setState(713);
      instName();
      setState(714);
      match(UIRParser::T__71);
      setState(715);
      wpid();
      setState(716);
      typeList();
      setState(717);
      dynamic_cast<InstWatchPointContext *>(_localctx)->dis = destClause();
      setState(718);
      dynamic_cast<InstWatchPointContext *>(_localctx)->ena = destClause();
      setState(724);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__72) {
        setState(719);
        match(UIRParser::T__72);
        setState(720);
        match(UIRParser::T__33);
        setState(721);
        dynamic_cast<InstWatchPointContext *>(_localctx)->wpExc = destClause();
        setState(722);
        match(UIRParser::T__34);
      }
      setState(727);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__82) {
        setState(726);
        keepaliveClause();
      }
      break;
    }

    case 34: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstWPBranchContext>(_localctx));
      enterOuterAlt(_localctx, 34);
      setState(729);
      instName();
      setState(730);
      match(UIRParser::T__73);
      setState(731);
      wpid();
      setState(732);
      dynamic_cast<InstWPBranchContext *>(_localctx)->dis = destClause();
      setState(733);
      dynamic_cast<InstWPBranchContext *>(_localctx)->ena = destClause();
      break;
    }

    case 35: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstCCallContext>(_localctx));
      enterOuterAlt(_localctx, 35);
      setState(735);
      instResults();
      setState(736);
      instName();
      setState(737);
      match(UIRParser::T__74);
      setState(738);
      callConv();
      setState(739);
      match(UIRParser::T__5);
      setState(740);
      dynamic_cast<InstCCallContext *>(_localctx)->funcTy = type();
      setState(741);
      funcSig();
      setState(742);
      match(UIRParser::T__6);
      setState(743);
      dynamic_cast<InstCCallContext *>(_localctx)->callee = value();
      setState(744);
      argList();
      setState(746);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__81) {
        setState(745);
        excClause();
      }
      setState(749);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__82) {
        setState(748);
        keepaliveClause();
      }
      break;
    }

    case 36: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstNewThreadContext>(_localctx));
      enterOuterAlt(_localctx, 36);
      setState(751);
      instResult();
      setState(752);
      instName();
      setState(753);
      match(UIRParser::T__75);
      setState(754);
      dynamic_cast<InstNewThreadContext *>(_localctx)->stack = value();
      setState(760);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__76) {
        setState(755);
        match(UIRParser::T__76);
        setState(756);
        match(UIRParser::T__33);
        setState(757);
        dynamic_cast<InstNewThreadContext *>(_localctx)->threadLocal = value();
        setState(758);
        match(UIRParser::T__34);
      }
      setState(762);
      newStackClause();
      setState(764);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__81) {
        setState(763);
        excClause();
      }
      break;
    }

    case 37: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstSwapStackContext>(_localctx));
      enterOuterAlt(_localctx, 37);
      setState(766);
      instResults();
      setState(767);
      instName();
      setState(768);
      match(UIRParser::T__77);
      setState(769);
      dynamic_cast<InstSwapStackContext *>(_localctx)->swappee = value();
      setState(770);
      curStackClause();
      setState(771);
      newStackClause();
      setState(773);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__81) {
        setState(772);
        excClause();
      }
      setState(776);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__82) {
        setState(775);
        keepaliveClause();
      }
      break;
    }

    case 38: {
      _localctx = dynamic_cast<InstContext *>(_tracker.createInstance<UIRParser::InstCommInstContext>(_localctx));
      enterOuterAlt(_localctx, 38);
      setState(778);
      instResults();
      setState(779);
      instName();
      setState(780);
      match(UIRParser::T__78);
      setState(781);
      commInst();
      setState(790);
      _errHandler->sync(this);

      switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 59, _ctx)) {
      case 1: {
        setState(782);
        match(UIRParser::T__38);
        setState(786);
        _errHandler->sync(this);
        _la = _input->LA(1);
        while (((((_la - 157) & ~ 0x3fULL) == 0) &&
          ((1ULL << (_la - 157)) & ((1ULL << (UIRParser::T__156 - 157))
          | (1ULL << (UIRParser::T__157 - 157))
          | (1ULL << (UIRParser::T__158 - 157))
          | (1ULL << (UIRParser::T__159 - 157))
          | (1ULL << (UIRParser::T__160 - 157)))) != 0)) {
          setState(783);
          dynamic_cast<InstCommInstContext *>(_localctx)->flagContext = flag();
          dynamic_cast<InstCommInstContext *>(_localctx)->flags.push_back(dynamic_cast<InstCommInstContext *>(_localctx)->flagContext);
          setState(788);
          _errHandler->sync(this);
          _la = _input->LA(1);
        }
        setState(789);
        match(UIRParser::T__39);
        break;
      }

      }
      setState(800);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__5) {
        setState(792);
        match(UIRParser::T__5);
        setState(796);
        _errHandler->sync(this);
        _la = _input->LA(1);
        while ((((_la & ~ 0x3fULL) == 0) &&
          ((1ULL << _la) & ((1ULL << UIRParser::T__14)
          | (1ULL << UIRParser::T__15)
          | (1ULL << UIRParser::T__16)
          | (1ULL << UIRParser::T__17)
          | (1ULL << UIRParser::T__18)
          | (1ULL << UIRParser::T__19)
          | (1ULL << UIRParser::T__20)
          | (1ULL << UIRParser::T__21)
          | (1ULL << UIRParser::T__22)
          | (1ULL << UIRParser::T__23)
          | (1ULL << UIRParser::T__24)
          | (1ULL << UIRParser::T__25)
          | (1ULL << UIRParser::T__26)
          | (1ULL << UIRParser::T__27)
          | (1ULL << UIRParser::T__28)
          | (1ULL << UIRParser::T__29)
          | (1ULL << UIRParser::T__30)
          | (1ULL << UIRParser::T__31)
          | (1ULL << UIRParser::T__32))) != 0) || ((((_la - 172) & ~ 0x3fULL) == 0) &&
          ((1ULL << (_la - 172)) & ((1ULL << (UIRParser::NAME - 172))
          | (1ULL << (UIRParser::GLOBAL_NAME - 172))
          | (1ULL << (UIRParser::LOCAL_NAME - 172)))) != 0)) {
          setState(793);
          dynamic_cast<InstCommInstContext *>(_localctx)->typeContext = type();
          dynamic_cast<InstCommInstContext *>(_localctx)->tys.push_back(dynamic_cast<InstCommInstContext *>(_localctx)->typeContext);
          setState(798);
          _errHandler->sync(this);
          _la = _input->LA(1);
        }
        setState(799);
        match(UIRParser::T__6);
      }
      setState(810);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__79) {
        setState(802);
        match(UIRParser::T__79);
        setState(806);
        _errHandler->sync(this);
        _la = _input->LA(1);
        while (_la == UIRParser::T__33 || ((((_la - 172) & ~ 0x3fULL) == 0) &&
          ((1ULL << (_la - 172)) & ((1ULL << (UIRParser::NAME - 172))
          | (1ULL << (UIRParser::GLOBAL_NAME - 172))
          | (1ULL << (UIRParser::LOCAL_NAME - 172)))) != 0)) {
          setState(803);
          dynamic_cast<InstCommInstContext *>(_localctx)->funcSigContext = funcSig();
          dynamic_cast<InstCommInstContext *>(_localctx)->sigs.push_back(dynamic_cast<InstCommInstContext *>(_localctx)->funcSigContext);
          setState(808);
          _errHandler->sync(this);
          _la = _input->LA(1);
        }
        setState(809);
        match(UIRParser::T__80);
      }
      setState(820);
      _errHandler->sync(this);

      switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 65, _ctx)) {
      case 1: {
        setState(812);
        match(UIRParser::T__33);
        setState(816);
        _errHandler->sync(this);
        _la = _input->LA(1);
        while (_la == UIRParser::T__5 || ((((_la - 172) & ~ 0x3fULL) == 0) &&
          ((1ULL << (_la - 172)) & ((1ULL << (UIRParser::NAME - 172))
          | (1ULL << (UIRParser::GLOBAL_NAME - 172))
          | (1ULL << (UIRParser::LOCAL_NAME - 172)))) != 0)) {
          setState(813);
          dynamic_cast<InstCommInstContext *>(_localctx)->valueContext = value();
          dynamic_cast<InstCommInstContext *>(_localctx)->args.push_back(dynamic_cast<InstCommInstContext *>(_localctx)->valueContext);
          setState(818);
          _errHandler->sync(this);
          _la = _input->LA(1);
        }
        setState(819);
        match(UIRParser::T__34);
        break;
      }

      }
      setState(823);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__81) {
        setState(822);
        excClause();
      }
      setState(826);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == UIRParser::T__82) {
        setState(825);
        keepaliveClause();
      }
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RetValsContext ------------------------------------------------------------------

UIRParser::RetValsContext::RetValsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<UIRParser::ValueContext *> UIRParser::RetValsContext::value() {
  return getRuleContexts<UIRParser::ValueContext>();
}

UIRParser::ValueContext* UIRParser::RetValsContext::value(size_t i) {
  return getRuleContext<UIRParser::ValueContext>(i);
}


size_t UIRParser::RetValsContext::getRuleIndex() const {
  return UIRParser::RuleRetVals;
}

antlrcpp::Any UIRParser::RetValsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitRetVals(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::RetValsContext* UIRParser::retVals() {
  RetValsContext *_localctx = _tracker.createInstance<RetValsContext>(_ctx, getState());
  enterRule(_localctx, 36, UIRParser::RuleRetVals);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(841);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 71, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(830);
      match(UIRParser::T__33);
      setState(834);
      _errHandler->sync(this);
      _la = _input->LA(1);
      while (_la == UIRParser::T__5 || ((((_la - 172) & ~ 0x3fULL) == 0) &&
        ((1ULL << (_la - 172)) & ((1ULL << (UIRParser::NAME - 172))
        | (1ULL << (UIRParser::GLOBAL_NAME - 172))
        | (1ULL << (UIRParser::LOCAL_NAME - 172)))) != 0)) {
        setState(831);
        dynamic_cast<RetValsContext *>(_localctx)->valueContext = value();
        dynamic_cast<RetValsContext *>(_localctx)->vals.push_back(dynamic_cast<RetValsContext *>(_localctx)->valueContext);
        setState(836);
        _errHandler->sync(this);
        _la = _input->LA(1);
      }
      setState(837);
      match(UIRParser::T__34);
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(839);
      _errHandler->sync(this);

      switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 70, _ctx)) {
      case 1: {
        setState(838);
        dynamic_cast<RetValsContext *>(_localctx)->valueContext = value();
        dynamic_cast<RetValsContext *>(_localctx)->vals.push_back(dynamic_cast<RetValsContext *>(_localctx)->valueContext);
        break;
      }

      }
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DestClauseContext ------------------------------------------------------------------

UIRParser::DestClauseContext::DestClauseContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

UIRParser::BbContext* UIRParser::DestClauseContext::bb() {
  return getRuleContext<UIRParser::BbContext>(0);
}

UIRParser::ArgListContext* UIRParser::DestClauseContext::argList() {
  return getRuleContext<UIRParser::ArgListContext>(0);
}


size_t UIRParser::DestClauseContext::getRuleIndex() const {
  return UIRParser::RuleDestClause;
}

antlrcpp::Any UIRParser::DestClauseContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitDestClause(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::DestClauseContext* UIRParser::destClause() {
  DestClauseContext *_localctx = _tracker.createInstance<DestClauseContext>(_ctx, getState());
  enterRule(_localctx, 38, UIRParser::RuleDestClause);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(843);
    bb();
    setState(844);
    argList();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BbContext ------------------------------------------------------------------

UIRParser::BbContext::BbContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

UIRParser::LocalNameContext* UIRParser::BbContext::localName() {
  return getRuleContext<UIRParser::LocalNameContext>(0);
}


size_t UIRParser::BbContext::getRuleIndex() const {
  return UIRParser::RuleBb;
}

antlrcpp::Any UIRParser::BbContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitBb(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::BbContext* UIRParser::bb() {
  BbContext *_localctx = _tracker.createInstance<BbContext>(_ctx, getState());
  enterRule(_localctx, 40, UIRParser::RuleBb);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(846);
    dynamic_cast<BbContext *>(_localctx)->nam = localName();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TypeListContext ------------------------------------------------------------------

UIRParser::TypeListContext::TypeListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<UIRParser::TypeContext *> UIRParser::TypeListContext::type() {
  return getRuleContexts<UIRParser::TypeContext>();
}

UIRParser::TypeContext* UIRParser::TypeListContext::type(size_t i) {
  return getRuleContext<UIRParser::TypeContext>(i);
}


size_t UIRParser::TypeListContext::getRuleIndex() const {
  return UIRParser::RuleTypeList;
}

antlrcpp::Any UIRParser::TypeListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitTypeList(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::TypeListContext* UIRParser::typeList() {
  TypeListContext *_localctx = _tracker.createInstance<TypeListContext>(_ctx, getState());
  enterRule(_localctx, 42, UIRParser::RuleTypeList);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(848);
    match(UIRParser::T__5);
    setState(852);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << UIRParser::T__14)
      | (1ULL << UIRParser::T__15)
      | (1ULL << UIRParser::T__16)
      | (1ULL << UIRParser::T__17)
      | (1ULL << UIRParser::T__18)
      | (1ULL << UIRParser::T__19)
      | (1ULL << UIRParser::T__20)
      | (1ULL << UIRParser::T__21)
      | (1ULL << UIRParser::T__22)
      | (1ULL << UIRParser::T__23)
      | (1ULL << UIRParser::T__24)
      | (1ULL << UIRParser::T__25)
      | (1ULL << UIRParser::T__26)
      | (1ULL << UIRParser::T__27)
      | (1ULL << UIRParser::T__28)
      | (1ULL << UIRParser::T__29)
      | (1ULL << UIRParser::T__30)
      | (1ULL << UIRParser::T__31)
      | (1ULL << UIRParser::T__32))) != 0) || ((((_la - 172) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 172)) & ((1ULL << (UIRParser::NAME - 172))
      | (1ULL << (UIRParser::GLOBAL_NAME - 172))
      | (1ULL << (UIRParser::LOCAL_NAME - 172)))) != 0)) {
      setState(849);
      dynamic_cast<TypeListContext *>(_localctx)->typeContext = type();
      dynamic_cast<TypeListContext *>(_localctx)->tys.push_back(dynamic_cast<TypeListContext *>(_localctx)->typeContext);
      setState(854);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(855);
    match(UIRParser::T__6);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExcClauseContext ------------------------------------------------------------------

UIRParser::ExcClauseContext::ExcClauseContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<UIRParser::DestClauseContext *> UIRParser::ExcClauseContext::destClause() {
  return getRuleContexts<UIRParser::DestClauseContext>();
}

UIRParser::DestClauseContext* UIRParser::ExcClauseContext::destClause(size_t i) {
  return getRuleContext<UIRParser::DestClauseContext>(i);
}


size_t UIRParser::ExcClauseContext::getRuleIndex() const {
  return UIRParser::RuleExcClause;
}

antlrcpp::Any UIRParser::ExcClauseContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitExcClause(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::ExcClauseContext* UIRParser::excClause() {
  ExcClauseContext *_localctx = _tracker.createInstance<ExcClauseContext>(_ctx, getState());
  enterRule(_localctx, 44, UIRParser::RuleExcClause);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(857);
    match(UIRParser::T__81);
    setState(858);
    match(UIRParser::T__33);
    setState(859);
    dynamic_cast<ExcClauseContext *>(_localctx)->nor = destClause();
    setState(860);
    dynamic_cast<ExcClauseContext *>(_localctx)->exc = destClause();
    setState(861);
    match(UIRParser::T__34);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- KeepaliveClauseContext ------------------------------------------------------------------

UIRParser::KeepaliveClauseContext::KeepaliveClauseContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<UIRParser::ValueContext *> UIRParser::KeepaliveClauseContext::value() {
  return getRuleContexts<UIRParser::ValueContext>();
}

UIRParser::ValueContext* UIRParser::KeepaliveClauseContext::value(size_t i) {
  return getRuleContext<UIRParser::ValueContext>(i);
}


size_t UIRParser::KeepaliveClauseContext::getRuleIndex() const {
  return UIRParser::RuleKeepaliveClause;
}

antlrcpp::Any UIRParser::KeepaliveClauseContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitKeepaliveClause(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::KeepaliveClauseContext* UIRParser::keepaliveClause() {
  KeepaliveClauseContext *_localctx = _tracker.createInstance<KeepaliveClauseContext>(_ctx, getState());
  enterRule(_localctx, 46, UIRParser::RuleKeepaliveClause);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(863);
    match(UIRParser::T__82);
    setState(864);
    match(UIRParser::T__33);
    setState(868);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == UIRParser::T__5 || ((((_la - 172) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 172)) & ((1ULL << (UIRParser::NAME - 172))
      | (1ULL << (UIRParser::GLOBAL_NAME - 172))
      | (1ULL << (UIRParser::LOCAL_NAME - 172)))) != 0)) {
      setState(865);
      value();
      setState(870);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(871);
    match(UIRParser::T__34);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ArgListContext ------------------------------------------------------------------

UIRParser::ArgListContext::ArgListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<UIRParser::ValueContext *> UIRParser::ArgListContext::value() {
  return getRuleContexts<UIRParser::ValueContext>();
}

UIRParser::ValueContext* UIRParser::ArgListContext::value(size_t i) {
  return getRuleContext<UIRParser::ValueContext>(i);
}


size_t UIRParser::ArgListContext::getRuleIndex() const {
  return UIRParser::RuleArgList;
}

antlrcpp::Any UIRParser::ArgListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitArgList(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::ArgListContext* UIRParser::argList() {
  ArgListContext *_localctx = _tracker.createInstance<ArgListContext>(_ctx, getState());
  enterRule(_localctx, 48, UIRParser::RuleArgList);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(873);
    match(UIRParser::T__33);
    setState(877);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == UIRParser::T__5 || ((((_la - 172) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 172)) & ((1ULL << (UIRParser::NAME - 172))
      | (1ULL << (UIRParser::GLOBAL_NAME - 172))
      | (1ULL << (UIRParser::LOCAL_NAME - 172)))) != 0)) {
      setState(874);
      value();
      setState(879);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(880);
    match(UIRParser::T__34);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CurStackClauseContext ------------------------------------------------------------------

UIRParser::CurStackClauseContext::CurStackClauseContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t UIRParser::CurStackClauseContext::getRuleIndex() const {
  return UIRParser::RuleCurStackClause;
}

void UIRParser::CurStackClauseContext::copyFrom(CurStackClauseContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- CurStackRetWithContext ------------------------------------------------------------------

UIRParser::TypeListContext* UIRParser::CurStackRetWithContext::typeList() {
  return getRuleContext<UIRParser::TypeListContext>(0);
}

UIRParser::CurStackRetWithContext::CurStackRetWithContext(CurStackClauseContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::CurStackRetWithContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitCurStackRetWith(this);
  else
    return visitor->visitChildren(this);
}
//----------------- CurStackKillOldContext ------------------------------------------------------------------

UIRParser::CurStackKillOldContext::CurStackKillOldContext(CurStackClauseContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::CurStackKillOldContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitCurStackKillOld(this);
  else
    return visitor->visitChildren(this);
}
UIRParser::CurStackClauseContext* UIRParser::curStackClause() {
  CurStackClauseContext *_localctx = _tracker.createInstance<CurStackClauseContext>(_ctx, getState());
  enterRule(_localctx, 50, UIRParser::RuleCurStackClause);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(885);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case UIRParser::T__83: {
        _localctx = dynamic_cast<CurStackClauseContext *>(_tracker.createInstance<UIRParser::CurStackRetWithContext>(_localctx));
        enterOuterAlt(_localctx, 1);
        setState(882);
        match(UIRParser::T__83);
        setState(883);
        typeList();
        break;
      }

      case UIRParser::T__84: {
        _localctx = dynamic_cast<CurStackClauseContext *>(_tracker.createInstance<UIRParser::CurStackKillOldContext>(_localctx));
        enterOuterAlt(_localctx, 2);
        setState(884);
        match(UIRParser::T__84);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- NewStackClauseContext ------------------------------------------------------------------

UIRParser::NewStackClauseContext::NewStackClauseContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t UIRParser::NewStackClauseContext::getRuleIndex() const {
  return UIRParser::RuleNewStackClause;
}

void UIRParser::NewStackClauseContext::copyFrom(NewStackClauseContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- NewStackThrowExcContext ------------------------------------------------------------------

UIRParser::ValueContext* UIRParser::NewStackThrowExcContext::value() {
  return getRuleContext<UIRParser::ValueContext>(0);
}

UIRParser::NewStackThrowExcContext::NewStackThrowExcContext(NewStackClauseContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::NewStackThrowExcContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitNewStackThrowExc(this);
  else
    return visitor->visitChildren(this);
}
//----------------- NewStackPassValueContext ------------------------------------------------------------------

UIRParser::TypeListContext* UIRParser::NewStackPassValueContext::typeList() {
  return getRuleContext<UIRParser::TypeListContext>(0);
}

UIRParser::ArgListContext* UIRParser::NewStackPassValueContext::argList() {
  return getRuleContext<UIRParser::ArgListContext>(0);
}

UIRParser::NewStackPassValueContext::NewStackPassValueContext(NewStackClauseContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::NewStackPassValueContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitNewStackPassValue(this);
  else
    return visitor->visitChildren(this);
}
UIRParser::NewStackClauseContext* UIRParser::newStackClause() {
  NewStackClauseContext *_localctx = _tracker.createInstance<NewStackClauseContext>(_ctx, getState());
  enterRule(_localctx, 52, UIRParser::RuleNewStackClause);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(893);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case UIRParser::T__85: {
        _localctx = dynamic_cast<NewStackClauseContext *>(_tracker.createInstance<UIRParser::NewStackPassValueContext>(_localctx));
        enterOuterAlt(_localctx, 1);
        setState(887);
        match(UIRParser::T__85);
        setState(888);
        typeList();
        setState(889);
        argList();
        break;
      }

      case UIRParser::T__86: {
        _localctx = dynamic_cast<NewStackClauseContext *>(_tracker.createInstance<UIRParser::NewStackThrowExcContext>(_localctx));
        enterOuterAlt(_localctx, 2);
        setState(891);
        match(UIRParser::T__86);
        setState(892);
        dynamic_cast<NewStackThrowExcContext *>(_localctx)->exc = value();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BinopContext ------------------------------------------------------------------

UIRParser::BinopContext::BinopContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t UIRParser::BinopContext::getRuleIndex() const {
  return UIRParser::RuleBinop;
}

antlrcpp::Any UIRParser::BinopContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitBinop(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::BinopContext* UIRParser::binop() {
  BinopContext *_localctx = _tracker.createInstance<BinopContext>(_ctx, getState());
  enterRule(_localctx, 54, UIRParser::RuleBinop);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(895);
    _la = _input->LA(1);
    if (!(((((_la - 88) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 88)) & ((1ULL << (UIRParser::T__87 - 88))
      | (1ULL << (UIRParser::T__88 - 88))
      | (1ULL << (UIRParser::T__89 - 88))
      | (1ULL << (UIRParser::T__90 - 88))
      | (1ULL << (UIRParser::T__91 - 88))
      | (1ULL << (UIRParser::T__92 - 88))
      | (1ULL << (UIRParser::T__93 - 88))
      | (1ULL << (UIRParser::T__94 - 88))
      | (1ULL << (UIRParser::T__95 - 88))
      | (1ULL << (UIRParser::T__96 - 88))
      | (1ULL << (UIRParser::T__97 - 88))
      | (1ULL << (UIRParser::T__98 - 88))
      | (1ULL << (UIRParser::T__99 - 88))
      | (1ULL << (UIRParser::T__100 - 88))
      | (1ULL << (UIRParser::T__101 - 88))
      | (1ULL << (UIRParser::T__102 - 88))
      | (1ULL << (UIRParser::T__103 - 88))
      | (1ULL << (UIRParser::T__104 - 88)))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CmpopContext ------------------------------------------------------------------

UIRParser::CmpopContext::CmpopContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t UIRParser::CmpopContext::getRuleIndex() const {
  return UIRParser::RuleCmpop;
}

antlrcpp::Any UIRParser::CmpopContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitCmpop(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::CmpopContext* UIRParser::cmpop() {
  CmpopContext *_localctx = _tracker.createInstance<CmpopContext>(_ctx, getState());
  enterRule(_localctx, 56, UIRParser::RuleCmpop);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(897);
    _la = _input->LA(1);
    if (!(((((_la - 106) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 106)) & ((1ULL << (UIRParser::T__105 - 106))
      | (1ULL << (UIRParser::T__106 - 106))
      | (1ULL << (UIRParser::T__107 - 106))
      | (1ULL << (UIRParser::T__108 - 106))
      | (1ULL << (UIRParser::T__109 - 106))
      | (1ULL << (UIRParser::T__110 - 106))
      | (1ULL << (UIRParser::T__111 - 106))
      | (1ULL << (UIRParser::T__112 - 106))
      | (1ULL << (UIRParser::T__113 - 106))
      | (1ULL << (UIRParser::T__114 - 106))
      | (1ULL << (UIRParser::T__115 - 106))
      | (1ULL << (UIRParser::T__116 - 106))
      | (1ULL << (UIRParser::T__117 - 106))
      | (1ULL << (UIRParser::T__118 - 106))
      | (1ULL << (UIRParser::T__119 - 106))
      | (1ULL << (UIRParser::T__120 - 106))
      | (1ULL << (UIRParser::T__121 - 106))
      | (1ULL << (UIRParser::T__122 - 106))
      | (1ULL << (UIRParser::T__123 - 106))
      | (1ULL << (UIRParser::T__124 - 106))
      | (1ULL << (UIRParser::T__125 - 106))
      | (1ULL << (UIRParser::T__126 - 106))
      | (1ULL << (UIRParser::T__127 - 106))
      | (1ULL << (UIRParser::T__128 - 106))
      | (1ULL << (UIRParser::T__129 - 106))
      | (1ULL << (UIRParser::T__130 - 106)))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ConvopContext ------------------------------------------------------------------

UIRParser::ConvopContext::ConvopContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t UIRParser::ConvopContext::getRuleIndex() const {
  return UIRParser::RuleConvop;
}

antlrcpp::Any UIRParser::ConvopContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitConvop(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::ConvopContext* UIRParser::convop() {
  ConvopContext *_localctx = _tracker.createInstance<ConvopContext>(_ctx, getState());
  enterRule(_localctx, 58, UIRParser::RuleConvop);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(899);
    _la = _input->LA(1);
    if (!(((((_la - 132) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 132)) & ((1ULL << (UIRParser::T__131 - 132))
      | (1ULL << (UIRParser::T__132 - 132))
      | (1ULL << (UIRParser::T__133 - 132))
      | (1ULL << (UIRParser::T__134 - 132))
      | (1ULL << (UIRParser::T__135 - 132))
      | (1ULL << (UIRParser::T__136 - 132))
      | (1ULL << (UIRParser::T__137 - 132))
      | (1ULL << (UIRParser::T__138 - 132))
      | (1ULL << (UIRParser::T__139 - 132))
      | (1ULL << (UIRParser::T__140 - 132))
      | (1ULL << (UIRParser::T__141 - 132))
      | (1ULL << (UIRParser::T__142 - 132)))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MemordContext ------------------------------------------------------------------

UIRParser::MemordContext::MemordContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t UIRParser::MemordContext::getRuleIndex() const {
  return UIRParser::RuleMemord;
}

antlrcpp::Any UIRParser::MemordContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitMemord(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::MemordContext* UIRParser::memord() {
  MemordContext *_localctx = _tracker.createInstance<MemordContext>(_ctx, getState());
  enterRule(_localctx, 60, UIRParser::RuleMemord);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(901);
    _la = _input->LA(1);
    if (!(((((_la - 144) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 144)) & ((1ULL << (UIRParser::T__143 - 144))
      | (1ULL << (UIRParser::T__144 - 144))
      | (1ULL << (UIRParser::T__145 - 144))
      | (1ULL << (UIRParser::T__146 - 144))
      | (1ULL << (UIRParser::T__147 - 144))
      | (1ULL << (UIRParser::T__148 - 144))
      | (1ULL << (UIRParser::T__149 - 144)))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AtomicrmwopContext ------------------------------------------------------------------

UIRParser::AtomicrmwopContext::AtomicrmwopContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t UIRParser::AtomicrmwopContext::getRuleIndex() const {
  return UIRParser::RuleAtomicrmwop;
}

antlrcpp::Any UIRParser::AtomicrmwopContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitAtomicrmwop(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::AtomicrmwopContext* UIRParser::atomicrmwop() {
  AtomicrmwopContext *_localctx = _tracker.createInstance<AtomicrmwopContext>(_ctx, getState());
  enterRule(_localctx, 62, UIRParser::RuleAtomicrmwop);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(903);
    _la = _input->LA(1);
    if (!(((((_la - 88) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 88)) & ((1ULL << (UIRParser::T__87 - 88))
      | (1ULL << (UIRParser::T__88 - 88))
      | (1ULL << (UIRParser::T__97 - 88))
      | (1ULL << (UIRParser::T__98 - 88))
      | (1ULL << (UIRParser::T__99 - 88))
      | (1ULL << (UIRParser::T__150 - 88)))) != 0) || ((((_la - 152) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 152)) & ((1ULL << (UIRParser::T__151 - 152))
      | (1ULL << (UIRParser::T__152 - 152))
      | (1ULL << (UIRParser::T__153 - 152))
      | (1ULL << (UIRParser::T__154 - 152))
      | (1ULL << (UIRParser::T__155 - 152)))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BinopStatusContext ------------------------------------------------------------------

UIRParser::BinopStatusContext::BinopStatusContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t UIRParser::BinopStatusContext::getRuleIndex() const {
  return UIRParser::RuleBinopStatus;
}

antlrcpp::Any UIRParser::BinopStatusContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitBinopStatus(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::BinopStatusContext* UIRParser::binopStatus() {
  BinopStatusContext *_localctx = _tracker.createInstance<BinopStatusContext>(_ctx, getState());
  enterRule(_localctx, 64, UIRParser::RuleBinopStatus);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(905);
    _la = _input->LA(1);
    if (!(((((_la - 157) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 157)) & ((1ULL << (UIRParser::T__156 - 157))
      | (1ULL << (UIRParser::T__157 - 157))
      | (1ULL << (UIRParser::T__158 - 157))
      | (1ULL << (UIRParser::T__159 - 157)))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CallConvContext ------------------------------------------------------------------

UIRParser::CallConvContext::CallConvContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t UIRParser::CallConvContext::getRuleIndex() const {
  return UIRParser::RuleCallConv;
}

antlrcpp::Any UIRParser::CallConvContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitCallConv(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::CallConvContext* UIRParser::callConv() {
  CallConvContext *_localctx = _tracker.createInstance<CallConvContext>(_ctx, getState());
  enterRule(_localctx, 66, UIRParser::RuleCallConv);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(907);
    match(UIRParser::T__160);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FlagContext ------------------------------------------------------------------

UIRParser::FlagContext::FlagContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

UIRParser::CallConvContext* UIRParser::FlagContext::callConv() {
  return getRuleContext<UIRParser::CallConvContext>(0);
}

UIRParser::BinopStatusContext* UIRParser::FlagContext::binopStatus() {
  return getRuleContext<UIRParser::BinopStatusContext>(0);
}


size_t UIRParser::FlagContext::getRuleIndex() const {
  return UIRParser::RuleFlag;
}

antlrcpp::Any UIRParser::FlagContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitFlag(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::FlagContext* UIRParser::flag() {
  FlagContext *_localctx = _tracker.createInstance<FlagContext>(_ctx, getState());
  enterRule(_localctx, 68, UIRParser::RuleFlag);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(911);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case UIRParser::T__160: {
        enterOuterAlt(_localctx, 1);
        setState(909);
        callConv();
        break;
      }

      case UIRParser::T__156:
      case UIRParser::T__157:
      case UIRParser::T__158:
      case UIRParser::T__159: {
        enterOuterAlt(_localctx, 2);
        setState(910);
        binopStatus();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- WpidContext ------------------------------------------------------------------

UIRParser::WpidContext::WpidContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

UIRParser::IntLiteralContext* UIRParser::WpidContext::intLiteral() {
  return getRuleContext<UIRParser::IntLiteralContext>(0);
}


size_t UIRParser::WpidContext::getRuleIndex() const {
  return UIRParser::RuleWpid;
}

antlrcpp::Any UIRParser::WpidContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitWpid(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::WpidContext* UIRParser::wpid() {
  WpidContext *_localctx = _tracker.createInstance<WpidContext>(_ctx, getState());
  enterRule(_localctx, 70, UIRParser::RuleWpid);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(913);
    intLiteral();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- IntParamContext ------------------------------------------------------------------

UIRParser::IntParamContext::IntParamContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

UIRParser::IntLiteralContext* UIRParser::IntParamContext::intLiteral() {
  return getRuleContext<UIRParser::IntLiteralContext>(0);
}


size_t UIRParser::IntParamContext::getRuleIndex() const {
  return UIRParser::RuleIntParam;
}

antlrcpp::Any UIRParser::IntParamContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitIntParam(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::IntParamContext* UIRParser::intParam() {
  IntParamContext *_localctx = _tracker.createInstance<IntParamContext>(_ctx, getState());
  enterRule(_localctx, 72, UIRParser::RuleIntParam);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(915);
    intLiteral();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SizeParamContext ------------------------------------------------------------------

UIRParser::SizeParamContext::SizeParamContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

UIRParser::IntLiteralContext* UIRParser::SizeParamContext::intLiteral() {
  return getRuleContext<UIRParser::IntLiteralContext>(0);
}


size_t UIRParser::SizeParamContext::getRuleIndex() const {
  return UIRParser::RuleSizeParam;
}

antlrcpp::Any UIRParser::SizeParamContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitSizeParam(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::SizeParamContext* UIRParser::sizeParam() {
  SizeParamContext *_localctx = _tracker.createInstance<SizeParamContext>(_ctx, getState());
  enterRule(_localctx, 74, UIRParser::RuleSizeParam);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(917);
    intLiteral();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- IntLiteralContext ------------------------------------------------------------------

UIRParser::IntLiteralContext::IntLiteralContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* UIRParser::IntLiteralContext::INT_DEC() {
  return getToken(UIRParser::INT_DEC, 0);
}

tree::TerminalNode* UIRParser::IntLiteralContext::INT_HEX() {
  return getToken(UIRParser::INT_HEX, 0);
}

tree::TerminalNode* UIRParser::IntLiteralContext::INT_OCT() {
  return getToken(UIRParser::INT_OCT, 0);
}


size_t UIRParser::IntLiteralContext::getRuleIndex() const {
  return UIRParser::RuleIntLiteral;
}

antlrcpp::Any UIRParser::IntLiteralContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitIntLiteral(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::IntLiteralContext* UIRParser::intLiteral() {
  IntLiteralContext *_localctx = _tracker.createInstance<IntLiteralContext>(_ctx, getState());
  enterRule(_localctx, 76, UIRParser::RuleIntLiteral);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(919);
    _la = _input->LA(1);
    if (!(((((_la - 166) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 166)) & ((1ULL << (UIRParser::INT_DEC - 166))
      | (1ULL << (UIRParser::INT_OCT - 166))
      | (1ULL << (UIRParser::INT_HEX - 166)))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FloatLiteralContext ------------------------------------------------------------------

UIRParser::FloatLiteralContext::FloatLiteralContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t UIRParser::FloatLiteralContext::getRuleIndex() const {
  return UIRParser::RuleFloatLiteral;
}

void UIRParser::FloatLiteralContext::copyFrom(FloatLiteralContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- FloatBitsContext ------------------------------------------------------------------

UIRParser::IntLiteralContext* UIRParser::FloatBitsContext::intLiteral() {
  return getRuleContext<UIRParser::IntLiteralContext>(0);
}

UIRParser::FloatBitsContext::FloatBitsContext(FloatLiteralContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::FloatBitsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitFloatBits(this);
  else
    return visitor->visitChildren(this);
}
//----------------- FloatNanContext ------------------------------------------------------------------

tree::TerminalNode* UIRParser::FloatNanContext::NAN_FP() {
  return getToken(UIRParser::NAN_FP, 0);
}

UIRParser::FloatNanContext::FloatNanContext(FloatLiteralContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::FloatNanContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitFloatNan(this);
  else
    return visitor->visitChildren(this);
}
//----------------- FloatNumberContext ------------------------------------------------------------------

tree::TerminalNode* UIRParser::FloatNumberContext::FP_NUM() {
  return getToken(UIRParser::FP_NUM, 0);
}

UIRParser::FloatNumberContext::FloatNumberContext(FloatLiteralContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::FloatNumberContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitFloatNumber(this);
  else
    return visitor->visitChildren(this);
}
//----------------- FloatInfContext ------------------------------------------------------------------

tree::TerminalNode* UIRParser::FloatInfContext::INF() {
  return getToken(UIRParser::INF, 0);
}

UIRParser::FloatInfContext::FloatInfContext(FloatLiteralContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::FloatInfContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitFloatInf(this);
  else
    return visitor->visitChildren(this);
}
UIRParser::FloatLiteralContext* UIRParser::floatLiteral() {
  FloatLiteralContext *_localctx = _tracker.createInstance<FloatLiteralContext>(_ctx, getState());
  enterRule(_localctx, 78, UIRParser::RuleFloatLiteral);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(932);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case UIRParser::FP_NUM: {
        _localctx = dynamic_cast<FloatLiteralContext *>(_tracker.createInstance<UIRParser::FloatNumberContext>(_localctx));
        enterOuterAlt(_localctx, 1);
        setState(921);
        match(UIRParser::FP_NUM);
        setState(922);
        match(UIRParser::T__161);
        break;
      }

      case UIRParser::INF: {
        _localctx = dynamic_cast<FloatLiteralContext *>(_tracker.createInstance<UIRParser::FloatInfContext>(_localctx));
        enterOuterAlt(_localctx, 2);
        setState(923);
        match(UIRParser::INF);
        setState(924);
        match(UIRParser::T__161);
        break;
      }

      case UIRParser::NAN_FP: {
        _localctx = dynamic_cast<FloatLiteralContext *>(_tracker.createInstance<UIRParser::FloatNanContext>(_localctx));
        enterOuterAlt(_localctx, 3);
        setState(925);
        match(UIRParser::NAN_FP);
        setState(926);
        match(UIRParser::T__161);
        break;
      }

      case UIRParser::T__162: {
        _localctx = dynamic_cast<FloatLiteralContext *>(_tracker.createInstance<UIRParser::FloatBitsContext>(_localctx));
        enterOuterAlt(_localctx, 4);
        setState(927);
        match(UIRParser::T__162);
        setState(928);
        match(UIRParser::T__33);
        setState(929);
        intLiteral();
        setState(930);
        match(UIRParser::T__34);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DoubleLiteralContext ------------------------------------------------------------------

UIRParser::DoubleLiteralContext::DoubleLiteralContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t UIRParser::DoubleLiteralContext::getRuleIndex() const {
  return UIRParser::RuleDoubleLiteral;
}

void UIRParser::DoubleLiteralContext::copyFrom(DoubleLiteralContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- DoubleBitsContext ------------------------------------------------------------------

UIRParser::IntLiteralContext* UIRParser::DoubleBitsContext::intLiteral() {
  return getRuleContext<UIRParser::IntLiteralContext>(0);
}

UIRParser::DoubleBitsContext::DoubleBitsContext(DoubleLiteralContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::DoubleBitsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitDoubleBits(this);
  else
    return visitor->visitChildren(this);
}
//----------------- DoubleNumberContext ------------------------------------------------------------------

tree::TerminalNode* UIRParser::DoubleNumberContext::FP_NUM() {
  return getToken(UIRParser::FP_NUM, 0);
}

UIRParser::DoubleNumberContext::DoubleNumberContext(DoubleLiteralContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::DoubleNumberContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitDoubleNumber(this);
  else
    return visitor->visitChildren(this);
}
//----------------- DoubleInfContext ------------------------------------------------------------------

tree::TerminalNode* UIRParser::DoubleInfContext::INF() {
  return getToken(UIRParser::INF, 0);
}

UIRParser::DoubleInfContext::DoubleInfContext(DoubleLiteralContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::DoubleInfContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitDoubleInf(this);
  else
    return visitor->visitChildren(this);
}
//----------------- DoubleNanContext ------------------------------------------------------------------

tree::TerminalNode* UIRParser::DoubleNanContext::NAN_FP() {
  return getToken(UIRParser::NAN_FP, 0);
}

UIRParser::DoubleNanContext::DoubleNanContext(DoubleLiteralContext *ctx) { copyFrom(ctx); }

antlrcpp::Any UIRParser::DoubleNanContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitDoubleNan(this);
  else
    return visitor->visitChildren(this);
}
UIRParser::DoubleLiteralContext* UIRParser::doubleLiteral() {
  DoubleLiteralContext *_localctx = _tracker.createInstance<DoubleLiteralContext>(_ctx, getState());
  enterRule(_localctx, 80, UIRParser::RuleDoubleLiteral);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(945);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case UIRParser::FP_NUM: {
        _localctx = dynamic_cast<DoubleLiteralContext *>(_tracker.createInstance<UIRParser::DoubleNumberContext>(_localctx));
        enterOuterAlt(_localctx, 1);
        setState(934);
        match(UIRParser::FP_NUM);
        setState(935);
        match(UIRParser::T__163);
        break;
      }

      case UIRParser::INF: {
        _localctx = dynamic_cast<DoubleLiteralContext *>(_tracker.createInstance<UIRParser::DoubleInfContext>(_localctx));
        enterOuterAlt(_localctx, 2);
        setState(936);
        match(UIRParser::INF);
        setState(937);
        match(UIRParser::T__163);
        break;
      }

      case UIRParser::NAN_FP: {
        _localctx = dynamic_cast<DoubleLiteralContext *>(_tracker.createInstance<UIRParser::DoubleNanContext>(_localctx));
        enterOuterAlt(_localctx, 3);
        setState(938);
        match(UIRParser::NAN_FP);
        setState(939);
        match(UIRParser::T__163);
        break;
      }

      case UIRParser::T__164: {
        _localctx = dynamic_cast<DoubleLiteralContext *>(_tracker.createInstance<UIRParser::DoubleBitsContext>(_localctx));
        enterOuterAlt(_localctx, 4);
        setState(940);
        match(UIRParser::T__164);
        setState(941);
        match(UIRParser::T__33);
        setState(942);
        intLiteral();
        setState(943);
        match(UIRParser::T__34);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StringLiteralContext ------------------------------------------------------------------

UIRParser::StringLiteralContext::StringLiteralContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* UIRParser::StringLiteralContext::STRING_LITERAL() {
  return getToken(UIRParser::STRING_LITERAL, 0);
}


size_t UIRParser::StringLiteralContext::getRuleIndex() const {
  return UIRParser::RuleStringLiteral;
}

antlrcpp::Any UIRParser::StringLiteralContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitStringLiteral(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::StringLiteralContext* UIRParser::stringLiteral() {
  StringLiteralContext *_localctx = _tracker.createInstance<StringLiteralContext>(_ctx, getState());
  enterRule(_localctx, 82, UIRParser::RuleStringLiteral);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(947);
    match(UIRParser::STRING_LITERAL);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- GlobalNameContext ------------------------------------------------------------------

UIRParser::GlobalNameContext::GlobalNameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* UIRParser::GlobalNameContext::NAME() {
  return getToken(UIRParser::NAME, 0);
}

tree::TerminalNode* UIRParser::GlobalNameContext::LOCAL_NAME() {
  return getToken(UIRParser::LOCAL_NAME, 0);
}

tree::TerminalNode* UIRParser::GlobalNameContext::GLOBAL_NAME() {
  return getToken(UIRParser::GLOBAL_NAME, 0);
}


size_t UIRParser::GlobalNameContext::getRuleIndex() const {
  return UIRParser::RuleGlobalName;
}

antlrcpp::Any UIRParser::GlobalNameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitGlobalName(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::GlobalNameContext* UIRParser::globalName() {
  GlobalNameContext *_localctx = _tracker.createInstance<GlobalNameContext>(_ctx, getState());
  enterRule(_localctx, 84, UIRParser::RuleGlobalName);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(949);
    _la = _input->LA(1);
    if (!(((((_la - 172) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 172)) & ((1ULL << (UIRParser::NAME - 172))
      | (1ULL << (UIRParser::GLOBAL_NAME - 172))
      | (1ULL << (UIRParser::LOCAL_NAME - 172)))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LocalNameContext ------------------------------------------------------------------

UIRParser::LocalNameContext::LocalNameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* UIRParser::LocalNameContext::NAME() {
  return getToken(UIRParser::NAME, 0);
}

tree::TerminalNode* UIRParser::LocalNameContext::GLOBAL_NAME() {
  return getToken(UIRParser::GLOBAL_NAME, 0);
}

tree::TerminalNode* UIRParser::LocalNameContext::LOCAL_NAME() {
  return getToken(UIRParser::LOCAL_NAME, 0);
}


size_t UIRParser::LocalNameContext::getRuleIndex() const {
  return UIRParser::RuleLocalName;
}

antlrcpp::Any UIRParser::LocalNameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitLocalName(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::LocalNameContext* UIRParser::localName() {
  LocalNameContext *_localctx = _tracker.createInstance<LocalNameContext>(_ctx, getState());
  enterRule(_localctx, 86, UIRParser::RuleLocalName);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(951);
    _la = _input->LA(1);
    if (!(((((_la - 172) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 172)) & ((1ULL << (UIRParser::NAME - 172))
      | (1ULL << (UIRParser::GLOBAL_NAME - 172))
      | (1ULL << (UIRParser::LOCAL_NAME - 172)))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- NameContext ------------------------------------------------------------------

UIRParser::NameContext::NameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* UIRParser::NameContext::NAME() {
  return getToken(UIRParser::NAME, 0);
}

tree::TerminalNode* UIRParser::NameContext::GLOBAL_NAME() {
  return getToken(UIRParser::GLOBAL_NAME, 0);
}

tree::TerminalNode* UIRParser::NameContext::LOCAL_NAME() {
  return getToken(UIRParser::LOCAL_NAME, 0);
}


size_t UIRParser::NameContext::getRuleIndex() const {
  return UIRParser::RuleName;
}

antlrcpp::Any UIRParser::NameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitName(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::NameContext* UIRParser::name() {
  NameContext *_localctx = _tracker.createInstance<NameContext>(_ctx, getState());
  enterRule(_localctx, 88, UIRParser::RuleName);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(953);
    _la = _input->LA(1);
    if (!(((((_la - 172) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 172)) & ((1ULL << (UIRParser::NAME - 172))
      | (1ULL << (UIRParser::GLOBAL_NAME - 172))
      | (1ULL << (UIRParser::LOCAL_NAME - 172)))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CommInstContext ------------------------------------------------------------------

UIRParser::CommInstContext::CommInstContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* UIRParser::CommInstContext::NAME() {
  return getToken(UIRParser::NAME, 0);
}

tree::TerminalNode* UIRParser::CommInstContext::GLOBAL_NAME() {
  return getToken(UIRParser::GLOBAL_NAME, 0);
}


size_t UIRParser::CommInstContext::getRuleIndex() const {
  return UIRParser::RuleCommInst;
}

antlrcpp::Any UIRParser::CommInstContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<UIRVisitor*>(visitor))
    return parserVisitor->visitCommInst(this);
  else
    return visitor->visitChildren(this);
}

UIRParser::CommInstContext* UIRParser::commInst() {
  CommInstContext *_localctx = _tracker.createInstance<CommInstContext>(_ctx, getState());
  enterRule(_localctx, 90, UIRParser::RuleCommInst);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(955);
    _la = _input->LA(1);
    if (!(_la == UIRParser::NAME

    || _la == UIRParser::GLOBAL_NAME)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

// Static vars and initialization.
std::vector<dfa::DFA> UIRParser::_decisionToDFA;
atn::PredictionContextCache UIRParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN UIRParser::_atn;
std::vector<uint16_t> UIRParser::_serializedATN;

std::vector<std::string> UIRParser::_ruleNames = {
  "ir", "topLevelDef", "typeConstructor", "funcSigConstructor", "constConstructor", 
  "type", "funcSig", "value", "constant", "func", "globalVar", "basicBlock", 
  "instName", "instResult", "instResults", "pairResult", "pairResults", 
  "inst", "retVals", "destClause", "bb", "typeList", "excClause", "keepaliveClause", 
  "argList", "curStackClause", "newStackClause", "binop", "cmpop", "convop", 
  "memord", "atomicrmwop", "binopStatus", "callConv", "flag", "wpid", "intParam", 
  "sizeParam", "intLiteral", "floatLiteral", "doubleLiteral", "stringLiteral", 
  "globalName", "localName", "name", "commInst"
};

std::vector<std::string> UIRParser::_literalNames = {
  "", "'.bundle'", "'.typedef'", "'='", "'.funcsig'", "'.const'", "'<'", 
  "'>'", "'.global'", "'.funcdecl'", "'.expose'", "'.funcdef'", "'VERSION'", 
  "'{'", "'}'", "'int'", "'float'", "'double'", "'uptr'", "'ufuncptr'", 
  "'struct'", "'hybrid'", "'array'", "'vector'", "'void'", "'ref'", "'iref'", 
  "'weakref'", "'tagref64'", "'funcref'", "'threadref'", "'stackref'", "'framecursorref'", 
  "'irbuilderref'", "'('", "')'", "'->'", "'NULL'", "'EXTERN'", "'['", "']'", 
  "':'", "'SELECT'", "'BRANCH'", "'BRANCH2'", "'SWITCH'", "'CALL'", "'TAILCALL'", 
  "'RET'", "'THROW'", "'EXTRACTVALUE'", "'INSERTVALUE'", "'EXTRACTELEMENT'", 
  "'INSERTELEMENT'", "'SHUFFLEVECTOR'", "'NEW'", "'NEWHYBRID'", "'ALLOCA'", 
  "'ALLOCAHYBRID'", "'GETIREF'", "'GETFIELDIREF'", "'PTR'", "'GETELEMIREF'", 
  "'SHIFTIREF'", "'GETVARPARTIREF'", "'LOAD'", "'STORE'", "'CMPXCHG'", "'WEAK'", 
  "'ATOMICRMW'", "'FENCE'", "'TRAP'", "'WATCHPOINT'", "'WPEXC'", "'WPBRANCH'", 
  "'CCALL'", "'NEWTHREAD'", "'THREADLOCAL'", "'SWAPSTACK'", "'COMMINST'", 
  "'<['", "']>'", "'EXC'", "'KEEPALIVE'", "'RET_WITH'", "'KILL_OLD'", "'PASS_VALUES'", 
  "'THROW_EXC'", "'ADD'", "'SUB'", "'MUL'", "'UDIV'", "'SDIV'", "'UREM'", 
  "'SREM'", "'SHL'", "'LSHR'", "'ASHR'", "'AND'", "'OR'", "'XOR'", "'FADD'", 
  "'FSUB'", "'FMUL'", "'FDIV'", "'FREM'", "'EQ'", "'NE'", "'SGT'", "'SLT'", 
  "'SGE'", "'SLE'", "'UGT'", "'ULT'", "'UGE'", "'ULE'", "'FTRUE'", "'FFALSE'", 
  "'FUNO'", "'FUEQ'", "'FUNE'", "'FUGT'", "'FULT'", "'FUGE'", "'FULE'", 
  "'FORD'", "'FOEQ'", "'FONE'", "'FOGT'", "'FOLT'", "'FOGE'", "'FOLE'", 
  "'TRUNC'", "'ZEXT'", "'SEXT'", "'FPTRUNC'", "'FPEXT'", "'FPTOUI'", "'FPTOSI'", 
  "'UITOFP'", "'SITOFP'", "'BITCAST'", "'REFCAST'", "'PTRCAST'", "'NOT_ATOMIC'", 
  "'RELAXED'", "'CONSUME'", "'ACQUIRE'", "'RELEASE'", "'ACQ_REL'", "'SEQ_CST'", 
  "'XCHG'", "'NAND'", "'MAX'", "'MIN'", "'UMAX'", "'UMIN'", "'#N'", "'#Z'", 
  "'#C'", "'#V'", "'#DEFAULT'", "'f'", "'bitsf'", "'d'", "'bitsd'", "", 
  "", "", "", "", "'nan'"
};

std::vector<std::string> UIRParser::_symbolicNames = {
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "INT_DEC", "INT_OCT", "INT_HEX", "FP_NUM", "INF", "NAN_FP", 
  "NAME", "GLOBAL_NAME", "LOCAL_NAME", "STRING_LITERAL", "WS", "LINE_COMMENT", 
  "C_COMMENT"
};

dfa::Vocabulary UIRParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> UIRParser::_tokenNames;

UIRParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0xb4, 0x3c0, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 
    0x9, 0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7, 
    0x4, 0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x4, 0xa, 0x9, 0xa, 0x4, 0xb, 
    0x9, 0xb, 0x4, 0xc, 0x9, 0xc, 0x4, 0xd, 0x9, 0xd, 0x4, 0xe, 0x9, 0xe, 
    0x4, 0xf, 0x9, 0xf, 0x4, 0x10, 0x9, 0x10, 0x4, 0x11, 0x9, 0x11, 0x4, 
    0x12, 0x9, 0x12, 0x4, 0x13, 0x9, 0x13, 0x4, 0x14, 0x9, 0x14, 0x4, 0x15, 
    0x9, 0x15, 0x4, 0x16, 0x9, 0x16, 0x4, 0x17, 0x9, 0x17, 0x4, 0x18, 0x9, 
    0x18, 0x4, 0x19, 0x9, 0x19, 0x4, 0x1a, 0x9, 0x1a, 0x4, 0x1b, 0x9, 0x1b, 
    0x4, 0x1c, 0x9, 0x1c, 0x4, 0x1d, 0x9, 0x1d, 0x4, 0x1e, 0x9, 0x1e, 0x4, 
    0x1f, 0x9, 0x1f, 0x4, 0x20, 0x9, 0x20, 0x4, 0x21, 0x9, 0x21, 0x4, 0x22, 
    0x9, 0x22, 0x4, 0x23, 0x9, 0x23, 0x4, 0x24, 0x9, 0x24, 0x4, 0x25, 0x9, 
    0x25, 0x4, 0x26, 0x9, 0x26, 0x4, 0x27, 0x9, 0x27, 0x4, 0x28, 0x9, 0x28, 
    0x4, 0x29, 0x9, 0x29, 0x4, 0x2a, 0x9, 0x2a, 0x4, 0x2b, 0x9, 0x2b, 0x4, 
    0x2c, 0x9, 0x2c, 0x4, 0x2d, 0x9, 0x2d, 0x4, 0x2e, 0x9, 0x2e, 0x4, 0x2f, 
    0x9, 0x2f, 0x3, 0x2, 0x7, 0x2, 0x60, 0xa, 0x2, 0xc, 0x2, 0xe, 0x2, 0x63, 
    0xb, 0x2, 0x3, 0x2, 0x3, 0x2, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x5, 0x3, 0x95, 0xa, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x3, 0x3, 0x3, 0x5, 0x3, 0x9d, 0xa, 0x3, 0x5, 0x3, 0x9f, 0xa, 0x3, 0x3, 
    0x3, 0x3, 0x3, 0x6, 0x3, 0xa3, 0xa, 0x3, 0xd, 0x3, 0xe, 0x3, 0xa4, 0x3, 
    0x3, 0x3, 0x3, 0x5, 0x3, 0xa9, 0xa, 0x3, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 
    0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 
    0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 
    0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x6, 0x4, 0xbf, 0xa, 0x4, 0xd, 0x4, 0xe, 
    0x4, 0xc0, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x7, 0x4, 
    0xc8, 0xa, 0x4, 0xc, 0x4, 0xe, 0x4, 0xcb, 0xb, 0x4, 0x3, 0x4, 0x3, 0x4, 
    0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 
    0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 
    0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 
    0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 
    0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 
    0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x5, 0x4, 0xf6, 0xa, 0x4, 0x3, 
    0x5, 0x3, 0x5, 0x7, 0x5, 0xfa, 0xa, 0x5, 0xc, 0x5, 0xe, 0x5, 0xfd, 0xb, 
    0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x7, 0x5, 0x103, 0xa, 0x5, 
    0xc, 0x5, 0xe, 0x5, 0x106, 0xb, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x6, 0x3, 
    0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x7, 0x6, 0x110, 0xa, 0x6, 
    0xc, 0x6, 0xe, 0x6, 0x113, 0xb, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x5, 
    0x6, 0x118, 0xa, 0x6, 0x3, 0x7, 0x3, 0x7, 0x5, 0x7, 0x11c, 0xa, 0x7, 
    0x3, 0x8, 0x3, 0x8, 0x5, 0x8, 0x120, 0xa, 0x8, 0x3, 0x9, 0x3, 0x9, 0x5, 
    0x9, 0x124, 0xa, 0x9, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 
    0x3, 0xa, 0x5, 0xa, 0x12c, 0xa, 0xa, 0x3, 0xb, 0x3, 0xb, 0x3, 0xc, 0x3, 
    0xc, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 
    0xd, 0x7, 0xd, 0x139, 0xa, 0xd, 0xc, 0xd, 0xe, 0xd, 0x13c, 0xb, 0xd, 
    0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x5, 0xd, 0x143, 0xa, 
    0xd, 0x3, 0xd, 0x3, 0xd, 0x6, 0xd, 0x147, 0xa, 0xd, 0xd, 0xd, 0xe, 0xd, 
    0x148, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x5, 0xe, 0x14f, 0xa, 
    0xe, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 
    0x3, 0x10, 0x3, 0x10, 0x7, 0x10, 0x159, 0xa, 0x10, 0xc, 0x10, 0xe, 0x10, 
    0x15c, 0xb, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x5, 0x10, 0x161, 
    0xa, 0x10, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 
    0x11, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 
    0x6, 0x12, 0x16f, 0xa, 0x12, 0xd, 0x12, 0xe, 0x12, 0x170, 0x3, 0x12, 
    0x3, 0x12, 0x3, 0x12, 0x5, 0x12, 0x176, 0xa, 0x12, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x5, 0x13, 0x181, 0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x5, 0x13, 0x18d, 0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x3, 0x13, 0x7, 0x13, 0x1c1, 0xa, 0x13, 0xc, 0x13, 0xe, 0x13, 
    0x1c4, 0xb, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x5, 
    0x13, 0x1d1, 0xa, 0x13, 0x3, 0x13, 0x5, 0x13, 0x1d4, 0xa, 0x13, 0x3, 
    0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x5, 0x13, 0x220, 0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x5, 0x13, 0x22b, 0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x5, 0x13, 0x234, 0xa, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x3, 0x13, 0x3, 0x13, 0x5, 0x13, 0x23f, 0xa, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x5, 0x13, 0x24d, 
    0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x5, 0x13, 0x259, 
    0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x5, 0x13, 
    0x266, 0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x5, 
    0x13, 0x273, 0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x5, 0x13, 0x27e, 
    0xa, 0x13, 0x3, 0x13, 0x5, 0x13, 0x281, 0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x5, 0x13, 0x288, 0xa, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x5, 0x13, 0x28d, 0xa, 0x13, 0x3, 0x13, 0x5, 0x13, 
    0x290, 0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x5, 0x13, 0x298, 0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x5, 0x13, 0x29e, 0xa, 0x13, 0x3, 0x13, 0x5, 0x13, 0x2a1, 
    0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x5, 0x13, 0x2ac, 0xa, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x5, 0x13, 0x2b2, 0xa, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x3, 0x13, 0x5, 0x13, 0x2bc, 0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x5, 0x13, 0x2c7, 0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x5, 0x13, 0x2d7, 
    0xa, 0x13, 0x3, 0x13, 0x5, 0x13, 0x2da, 0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x5, 0x13, 0x2ed, 0xa, 0x13, 0x3, 0x13, 0x5, 0x13, 
    0x2f0, 0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x5, 0x13, 0x2fb, 0xa, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x5, 0x13, 0x2ff, 0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x5, 0x13, 0x308, 
    0xa, 0x13, 0x3, 0x13, 0x5, 0x13, 0x30b, 0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x7, 0x13, 0x313, 0xa, 0x13, 
    0xc, 0x13, 0xe, 0x13, 0x316, 0xb, 0x13, 0x3, 0x13, 0x5, 0x13, 0x319, 
    0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 0x7, 0x13, 0x31d, 0xa, 0x13, 0xc, 0x13, 
    0xe, 0x13, 0x320, 0xb, 0x13, 0x3, 0x13, 0x5, 0x13, 0x323, 0xa, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x7, 0x13, 0x327, 0xa, 0x13, 0xc, 0x13, 0xe, 0x13, 
    0x32a, 0xb, 0x13, 0x3, 0x13, 0x5, 0x13, 0x32d, 0xa, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x7, 0x13, 0x331, 0xa, 0x13, 0xc, 0x13, 0xe, 0x13, 0x334, 
    0xb, 0x13, 0x3, 0x13, 0x5, 0x13, 0x337, 0xa, 0x13, 0x3, 0x13, 0x5, 0x13, 
    0x33a, 0xa, 0x13, 0x3, 0x13, 0x5, 0x13, 0x33d, 0xa, 0x13, 0x5, 0x13, 
    0x33f, 0xa, 0x13, 0x3, 0x14, 0x3, 0x14, 0x7, 0x14, 0x343, 0xa, 0x14, 
    0xc, 0x14, 0xe, 0x14, 0x346, 0xb, 0x14, 0x3, 0x14, 0x3, 0x14, 0x5, 0x14, 
    0x34a, 0xa, 0x14, 0x5, 0x14, 0x34c, 0xa, 0x14, 0x3, 0x15, 0x3, 0x15, 
    0x3, 0x15, 0x3, 0x16, 0x3, 0x16, 0x3, 0x17, 0x3, 0x17, 0x7, 0x17, 0x355, 
    0xa, 0x17, 0xc, 0x17, 0xe, 0x17, 0x358, 0xb, 0x17, 0x3, 0x17, 0x3, 0x17, 
    0x3, 0x18, 0x3, 0x18, 0x3, 0x18, 0x3, 0x18, 0x3, 0x18, 0x3, 0x18, 0x3, 
    0x19, 0x3, 0x19, 0x3, 0x19, 0x7, 0x19, 0x365, 0xa, 0x19, 0xc, 0x19, 
    0xe, 0x19, 0x368, 0xb, 0x19, 0x3, 0x19, 0x3, 0x19, 0x3, 0x1a, 0x3, 0x1a, 
    0x7, 0x1a, 0x36e, 0xa, 0x1a, 0xc, 0x1a, 0xe, 0x1a, 0x371, 0xb, 0x1a, 
    0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1b, 0x3, 0x1b, 0x3, 0x1b, 0x5, 0x1b, 0x378, 
    0xa, 0x1b, 0x3, 0x1c, 0x3, 0x1c, 0x3, 0x1c, 0x3, 0x1c, 0x3, 0x1c, 0x3, 
    0x1c, 0x5, 0x1c, 0x380, 0xa, 0x1c, 0x3, 0x1d, 0x3, 0x1d, 0x3, 0x1e, 
    0x3, 0x1e, 0x3, 0x1f, 0x3, 0x1f, 0x3, 0x20, 0x3, 0x20, 0x3, 0x21, 0x3, 
    0x21, 0x3, 0x22, 0x3, 0x22, 0x3, 0x23, 0x3, 0x23, 0x3, 0x24, 0x3, 0x24, 
    0x5, 0x24, 0x392, 0xa, 0x24, 0x3, 0x25, 0x3, 0x25, 0x3, 0x26, 0x3, 0x26, 
    0x3, 0x27, 0x3, 0x27, 0x3, 0x28, 0x3, 0x28, 0x3, 0x29, 0x3, 0x29, 0x3, 
    0x29, 0x3, 0x29, 0x3, 0x29, 0x3, 0x29, 0x3, 0x29, 0x3, 0x29, 0x3, 0x29, 
    0x3, 0x29, 0x3, 0x29, 0x5, 0x29, 0x3a7, 0xa, 0x29, 0x3, 0x2a, 0x3, 0x2a, 
    0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x3, 
    0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x5, 0x2a, 0x3b4, 0xa, 0x2a, 0x3, 0x2b, 
    0x3, 0x2b, 0x3, 0x2c, 0x3, 0x2c, 0x3, 0x2d, 0x3, 0x2d, 0x3, 0x2e, 0x3, 
    0x2e, 0x3, 0x2f, 0x3, 0x2f, 0x3, 0x2f, 0x2, 0x2, 0x30, 0x2, 0x4, 0x6, 
    0x8, 0xa, 0xc, 0xe, 0x10, 0x12, 0x14, 0x16, 0x18, 0x1a, 0x1c, 0x1e, 
    0x20, 0x22, 0x24, 0x26, 0x28, 0x2a, 0x2c, 0x2e, 0x30, 0x32, 0x34, 0x36, 
    0x38, 0x3a, 0x3c, 0x3e, 0x40, 0x42, 0x44, 0x46, 0x48, 0x4a, 0x4c, 0x4e, 
    0x50, 0x52, 0x54, 0x56, 0x58, 0x5a, 0x5c, 0x2, 0xb, 0x3, 0x2, 0xae, 
    0xaf, 0x3, 0x2, 0x5a, 0x6b, 0x3, 0x2, 0x6c, 0x85, 0x3, 0x2, 0x86, 0x91, 
    0x3, 0x2, 0x92, 0x98, 0x5, 0x2, 0x5a, 0x5b, 0x64, 0x66, 0x99, 0x9e, 
    0x3, 0x2, 0x9f, 0xa2, 0x3, 0x2, 0xa8, 0xaa, 0x3, 0x2, 0xae, 0xb0, 0x2, 
    0x426, 0x2, 0x61, 0x3, 0x2, 0x2, 0x2, 0x4, 0xa8, 0x3, 0x2, 0x2, 0x2, 
    0x6, 0xf5, 0x3, 0x2, 0x2, 0x2, 0x8, 0xf7, 0x3, 0x2, 0x2, 0x2, 0xa, 0x117, 
    0x3, 0x2, 0x2, 0x2, 0xc, 0x11b, 0x3, 0x2, 0x2, 0x2, 0xe, 0x11f, 0x3, 
    0x2, 0x2, 0x2, 0x10, 0x123, 0x3, 0x2, 0x2, 0x2, 0x12, 0x12b, 0x3, 0x2, 
    0x2, 0x2, 0x14, 0x12d, 0x3, 0x2, 0x2, 0x2, 0x16, 0x12f, 0x3, 0x2, 0x2, 
    0x2, 0x18, 0x131, 0x3, 0x2, 0x2, 0x2, 0x1a, 0x14e, 0x3, 0x2, 0x2, 0x2, 
    0x1c, 0x150, 0x3, 0x2, 0x2, 0x2, 0x1e, 0x160, 0x3, 0x2, 0x2, 0x2, 0x20, 
    0x162, 0x3, 0x2, 0x2, 0x2, 0x22, 0x175, 0x3, 0x2, 0x2, 0x2, 0x24, 0x33e, 
    0x3, 0x2, 0x2, 0x2, 0x26, 0x34b, 0x3, 0x2, 0x2, 0x2, 0x28, 0x34d, 0x3, 
    0x2, 0x2, 0x2, 0x2a, 0x350, 0x3, 0x2, 0x2, 0x2, 0x2c, 0x352, 0x3, 0x2, 
    0x2, 0x2, 0x2e, 0x35b, 0x3, 0x2, 0x2, 0x2, 0x30, 0x361, 0x3, 0x2, 0x2, 
    0x2, 0x32, 0x36b, 0x3, 0x2, 0x2, 0x2, 0x34, 0x377, 0x3, 0x2, 0x2, 0x2, 
    0x36, 0x37f, 0x3, 0x2, 0x2, 0x2, 0x38, 0x381, 0x3, 0x2, 0x2, 0x2, 0x3a, 
    0x383, 0x3, 0x2, 0x2, 0x2, 0x3c, 0x385, 0x3, 0x2, 0x2, 0x2, 0x3e, 0x387, 
    0x3, 0x2, 0x2, 0x2, 0x40, 0x389, 0x3, 0x2, 0x2, 0x2, 0x42, 0x38b, 0x3, 
    0x2, 0x2, 0x2, 0x44, 0x38d, 0x3, 0x2, 0x2, 0x2, 0x46, 0x391, 0x3, 0x2, 
    0x2, 0x2, 0x48, 0x393, 0x3, 0x2, 0x2, 0x2, 0x4a, 0x395, 0x3, 0x2, 0x2, 
    0x2, 0x4c, 0x397, 0x3, 0x2, 0x2, 0x2, 0x4e, 0x399, 0x3, 0x2, 0x2, 0x2, 
    0x50, 0x3a6, 0x3, 0x2, 0x2, 0x2, 0x52, 0x3b3, 0x3, 0x2, 0x2, 0x2, 0x54, 
    0x3b5, 0x3, 0x2, 0x2, 0x2, 0x56, 0x3b7, 0x3, 0x2, 0x2, 0x2, 0x58, 0x3b9, 
    0x3, 0x2, 0x2, 0x2, 0x5a, 0x3bb, 0x3, 0x2, 0x2, 0x2, 0x5c, 0x3bd, 0x3, 
    0x2, 0x2, 0x2, 0x5e, 0x60, 0x5, 0x4, 0x3, 0x2, 0x5f, 0x5e, 0x3, 0x2, 
    0x2, 0x2, 0x60, 0x63, 0x3, 0x2, 0x2, 0x2, 0x61, 0x5f, 0x3, 0x2, 0x2, 
    0x2, 0x61, 0x62, 0x3, 0x2, 0x2, 0x2, 0x62, 0x64, 0x3, 0x2, 0x2, 0x2, 
    0x63, 0x61, 0x3, 0x2, 0x2, 0x2, 0x64, 0x65, 0x7, 0x2, 0x2, 0x3, 0x65, 
    0x3, 0x3, 0x2, 0x2, 0x2, 0x66, 0x67, 0x7, 0x3, 0x2, 0x2, 0x67, 0xa9, 
    0x9, 0x2, 0x2, 0x2, 0x68, 0x69, 0x7, 0x4, 0x2, 0x2, 0x69, 0x6a, 0x5, 
    0x56, 0x2c, 0x2, 0x6a, 0x6b, 0x7, 0x5, 0x2, 0x2, 0x6b, 0x6c, 0x5, 0x6, 
    0x4, 0x2, 0x6c, 0xa9, 0x3, 0x2, 0x2, 0x2, 0x6d, 0x6e, 0x7, 0x6, 0x2, 
    0x2, 0x6e, 0x6f, 0x5, 0x56, 0x2c, 0x2, 0x6f, 0x70, 0x7, 0x5, 0x2, 0x2, 
    0x70, 0x71, 0x5, 0x8, 0x5, 0x2, 0x71, 0xa9, 0x3, 0x2, 0x2, 0x2, 0x72, 
    0x73, 0x7, 0x7, 0x2, 0x2, 0x73, 0x74, 0x5, 0x56, 0x2c, 0x2, 0x74, 0x75, 
    0x7, 0x8, 0x2, 0x2, 0x75, 0x76, 0x5, 0xc, 0x7, 0x2, 0x76, 0x77, 0x7, 
    0x9, 0x2, 0x2, 0x77, 0x78, 0x7, 0x5, 0x2, 0x2, 0x78, 0x79, 0x5, 0xa, 
    0x6, 0x2, 0x79, 0xa9, 0x3, 0x2, 0x2, 0x2, 0x7a, 0x7b, 0x7, 0xa, 0x2, 
    0x2, 0x7b, 0x7c, 0x5, 0x56, 0x2c, 0x2, 0x7c, 0x7d, 0x7, 0x8, 0x2, 0x2, 
    0x7d, 0x7e, 0x5, 0xc, 0x7, 0x2, 0x7e, 0x7f, 0x7, 0x9, 0x2, 0x2, 0x7f, 
    0xa9, 0x3, 0x2, 0x2, 0x2, 0x80, 0x81, 0x7, 0xb, 0x2, 0x2, 0x81, 0x82, 
    0x5, 0x14, 0xb, 0x2, 0x82, 0x83, 0x7, 0x8, 0x2, 0x2, 0x83, 0x84, 0x5, 
    0xe, 0x8, 0x2, 0x84, 0x85, 0x7, 0x9, 0x2, 0x2, 0x85, 0xa9, 0x3, 0x2, 
    0x2, 0x2, 0x86, 0x87, 0x7, 0xc, 0x2, 0x2, 0x87, 0x88, 0x5, 0x56, 0x2c, 
    0x2, 0x88, 0x89, 0x7, 0x5, 0x2, 0x2, 0x89, 0x8a, 0x5, 0x14, 0xb, 0x2, 
    0x8a, 0x8b, 0x5, 0x44, 0x23, 0x2, 0x8b, 0x8c, 0x5, 0x12, 0xa, 0x2, 0x8c, 
    0xa9, 0x3, 0x2, 0x2, 0x2, 0x8d, 0x8e, 0x7, 0xd, 0x2, 0x2, 0x8e, 0x9e, 
    0x5, 0x14, 0xb, 0x2, 0x8f, 0x90, 0x7, 0x8, 0x2, 0x2, 0x90, 0x91, 0x5, 
    0xe, 0x8, 0x2, 0x91, 0x94, 0x7, 0x9, 0x2, 0x2, 0x92, 0x93, 0x7, 0xe, 
    0x2, 0x2, 0x93, 0x95, 0x5, 0x58, 0x2d, 0x2, 0x94, 0x92, 0x3, 0x2, 0x2, 
    0x2, 0x94, 0x95, 0x3, 0x2, 0x2, 0x2, 0x95, 0x9f, 0x3, 0x2, 0x2, 0x2, 
    0x96, 0x97, 0x7, 0xe, 0x2, 0x2, 0x97, 0x9c, 0x5, 0x58, 0x2d, 0x2, 0x98, 
    0x99, 0x7, 0x8, 0x2, 0x2, 0x99, 0x9a, 0x5, 0xe, 0x8, 0x2, 0x9a, 0x9b, 
    0x7, 0x9, 0x2, 0x2, 0x9b, 0x9d, 0x3, 0x2, 0x2, 0x2, 0x9c, 0x98, 0x3, 
    0x2, 0x2, 0x2, 0x9c, 0x9d, 0x3, 0x2, 0x2, 0x2, 0x9d, 0x9f, 0x3, 0x2, 
    0x2, 0x2, 0x9e, 0x8f, 0x3, 0x2, 0x2, 0x2, 0x9e, 0x96, 0x3, 0x2, 0x2, 
    0x2, 0x9e, 0x9f, 0x3, 0x2, 0x2, 0x2, 0x9f, 0xa0, 0x3, 0x2, 0x2, 0x2, 
    0xa0, 0xa2, 0x7, 0xf, 0x2, 0x2, 0xa1, 0xa3, 0x5, 0x18, 0xd, 0x2, 0xa2, 
    0xa1, 0x3, 0x2, 0x2, 0x2, 0xa3, 0xa4, 0x3, 0x2, 0x2, 0x2, 0xa4, 0xa2, 
    0x3, 0x2, 0x2, 0x2, 0xa4, 0xa5, 0x3, 0x2, 0x2, 0x2, 0xa5, 0xa6, 0x3, 
    0x2, 0x2, 0x2, 0xa6, 0xa7, 0x7, 0x10, 0x2, 0x2, 0xa7, 0xa9, 0x3, 0x2, 
    0x2, 0x2, 0xa8, 0x66, 0x3, 0x2, 0x2, 0x2, 0xa8, 0x68, 0x3, 0x2, 0x2, 
    0x2, 0xa8, 0x6d, 0x3, 0x2, 0x2, 0x2, 0xa8, 0x72, 0x3, 0x2, 0x2, 0x2, 
    0xa8, 0x7a, 0x3, 0x2, 0x2, 0x2, 0xa8, 0x80, 0x3, 0x2, 0x2, 0x2, 0xa8, 
    0x86, 0x3, 0x2, 0x2, 0x2, 0xa8, 0x8d, 0x3, 0x2, 0x2, 0x2, 0xa9, 0x5, 
    0x3, 0x2, 0x2, 0x2, 0xaa, 0xab, 0x7, 0x11, 0x2, 0x2, 0xab, 0xac, 0x7, 
    0x8, 0x2, 0x2, 0xac, 0xad, 0x5, 0x4a, 0x26, 0x2, 0xad, 0xae, 0x7, 0x9, 
    0x2, 0x2, 0xae, 0xf6, 0x3, 0x2, 0x2, 0x2, 0xaf, 0xf6, 0x7, 0x12, 0x2, 
    0x2, 0xb0, 0xf6, 0x7, 0x13, 0x2, 0x2, 0xb1, 0xb2, 0x7, 0x14, 0x2, 0x2, 
    0xb2, 0xb3, 0x7, 0x8, 0x2, 0x2, 0xb3, 0xb4, 0x5, 0xc, 0x7, 0x2, 0xb4, 
    0xb5, 0x7, 0x9, 0x2, 0x2, 0xb5, 0xf6, 0x3, 0x2, 0x2, 0x2, 0xb6, 0xb7, 
    0x7, 0x15, 0x2, 0x2, 0xb7, 0xb8, 0x7, 0x8, 0x2, 0x2, 0xb8, 0xb9, 0x5, 
    0xe, 0x8, 0x2, 0xb9, 0xba, 0x7, 0x9, 0x2, 0x2, 0xba, 0xf6, 0x3, 0x2, 
    0x2, 0x2, 0xbb, 0xbc, 0x7, 0x16, 0x2, 0x2, 0xbc, 0xbe, 0x7, 0x8, 0x2, 
    0x2, 0xbd, 0xbf, 0x5, 0xc, 0x7, 0x2, 0xbe, 0xbd, 0x3, 0x2, 0x2, 0x2, 
    0xbf, 0xc0, 0x3, 0x2, 0x2, 0x2, 0xc0, 0xbe, 0x3, 0x2, 0x2, 0x2, 0xc0, 
    0xc1, 0x3, 0x2, 0x2, 0x2, 0xc1, 0xc2, 0x3, 0x2, 0x2, 0x2, 0xc2, 0xc3, 
    0x7, 0x9, 0x2, 0x2, 0xc3, 0xf6, 0x3, 0x2, 0x2, 0x2, 0xc4, 0xc5, 0x7, 
    0x17, 0x2, 0x2, 0xc5, 0xc9, 0x7, 0x8, 0x2, 0x2, 0xc6, 0xc8, 0x5, 0xc, 
    0x7, 0x2, 0xc7, 0xc6, 0x3, 0x2, 0x2, 0x2, 0xc8, 0xcb, 0x3, 0x2, 0x2, 
    0x2, 0xc9, 0xc7, 0x3, 0x2, 0x2, 0x2, 0xc9, 0xca, 0x3, 0x2, 0x2, 0x2, 
    0xca, 0xcc, 0x3, 0x2, 0x2, 0x2, 0xcb, 0xc9, 0x3, 0x2, 0x2, 0x2, 0xcc, 
    0xcd, 0x5, 0xc, 0x7, 0x2, 0xcd, 0xce, 0x7, 0x9, 0x2, 0x2, 0xce, 0xf6, 
    0x3, 0x2, 0x2, 0x2, 0xcf, 0xd0, 0x7, 0x18, 0x2, 0x2, 0xd0, 0xd1, 0x7, 
    0x8, 0x2, 0x2, 0xd1, 0xd2, 0x5, 0xc, 0x7, 0x2, 0xd2, 0xd3, 0x5, 0x4c, 
    0x27, 0x2, 0xd3, 0xd4, 0x7, 0x9, 0x2, 0x2, 0xd4, 0xf6, 0x3, 0x2, 0x2, 
    0x2, 0xd5, 0xd6, 0x7, 0x19, 0x2, 0x2, 0xd6, 0xd7, 0x7, 0x8, 0x2, 0x2, 
    0xd7, 0xd8, 0x5, 0xc, 0x7, 0x2, 0xd8, 0xd9, 0x5, 0x4c, 0x27, 0x2, 0xd9, 
    0xda, 0x7, 0x9, 0x2, 0x2, 0xda, 0xf6, 0x3, 0x2, 0x2, 0x2, 0xdb, 0xf6, 
    0x7, 0x1a, 0x2, 0x2, 0xdc, 0xdd, 0x7, 0x1b, 0x2, 0x2, 0xdd, 0xde, 0x7, 
    0x8, 0x2, 0x2, 0xde, 0xdf, 0x5, 0xc, 0x7, 0x2, 0xdf, 0xe0, 0x7, 0x9, 
    0x2, 0x2, 0xe0, 0xf6, 0x3, 0x2, 0x2, 0x2, 0xe1, 0xe2, 0x7, 0x1c, 0x2, 
    0x2, 0xe2, 0xe3, 0x7, 0x8, 0x2, 0x2, 0xe3, 0xe4, 0x5, 0xc, 0x7, 0x2, 
    0xe4, 0xe5, 0x7, 0x9, 0x2, 0x2, 0xe5, 0xf6, 0x3, 0x2, 0x2, 0x2, 0xe6, 
    0xe7, 0x7, 0x1d, 0x2, 0x2, 0xe7, 0xe8, 0x7, 0x8, 0x2, 0x2, 0xe8, 0xe9, 
    0x5, 0xc, 0x7, 0x2, 0xe9, 0xea, 0x7, 0x9, 0x2, 0x2, 0xea, 0xf6, 0x3, 
    0x2, 0x2, 0x2, 0xeb, 0xf6, 0x7, 0x1e, 0x2, 0x2, 0xec, 0xed, 0x7, 0x1f, 
    0x2, 0x2, 0xed, 0xee, 0x7, 0x8, 0x2, 0x2, 0xee, 0xef, 0x5, 0xe, 0x8, 
    0x2, 0xef, 0xf0, 0x7, 0x9, 0x2, 0x2, 0xf0, 0xf6, 0x3, 0x2, 0x2, 0x2, 
    0xf1, 0xf6, 0x7, 0x20, 0x2, 0x2, 0xf2, 0xf6, 0x7, 0x21, 0x2, 0x2, 0xf3, 
    0xf6, 0x7, 0x22, 0x2, 0x2, 0xf4, 0xf6, 0x7, 0x23, 0x2, 0x2, 0xf5, 0xaa, 
    0x3, 0x2, 0x2, 0x2, 0xf5, 0xaf, 0x3, 0x2, 0x2, 0x2, 0xf5, 0xb0, 0x3, 
    0x2, 0x2, 0x2, 0xf5, 0xb1, 0x3, 0x2, 0x2, 0x2, 0xf5, 0xb6, 0x3, 0x2, 
    0x2, 0x2, 0xf5, 0xbb, 0x3, 0x2, 0x2, 0x2, 0xf5, 0xc4, 0x3, 0x2, 0x2, 
    0x2, 0xf5, 0xcf, 0x3, 0x2, 0x2, 0x2, 0xf5, 0xd5, 0x3, 0x2, 0x2, 0x2, 
    0xf5, 0xdb, 0x3, 0x2, 0x2, 0x2, 0xf5, 0xdc, 0x3, 0x2, 0x2, 0x2, 0xf5, 
    0xe1, 0x3, 0x2, 0x2, 0x2, 0xf5, 0xe6, 0x3, 0x2, 0x2, 0x2, 0xf5, 0xeb, 
    0x3, 0x2, 0x2, 0x2, 0xf5, 0xec, 0x3, 0x2, 0x2, 0x2, 0xf5, 0xf1, 0x3, 
    0x2, 0x2, 0x2, 0xf5, 0xf2, 0x3, 0x2, 0x2, 0x2, 0xf5, 0xf3, 0x3, 0x2, 
    0x2, 0x2, 0xf5, 0xf4, 0x3, 0x2, 0x2, 0x2, 0xf6, 0x7, 0x3, 0x2, 0x2, 
    0x2, 0xf7, 0xfb, 0x7, 0x24, 0x2, 0x2, 0xf8, 0xfa, 0x5, 0xc, 0x7, 0x2, 
    0xf9, 0xf8, 0x3, 0x2, 0x2, 0x2, 0xfa, 0xfd, 0x3, 0x2, 0x2, 0x2, 0xfb, 
    0xf9, 0x3, 0x2, 0x2, 0x2, 0xfb, 0xfc, 0x3, 0x2, 0x2, 0x2, 0xfc, 0xfe, 
    0x3, 0x2, 0x2, 0x2, 0xfd, 0xfb, 0x3, 0x2, 0x2, 0x2, 0xfe, 0xff, 0x7, 
    0x25, 0x2, 0x2, 0xff, 0x100, 0x7, 0x26, 0x2, 0x2, 0x100, 0x104, 0x7, 
    0x24, 0x2, 0x2, 0x101, 0x103, 0x5, 0xc, 0x7, 0x2, 0x102, 0x101, 0x3, 
    0x2, 0x2, 0x2, 0x103, 0x106, 0x3, 0x2, 0x2, 0x2, 0x104, 0x102, 0x3, 
    0x2, 0x2, 0x2, 0x104, 0x105, 0x3, 0x2, 0x2, 0x2, 0x105, 0x107, 0x3, 
    0x2, 0x2, 0x2, 0x106, 0x104, 0x3, 0x2, 0x2, 0x2, 0x107, 0x108, 0x7, 
    0x25, 0x2, 0x2, 0x108, 0x9, 0x3, 0x2, 0x2, 0x2, 0x109, 0x118, 0x5, 0x4e, 
    0x28, 0x2, 0x10a, 0x118, 0x5, 0x50, 0x29, 0x2, 0x10b, 0x118, 0x5, 0x52, 
    0x2a, 0x2, 0x10c, 0x118, 0x7, 0x27, 0x2, 0x2, 0x10d, 0x111, 0x7, 0xf, 
    0x2, 0x2, 0x10e, 0x110, 0x5, 0x16, 0xc, 0x2, 0x10f, 0x10e, 0x3, 0x2, 
    0x2, 0x2, 0x110, 0x113, 0x3, 0x2, 0x2, 0x2, 0x111, 0x10f, 0x3, 0x2, 
    0x2, 0x2, 0x111, 0x112, 0x3, 0x2, 0x2, 0x2, 0x112, 0x114, 0x3, 0x2, 
    0x2, 0x2, 0x113, 0x111, 0x3, 0x2, 0x2, 0x2, 0x114, 0x118, 0x7, 0x10, 
    0x2, 0x2, 0x115, 0x116, 0x7, 0x28, 0x2, 0x2, 0x116, 0x118, 0x5, 0x54, 
    0x2b, 0x2, 0x117, 0x109, 0x3, 0x2, 0x2, 0x2, 0x117, 0x10a, 0x3, 0x2, 
    0x2, 0x2, 0x117, 0x10b, 0x3, 0x2, 0x2, 0x2, 0x117, 0x10c, 0x3, 0x2, 
    0x2, 0x2, 0x117, 0x10d, 0x3, 0x2, 0x2, 0x2, 0x117, 0x115, 0x3, 0x2, 
    0x2, 0x2, 0x118, 0xb, 0x3, 0x2, 0x2, 0x2, 0x119, 0x11c, 0x5, 0x56, 0x2c, 
    0x2, 0x11a, 0x11c, 0x5, 0x6, 0x4, 0x2, 0x11b, 0x119, 0x3, 0x2, 0x2, 
    0x2, 0x11b, 0x11a, 0x3, 0x2, 0x2, 0x2, 0x11c, 0xd, 0x3, 0x2, 0x2, 0x2, 
    0x11d, 0x120, 0x5, 0x56, 0x2c, 0x2, 0x11e, 0x120, 0x5, 0x8, 0x5, 0x2, 
    0x11f, 0x11d, 0x3, 0x2, 0x2, 0x2, 0x11f, 0x11e, 0x3, 0x2, 0x2, 0x2, 
    0x120, 0xf, 0x3, 0x2, 0x2, 0x2, 0x121, 0x124, 0x5, 0x5a, 0x2e, 0x2, 
    0x122, 0x124, 0x5, 0x12, 0xa, 0x2, 0x123, 0x121, 0x3, 0x2, 0x2, 0x2, 
    0x123, 0x122, 0x3, 0x2, 0x2, 0x2, 0x124, 0x11, 0x3, 0x2, 0x2, 0x2, 0x125, 
    0x12c, 0x5, 0x56, 0x2c, 0x2, 0x126, 0x127, 0x7, 0x8, 0x2, 0x2, 0x127, 
    0x128, 0x5, 0xc, 0x7, 0x2, 0x128, 0x129, 0x7, 0x9, 0x2, 0x2, 0x129, 
    0x12a, 0x5, 0xa, 0x6, 0x2, 0x12a, 0x12c, 0x3, 0x2, 0x2, 0x2, 0x12b, 
    0x125, 0x3, 0x2, 0x2, 0x2, 0x12b, 0x126, 0x3, 0x2, 0x2, 0x2, 0x12c, 
    0x13, 0x3, 0x2, 0x2, 0x2, 0x12d, 0x12e, 0x5, 0x56, 0x2c, 0x2, 0x12e, 
    0x15, 0x3, 0x2, 0x2, 0x2, 0x12f, 0x130, 0x5, 0x56, 0x2c, 0x2, 0x130, 
    0x17, 0x3, 0x2, 0x2, 0x2, 0x131, 0x132, 0x5, 0x58, 0x2d, 0x2, 0x132, 
    0x13a, 0x7, 0x24, 0x2, 0x2, 0x133, 0x134, 0x7, 0x8, 0x2, 0x2, 0x134, 
    0x135, 0x5, 0xc, 0x7, 0x2, 0x135, 0x136, 0x7, 0x9, 0x2, 0x2, 0x136, 
    0x137, 0x5, 0x58, 0x2d, 0x2, 0x137, 0x139, 0x3, 0x2, 0x2, 0x2, 0x138, 
    0x133, 0x3, 0x2, 0x2, 0x2, 0x139, 0x13c, 0x3, 0x2, 0x2, 0x2, 0x13a, 
    0x138, 0x3, 0x2, 0x2, 0x2, 0x13a, 0x13b, 0x3, 0x2, 0x2, 0x2, 0x13b, 
    0x13d, 0x3, 0x2, 0x2, 0x2, 0x13c, 0x13a, 0x3, 0x2, 0x2, 0x2, 0x13d, 
    0x142, 0x7, 0x25, 0x2, 0x2, 0x13e, 0x13f, 0x7, 0x29, 0x2, 0x2, 0x13f, 
    0x140, 0x5, 0x58, 0x2d, 0x2, 0x140, 0x141, 0x7, 0x2a, 0x2, 0x2, 0x141, 
    0x143, 0x3, 0x2, 0x2, 0x2, 0x142, 0x13e, 0x3, 0x2, 0x2, 0x2, 0x142, 
    0x143, 0x3, 0x2, 0x2, 0x2, 0x143, 0x144, 0x3, 0x2, 0x2, 0x2, 0x144, 
    0x146, 0x7, 0x2b, 0x2, 0x2, 0x145, 0x147, 0x5, 0x24, 0x13, 0x2, 0x146, 
    0x145, 0x3, 0x2, 0x2, 0x2, 0x147, 0x148, 0x3, 0x2, 0x2, 0x2, 0x148, 
    0x146, 0x3, 0x2, 0x2, 0x2, 0x148, 0x149, 0x3, 0x2, 0x2, 0x2, 0x149, 
    0x19, 0x3, 0x2, 0x2, 0x2, 0x14a, 0x14b, 0x7, 0x29, 0x2, 0x2, 0x14b, 
    0x14c, 0x5, 0x58, 0x2d, 0x2, 0x14c, 0x14d, 0x7, 0x2a, 0x2, 0x2, 0x14d, 
    0x14f, 0x3, 0x2, 0x2, 0x2, 0x14e, 0x14a, 0x3, 0x2, 0x2, 0x2, 0x14e, 
    0x14f, 0x3, 0x2, 0x2, 0x2, 0x14f, 0x1b, 0x3, 0x2, 0x2, 0x2, 0x150, 0x151, 
    0x5, 0x58, 0x2d, 0x2, 0x151, 0x152, 0x7, 0x5, 0x2, 0x2, 0x152, 0x1d, 
    0x3, 0x2, 0x2, 0x2, 0x153, 0x154, 0x5, 0x58, 0x2d, 0x2, 0x154, 0x155, 
    0x7, 0x5, 0x2, 0x2, 0x155, 0x161, 0x3, 0x2, 0x2, 0x2, 0x156, 0x15a, 
    0x7, 0x24, 0x2, 0x2, 0x157, 0x159, 0x5, 0x58, 0x2d, 0x2, 0x158, 0x157, 
    0x3, 0x2, 0x2, 0x2, 0x159, 0x15c, 0x3, 0x2, 0x2, 0x2, 0x15a, 0x158, 
    0x3, 0x2, 0x2, 0x2, 0x15a, 0x15b, 0x3, 0x2, 0x2, 0x2, 0x15b, 0x15d, 
    0x3, 0x2, 0x2, 0x2, 0x15c, 0x15a, 0x3, 0x2, 0x2, 0x2, 0x15d, 0x15e, 
    0x7, 0x25, 0x2, 0x2, 0x15e, 0x161, 0x7, 0x5, 0x2, 0x2, 0x15f, 0x161, 
    0x3, 0x2, 0x2, 0x2, 0x160, 0x153, 0x3, 0x2, 0x2, 0x2, 0x160, 0x156, 
    0x3, 0x2, 0x2, 0x2, 0x160, 0x15f, 0x3, 0x2, 0x2, 0x2, 0x161, 0x1f, 0x3, 
    0x2, 0x2, 0x2, 0x162, 0x163, 0x7, 0x24, 0x2, 0x2, 0x163, 0x164, 0x5, 
    0x58, 0x2d, 0x2, 0x164, 0x165, 0x5, 0x58, 0x2d, 0x2, 0x165, 0x166, 0x7, 
    0x25, 0x2, 0x2, 0x166, 0x167, 0x7, 0x5, 0x2, 0x2, 0x167, 0x21, 0x3, 
    0x2, 0x2, 0x2, 0x168, 0x169, 0x5, 0x58, 0x2d, 0x2, 0x169, 0x16a, 0x7, 
    0x5, 0x2, 0x2, 0x16a, 0x176, 0x3, 0x2, 0x2, 0x2, 0x16b, 0x16c, 0x7, 
    0x24, 0x2, 0x2, 0x16c, 0x16e, 0x5, 0x58, 0x2d, 0x2, 0x16d, 0x16f, 0x5, 
    0x58, 0x2d, 0x2, 0x16e, 0x16d, 0x3, 0x2, 0x2, 0x2, 0x16f, 0x170, 0x3, 
    0x2, 0x2, 0x2, 0x170, 0x16e, 0x3, 0x2, 0x2, 0x2, 0x170, 0x171, 0x3, 
    0x2, 0x2, 0x2, 0x171, 0x172, 0x3, 0x2, 0x2, 0x2, 0x172, 0x173, 0x7, 
    0x25, 0x2, 0x2, 0x173, 0x174, 0x7, 0x5, 0x2, 0x2, 0x174, 0x176, 0x3, 
    0x2, 0x2, 0x2, 0x175, 0x168, 0x3, 0x2, 0x2, 0x2, 0x175, 0x16b, 0x3, 
    0x2, 0x2, 0x2, 0x176, 0x23, 0x3, 0x2, 0x2, 0x2, 0x177, 0x178, 0x5, 0x1c, 
    0xf, 0x2, 0x178, 0x179, 0x5, 0x1a, 0xe, 0x2, 0x179, 0x17a, 0x5, 0x38, 
    0x1d, 0x2, 0x17a, 0x17b, 0x7, 0x8, 0x2, 0x2, 0x17b, 0x17c, 0x5, 0xc, 
    0x7, 0x2, 0x17c, 0x17d, 0x7, 0x9, 0x2, 0x2, 0x17d, 0x17e, 0x5, 0x10, 
    0x9, 0x2, 0x17e, 0x180, 0x5, 0x10, 0x9, 0x2, 0x17f, 0x181, 0x5, 0x2e, 
    0x18, 0x2, 0x180, 0x17f, 0x3, 0x2, 0x2, 0x2, 0x180, 0x181, 0x3, 0x2, 
    0x2, 0x2, 0x181, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x182, 0x183, 0x5, 0x22, 
    0x12, 0x2, 0x183, 0x184, 0x5, 0x1a, 0xe, 0x2, 0x184, 0x185, 0x5, 0x38, 
    0x1d, 0x2, 0x185, 0x186, 0x5, 0x42, 0x22, 0x2, 0x186, 0x187, 0x7, 0x8, 
    0x2, 0x2, 0x187, 0x188, 0x5, 0xc, 0x7, 0x2, 0x188, 0x189, 0x7, 0x9, 
    0x2, 0x2, 0x189, 0x18a, 0x5, 0x10, 0x9, 0x2, 0x18a, 0x18c, 0x5, 0x10, 
    0x9, 0x2, 0x18b, 0x18d, 0x5, 0x2e, 0x18, 0x2, 0x18c, 0x18b, 0x3, 0x2, 
    0x2, 0x2, 0x18c, 0x18d, 0x3, 0x2, 0x2, 0x2, 0x18d, 0x33f, 0x3, 0x2, 
    0x2, 0x2, 0x18e, 0x18f, 0x5, 0x1c, 0xf, 0x2, 0x18f, 0x190, 0x5, 0x1a, 
    0xe, 0x2, 0x190, 0x191, 0x5, 0x3a, 0x1e, 0x2, 0x191, 0x192, 0x7, 0x8, 
    0x2, 0x2, 0x192, 0x193, 0x5, 0xc, 0x7, 0x2, 0x193, 0x194, 0x7, 0x9, 
    0x2, 0x2, 0x194, 0x195, 0x5, 0x10, 0x9, 0x2, 0x195, 0x196, 0x5, 0x10, 
    0x9, 0x2, 0x196, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x197, 0x198, 0x5, 0x1c, 
    0xf, 0x2, 0x198, 0x199, 0x5, 0x1a, 0xe, 0x2, 0x199, 0x19a, 0x5, 0x3c, 
    0x1f, 0x2, 0x19a, 0x19b, 0x7, 0x8, 0x2, 0x2, 0x19b, 0x19c, 0x5, 0xc, 
    0x7, 0x2, 0x19c, 0x19d, 0x5, 0xc, 0x7, 0x2, 0x19d, 0x19e, 0x7, 0x9, 
    0x2, 0x2, 0x19e, 0x19f, 0x5, 0x10, 0x9, 0x2, 0x19f, 0x33f, 0x3, 0x2, 
    0x2, 0x2, 0x1a0, 0x1a1, 0x5, 0x1c, 0xf, 0x2, 0x1a1, 0x1a2, 0x5, 0x1a, 
    0xe, 0x2, 0x1a2, 0x1a3, 0x7, 0x2c, 0x2, 0x2, 0x1a3, 0x1a4, 0x7, 0x8, 
    0x2, 0x2, 0x1a4, 0x1a5, 0x5, 0xc, 0x7, 0x2, 0x1a5, 0x1a6, 0x5, 0xc, 
    0x7, 0x2, 0x1a6, 0x1a7, 0x7, 0x9, 0x2, 0x2, 0x1a7, 0x1a8, 0x5, 0x10, 
    0x9, 0x2, 0x1a8, 0x1a9, 0x5, 0x10, 0x9, 0x2, 0x1a9, 0x1aa, 0x5, 0x10, 
    0x9, 0x2, 0x1aa, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x1ab, 0x1ac, 0x5, 0x1a, 
    0xe, 0x2, 0x1ac, 0x1ad, 0x7, 0x2d, 0x2, 0x2, 0x1ad, 0x1ae, 0x5, 0x28, 
    0x15, 0x2, 0x1ae, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x1af, 0x1b0, 0x5, 0x1a, 
    0xe, 0x2, 0x1b0, 0x1b1, 0x7, 0x2e, 0x2, 0x2, 0x1b1, 0x1b2, 0x5, 0x10, 
    0x9, 0x2, 0x1b2, 0x1b3, 0x5, 0x28, 0x15, 0x2, 0x1b3, 0x1b4, 0x5, 0x28, 
    0x15, 0x2, 0x1b4, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x1b5, 0x1b6, 0x5, 0x1a, 
    0xe, 0x2, 0x1b6, 0x1b7, 0x7, 0x2f, 0x2, 0x2, 0x1b7, 0x1b8, 0x7, 0x8, 
    0x2, 0x2, 0x1b8, 0x1b9, 0x5, 0xc, 0x7, 0x2, 0x1b9, 0x1ba, 0x7, 0x9, 
    0x2, 0x2, 0x1ba, 0x1bb, 0x5, 0x10, 0x9, 0x2, 0x1bb, 0x1bc, 0x5, 0x28, 
    0x15, 0x2, 0x1bc, 0x1c2, 0x7, 0xf, 0x2, 0x2, 0x1bd, 0x1be, 0x5, 0x12, 
    0xa, 0x2, 0x1be, 0x1bf, 0x5, 0x28, 0x15, 0x2, 0x1bf, 0x1c1, 0x3, 0x2, 
    0x2, 0x2, 0x1c0, 0x1bd, 0x3, 0x2, 0x2, 0x2, 0x1c1, 0x1c4, 0x3, 0x2, 
    0x2, 0x2, 0x1c2, 0x1c0, 0x3, 0x2, 0x2, 0x2, 0x1c2, 0x1c3, 0x3, 0x2, 
    0x2, 0x2, 0x1c3, 0x1c5, 0x3, 0x2, 0x2, 0x2, 0x1c4, 0x1c2, 0x3, 0x2, 
    0x2, 0x2, 0x1c5, 0x1c6, 0x7, 0x10, 0x2, 0x2, 0x1c6, 0x33f, 0x3, 0x2, 
    0x2, 0x2, 0x1c7, 0x1c8, 0x5, 0x1e, 0x10, 0x2, 0x1c8, 0x1c9, 0x5, 0x1a, 
    0xe, 0x2, 0x1c9, 0x1ca, 0x7, 0x30, 0x2, 0x2, 0x1ca, 0x1cb, 0x7, 0x8, 
    0x2, 0x2, 0x1cb, 0x1cc, 0x5, 0xe, 0x8, 0x2, 0x1cc, 0x1cd, 0x7, 0x9, 
    0x2, 0x2, 0x1cd, 0x1ce, 0x5, 0x10, 0x9, 0x2, 0x1ce, 0x1d0, 0x5, 0x32, 
    0x1a, 0x2, 0x1cf, 0x1d1, 0x5, 0x2e, 0x18, 0x2, 0x1d0, 0x1cf, 0x3, 0x2, 
    0x2, 0x2, 0x1d0, 0x1d1, 0x3, 0x2, 0x2, 0x2, 0x1d1, 0x1d3, 0x3, 0x2, 
    0x2, 0x2, 0x1d2, 0x1d4, 0x5, 0x30, 0x19, 0x2, 0x1d3, 0x1d2, 0x3, 0x2, 
    0x2, 0x2, 0x1d3, 0x1d4, 0x3, 0x2, 0x2, 0x2, 0x1d4, 0x33f, 0x3, 0x2, 
    0x2, 0x2, 0x1d5, 0x1d6, 0x5, 0x1a, 0xe, 0x2, 0x1d6, 0x1d7, 0x7, 0x31, 
    0x2, 0x2, 0x1d7, 0x1d8, 0x7, 0x8, 0x2, 0x2, 0x1d8, 0x1d9, 0x5, 0xe, 
    0x8, 0x2, 0x1d9, 0x1da, 0x7, 0x9, 0x2, 0x2, 0x1da, 0x1db, 0x5, 0x10, 
    0x9, 0x2, 0x1db, 0x1dc, 0x5, 0x32, 0x1a, 0x2, 0x1dc, 0x33f, 0x3, 0x2, 
    0x2, 0x2, 0x1dd, 0x1de, 0x5, 0x1a, 0xe, 0x2, 0x1de, 0x1df, 0x7, 0x32, 
    0x2, 0x2, 0x1df, 0x1e0, 0x5, 0x26, 0x14, 0x2, 0x1e0, 0x33f, 0x3, 0x2, 
    0x2, 0x2, 0x1e1, 0x1e2, 0x5, 0x1a, 0xe, 0x2, 0x1e2, 0x1e3, 0x7, 0x33, 
    0x2, 0x2, 0x1e3, 0x1e4, 0x5, 0x10, 0x9, 0x2, 0x1e4, 0x33f, 0x3, 0x2, 
    0x2, 0x2, 0x1e5, 0x1e6, 0x5, 0x1c, 0xf, 0x2, 0x1e6, 0x1e7, 0x5, 0x1a, 
    0xe, 0x2, 0x1e7, 0x1e8, 0x7, 0x34, 0x2, 0x2, 0x1e8, 0x1e9, 0x7, 0x8, 
    0x2, 0x2, 0x1e9, 0x1ea, 0x5, 0xc, 0x7, 0x2, 0x1ea, 0x1eb, 0x5, 0x4a, 
    0x26, 0x2, 0x1eb, 0x1ec, 0x7, 0x9, 0x2, 0x2, 0x1ec, 0x1ed, 0x5, 0x10, 
    0x9, 0x2, 0x1ed, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x1ee, 0x1ef, 0x5, 0x1c, 
    0xf, 0x2, 0x1ef, 0x1f0, 0x5, 0x1a, 0xe, 0x2, 0x1f0, 0x1f1, 0x7, 0x35, 
    0x2, 0x2, 0x1f1, 0x1f2, 0x7, 0x8, 0x2, 0x2, 0x1f2, 0x1f3, 0x5, 0xc, 
    0x7, 0x2, 0x1f3, 0x1f4, 0x5, 0x4a, 0x26, 0x2, 0x1f4, 0x1f5, 0x7, 0x9, 
    0x2, 0x2, 0x1f5, 0x1f6, 0x5, 0x10, 0x9, 0x2, 0x1f6, 0x1f7, 0x5, 0x10, 
    0x9, 0x2, 0x1f7, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x1f8, 0x1f9, 0x5, 0x1c, 
    0xf, 0x2, 0x1f9, 0x1fa, 0x5, 0x1a, 0xe, 0x2, 0x1fa, 0x1fb, 0x7, 0x36, 
    0x2, 0x2, 0x1fb, 0x1fc, 0x7, 0x8, 0x2, 0x2, 0x1fc, 0x1fd, 0x5, 0xc, 
    0x7, 0x2, 0x1fd, 0x1fe, 0x5, 0xc, 0x7, 0x2, 0x1fe, 0x1ff, 0x7, 0x9, 
    0x2, 0x2, 0x1ff, 0x200, 0x5, 0x10, 0x9, 0x2, 0x200, 0x201, 0x5, 0x10, 
    0x9, 0x2, 0x201, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x202, 0x203, 0x5, 0x1c, 
    0xf, 0x2, 0x203, 0x204, 0x5, 0x1a, 0xe, 0x2, 0x204, 0x205, 0x7, 0x37, 
    0x2, 0x2, 0x205, 0x206, 0x7, 0x8, 0x2, 0x2, 0x206, 0x207, 0x5, 0xc, 
    0x7, 0x2, 0x207, 0x208, 0x5, 0xc, 0x7, 0x2, 0x208, 0x209, 0x7, 0x9, 
    0x2, 0x2, 0x209, 0x20a, 0x5, 0x10, 0x9, 0x2, 0x20a, 0x20b, 0x5, 0x10, 
    0x9, 0x2, 0x20b, 0x20c, 0x5, 0x10, 0x9, 0x2, 0x20c, 0x33f, 0x3, 0x2, 
    0x2, 0x2, 0x20d, 0x20e, 0x5, 0x1c, 0xf, 0x2, 0x20e, 0x20f, 0x5, 0x1a, 
    0xe, 0x2, 0x20f, 0x210, 0x7, 0x38, 0x2, 0x2, 0x210, 0x211, 0x7, 0x8, 
    0x2, 0x2, 0x211, 0x212, 0x5, 0xc, 0x7, 0x2, 0x212, 0x213, 0x5, 0xc, 
    0x7, 0x2, 0x213, 0x214, 0x7, 0x9, 0x2, 0x2, 0x214, 0x215, 0x5, 0x10, 
    0x9, 0x2, 0x215, 0x216, 0x5, 0x10, 0x9, 0x2, 0x216, 0x217, 0x5, 0x10, 
    0x9, 0x2, 0x217, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x218, 0x219, 0x5, 0x1c, 
    0xf, 0x2, 0x219, 0x21a, 0x5, 0x1a, 0xe, 0x2, 0x21a, 0x21b, 0x7, 0x39, 
    0x2, 0x2, 0x21b, 0x21c, 0x7, 0x8, 0x2, 0x2, 0x21c, 0x21d, 0x5, 0xc, 
    0x7, 0x2, 0x21d, 0x21f, 0x7, 0x9, 0x2, 0x2, 0x21e, 0x220, 0x5, 0x2e, 
    0x18, 0x2, 0x21f, 0x21e, 0x3, 0x2, 0x2, 0x2, 0x21f, 0x220, 0x3, 0x2, 
    0x2, 0x2, 0x220, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x221, 0x222, 0x5, 0x1c, 
    0xf, 0x2, 0x222, 0x223, 0x5, 0x1a, 0xe, 0x2, 0x223, 0x224, 0x7, 0x3a, 
    0x2, 0x2, 0x224, 0x225, 0x7, 0x8, 0x2, 0x2, 0x225, 0x226, 0x5, 0xc, 
    0x7, 0x2, 0x226, 0x227, 0x5, 0xc, 0x7, 0x2, 0x227, 0x228, 0x7, 0x9, 
    0x2, 0x2, 0x228, 0x22a, 0x5, 0x10, 0x9, 0x2, 0x229, 0x22b, 0x5, 0x2e, 
    0x18, 0x2, 0x22a, 0x229, 0x3, 0x2, 0x2, 0x2, 0x22a, 0x22b, 0x3, 0x2, 
    0x2, 0x2, 0x22b, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x22c, 0x22d, 0x5, 0x1c, 
    0xf, 0x2, 0x22d, 0x22e, 0x5, 0x1a, 0xe, 0x2, 0x22e, 0x22f, 0x7, 0x3b, 
    0x2, 0x2, 0x22f, 0x230, 0x7, 0x8, 0x2, 0x2, 0x230, 0x231, 0x5, 0xc, 
    0x7, 0x2, 0x231, 0x233, 0x7, 0x9, 0x2, 0x2, 0x232, 0x234, 0x5, 0x2e, 
    0x18, 0x2, 0x233, 0x232, 0x3, 0x2, 0x2, 0x2, 0x233, 0x234, 0x3, 0x2, 
    0x2, 0x2, 0x234, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x235, 0x236, 0x5, 0x1c, 
    0xf, 0x2, 0x236, 0x237, 0x5, 0x1a, 0xe, 0x2, 0x237, 0x238, 0x7, 0x3c, 
    0x2, 0x2, 0x238, 0x239, 0x7, 0x8, 0x2, 0x2, 0x239, 0x23a, 0x5, 0xc, 
    0x7, 0x2, 0x23a, 0x23b, 0x5, 0xc, 0x7, 0x2, 0x23b, 0x23c, 0x7, 0x9, 
    0x2, 0x2, 0x23c, 0x23e, 0x5, 0x10, 0x9, 0x2, 0x23d, 0x23f, 0x5, 0x2e, 
    0x18, 0x2, 0x23e, 0x23d, 0x3, 0x2, 0x2, 0x2, 0x23e, 0x23f, 0x3, 0x2, 
    0x2, 0x2, 0x23f, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x240, 0x241, 0x5, 0x1c, 
    0xf, 0x2, 0x241, 0x242, 0x5, 0x1a, 0xe, 0x2, 0x242, 0x243, 0x7, 0x3d, 
    0x2, 0x2, 0x243, 0x244, 0x7, 0x8, 0x2, 0x2, 0x244, 0x245, 0x5, 0xc, 
    0x7, 0x2, 0x245, 0x246, 0x7, 0x9, 0x2, 0x2, 0x246, 0x247, 0x5, 0x10, 
    0x9, 0x2, 0x247, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x248, 0x249, 0x5, 0x1c, 
    0xf, 0x2, 0x249, 0x24a, 0x5, 0x1a, 0xe, 0x2, 0x24a, 0x24c, 0x7, 0x3e, 
    0x2, 0x2, 0x24b, 0x24d, 0x7, 0x3f, 0x2, 0x2, 0x24c, 0x24b, 0x3, 0x2, 
    0x2, 0x2, 0x24c, 0x24d, 0x3, 0x2, 0x2, 0x2, 0x24d, 0x24e, 0x3, 0x2, 
    0x2, 0x2, 0x24e, 0x24f, 0x7, 0x8, 0x2, 0x2, 0x24f, 0x250, 0x5, 0xc, 
    0x7, 0x2, 0x250, 0x251, 0x5, 0x4a, 0x26, 0x2, 0x251, 0x252, 0x7, 0x9, 
    0x2, 0x2, 0x252, 0x253, 0x5, 0x10, 0x9, 0x2, 0x253, 0x33f, 0x3, 0x2, 
    0x2, 0x2, 0x254, 0x255, 0x5, 0x1c, 0xf, 0x2, 0x255, 0x256, 0x5, 0x1a, 
    0xe, 0x2, 0x256, 0x258, 0x7, 0x40, 0x2, 0x2, 0x257, 0x259, 0x7, 0x3f, 
    0x2, 0x2, 0x258, 0x257, 0x3, 0x2, 0x2, 0x2, 0x258, 0x259, 0x3, 0x2, 
    0x2, 0x2, 0x259, 0x25a, 0x3, 0x2, 0x2, 0x2, 0x25a, 0x25b, 0x7, 0x8, 
    0x2, 0x2, 0x25b, 0x25c, 0x5, 0xc, 0x7, 0x2, 0x25c, 0x25d, 0x5, 0xc, 
    0x7, 0x2, 0x25d, 0x25e, 0x7, 0x9, 0x2, 0x2, 0x25e, 0x25f, 0x5, 0x10, 
    0x9, 0x2, 0x25f, 0x260, 0x5, 0x10, 0x9, 0x2, 0x260, 0x33f, 0x3, 0x2, 
    0x2, 0x2, 0x261, 0x262, 0x5, 0x1c, 0xf, 0x2, 0x262, 0x263, 0x5, 0x1a, 
    0xe, 0x2, 0x263, 0x265, 0x7, 0x41, 0x2, 0x2, 0x264, 0x266, 0x7, 0x3f, 
    0x2, 0x2, 0x265, 0x264, 0x3, 0x2, 0x2, 0x2, 0x265, 0x266, 0x3, 0x2, 
    0x2, 0x2, 0x266, 0x267, 0x3, 0x2, 0x2, 0x2, 0x267, 0x268, 0x7, 0x8, 
    0x2, 0x2, 0x268, 0x269, 0x5, 0xc, 0x7, 0x2, 0x269, 0x26a, 0x5, 0xc, 
    0x7, 0x2, 0x26a, 0x26b, 0x7, 0x9, 0x2, 0x2, 0x26b, 0x26c, 0x5, 0x10, 
    0x9, 0x2, 0x26c, 0x26d, 0x5, 0x10, 0x9, 0x2, 0x26d, 0x33f, 0x3, 0x2, 
    0x2, 0x2, 0x26e, 0x26f, 0x5, 0x1c, 0xf, 0x2, 0x26f, 0x270, 0x5, 0x1a, 
    0xe, 0x2, 0x270, 0x272, 0x7, 0x42, 0x2, 0x2, 0x271, 0x273, 0x7, 0x3f, 
    0x2, 0x2, 0x272, 0x271, 0x3, 0x2, 0x2, 0x2, 0x272, 0x273, 0x3, 0x2, 
    0x2, 0x2, 0x273, 0x274, 0x3, 0x2, 0x2, 0x2, 0x274, 0x275, 0x7, 0x8, 
    0x2, 0x2, 0x275, 0x276, 0x5, 0xc, 0x7, 0x2, 0x276, 0x277, 0x7, 0x9, 
    0x2, 0x2, 0x277, 0x278, 0x5, 0x10, 0x9, 0x2, 0x278, 0x33f, 0x3, 0x2, 
    0x2, 0x2, 0x279, 0x27a, 0x5, 0x1c, 0xf, 0x2, 0x27a, 0x27b, 0x5, 0x1a, 
    0xe, 0x2, 0x27b, 0x27d, 0x7, 0x43, 0x2, 0x2, 0x27c, 0x27e, 0x7, 0x3f, 
    0x2, 0x2, 0x27d, 0x27c, 0x3, 0x2, 0x2, 0x2, 0x27d, 0x27e, 0x3, 0x2, 
    0x2, 0x2, 0x27e, 0x280, 0x3, 0x2, 0x2, 0x2, 0x27f, 0x281, 0x5, 0x3e, 
    0x20, 0x2, 0x280, 0x27f, 0x3, 0x2, 0x2, 0x2, 0x280, 0x281, 0x3, 0x2, 
    0x2, 0x2, 0x281, 0x282, 0x3, 0x2, 0x2, 0x2, 0x282, 0x283, 0x7, 0x8, 
    0x2, 0x2, 0x283, 0x284, 0x5, 0xc, 0x7, 0x2, 0x284, 0x285, 0x7, 0x9, 
    0x2, 0x2, 0x285, 0x287, 0x5, 0x10, 0x9, 0x2, 0x286, 0x288, 0x5, 0x2e, 
    0x18, 0x2, 0x287, 0x286, 0x3, 0x2, 0x2, 0x2, 0x287, 0x288, 0x3, 0x2, 
    0x2, 0x2, 0x288, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x289, 0x28a, 0x5, 0x1a, 
    0xe, 0x2, 0x28a, 0x28c, 0x7, 0x44, 0x2, 0x2, 0x28b, 0x28d, 0x7, 0x3f, 
    0x2, 0x2, 0x28c, 0x28b, 0x3, 0x2, 0x2, 0x2, 0x28c, 0x28d, 0x3, 0x2, 
    0x2, 0x2, 0x28d, 0x28f, 0x3, 0x2, 0x2, 0x2, 0x28e, 0x290, 0x5, 0x3e, 
    0x20, 0x2, 0x28f, 0x28e, 0x3, 0x2, 0x2, 0x2, 0x28f, 0x290, 0x3, 0x2, 
    0x2, 0x2, 0x290, 0x291, 0x3, 0x2, 0x2, 0x2, 0x291, 0x292, 0x7, 0x8, 
    0x2, 0x2, 0x292, 0x293, 0x5, 0xc, 0x7, 0x2, 0x293, 0x294, 0x7, 0x9, 
    0x2, 0x2, 0x294, 0x295, 0x5, 0x10, 0x9, 0x2, 0x295, 0x297, 0x5, 0x10, 
    0x9, 0x2, 0x296, 0x298, 0x5, 0x2e, 0x18, 0x2, 0x297, 0x296, 0x3, 0x2, 
    0x2, 0x2, 0x297, 0x298, 0x3, 0x2, 0x2, 0x2, 0x298, 0x33f, 0x3, 0x2, 
    0x2, 0x2, 0x299, 0x29a, 0x5, 0x20, 0x11, 0x2, 0x29a, 0x29b, 0x5, 0x1a, 
    0xe, 0x2, 0x29b, 0x29d, 0x7, 0x45, 0x2, 0x2, 0x29c, 0x29e, 0x7, 0x3f, 
    0x2, 0x2, 0x29d, 0x29c, 0x3, 0x2, 0x2, 0x2, 0x29d, 0x29e, 0x3, 0x2, 
    0x2, 0x2, 0x29e, 0x2a0, 0x3, 0x2, 0x2, 0x2, 0x29f, 0x2a1, 0x7, 0x46, 
    0x2, 0x2, 0x2a0, 0x29f, 0x3, 0x2, 0x2, 0x2, 0x2a0, 0x2a1, 0x3, 0x2, 
    0x2, 0x2, 0x2a1, 0x2a2, 0x3, 0x2, 0x2, 0x2, 0x2a2, 0x2a3, 0x5, 0x3e, 
    0x20, 0x2, 0x2a3, 0x2a4, 0x5, 0x3e, 0x20, 0x2, 0x2a4, 0x2a5, 0x7, 0x8, 
    0x2, 0x2, 0x2a5, 0x2a6, 0x5, 0xc, 0x7, 0x2, 0x2a6, 0x2a7, 0x7, 0x9, 
    0x2, 0x2, 0x2a7, 0x2a8, 0x5, 0x10, 0x9, 0x2, 0x2a8, 0x2a9, 0x5, 0x10, 
    0x9, 0x2, 0x2a9, 0x2ab, 0x5, 0x10, 0x9, 0x2, 0x2aa, 0x2ac, 0x5, 0x2e, 
    0x18, 0x2, 0x2ab, 0x2aa, 0x3, 0x2, 0x2, 0x2, 0x2ab, 0x2ac, 0x3, 0x2, 
    0x2, 0x2, 0x2ac, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x2ad, 0x2ae, 0x5, 0x1c, 
    0xf, 0x2, 0x2ae, 0x2af, 0x5, 0x1a, 0xe, 0x2, 0x2af, 0x2b1, 0x7, 0x47, 
    0x2, 0x2, 0x2b0, 0x2b2, 0x7, 0x3f, 0x2, 0x2, 0x2b1, 0x2b0, 0x3, 0x2, 
    0x2, 0x2, 0x2b1, 0x2b2, 0x3, 0x2, 0x2, 0x2, 0x2b2, 0x2b3, 0x3, 0x2, 
    0x2, 0x2, 0x2b3, 0x2b4, 0x5, 0x3e, 0x20, 0x2, 0x2b4, 0x2b5, 0x5, 0x40, 
    0x21, 0x2, 0x2b5, 0x2b6, 0x7, 0x8, 0x2, 0x2, 0x2b6, 0x2b7, 0x5, 0xc, 
    0x7, 0x2, 0x2b7, 0x2b8, 0x7, 0x9, 0x2, 0x2, 0x2b8, 0x2b9, 0x5, 0x10, 
    0x9, 0x2, 0x2b9, 0x2bb, 0x5, 0x10, 0x9, 0x2, 0x2ba, 0x2bc, 0x5, 0x2e, 
    0x18, 0x2, 0x2bb, 0x2ba, 0x3, 0x2, 0x2, 0x2, 0x2bb, 0x2bc, 0x3, 0x2, 
    0x2, 0x2, 0x2bc, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x2bd, 0x2be, 0x5, 0x1a, 
    0xe, 0x2, 0x2be, 0x2bf, 0x7, 0x48, 0x2, 0x2, 0x2bf, 0x2c0, 0x5, 0x3e, 
    0x20, 0x2, 0x2c0, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x2c1, 0x2c2, 0x5, 0x1e, 
    0x10, 0x2, 0x2c2, 0x2c3, 0x5, 0x1a, 0xe, 0x2, 0x2c3, 0x2c4, 0x7, 0x49, 
    0x2, 0x2, 0x2c4, 0x2c6, 0x5, 0x2c, 0x17, 0x2, 0x2c5, 0x2c7, 0x5, 0x2e, 
    0x18, 0x2, 0x2c6, 0x2c5, 0x3, 0x2, 0x2, 0x2, 0x2c6, 0x2c7, 0x3, 0x2, 
    0x2, 0x2, 0x2c7, 0x2c8, 0x3, 0x2, 0x2, 0x2, 0x2c8, 0x2c9, 0x5, 0x30, 
    0x19, 0x2, 0x2c9, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x2ca, 0x2cb, 0x5, 0x1e, 
    0x10, 0x2, 0x2cb, 0x2cc, 0x5, 0x1a, 0xe, 0x2, 0x2cc, 0x2cd, 0x7, 0x4a, 
    0x2, 0x2, 0x2cd, 0x2ce, 0x5, 0x48, 0x25, 0x2, 0x2ce, 0x2cf, 0x5, 0x2c, 
    0x17, 0x2, 0x2cf, 0x2d0, 0x5, 0x28, 0x15, 0x2, 0x2d0, 0x2d6, 0x5, 0x28, 
    0x15, 0x2, 0x2d1, 0x2d2, 0x7, 0x4b, 0x2, 0x2, 0x2d2, 0x2d3, 0x7, 0x24, 
    0x2, 0x2, 0x2d3, 0x2d4, 0x5, 0x28, 0x15, 0x2, 0x2d4, 0x2d5, 0x7, 0x25, 
    0x2, 0x2, 0x2d5, 0x2d7, 0x3, 0x2, 0x2, 0x2, 0x2d6, 0x2d1, 0x3, 0x2, 
    0x2, 0x2, 0x2d6, 0x2d7, 0x3, 0x2, 0x2, 0x2, 0x2d7, 0x2d9, 0x3, 0x2, 
    0x2, 0x2, 0x2d8, 0x2da, 0x5, 0x30, 0x19, 0x2, 0x2d9, 0x2d8, 0x3, 0x2, 
    0x2, 0x2, 0x2d9, 0x2da, 0x3, 0x2, 0x2, 0x2, 0x2da, 0x33f, 0x3, 0x2, 
    0x2, 0x2, 0x2db, 0x2dc, 0x5, 0x1a, 0xe, 0x2, 0x2dc, 0x2dd, 0x7, 0x4c, 
    0x2, 0x2, 0x2dd, 0x2de, 0x5, 0x48, 0x25, 0x2, 0x2de, 0x2df, 0x5, 0x28, 
    0x15, 0x2, 0x2df, 0x2e0, 0x5, 0x28, 0x15, 0x2, 0x2e0, 0x33f, 0x3, 0x2, 
    0x2, 0x2, 0x2e1, 0x2e2, 0x5, 0x1e, 0x10, 0x2, 0x2e2, 0x2e3, 0x5, 0x1a, 
    0xe, 0x2, 0x2e3, 0x2e4, 0x7, 0x4d, 0x2, 0x2, 0x2e4, 0x2e5, 0x5, 0x44, 
    0x23, 0x2, 0x2e5, 0x2e6, 0x7, 0x8, 0x2, 0x2, 0x2e6, 0x2e7, 0x5, 0xc, 
    0x7, 0x2, 0x2e7, 0x2e8, 0x5, 0xe, 0x8, 0x2, 0x2e8, 0x2e9, 0x7, 0x9, 
    0x2, 0x2, 0x2e9, 0x2ea, 0x5, 0x10, 0x9, 0x2, 0x2ea, 0x2ec, 0x5, 0x32, 
    0x1a, 0x2, 0x2eb, 0x2ed, 0x5, 0x2e, 0x18, 0x2, 0x2ec, 0x2eb, 0x3, 0x2, 
    0x2, 0x2, 0x2ec, 0x2ed, 0x3, 0x2, 0x2, 0x2, 0x2ed, 0x2ef, 0x3, 0x2, 
    0x2, 0x2, 0x2ee, 0x2f0, 0x5, 0x30, 0x19, 0x2, 0x2ef, 0x2ee, 0x3, 0x2, 
    0x2, 0x2, 0x2ef, 0x2f0, 0x3, 0x2, 0x2, 0x2, 0x2f0, 0x33f, 0x3, 0x2, 
    0x2, 0x2, 0x2f1, 0x2f2, 0x5, 0x1c, 0xf, 0x2, 0x2f2, 0x2f3, 0x5, 0x1a, 
    0xe, 0x2, 0x2f3, 0x2f4, 0x7, 0x4e, 0x2, 0x2, 0x2f4, 0x2fa, 0x5, 0x10, 
    0x9, 0x2, 0x2f5, 0x2f6, 0x7, 0x4f, 0x2, 0x2, 0x2f6, 0x2f7, 0x7, 0x24, 
    0x2, 0x2, 0x2f7, 0x2f8, 0x5, 0x10, 0x9, 0x2, 0x2f8, 0x2f9, 0x7, 0x25, 
    0x2, 0x2, 0x2f9, 0x2fb, 0x3, 0x2, 0x2, 0x2, 0x2fa, 0x2f5, 0x3, 0x2, 
    0x2, 0x2, 0x2fa, 0x2fb, 0x3, 0x2, 0x2, 0x2, 0x2fb, 0x2fc, 0x3, 0x2, 
    0x2, 0x2, 0x2fc, 0x2fe, 0x5, 0x36, 0x1c, 0x2, 0x2fd, 0x2ff, 0x5, 0x2e, 
    0x18, 0x2, 0x2fe, 0x2fd, 0x3, 0x2, 0x2, 0x2, 0x2fe, 0x2ff, 0x3, 0x2, 
    0x2, 0x2, 0x2ff, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x300, 0x301, 0x5, 0x1e, 
    0x10, 0x2, 0x301, 0x302, 0x5, 0x1a, 0xe, 0x2, 0x302, 0x303, 0x7, 0x50, 
    0x2, 0x2, 0x303, 0x304, 0x5, 0x10, 0x9, 0x2, 0x304, 0x305, 0x5, 0x34, 
    0x1b, 0x2, 0x305, 0x307, 0x5, 0x36, 0x1c, 0x2, 0x306, 0x308, 0x5, 0x2e, 
    0x18, 0x2, 0x307, 0x306, 0x3, 0x2, 0x2, 0x2, 0x307, 0x308, 0x3, 0x2, 
    0x2, 0x2, 0x308, 0x30a, 0x3, 0x2, 0x2, 0x2, 0x309, 0x30b, 0x5, 0x30, 
    0x19, 0x2, 0x30a, 0x309, 0x3, 0x2, 0x2, 0x2, 0x30a, 0x30b, 0x3, 0x2, 
    0x2, 0x2, 0x30b, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x30c, 0x30d, 0x5, 0x1e, 
    0x10, 0x2, 0x30d, 0x30e, 0x5, 0x1a, 0xe, 0x2, 0x30e, 0x30f, 0x7, 0x51, 
    0x2, 0x2, 0x30f, 0x318, 0x5, 0x5c, 0x2f, 0x2, 0x310, 0x314, 0x7, 0x29, 
    0x2, 0x2, 0x311, 0x313, 0x5, 0x46, 0x24, 0x2, 0x312, 0x311, 0x3, 0x2, 
    0x2, 0x2, 0x313, 0x316, 0x3, 0x2, 0x2, 0x2, 0x314, 0x312, 0x3, 0x2, 
    0x2, 0x2, 0x314, 0x315, 0x3, 0x2, 0x2, 0x2, 0x315, 0x317, 0x3, 0x2, 
    0x2, 0x2, 0x316, 0x314, 0x3, 0x2, 0x2, 0x2, 0x317, 0x319, 0x7, 0x2a, 
    0x2, 0x2, 0x318, 0x310, 0x3, 0x2, 0x2, 0x2, 0x318, 0x319, 0x3, 0x2, 
    0x2, 0x2, 0x319, 0x322, 0x3, 0x2, 0x2, 0x2, 0x31a, 0x31e, 0x7, 0x8, 
    0x2, 0x2, 0x31b, 0x31d, 0x5, 0xc, 0x7, 0x2, 0x31c, 0x31b, 0x3, 0x2, 
    0x2, 0x2, 0x31d, 0x320, 0x3, 0x2, 0x2, 0x2, 0x31e, 0x31c, 0x3, 0x2, 
    0x2, 0x2, 0x31e, 0x31f, 0x3, 0x2, 0x2, 0x2, 0x31f, 0x321, 0x3, 0x2, 
    0x2, 0x2, 0x320, 0x31e, 0x3, 0x2, 0x2, 0x2, 0x321, 0x323, 0x7, 0x9, 
    0x2, 0x2, 0x322, 0x31a, 0x3, 0x2, 0x2, 0x2, 0x322, 0x323, 0x3, 0x2, 
    0x2, 0x2, 0x323, 0x32c, 0x3, 0x2, 0x2, 0x2, 0x324, 0x328, 0x7, 0x52, 
    0x2, 0x2, 0x325, 0x327, 0x5, 0xe, 0x8, 0x2, 0x326, 0x325, 0x3, 0x2, 
    0x2, 0x2, 0x327, 0x32a, 0x3, 0x2, 0x2, 0x2, 0x328, 0x326, 0x3, 0x2, 
    0x2, 0x2, 0x328, 0x329, 0x3, 0x2, 0x2, 0x2, 0x329, 0x32b, 0x3, 0x2, 
    0x2, 0x2, 0x32a, 0x328, 0x3, 0x2, 0x2, 0x2, 0x32b, 0x32d, 0x7, 0x53, 
    0x2, 0x2, 0x32c, 0x324, 0x3, 0x2, 0x2, 0x2, 0x32c, 0x32d, 0x3, 0x2, 
    0x2, 0x2, 0x32d, 0x336, 0x3, 0x2, 0x2, 0x2, 0x32e, 0x332, 0x7, 0x24, 
    0x2, 0x2, 0x32f, 0x331, 0x5, 0x10, 0x9, 0x2, 0x330, 0x32f, 0x3, 0x2, 
    0x2, 0x2, 0x331, 0x334, 0x3, 0x2, 0x2, 0x2, 0x332, 0x330, 0x3, 0x2, 
    0x2, 0x2, 0x332, 0x333, 0x3, 0x2, 0x2, 0x2, 0x333, 0x335, 0x3, 0x2, 
    0x2, 0x2, 0x334, 0x332, 0x3, 0x2, 0x2, 0x2, 0x335, 0x337, 0x7, 0x25, 
    0x2, 0x2, 0x336, 0x32e, 0x3, 0x2, 0x2, 0x2, 0x336, 0x337, 0x3, 0x2, 
    0x2, 0x2, 0x337, 0x339, 0x3, 0x2, 0x2, 0x2, 0x338, 0x33a, 0x5, 0x2e, 
    0x18, 0x2, 0x339, 0x338, 0x3, 0x2, 0x2, 0x2, 0x339, 0x33a, 0x3, 0x2, 
    0x2, 0x2, 0x33a, 0x33c, 0x3, 0x2, 0x2, 0x2, 0x33b, 0x33d, 0x5, 0x30, 
    0x19, 0x2, 0x33c, 0x33b, 0x3, 0x2, 0x2, 0x2, 0x33c, 0x33d, 0x3, 0x2, 
    0x2, 0x2, 0x33d, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x177, 0x3, 0x2, 
    0x2, 0x2, 0x33e, 0x182, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x18e, 0x3, 0x2, 
    0x2, 0x2, 0x33e, 0x197, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x1a0, 0x3, 0x2, 
    0x2, 0x2, 0x33e, 0x1ab, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x1af, 0x3, 0x2, 
    0x2, 0x2, 0x33e, 0x1b5, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x1c7, 0x3, 0x2, 
    0x2, 0x2, 0x33e, 0x1d5, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x1dd, 0x3, 0x2, 
    0x2, 0x2, 0x33e, 0x1e1, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x1e5, 0x3, 0x2, 
    0x2, 0x2, 0x33e, 0x1ee, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x1f8, 0x3, 0x2, 
    0x2, 0x2, 0x33e, 0x202, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x20d, 0x3, 0x2, 
    0x2, 0x2, 0x33e, 0x218, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x221, 0x3, 0x2, 
    0x2, 0x2, 0x33e, 0x22c, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x235, 0x3, 0x2, 
    0x2, 0x2, 0x33e, 0x240, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x248, 0x3, 0x2, 
    0x2, 0x2, 0x33e, 0x254, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x261, 0x3, 0x2, 
    0x2, 0x2, 0x33e, 0x26e, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x279, 0x3, 0x2, 
    0x2, 0x2, 0x33e, 0x289, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x299, 0x3, 0x2, 
    0x2, 0x2, 0x33e, 0x2ad, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x2bd, 0x3, 0x2, 
    0x2, 0x2, 0x33e, 0x2c1, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x2ca, 0x3, 0x2, 
    0x2, 0x2, 0x33e, 0x2db, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x2e1, 0x3, 0x2, 
    0x2, 0x2, 0x33e, 0x2f1, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x300, 0x3, 0x2, 
    0x2, 0x2, 0x33e, 0x30c, 0x3, 0x2, 0x2, 0x2, 0x33f, 0x25, 0x3, 0x2, 0x2, 
    0x2, 0x340, 0x344, 0x7, 0x24, 0x2, 0x2, 0x341, 0x343, 0x5, 0x10, 0x9, 
    0x2, 0x342, 0x341, 0x3, 0x2, 0x2, 0x2, 0x343, 0x346, 0x3, 0x2, 0x2, 
    0x2, 0x344, 0x342, 0x3, 0x2, 0x2, 0x2, 0x344, 0x345, 0x3, 0x2, 0x2, 
    0x2, 0x345, 0x347, 0x3, 0x2, 0x2, 0x2, 0x346, 0x344, 0x3, 0x2, 0x2, 
    0x2, 0x347, 0x34c, 0x7, 0x25, 0x2, 0x2, 0x348, 0x34a, 0x5, 0x10, 0x9, 
    0x2, 0x349, 0x348, 0x3, 0x2, 0x2, 0x2, 0x349, 0x34a, 0x3, 0x2, 0x2, 
    0x2, 0x34a, 0x34c, 0x3, 0x2, 0x2, 0x2, 0x34b, 0x340, 0x3, 0x2, 0x2, 
    0x2, 0x34b, 0x349, 0x3, 0x2, 0x2, 0x2, 0x34c, 0x27, 0x3, 0x2, 0x2, 0x2, 
    0x34d, 0x34e, 0x5, 0x2a, 0x16, 0x2, 0x34e, 0x34f, 0x5, 0x32, 0x1a, 0x2, 
    0x34f, 0x29, 0x3, 0x2, 0x2, 0x2, 0x350, 0x351, 0x5, 0x58, 0x2d, 0x2, 
    0x351, 0x2b, 0x3, 0x2, 0x2, 0x2, 0x352, 0x356, 0x7, 0x8, 0x2, 0x2, 0x353, 
    0x355, 0x5, 0xc, 0x7, 0x2, 0x354, 0x353, 0x3, 0x2, 0x2, 0x2, 0x355, 
    0x358, 0x3, 0x2, 0x2, 0x2, 0x356, 0x354, 0x3, 0x2, 0x2, 0x2, 0x356, 
    0x357, 0x3, 0x2, 0x2, 0x2, 0x357, 0x359, 0x3, 0x2, 0x2, 0x2, 0x358, 
    0x356, 0x3, 0x2, 0x2, 0x2, 0x359, 0x35a, 0x7, 0x9, 0x2, 0x2, 0x35a, 
    0x2d, 0x3, 0x2, 0x2, 0x2, 0x35b, 0x35c, 0x7, 0x54, 0x2, 0x2, 0x35c, 
    0x35d, 0x7, 0x24, 0x2, 0x2, 0x35d, 0x35e, 0x5, 0x28, 0x15, 0x2, 0x35e, 
    0x35f, 0x5, 0x28, 0x15, 0x2, 0x35f, 0x360, 0x7, 0x25, 0x2, 0x2, 0x360, 
    0x2f, 0x3, 0x2, 0x2, 0x2, 0x361, 0x362, 0x7, 0x55, 0x2, 0x2, 0x362, 
    0x366, 0x7, 0x24, 0x2, 0x2, 0x363, 0x365, 0x5, 0x10, 0x9, 0x2, 0x364, 
    0x363, 0x3, 0x2, 0x2, 0x2, 0x365, 0x368, 0x3, 0x2, 0x2, 0x2, 0x366, 
    0x364, 0x3, 0x2, 0x2, 0x2, 0x366, 0x367, 0x3, 0x2, 0x2, 0x2, 0x367, 
    0x369, 0x3, 0x2, 0x2, 0x2, 0x368, 0x366, 0x3, 0x2, 0x2, 0x2, 0x369, 
    0x36a, 0x7, 0x25, 0x2, 0x2, 0x36a, 0x31, 0x3, 0x2, 0x2, 0x2, 0x36b, 
    0x36f, 0x7, 0x24, 0x2, 0x2, 0x36c, 0x36e, 0x5, 0x10, 0x9, 0x2, 0x36d, 
    0x36c, 0x3, 0x2, 0x2, 0x2, 0x36e, 0x371, 0x3, 0x2, 0x2, 0x2, 0x36f, 
    0x36d, 0x3, 0x2, 0x2, 0x2, 0x36f, 0x370, 0x3, 0x2, 0x2, 0x2, 0x370, 
    0x372, 0x3, 0x2, 0x2, 0x2, 0x371, 0x36f, 0x3, 0x2, 0x2, 0x2, 0x372, 
    0x373, 0x7, 0x25, 0x2, 0x2, 0x373, 0x33, 0x3, 0x2, 0x2, 0x2, 0x374, 
    0x375, 0x7, 0x56, 0x2, 0x2, 0x375, 0x378, 0x5, 0x2c, 0x17, 0x2, 0x376, 
    0x378, 0x7, 0x57, 0x2, 0x2, 0x377, 0x374, 0x3, 0x2, 0x2, 0x2, 0x377, 
    0x376, 0x3, 0x2, 0x2, 0x2, 0x378, 0x35, 0x3, 0x2, 0x2, 0x2, 0x379, 0x37a, 
    0x7, 0x58, 0x2, 0x2, 0x37a, 0x37b, 0x5, 0x2c, 0x17, 0x2, 0x37b, 0x37c, 
    0x5, 0x32, 0x1a, 0x2, 0x37c, 0x380, 0x3, 0x2, 0x2, 0x2, 0x37d, 0x37e, 
    0x7, 0x59, 0x2, 0x2, 0x37e, 0x380, 0x5, 0x10, 0x9, 0x2, 0x37f, 0x379, 
    0x3, 0x2, 0x2, 0x2, 0x37f, 0x37d, 0x3, 0x2, 0x2, 0x2, 0x380, 0x37, 0x3, 
    0x2, 0x2, 0x2, 0x381, 0x382, 0x9, 0x3, 0x2, 0x2, 0x382, 0x39, 0x3, 0x2, 
    0x2, 0x2, 0x383, 0x384, 0x9, 0x4, 0x2, 0x2, 0x384, 0x3b, 0x3, 0x2, 0x2, 
    0x2, 0x385, 0x386, 0x9, 0x5, 0x2, 0x2, 0x386, 0x3d, 0x3, 0x2, 0x2, 0x2, 
    0x387, 0x388, 0x9, 0x6, 0x2, 0x2, 0x388, 0x3f, 0x3, 0x2, 0x2, 0x2, 0x389, 
    0x38a, 0x9, 0x7, 0x2, 0x2, 0x38a, 0x41, 0x3, 0x2, 0x2, 0x2, 0x38b, 0x38c, 
    0x9, 0x8, 0x2, 0x2, 0x38c, 0x43, 0x3, 0x2, 0x2, 0x2, 0x38d, 0x38e, 0x7, 
    0xa3, 0x2, 0x2, 0x38e, 0x45, 0x3, 0x2, 0x2, 0x2, 0x38f, 0x392, 0x5, 
    0x44, 0x23, 0x2, 0x390, 0x392, 0x5, 0x42, 0x22, 0x2, 0x391, 0x38f, 0x3, 
    0x2, 0x2, 0x2, 0x391, 0x390, 0x3, 0x2, 0x2, 0x2, 0x392, 0x47, 0x3, 0x2, 
    0x2, 0x2, 0x393, 0x394, 0x5, 0x4e, 0x28, 0x2, 0x394, 0x49, 0x3, 0x2, 
    0x2, 0x2, 0x395, 0x396, 0x5, 0x4e, 0x28, 0x2, 0x396, 0x4b, 0x3, 0x2, 
    0x2, 0x2, 0x397, 0x398, 0x5, 0x4e, 0x28, 0x2, 0x398, 0x4d, 0x3, 0x2, 
    0x2, 0x2, 0x399, 0x39a, 0x9, 0x9, 0x2, 0x2, 0x39a, 0x4f, 0x3, 0x2, 0x2, 
    0x2, 0x39b, 0x39c, 0x7, 0xab, 0x2, 0x2, 0x39c, 0x3a7, 0x7, 0xa4, 0x2, 
    0x2, 0x39d, 0x39e, 0x7, 0xac, 0x2, 0x2, 0x39e, 0x3a7, 0x7, 0xa4, 0x2, 
    0x2, 0x39f, 0x3a0, 0x7, 0xad, 0x2, 0x2, 0x3a0, 0x3a7, 0x7, 0xa4, 0x2, 
    0x2, 0x3a1, 0x3a2, 0x7, 0xa5, 0x2, 0x2, 0x3a2, 0x3a3, 0x7, 0x24, 0x2, 
    0x2, 0x3a3, 0x3a4, 0x5, 0x4e, 0x28, 0x2, 0x3a4, 0x3a5, 0x7, 0x25, 0x2, 
    0x2, 0x3a5, 0x3a7, 0x3, 0x2, 0x2, 0x2, 0x3a6, 0x39b, 0x3, 0x2, 0x2, 
    0x2, 0x3a6, 0x39d, 0x3, 0x2, 0x2, 0x2, 0x3a6, 0x39f, 0x3, 0x2, 0x2, 
    0x2, 0x3a6, 0x3a1, 0x3, 0x2, 0x2, 0x2, 0x3a7, 0x51, 0x3, 0x2, 0x2, 0x2, 
    0x3a8, 0x3a9, 0x7, 0xab, 0x2, 0x2, 0x3a9, 0x3b4, 0x7, 0xa6, 0x2, 0x2, 
    0x3aa, 0x3ab, 0x7, 0xac, 0x2, 0x2, 0x3ab, 0x3b4, 0x7, 0xa6, 0x2, 0x2, 
    0x3ac, 0x3ad, 0x7, 0xad, 0x2, 0x2, 0x3ad, 0x3b4, 0x7, 0xa6, 0x2, 0x2, 
    0x3ae, 0x3af, 0x7, 0xa7, 0x2, 0x2, 0x3af, 0x3b0, 0x7, 0x24, 0x2, 0x2, 
    0x3b0, 0x3b1, 0x5, 0x4e, 0x28, 0x2, 0x3b1, 0x3b2, 0x7, 0x25, 0x2, 0x2, 
    0x3b2, 0x3b4, 0x3, 0x2, 0x2, 0x2, 0x3b3, 0x3a8, 0x3, 0x2, 0x2, 0x2, 
    0x3b3, 0x3aa, 0x3, 0x2, 0x2, 0x2, 0x3b3, 0x3ac, 0x3, 0x2, 0x2, 0x2, 
    0x3b3, 0x3ae, 0x3, 0x2, 0x2, 0x2, 0x3b4, 0x53, 0x3, 0x2, 0x2, 0x2, 0x3b5, 
    0x3b6, 0x7, 0xb1, 0x2, 0x2, 0x3b6, 0x55, 0x3, 0x2, 0x2, 0x2, 0x3b7, 
    0x3b8, 0x9, 0xa, 0x2, 0x2, 0x3b8, 0x57, 0x3, 0x2, 0x2, 0x2, 0x3b9, 0x3ba, 
    0x9, 0xa, 0x2, 0x2, 0x3ba, 0x59, 0x3, 0x2, 0x2, 0x2, 0x3bb, 0x3bc, 0x9, 
    0xa, 0x2, 0x2, 0x3bc, 0x5b, 0x3, 0x2, 0x2, 0x2, 0x3bd, 0x3be, 0x9, 0x2, 
    0x2, 0x2, 0x3be, 0x5d, 0x3, 0x2, 0x2, 0x2, 0x52, 0x61, 0x94, 0x9c, 0x9e, 
    0xa4, 0xa8, 0xc0, 0xc9, 0xf5, 0xfb, 0x104, 0x111, 0x117, 0x11b, 0x11f, 
    0x123, 0x12b, 0x13a, 0x142, 0x148, 0x14e, 0x15a, 0x160, 0x170, 0x175, 
    0x180, 0x18c, 0x1c2, 0x1d0, 0x1d3, 0x21f, 0x22a, 0x233, 0x23e, 0x24c, 
    0x258, 0x265, 0x272, 0x27d, 0x280, 0x287, 0x28c, 0x28f, 0x297, 0x29d, 
    0x2a0, 0x2ab, 0x2b1, 0x2bb, 0x2c6, 0x2d6, 0x2d9, 0x2ec, 0x2ef, 0x2fa, 
    0x2fe, 0x307, 0x30a, 0x314, 0x318, 0x31e, 0x322, 0x328, 0x32c, 0x332, 
    0x336, 0x339, 0x33c, 0x33e, 0x344, 0x349, 0x34b, 0x356, 0x366, 0x36f, 
    0x377, 0x37f, 0x391, 0x3a6, 0x3b3, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

UIRParser::Initializer UIRParser::_init;
