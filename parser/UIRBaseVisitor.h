
// Generated from UIR.g4 by ANTLR 4.7

#pragma once


#include "antlr4-runtime.h"
#include "UIRVisitor.h"


/**
 * This class provides an empty implementation of UIRVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  UIRBaseVisitor : public UIRVisitor {
public:

  virtual antlrcpp::Any visitIr(UIRParser::IrContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBundleDef(UIRParser::BundleDefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeDef(UIRParser::TypeDefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFuncSigDef(UIRParser::FuncSigDefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitConstDef(UIRParser::ConstDefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitGlobalDef(UIRParser::GlobalDefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFuncDecl(UIRParser::FuncDeclContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFuncExpDef(UIRParser::FuncExpDefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFuncDef(UIRParser::FuncDefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeInt(UIRParser::TypeIntContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeFloat(UIRParser::TypeFloatContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeDouble(UIRParser::TypeDoubleContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeUPtr(UIRParser::TypeUPtrContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeUFuncPtr(UIRParser::TypeUFuncPtrContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeStruct(UIRParser::TypeStructContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeHybrid(UIRParser::TypeHybridContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeArray(UIRParser::TypeArrayContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeVector(UIRParser::TypeVectorContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeVoid(UIRParser::TypeVoidContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeRef(UIRParser::TypeRefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeIRef(UIRParser::TypeIRefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeWeakRef(UIRParser::TypeWeakRefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeTagRef64(UIRParser::TypeTagRef64Context *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeFuncRef(UIRParser::TypeFuncRefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeThreadRef(UIRParser::TypeThreadRefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeStackRef(UIRParser::TypeStackRefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeFrameCursorRef(UIRParser::TypeFrameCursorRefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeIRBuilderRef(UIRParser::TypeIRBuilderRefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFuncSigConstructor(UIRParser::FuncSigConstructorContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCtorInt(UIRParser::CtorIntContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCtorFloat(UIRParser::CtorFloatContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCtorDouble(UIRParser::CtorDoubleContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCtorNull(UIRParser::CtorNullContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCtorList(UIRParser::CtorListContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCtorExtern(UIRParser::CtorExternContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitType(UIRParser::TypeContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFuncSig(UIRParser::FuncSigContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitValue(UIRParser::ValueContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitConstant(UIRParser::ConstantContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFunc(UIRParser::FuncContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitGlobalVar(UIRParser::GlobalVarContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBasicBlock(UIRParser::BasicBlockContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstName(UIRParser::InstNameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstResult(UIRParser::InstResultContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstResults(UIRParser::InstResultsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitPairResult(UIRParser::PairResultContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitPairResults(UIRParser::PairResultsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstBinOp(UIRParser::InstBinOpContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstBinOpStatus(UIRParser::InstBinOpStatusContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstCmp(UIRParser::InstCmpContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstConversion(UIRParser::InstConversionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstSelect(UIRParser::InstSelectContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstBranch(UIRParser::InstBranchContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstBranch2(UIRParser::InstBranch2Context *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstSwitch(UIRParser::InstSwitchContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstCall(UIRParser::InstCallContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstTailCall(UIRParser::InstTailCallContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstRet(UIRParser::InstRetContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstThrow(UIRParser::InstThrowContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstExtractValue(UIRParser::InstExtractValueContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstInsertValue(UIRParser::InstInsertValueContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstExtractElement(UIRParser::InstExtractElementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstInsertElement(UIRParser::InstInsertElementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstShuffleVector(UIRParser::InstShuffleVectorContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstNew(UIRParser::InstNewContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstNewHybrid(UIRParser::InstNewHybridContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstAlloca(UIRParser::InstAllocaContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstAllocaHybrid(UIRParser::InstAllocaHybridContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstGetIRef(UIRParser::InstGetIRefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstGetFieldIRef(UIRParser::InstGetFieldIRefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstGetElemIRef(UIRParser::InstGetElemIRefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstShiftIRef(UIRParser::InstShiftIRefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstGetVarPartIRef(UIRParser::InstGetVarPartIRefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstLoad(UIRParser::InstLoadContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstStore(UIRParser::InstStoreContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstCmpXchg(UIRParser::InstCmpXchgContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstAtomicRMW(UIRParser::InstAtomicRMWContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstFence(UIRParser::InstFenceContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstTrap(UIRParser::InstTrapContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstWatchPoint(UIRParser::InstWatchPointContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstWPBranch(UIRParser::InstWPBranchContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstCCall(UIRParser::InstCCallContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstNewThread(UIRParser::InstNewThreadContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstSwapStack(UIRParser::InstSwapStackContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstCommInst(UIRParser::InstCommInstContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitRetVals(UIRParser::RetValsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDestClause(UIRParser::DestClauseContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBb(UIRParser::BbContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeList(UIRParser::TypeListContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExcClause(UIRParser::ExcClauseContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitKeepaliveClause(UIRParser::KeepaliveClauseContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitArgList(UIRParser::ArgListContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCurStackRetWith(UIRParser::CurStackRetWithContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCurStackKillOld(UIRParser::CurStackKillOldContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitNewStackPassValue(UIRParser::NewStackPassValueContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitNewStackThrowExc(UIRParser::NewStackThrowExcContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBinop(UIRParser::BinopContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCmpop(UIRParser::CmpopContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitConvop(UIRParser::ConvopContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitMemord(UIRParser::MemordContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAtomicrmwop(UIRParser::AtomicrmwopContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBinopStatus(UIRParser::BinopStatusContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCallConv(UIRParser::CallConvContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFlag(UIRParser::FlagContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitWpid(UIRParser::WpidContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitIntParam(UIRParser::IntParamContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitSizeParam(UIRParser::SizeParamContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitIntLiteral(UIRParser::IntLiteralContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFloatNumber(UIRParser::FloatNumberContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFloatInf(UIRParser::FloatInfContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFloatNan(UIRParser::FloatNanContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFloatBits(UIRParser::FloatBitsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDoubleNumber(UIRParser::DoubleNumberContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDoubleInf(UIRParser::DoubleInfContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDoubleNan(UIRParser::DoubleNanContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDoubleBits(UIRParser::DoubleBitsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitStringLiteral(UIRParser::StringLiteralContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitGlobalName(UIRParser::GlobalNameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLocalName(UIRParser::LocalNameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitName(UIRParser::NameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCommInst(UIRParser::CommInstContext *ctx) override {
    return visitChildren(ctx);
  }


};

