
// Generated from UIR.g4 by ANTLR 4.7

#pragma once


#include "antlr4-runtime.h"




class  UIRParser : public antlr4::Parser {
public:
  enum {
    T__0 = 1, T__1 = 2, T__2 = 3, T__3 = 4, T__4 = 5, T__5 = 6, T__6 = 7, 
    T__7 = 8, T__8 = 9, T__9 = 10, T__10 = 11, T__11 = 12, T__12 = 13, T__13 = 14, 
    T__14 = 15, T__15 = 16, T__16 = 17, T__17 = 18, T__18 = 19, T__19 = 20, 
    T__20 = 21, T__21 = 22, T__22 = 23, T__23 = 24, T__24 = 25, T__25 = 26, 
    T__26 = 27, T__27 = 28, T__28 = 29, T__29 = 30, T__30 = 31, T__31 = 32, 
    T__32 = 33, T__33 = 34, T__34 = 35, T__35 = 36, T__36 = 37, T__37 = 38, 
    T__38 = 39, T__39 = 40, T__40 = 41, T__41 = 42, T__42 = 43, T__43 = 44, 
    T__44 = 45, T__45 = 46, T__46 = 47, T__47 = 48, T__48 = 49, T__49 = 50, 
    T__50 = 51, T__51 = 52, T__52 = 53, T__53 = 54, T__54 = 55, T__55 = 56, 
    T__56 = 57, T__57 = 58, T__58 = 59, T__59 = 60, T__60 = 61, T__61 = 62, 
    T__62 = 63, T__63 = 64, T__64 = 65, T__65 = 66, T__66 = 67, T__67 = 68, 
    T__68 = 69, T__69 = 70, T__70 = 71, T__71 = 72, T__72 = 73, T__73 = 74, 
    T__74 = 75, T__75 = 76, T__76 = 77, T__77 = 78, T__78 = 79, T__79 = 80, 
    T__80 = 81, T__81 = 82, T__82 = 83, T__83 = 84, T__84 = 85, T__85 = 86, 
    T__86 = 87, T__87 = 88, T__88 = 89, T__89 = 90, T__90 = 91, T__91 = 92, 
    T__92 = 93, T__93 = 94, T__94 = 95, T__95 = 96, T__96 = 97, T__97 = 98, 
    T__98 = 99, T__99 = 100, T__100 = 101, T__101 = 102, T__102 = 103, T__103 = 104, 
    T__104 = 105, T__105 = 106, T__106 = 107, T__107 = 108, T__108 = 109, 
    T__109 = 110, T__110 = 111, T__111 = 112, T__112 = 113, T__113 = 114, 
    T__114 = 115, T__115 = 116, T__116 = 117, T__117 = 118, T__118 = 119, 
    T__119 = 120, T__120 = 121, T__121 = 122, T__122 = 123, T__123 = 124, 
    T__124 = 125, T__125 = 126, T__126 = 127, T__127 = 128, T__128 = 129, 
    T__129 = 130, T__130 = 131, T__131 = 132, T__132 = 133, T__133 = 134, 
    T__134 = 135, T__135 = 136, T__136 = 137, T__137 = 138, T__138 = 139, 
    T__139 = 140, T__140 = 141, T__141 = 142, T__142 = 143, T__143 = 144, 
    T__144 = 145, T__145 = 146, T__146 = 147, T__147 = 148, T__148 = 149, 
    T__149 = 150, T__150 = 151, T__151 = 152, T__152 = 153, T__153 = 154, 
    T__154 = 155, T__155 = 156, T__156 = 157, T__157 = 158, T__158 = 159, 
    T__159 = 160, T__160 = 161, T__161 = 162, T__162 = 163, T__163 = 164, 
    T__164 = 165, INT_DEC = 166, INT_OCT = 167, INT_HEX = 168, FP_NUM = 169, 
    INF = 170, NAN_FP = 171, NAME = 172, GLOBAL_NAME = 173, LOCAL_NAME = 174, 
    STRING_LITERAL = 175, WS = 176, LINE_COMMENT = 177, C_COMMENT = 178
  };

  enum {
    RuleIr = 0, RuleTopLevelDef = 1, RuleTypeConstructor = 2, RuleFuncSigConstructor = 3, 
    RuleConstConstructor = 4, RuleType = 5, RuleFuncSig = 6, RuleValue = 7, 
    RuleConstant = 8, RuleFunc = 9, RuleGlobalVar = 10, RuleBasicBlock = 11, 
    RuleInstName = 12, RuleInstResult = 13, RuleInstResults = 14, RulePairResult = 15, 
    RulePairResults = 16, RuleInst = 17, RuleRetVals = 18, RuleDestClause = 19, 
    RuleBb = 20, RuleTypeList = 21, RuleExcClause = 22, RuleKeepaliveClause = 23, 
    RuleArgList = 24, RuleCurStackClause = 25, RuleNewStackClause = 26, 
    RuleBinop = 27, RuleCmpop = 28, RuleConvop = 29, RuleMemord = 30, RuleAtomicrmwop = 31, 
    RuleBinopStatus = 32, RuleCallConv = 33, RuleFlag = 34, RuleWpid = 35, 
    RuleIntParam = 36, RuleSizeParam = 37, RuleIntLiteral = 38, RuleFloatLiteral = 39, 
    RuleDoubleLiteral = 40, RuleStringLiteral = 41, RuleGlobalName = 42, 
    RuleLocalName = 43, RuleName = 44, RuleCommInst = 45
  };

  UIRParser(antlr4::TokenStream *input);
  ~UIRParser();

  virtual std::string getGrammarFileName() const override;
  virtual const antlr4::atn::ATN& getATN() const override { return _atn; };
  virtual const std::vector<std::string>& getTokenNames() const override { return _tokenNames; }; // deprecated: use vocabulary instead.
  virtual const std::vector<std::string>& getRuleNames() const override;
  virtual antlr4::dfa::Vocabulary& getVocabulary() const override;


  class IrContext;
  class TopLevelDefContext;
  class TypeConstructorContext;
  class FuncSigConstructorContext;
  class ConstConstructorContext;
  class TypeContext;
  class FuncSigContext;
  class ValueContext;
  class ConstantContext;
  class FuncContext;
  class GlobalVarContext;
  class BasicBlockContext;
  class InstNameContext;
  class InstResultContext;
  class InstResultsContext;
  class PairResultContext;
  class PairResultsContext;
  class InstContext;
  class RetValsContext;
  class DestClauseContext;
  class BbContext;
  class TypeListContext;
  class ExcClauseContext;
  class KeepaliveClauseContext;
  class ArgListContext;
  class CurStackClauseContext;
  class NewStackClauseContext;
  class BinopContext;
  class CmpopContext;
  class ConvopContext;
  class MemordContext;
  class AtomicrmwopContext;
  class BinopStatusContext;
  class CallConvContext;
  class FlagContext;
  class WpidContext;
  class IntParamContext;
  class SizeParamContext;
  class IntLiteralContext;
  class FloatLiteralContext;
  class DoubleLiteralContext;
  class StringLiteralContext;
  class GlobalNameContext;
  class LocalNameContext;
  class NameContext;
  class CommInstContext; 

  class  IrContext : public antlr4::ParserRuleContext {
  public:
    IrContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *EOF();
    std::vector<TopLevelDefContext *> topLevelDef();
    TopLevelDefContext* topLevelDef(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  IrContext* ir();

  class  TopLevelDefContext : public antlr4::ParserRuleContext {
  public:
    TopLevelDefContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    TopLevelDefContext() : antlr4::ParserRuleContext() { }
    void copyFrom(TopLevelDefContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  BundleDefContext : public TopLevelDefContext {
  public:
    BundleDefContext(TopLevelDefContext *ctx);

    antlr4::tree::TerminalNode *NAME();
    antlr4::tree::TerminalNode *GLOBAL_NAME();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ConstDefContext : public TopLevelDefContext {
  public:
    ConstDefContext(TopLevelDefContext *ctx);

    UIRParser::GlobalNameContext *nam = nullptr;
    UIRParser::TypeContext *ty = nullptr;
    UIRParser::ConstConstructorContext *ctor = nullptr;
    GlobalNameContext *globalName();
    TypeContext *type();
    ConstConstructorContext *constConstructor();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  GlobalDefContext : public TopLevelDefContext {
  public:
    GlobalDefContext(TopLevelDefContext *ctx);

    UIRParser::GlobalNameContext *nam = nullptr;
    UIRParser::TypeContext *ty = nullptr;
    GlobalNameContext *globalName();
    TypeContext *type();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeDefContext : public TopLevelDefContext {
  public:
    TypeDefContext(TopLevelDefContext *ctx);

    UIRParser::GlobalNameContext *nam = nullptr;
    UIRParser::TypeConstructorContext *ctor = nullptr;
    GlobalNameContext *globalName();
    TypeConstructorContext *typeConstructor();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  FuncDefContext : public TopLevelDefContext {
  public:
    FuncDefContext(TopLevelDefContext *ctx);

    UIRParser::FuncContext *nam = nullptr;
    UIRParser::FuncSigContext *sig = nullptr;
    UIRParser::LocalNameContext *ver = nullptr;
    UIRParser::BasicBlockContext *basicBlockContext = nullptr;
    std::vector<BasicBlockContext *> body;
    FuncContext *func();
    std::vector<BasicBlockContext *> basicBlock();
    BasicBlockContext* basicBlock(size_t i);
    FuncSigContext *funcSig();
    LocalNameContext *localName();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  FuncSigDefContext : public TopLevelDefContext {
  public:
    FuncSigDefContext(TopLevelDefContext *ctx);

    UIRParser::GlobalNameContext *nam = nullptr;
    UIRParser::FuncSigConstructorContext *ctor = nullptr;
    GlobalNameContext *globalName();
    FuncSigConstructorContext *funcSigConstructor();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  FuncDeclContext : public TopLevelDefContext {
  public:
    FuncDeclContext(TopLevelDefContext *ctx);

    UIRParser::FuncContext *nam = nullptr;
    UIRParser::FuncSigContext *sig = nullptr;
    FuncContext *func();
    FuncSigContext *funcSig();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  FuncExpDefContext : public TopLevelDefContext {
  public:
    FuncExpDefContext(TopLevelDefContext *ctx);

    UIRParser::GlobalNameContext *nam = nullptr;
    UIRParser::ConstantContext *cookie = nullptr;
    FuncContext *func();
    CallConvContext *callConv();
    GlobalNameContext *globalName();
    ConstantContext *constant();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  TopLevelDefContext* topLevelDef();

  class  TypeConstructorContext : public antlr4::ParserRuleContext {
  public:
    TypeConstructorContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    TypeConstructorContext() : antlr4::ParserRuleContext() { }
    void copyFrom(TypeConstructorContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  TypeArrayContext : public TypeConstructorContext {
  public:
    TypeArrayContext(TypeConstructorContext *ctx);

    UIRParser::TypeContext *ty = nullptr;
    UIRParser::SizeParamContext *length = nullptr;
    TypeContext *type();
    SizeParamContext *sizeParam();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeFloatContext : public TypeConstructorContext {
  public:
    TypeFloatContext(TypeConstructorContext *ctx);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeIRefContext : public TypeConstructorContext {
  public:
    TypeIRefContext(TypeConstructorContext *ctx);

    UIRParser::TypeContext *ty = nullptr;
    TypeContext *type();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeVoidContext : public TypeConstructorContext {
  public:
    TypeVoidContext(TypeConstructorContext *ctx);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeRefContext : public TypeConstructorContext {
  public:
    TypeRefContext(TypeConstructorContext *ctx);

    UIRParser::TypeContext *ty = nullptr;
    TypeContext *type();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeStackRefContext : public TypeConstructorContext {
  public:
    TypeStackRefContext(TypeConstructorContext *ctx);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeHybridContext : public TypeConstructorContext {
  public:
    TypeHybridContext(TypeConstructorContext *ctx);

    UIRParser::TypeContext *typeContext = nullptr;
    std::vector<TypeContext *> fieldTys;
    UIRParser::TypeContext *varTy = nullptr;
    std::vector<TypeContext *> type();
    TypeContext* type(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeTagRef64Context : public TypeConstructorContext {
  public:
    TypeTagRef64Context(TypeConstructorContext *ctx);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeWeakRefContext : public TypeConstructorContext {
  public:
    TypeWeakRefContext(TypeConstructorContext *ctx);

    UIRParser::TypeContext *ty = nullptr;
    TypeContext *type();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeIntContext : public TypeConstructorContext {
  public:
    TypeIntContext(TypeConstructorContext *ctx);

    UIRParser::IntParamContext *length = nullptr;
    IntParamContext *intParam();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeStructContext : public TypeConstructorContext {
  public:
    TypeStructContext(TypeConstructorContext *ctx);

    UIRParser::TypeContext *typeContext = nullptr;
    std::vector<TypeContext *> fieldTys;
    std::vector<TypeContext *> type();
    TypeContext* type(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeFuncRefContext : public TypeConstructorContext {
  public:
    TypeFuncRefContext(TypeConstructorContext *ctx);

    FuncSigContext *funcSig();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeIRBuilderRefContext : public TypeConstructorContext {
  public:
    TypeIRBuilderRefContext(TypeConstructorContext *ctx);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeDoubleContext : public TypeConstructorContext {
  public:
    TypeDoubleContext(TypeConstructorContext *ctx);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeUFuncPtrContext : public TypeConstructorContext {
  public:
    TypeUFuncPtrContext(TypeConstructorContext *ctx);

    FuncSigContext *funcSig();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeFrameCursorRefContext : public TypeConstructorContext {
  public:
    TypeFrameCursorRefContext(TypeConstructorContext *ctx);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeVectorContext : public TypeConstructorContext {
  public:
    TypeVectorContext(TypeConstructorContext *ctx);

    UIRParser::TypeContext *ty = nullptr;
    UIRParser::SizeParamContext *length = nullptr;
    TypeContext *type();
    SizeParamContext *sizeParam();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeUPtrContext : public TypeConstructorContext {
  public:
    TypeUPtrContext(TypeConstructorContext *ctx);

    UIRParser::TypeContext *ty = nullptr;
    TypeContext *type();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeThreadRefContext : public TypeConstructorContext {
  public:
    TypeThreadRefContext(TypeConstructorContext *ctx);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  TypeConstructorContext* typeConstructor();

  class  FuncSigConstructorContext : public antlr4::ParserRuleContext {
  public:
    UIRParser::TypeContext *typeContext = nullptr;;
    std::vector<TypeContext *> paramTys;;
    std::vector<TypeContext *> retTys;;
    FuncSigConstructorContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<TypeContext *> type();
    TypeContext* type(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FuncSigConstructorContext* funcSigConstructor();

  class  ConstConstructorContext : public antlr4::ParserRuleContext {
  public:
    ConstConstructorContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    ConstConstructorContext() : antlr4::ParserRuleContext() { }
    void copyFrom(ConstConstructorContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  CtorFloatContext : public ConstConstructorContext {
  public:
    CtorFloatContext(ConstConstructorContext *ctx);

    FloatLiteralContext *floatLiteral();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  CtorListContext : public ConstConstructorContext {
  public:
    CtorListContext(ConstConstructorContext *ctx);

    std::vector<GlobalVarContext *> globalVar();
    GlobalVarContext* globalVar(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  CtorDoubleContext : public ConstConstructorContext {
  public:
    CtorDoubleContext(ConstConstructorContext *ctx);

    DoubleLiteralContext *doubleLiteral();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  CtorExternContext : public ConstConstructorContext {
  public:
    CtorExternContext(ConstConstructorContext *ctx);

    UIRParser::StringLiteralContext *symbol = nullptr;
    StringLiteralContext *stringLiteral();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  CtorNullContext : public ConstConstructorContext {
  public:
    CtorNullContext(ConstConstructorContext *ctx);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  CtorIntContext : public ConstConstructorContext {
  public:
    CtorIntContext(ConstConstructorContext *ctx);

    IntLiteralContext *intLiteral();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  ConstConstructorContext* constConstructor();

  class  TypeContext : public antlr4::ParserRuleContext {
  public:
    UIRParser::GlobalNameContext *nam = nullptr;;
    TypeContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    GlobalNameContext *globalName();
    TypeConstructorContext *typeConstructor();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  TypeContext* type();

  class  FuncSigContext : public antlr4::ParserRuleContext {
  public:
    UIRParser::GlobalNameContext *nam = nullptr;;
    FuncSigContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    GlobalNameContext *globalName();
    FuncSigConstructorContext *funcSigConstructor();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FuncSigContext* funcSig();

  class  ValueContext : public antlr4::ParserRuleContext {
  public:
    UIRParser::NameContext *nam = nullptr;;
    ValueContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    NameContext *name();
    ConstantContext *constant();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ValueContext* value();

  class  ConstantContext : public antlr4::ParserRuleContext {
  public:
    UIRParser::GlobalNameContext *nam = nullptr;;
    UIRParser::TypeContext *ty = nullptr;;
    ConstantContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    GlobalNameContext *globalName();
    ConstConstructorContext *constConstructor();
    TypeContext *type();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ConstantContext* constant();

  class  FuncContext : public antlr4::ParserRuleContext {
  public:
    UIRParser::GlobalNameContext *nam = nullptr;;
    FuncContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    GlobalNameContext *globalName();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FuncContext* func();

  class  GlobalVarContext : public antlr4::ParserRuleContext {
  public:
    UIRParser::GlobalNameContext *nam = nullptr;;
    GlobalVarContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    GlobalNameContext *globalName();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  GlobalVarContext* globalVar();

  class  BasicBlockContext : public antlr4::ParserRuleContext {
  public:
    UIRParser::LocalNameContext *nam = nullptr;;
    UIRParser::TypeContext *typeContext = nullptr;;
    std::vector<TypeContext *> param_tys;;
    UIRParser::LocalNameContext *localNameContext = nullptr;;
    std::vector<LocalNameContext *> params;;
    UIRParser::LocalNameContext *exc = nullptr;;
    BasicBlockContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<LocalNameContext *> localName();
    LocalNameContext* localName(size_t i);
    std::vector<InstContext *> inst();
    InstContext* inst(size_t i);
    std::vector<TypeContext *> type();
    TypeContext* type(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  BasicBlockContext* basicBlock();

  class  InstNameContext : public antlr4::ParserRuleContext {
  public:
    UIRParser::LocalNameContext *nam = nullptr;;
    InstNameContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    LocalNameContext *localName();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  InstNameContext* instName();

  class  InstResultContext : public antlr4::ParserRuleContext {
  public:
    UIRParser::LocalNameContext *result = nullptr;;
    InstResultContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    LocalNameContext *localName();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  InstResultContext* instResult();

  class  InstResultsContext : public antlr4::ParserRuleContext {
  public:
    UIRParser::LocalNameContext *localNameContext = nullptr;;
    std::vector<LocalNameContext *> results;;
    InstResultsContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<LocalNameContext *> localName();
    LocalNameContext* localName(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  InstResultsContext* instResults();

  class  PairResultContext : public antlr4::ParserRuleContext {
  public:
    UIRParser::LocalNameContext *first = nullptr;;
    UIRParser::LocalNameContext *second = nullptr;;
    PairResultContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<LocalNameContext *> localName();
    LocalNameContext* localName(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  PairResultContext* pairResult();

  class  PairResultsContext : public antlr4::ParserRuleContext {
  public:
    UIRParser::LocalNameContext *first = nullptr;;
    UIRParser::LocalNameContext *localNameContext = nullptr;;
    std::vector<LocalNameContext *> second;;
    PairResultsContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<LocalNameContext *> localName();
    LocalNameContext* localName(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  PairResultsContext* pairResults();

  class  InstContext : public antlr4::ParserRuleContext {
  public:
    InstContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    InstContext() : antlr4::ParserRuleContext() { }
    void copyFrom(InstContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  InstExtractElementContext : public InstContext {
  public:
    InstExtractElementContext(InstContext *ctx);

    UIRParser::TypeContext *seqTy = nullptr;
    UIRParser::TypeContext *indTy = nullptr;
    UIRParser::ValueContext *opnd = nullptr;
    UIRParser::ValueContext *index = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    std::vector<TypeContext *> type();
    TypeContext* type(size_t i);
    std::vector<ValueContext *> value();
    ValueContext* value(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstFenceContext : public InstContext {
  public:
    InstFenceContext(InstContext *ctx);

    InstNameContext *instName();
    MemordContext *memord();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstWatchPointContext : public InstContext {
  public:
    InstWatchPointContext(InstContext *ctx);

    UIRParser::DestClauseContext *dis = nullptr;
    UIRParser::DestClauseContext *ena = nullptr;
    UIRParser::DestClauseContext *wpExc = nullptr;
    InstResultsContext *instResults();
    InstNameContext *instName();
    WpidContext *wpid();
    TypeListContext *typeList();
    std::vector<DestClauseContext *> destClause();
    DestClauseContext* destClause(size_t i);
    KeepaliveClauseContext *keepaliveClause();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstCallContext : public InstContext {
  public:
    InstCallContext(InstContext *ctx);

    UIRParser::ValueContext *callee = nullptr;
    InstResultsContext *instResults();
    InstNameContext *instName();
    FuncSigContext *funcSig();
    ArgListContext *argList();
    ValueContext *value();
    ExcClauseContext *excClause();
    KeepaliveClauseContext *keepaliveClause();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstInsertValueContext : public InstContext {
  public:
    InstInsertValueContext(InstContext *ctx);

    UIRParser::TypeContext *ty = nullptr;
    UIRParser::ValueContext *opnd = nullptr;
    UIRParser::ValueContext *newVal = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    IntParamContext *intParam();
    TypeContext *type();
    std::vector<ValueContext *> value();
    ValueContext* value(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstConversionContext : public InstContext {
  public:
    InstConversionContext(InstContext *ctx);

    UIRParser::TypeContext *fromTy = nullptr;
    UIRParser::TypeContext *toTy = nullptr;
    UIRParser::ValueContext *opnd = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    ConvopContext *convop();
    std::vector<TypeContext *> type();
    TypeContext* type(size_t i);
    ValueContext *value();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstSwitchContext : public InstContext {
  public:
    InstSwitchContext(InstContext *ctx);

    UIRParser::TypeContext *ty = nullptr;
    UIRParser::ValueContext *opnd = nullptr;
    UIRParser::DestClauseContext *defDest = nullptr;
    UIRParser::ConstantContext *constantContext = nullptr;
    std::vector<ConstantContext *> caseVal;
    UIRParser::DestClauseContext *destClauseContext = nullptr;
    std::vector<DestClauseContext *> caseDest;
    InstNameContext *instName();
    TypeContext *type();
    ValueContext *value();
    std::vector<DestClauseContext *> destClause();
    DestClauseContext* destClause(size_t i);
    std::vector<ConstantContext *> constant();
    ConstantContext* constant(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstBinOpStatusContext : public InstContext {
  public:
    InstBinOpStatusContext(InstContext *ctx);

    UIRParser::TypeContext *ty = nullptr;
    UIRParser::ValueContext *op1 = nullptr;
    UIRParser::ValueContext *op2 = nullptr;
    PairResultsContext *pairResults();
    InstNameContext *instName();
    BinopContext *binop();
    BinopStatusContext *binopStatus();
    TypeContext *type();
    std::vector<ValueContext *> value();
    ValueContext* value(size_t i);
    ExcClauseContext *excClause();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstLoadContext : public InstContext {
  public:
    InstLoadContext(InstContext *ctx);

    antlr4::Token *ptr = nullptr;
    UIRParser::ValueContext *loc = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    TypeContext *type();
    ValueContext *value();
    MemordContext *memord();
    ExcClauseContext *excClause();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstAtomicRMWContext : public InstContext {
  public:
    InstAtomicRMWContext(InstContext *ctx);

    antlr4::Token *ptr = nullptr;
    UIRParser::ValueContext *loc = nullptr;
    UIRParser::ValueContext *opnd = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    MemordContext *memord();
    AtomicrmwopContext *atomicrmwop();
    TypeContext *type();
    std::vector<ValueContext *> value();
    ValueContext* value(size_t i);
    ExcClauseContext *excClause();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstAllocaHybridContext : public InstContext {
  public:
    InstAllocaHybridContext(InstContext *ctx);

    UIRParser::TypeContext *allocTy = nullptr;
    UIRParser::TypeContext *lenTy = nullptr;
    UIRParser::ValueContext *length = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    std::vector<TypeContext *> type();
    TypeContext* type(size_t i);
    ValueContext *value();
    ExcClauseContext *excClause();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstInsertElementContext : public InstContext {
  public:
    InstInsertElementContext(InstContext *ctx);

    UIRParser::TypeContext *seqTy = nullptr;
    UIRParser::TypeContext *indTy = nullptr;
    UIRParser::ValueContext *opnd = nullptr;
    UIRParser::ValueContext *index = nullptr;
    UIRParser::ValueContext *newVal = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    std::vector<TypeContext *> type();
    TypeContext* type(size_t i);
    std::vector<ValueContext *> value();
    ValueContext* value(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstBranchContext : public InstContext {
  public:
    InstBranchContext(InstContext *ctx);

    UIRParser::DestClauseContext *dest = nullptr;
    InstNameContext *instName();
    DestClauseContext *destClause();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstGetVarPartIRefContext : public InstContext {
  public:
    InstGetVarPartIRefContext(InstContext *ctx);

    antlr4::Token *ptr = nullptr;
    UIRParser::TypeContext *refTy = nullptr;
    UIRParser::ValueContext *opnd = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    TypeContext *type();
    ValueContext *value();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstThrowContext : public InstContext {
  public:
    InstThrowContext(InstContext *ctx);

    UIRParser::ValueContext *exc = nullptr;
    InstNameContext *instName();
    ValueContext *value();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstTrapContext : public InstContext {
  public:
    InstTrapContext(InstContext *ctx);

    InstResultsContext *instResults();
    InstNameContext *instName();
    TypeListContext *typeList();
    KeepaliveClauseContext *keepaliveClause();
    ExcClauseContext *excClause();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstExtractValueContext : public InstContext {
  public:
    InstExtractValueContext(InstContext *ctx);

    UIRParser::TypeContext *ty = nullptr;
    UIRParser::ValueContext *opnd = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    IntParamContext *intParam();
    TypeContext *type();
    ValueContext *value();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstSwapStackContext : public InstContext {
  public:
    InstSwapStackContext(InstContext *ctx);

    UIRParser::ValueContext *swappee = nullptr;
    InstResultsContext *instResults();
    InstNameContext *instName();
    CurStackClauseContext *curStackClause();
    NewStackClauseContext *newStackClause();
    ValueContext *value();
    ExcClauseContext *excClause();
    KeepaliveClauseContext *keepaliveClause();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstTailCallContext : public InstContext {
  public:
    InstTailCallContext(InstContext *ctx);

    UIRParser::ValueContext *callee = nullptr;
    InstNameContext *instName();
    FuncSigContext *funcSig();
    ArgListContext *argList();
    ValueContext *value();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstGetIRefContext : public InstContext {
  public:
    InstGetIRefContext(InstContext *ctx);

    UIRParser::TypeContext *refTy = nullptr;
    UIRParser::ValueContext *opnd = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    TypeContext *type();
    ValueContext *value();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstShuffleVectorContext : public InstContext {
  public:
    InstShuffleVectorContext(InstContext *ctx);

    UIRParser::TypeContext *vecTy = nullptr;
    UIRParser::TypeContext *maskTy = nullptr;
    UIRParser::ValueContext *vec1 = nullptr;
    UIRParser::ValueContext *vec2 = nullptr;
    UIRParser::ValueContext *mask = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    std::vector<TypeContext *> type();
    TypeContext* type(size_t i);
    std::vector<ValueContext *> value();
    ValueContext* value(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstCmpContext : public InstContext {
  public:
    InstCmpContext(InstContext *ctx);

    UIRParser::TypeContext *ty = nullptr;
    UIRParser::ValueContext *op1 = nullptr;
    UIRParser::ValueContext *op2 = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    CmpopContext *cmpop();
    TypeContext *type();
    std::vector<ValueContext *> value();
    ValueContext* value(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstShiftIRefContext : public InstContext {
  public:
    InstShiftIRefContext(InstContext *ctx);

    antlr4::Token *ptr = nullptr;
    UIRParser::TypeContext *refTy = nullptr;
    UIRParser::TypeContext *offTy = nullptr;
    UIRParser::ValueContext *opnd = nullptr;
    UIRParser::ValueContext *offset = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    std::vector<TypeContext *> type();
    TypeContext* type(size_t i);
    std::vector<ValueContext *> value();
    ValueContext* value(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstCCallContext : public InstContext {
  public:
    InstCCallContext(InstContext *ctx);

    UIRParser::TypeContext *funcTy = nullptr;
    UIRParser::ValueContext *callee = nullptr;
    InstResultsContext *instResults();
    InstNameContext *instName();
    CallConvContext *callConv();
    FuncSigContext *funcSig();
    ArgListContext *argList();
    TypeContext *type();
    ValueContext *value();
    ExcClauseContext *excClause();
    KeepaliveClauseContext *keepaliveClause();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstNewHybridContext : public InstContext {
  public:
    InstNewHybridContext(InstContext *ctx);

    UIRParser::TypeContext *allocTy = nullptr;
    UIRParser::TypeContext *lenTy = nullptr;
    UIRParser::ValueContext *length = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    std::vector<TypeContext *> type();
    TypeContext* type(size_t i);
    ValueContext *value();
    ExcClauseContext *excClause();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstGetFieldIRefContext : public InstContext {
  public:
    InstGetFieldIRefContext(InstContext *ctx);

    antlr4::Token *ptr = nullptr;
    UIRParser::TypeContext *refTy = nullptr;
    UIRParser::IntParamContext *index = nullptr;
    UIRParser::ValueContext *opnd = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    TypeContext *type();
    IntParamContext *intParam();
    ValueContext *value();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstCommInstContext : public InstContext {
  public:
    InstCommInstContext(InstContext *ctx);

    UIRParser::FlagContext *flagContext = nullptr;
    std::vector<FlagContext *> flags;
    UIRParser::TypeContext *typeContext = nullptr;
    std::vector<TypeContext *> tys;
    UIRParser::FuncSigContext *funcSigContext = nullptr;
    std::vector<FuncSigContext *> sigs;
    UIRParser::ValueContext *valueContext = nullptr;
    std::vector<ValueContext *> args;
    InstResultsContext *instResults();
    InstNameContext *instName();
    CommInstContext *commInst();
    ExcClauseContext *excClause();
    KeepaliveClauseContext *keepaliveClause();
    std::vector<FlagContext *> flag();
    FlagContext* flag(size_t i);
    std::vector<TypeContext *> type();
    TypeContext* type(size_t i);
    std::vector<FuncSigContext *> funcSig();
    FuncSigContext* funcSig(size_t i);
    std::vector<ValueContext *> value();
    ValueContext* value(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstCmpXchgContext : public InstContext {
  public:
    InstCmpXchgContext(InstContext *ctx);

    antlr4::Token *ptr = nullptr;
    antlr4::Token *isWeak = nullptr;
    UIRParser::MemordContext *ordSucc = nullptr;
    UIRParser::MemordContext *ordFail = nullptr;
    UIRParser::ValueContext *loc = nullptr;
    UIRParser::ValueContext *expected = nullptr;
    UIRParser::ValueContext *desired = nullptr;
    PairResultContext *pairResult();
    InstNameContext *instName();
    TypeContext *type();
    std::vector<MemordContext *> memord();
    MemordContext* memord(size_t i);
    std::vector<ValueContext *> value();
    ValueContext* value(size_t i);
    ExcClauseContext *excClause();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstAllocaContext : public InstContext {
  public:
    InstAllocaContext(InstContext *ctx);

    UIRParser::TypeContext *allocTy = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    TypeContext *type();
    ExcClauseContext *excClause();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstStoreContext : public InstContext {
  public:
    InstStoreContext(InstContext *ctx);

    antlr4::Token *ptr = nullptr;
    UIRParser::ValueContext *loc = nullptr;
    UIRParser::ValueContext *newVal = nullptr;
    InstNameContext *instName();
    TypeContext *type();
    std::vector<ValueContext *> value();
    ValueContext* value(size_t i);
    MemordContext *memord();
    ExcClauseContext *excClause();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstBranch2Context : public InstContext {
  public:
    InstBranch2Context(InstContext *ctx);

    UIRParser::ValueContext *cond = nullptr;
    UIRParser::DestClauseContext *ifTrue = nullptr;
    UIRParser::DestClauseContext *ifFalse = nullptr;
    InstNameContext *instName();
    ValueContext *value();
    std::vector<DestClauseContext *> destClause();
    DestClauseContext* destClause(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstBinOpContext : public InstContext {
  public:
    InstBinOpContext(InstContext *ctx);

    UIRParser::TypeContext *ty = nullptr;
    UIRParser::ValueContext *op1 = nullptr;
    UIRParser::ValueContext *op2 = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    BinopContext *binop();
    TypeContext *type();
    std::vector<ValueContext *> value();
    ValueContext* value(size_t i);
    ExcClauseContext *excClause();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstSelectContext : public InstContext {
  public:
    InstSelectContext(InstContext *ctx);

    UIRParser::TypeContext *condTy = nullptr;
    UIRParser::TypeContext *resTy = nullptr;
    UIRParser::ValueContext *cond = nullptr;
    UIRParser::ValueContext *ifTrue = nullptr;
    UIRParser::ValueContext *ifFalse = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    std::vector<TypeContext *> type();
    TypeContext* type(size_t i);
    std::vector<ValueContext *> value();
    ValueContext* value(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstNewThreadContext : public InstContext {
  public:
    InstNewThreadContext(InstContext *ctx);

    UIRParser::ValueContext *stack = nullptr;
    UIRParser::ValueContext *threadLocal = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    NewStackClauseContext *newStackClause();
    std::vector<ValueContext *> value();
    ValueContext* value(size_t i);
    ExcClauseContext *excClause();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstWPBranchContext : public InstContext {
  public:
    InstWPBranchContext(InstContext *ctx);

    UIRParser::DestClauseContext *dis = nullptr;
    UIRParser::DestClauseContext *ena = nullptr;
    InstNameContext *instName();
    WpidContext *wpid();
    std::vector<DestClauseContext *> destClause();
    DestClauseContext* destClause(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstGetElemIRefContext : public InstContext {
  public:
    InstGetElemIRefContext(InstContext *ctx);

    antlr4::Token *ptr = nullptr;
    UIRParser::TypeContext *refTy = nullptr;
    UIRParser::TypeContext *indTy = nullptr;
    UIRParser::ValueContext *opnd = nullptr;
    UIRParser::ValueContext *index = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    std::vector<TypeContext *> type();
    TypeContext* type(size_t i);
    std::vector<ValueContext *> value();
    ValueContext* value(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstNewContext : public InstContext {
  public:
    InstNewContext(InstContext *ctx);

    UIRParser::TypeContext *allocTy = nullptr;
    InstResultContext *instResult();
    InstNameContext *instName();
    TypeContext *type();
    ExcClauseContext *excClause();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstRetContext : public InstContext {
  public:
    InstRetContext(InstContext *ctx);

    InstNameContext *instName();
    RetValsContext *retVals();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  InstContext* inst();

  class  RetValsContext : public antlr4::ParserRuleContext {
  public:
    UIRParser::ValueContext *valueContext = nullptr;;
    std::vector<ValueContext *> vals;;
    RetValsContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<ValueContext *> value();
    ValueContext* value(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  RetValsContext* retVals();

  class  DestClauseContext : public antlr4::ParserRuleContext {
  public:
    DestClauseContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    BbContext *bb();
    ArgListContext *argList();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  DestClauseContext* destClause();

  class  BbContext : public antlr4::ParserRuleContext {
  public:
    UIRParser::LocalNameContext *nam = nullptr;;
    BbContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    LocalNameContext *localName();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  BbContext* bb();

  class  TypeListContext : public antlr4::ParserRuleContext {
  public:
    UIRParser::TypeContext *typeContext = nullptr;;
    std::vector<TypeContext *> tys;;
    TypeListContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<TypeContext *> type();
    TypeContext* type(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  TypeListContext* typeList();

  class  ExcClauseContext : public antlr4::ParserRuleContext {
  public:
    UIRParser::DestClauseContext *nor = nullptr;;
    UIRParser::DestClauseContext *exc = nullptr;;
    ExcClauseContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<DestClauseContext *> destClause();
    DestClauseContext* destClause(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ExcClauseContext* excClause();

  class  KeepaliveClauseContext : public antlr4::ParserRuleContext {
  public:
    KeepaliveClauseContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<ValueContext *> value();
    ValueContext* value(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  KeepaliveClauseContext* keepaliveClause();

  class  ArgListContext : public antlr4::ParserRuleContext {
  public:
    ArgListContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<ValueContext *> value();
    ValueContext* value(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ArgListContext* argList();

  class  CurStackClauseContext : public antlr4::ParserRuleContext {
  public:
    CurStackClauseContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    CurStackClauseContext() : antlr4::ParserRuleContext() { }
    void copyFrom(CurStackClauseContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  CurStackRetWithContext : public CurStackClauseContext {
  public:
    CurStackRetWithContext(CurStackClauseContext *ctx);

    TypeListContext *typeList();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  CurStackKillOldContext : public CurStackClauseContext {
  public:
    CurStackKillOldContext(CurStackClauseContext *ctx);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  CurStackClauseContext* curStackClause();

  class  NewStackClauseContext : public antlr4::ParserRuleContext {
  public:
    NewStackClauseContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    NewStackClauseContext() : antlr4::ParserRuleContext() { }
    void copyFrom(NewStackClauseContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  NewStackThrowExcContext : public NewStackClauseContext {
  public:
    NewStackThrowExcContext(NewStackClauseContext *ctx);

    UIRParser::ValueContext *exc = nullptr;
    ValueContext *value();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  NewStackPassValueContext : public NewStackClauseContext {
  public:
    NewStackPassValueContext(NewStackClauseContext *ctx);

    TypeListContext *typeList();
    ArgListContext *argList();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  NewStackClauseContext* newStackClause();

  class  BinopContext : public antlr4::ParserRuleContext {
  public:
    BinopContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  BinopContext* binop();

  class  CmpopContext : public antlr4::ParserRuleContext {
  public:
    CmpopContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CmpopContext* cmpop();

  class  ConvopContext : public antlr4::ParserRuleContext {
  public:
    ConvopContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ConvopContext* convop();

  class  MemordContext : public antlr4::ParserRuleContext {
  public:
    MemordContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  MemordContext* memord();

  class  AtomicrmwopContext : public antlr4::ParserRuleContext {
  public:
    AtomicrmwopContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  AtomicrmwopContext* atomicrmwop();

  class  BinopStatusContext : public antlr4::ParserRuleContext {
  public:
    BinopStatusContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  BinopStatusContext* binopStatus();

  class  CallConvContext : public antlr4::ParserRuleContext {
  public:
    CallConvContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CallConvContext* callConv();

  class  FlagContext : public antlr4::ParserRuleContext {
  public:
    FlagContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    CallConvContext *callConv();
    BinopStatusContext *binopStatus();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FlagContext* flag();

  class  WpidContext : public antlr4::ParserRuleContext {
  public:
    WpidContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    IntLiteralContext *intLiteral();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  WpidContext* wpid();

  class  IntParamContext : public antlr4::ParserRuleContext {
  public:
    IntParamContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    IntLiteralContext *intLiteral();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  IntParamContext* intParam();

  class  SizeParamContext : public antlr4::ParserRuleContext {
  public:
    SizeParamContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    IntLiteralContext *intLiteral();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  SizeParamContext* sizeParam();

  class  IntLiteralContext : public antlr4::ParserRuleContext {
  public:
    IntLiteralContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *INT_DEC();
    antlr4::tree::TerminalNode *INT_HEX();
    antlr4::tree::TerminalNode *INT_OCT();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  IntLiteralContext* intLiteral();

  class  FloatLiteralContext : public antlr4::ParserRuleContext {
  public:
    FloatLiteralContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    FloatLiteralContext() : antlr4::ParserRuleContext() { }
    void copyFrom(FloatLiteralContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  FloatBitsContext : public FloatLiteralContext {
  public:
    FloatBitsContext(FloatLiteralContext *ctx);

    IntLiteralContext *intLiteral();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  FloatNanContext : public FloatLiteralContext {
  public:
    FloatNanContext(FloatLiteralContext *ctx);

    antlr4::tree::TerminalNode *NAN_FP();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  FloatNumberContext : public FloatLiteralContext {
  public:
    FloatNumberContext(FloatLiteralContext *ctx);

    antlr4::tree::TerminalNode *FP_NUM();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  FloatInfContext : public FloatLiteralContext {
  public:
    FloatInfContext(FloatLiteralContext *ctx);

    antlr4::tree::TerminalNode *INF();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  FloatLiteralContext* floatLiteral();

  class  DoubleLiteralContext : public antlr4::ParserRuleContext {
  public:
    DoubleLiteralContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    DoubleLiteralContext() : antlr4::ParserRuleContext() { }
    void copyFrom(DoubleLiteralContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  DoubleBitsContext : public DoubleLiteralContext {
  public:
    DoubleBitsContext(DoubleLiteralContext *ctx);

    IntLiteralContext *intLiteral();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  DoubleNumberContext : public DoubleLiteralContext {
  public:
    DoubleNumberContext(DoubleLiteralContext *ctx);

    antlr4::tree::TerminalNode *FP_NUM();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  DoubleInfContext : public DoubleLiteralContext {
  public:
    DoubleInfContext(DoubleLiteralContext *ctx);

    antlr4::tree::TerminalNode *INF();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  DoubleNanContext : public DoubleLiteralContext {
  public:
    DoubleNanContext(DoubleLiteralContext *ctx);

    antlr4::tree::TerminalNode *NAN_FP();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  DoubleLiteralContext* doubleLiteral();

  class  StringLiteralContext : public antlr4::ParserRuleContext {
  public:
    StringLiteralContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *STRING_LITERAL();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  StringLiteralContext* stringLiteral();

  class  GlobalNameContext : public antlr4::ParserRuleContext {
  public:
    GlobalNameContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *NAME();
    antlr4::tree::TerminalNode *LOCAL_NAME();
    antlr4::tree::TerminalNode *GLOBAL_NAME();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  GlobalNameContext* globalName();

  class  LocalNameContext : public antlr4::ParserRuleContext {
  public:
    LocalNameContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *NAME();
    antlr4::tree::TerminalNode *GLOBAL_NAME();
    antlr4::tree::TerminalNode *LOCAL_NAME();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  LocalNameContext* localName();

  class  NameContext : public antlr4::ParserRuleContext {
  public:
    NameContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *NAME();
    antlr4::tree::TerminalNode *GLOBAL_NAME();
    antlr4::tree::TerminalNode *LOCAL_NAME();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  NameContext* name();

  class  CommInstContext : public antlr4::ParserRuleContext {
  public:
    CommInstContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *NAME();
    antlr4::tree::TerminalNode *GLOBAL_NAME();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CommInstContext* commInst();


private:
  static std::vector<antlr4::dfa::DFA> _decisionToDFA;
  static antlr4::atn::PredictionContextCache _sharedContextCache;
  static std::vector<std::string> _ruleNames;
  static std::vector<std::string> _tokenNames;

  static std::vector<std::string> _literalNames;
  static std::vector<std::string> _symbolicNames;
  static antlr4::dfa::Vocabulary _vocabulary;
  static antlr4::atn::ATN _atn;
  static std::vector<uint16_t> _serializedATN;


  struct Initializer {
    Initializer();
  };
  static Initializer _init;
};

