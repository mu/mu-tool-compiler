
// Generated from UIR.g4 by ANTLR 4.7

#pragma once


#include "antlr4-runtime.h"
#include "UIRParser.h"



/**
 * This class defines an abstract visitor for a parse tree
 * produced by UIRParser.
 */
class  UIRVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by UIRParser.
   */
    virtual antlrcpp::Any visitIr(UIRParser::IrContext *context) = 0;

    virtual antlrcpp::Any visitBundleDef(UIRParser::BundleDefContext *context) = 0;

    virtual antlrcpp::Any visitTypeDef(UIRParser::TypeDefContext *context) = 0;

    virtual antlrcpp::Any visitFuncSigDef(UIRParser::FuncSigDefContext *context) = 0;

    virtual antlrcpp::Any visitConstDef(UIRParser::ConstDefContext *context) = 0;

    virtual antlrcpp::Any visitGlobalDef(UIRParser::GlobalDefContext *context) = 0;

    virtual antlrcpp::Any visitFuncDecl(UIRParser::FuncDeclContext *context) = 0;

    virtual antlrcpp::Any visitFuncExpDef(UIRParser::FuncExpDefContext *context) = 0;

    virtual antlrcpp::Any visitFuncDef(UIRParser::FuncDefContext *context) = 0;

    virtual antlrcpp::Any visitTypeInt(UIRParser::TypeIntContext *context) = 0;

    virtual antlrcpp::Any visitTypeFloat(UIRParser::TypeFloatContext *context) = 0;

    virtual antlrcpp::Any visitTypeDouble(UIRParser::TypeDoubleContext *context) = 0;

    virtual antlrcpp::Any visitTypeUPtr(UIRParser::TypeUPtrContext *context) = 0;

    virtual antlrcpp::Any visitTypeUFuncPtr(UIRParser::TypeUFuncPtrContext *context) = 0;

    virtual antlrcpp::Any visitTypeStruct(UIRParser::TypeStructContext *context) = 0;

    virtual antlrcpp::Any visitTypeHybrid(UIRParser::TypeHybridContext *context) = 0;

    virtual antlrcpp::Any visitTypeArray(UIRParser::TypeArrayContext *context) = 0;

    virtual antlrcpp::Any visitTypeVector(UIRParser::TypeVectorContext *context) = 0;

    virtual antlrcpp::Any visitTypeVoid(UIRParser::TypeVoidContext *context) = 0;

    virtual antlrcpp::Any visitTypeRef(UIRParser::TypeRefContext *context) = 0;

    virtual antlrcpp::Any visitTypeIRef(UIRParser::TypeIRefContext *context) = 0;

    virtual antlrcpp::Any visitTypeWeakRef(UIRParser::TypeWeakRefContext *context) = 0;

    virtual antlrcpp::Any visitTypeTagRef64(UIRParser::TypeTagRef64Context *context) = 0;

    virtual antlrcpp::Any visitTypeFuncRef(UIRParser::TypeFuncRefContext *context) = 0;

    virtual antlrcpp::Any visitTypeThreadRef(UIRParser::TypeThreadRefContext *context) = 0;

    virtual antlrcpp::Any visitTypeStackRef(UIRParser::TypeStackRefContext *context) = 0;

    virtual antlrcpp::Any visitTypeFrameCursorRef(UIRParser::TypeFrameCursorRefContext *context) = 0;

    virtual antlrcpp::Any visitTypeIRBuilderRef(UIRParser::TypeIRBuilderRefContext *context) = 0;

    virtual antlrcpp::Any visitFuncSigConstructor(UIRParser::FuncSigConstructorContext *context) = 0;

    virtual antlrcpp::Any visitCtorInt(UIRParser::CtorIntContext *context) = 0;

    virtual antlrcpp::Any visitCtorFloat(UIRParser::CtorFloatContext *context) = 0;

    virtual antlrcpp::Any visitCtorDouble(UIRParser::CtorDoubleContext *context) = 0;

    virtual antlrcpp::Any visitCtorNull(UIRParser::CtorNullContext *context) = 0;

    virtual antlrcpp::Any visitCtorList(UIRParser::CtorListContext *context) = 0;

    virtual antlrcpp::Any visitCtorExtern(UIRParser::CtorExternContext *context) = 0;

    virtual antlrcpp::Any visitType(UIRParser::TypeContext *context) = 0;

    virtual antlrcpp::Any visitFuncSig(UIRParser::FuncSigContext *context) = 0;

    virtual antlrcpp::Any visitValue(UIRParser::ValueContext *context) = 0;

    virtual antlrcpp::Any visitConstant(UIRParser::ConstantContext *context) = 0;

    virtual antlrcpp::Any visitFunc(UIRParser::FuncContext *context) = 0;

    virtual antlrcpp::Any visitGlobalVar(UIRParser::GlobalVarContext *context) = 0;

    virtual antlrcpp::Any visitBasicBlock(UIRParser::BasicBlockContext *context) = 0;

    virtual antlrcpp::Any visitInstName(UIRParser::InstNameContext *context) = 0;

    virtual antlrcpp::Any visitInstResult(UIRParser::InstResultContext *context) = 0;

    virtual antlrcpp::Any visitInstResults(UIRParser::InstResultsContext *context) = 0;

    virtual antlrcpp::Any visitPairResult(UIRParser::PairResultContext *context) = 0;

    virtual antlrcpp::Any visitPairResults(UIRParser::PairResultsContext *context) = 0;

    virtual antlrcpp::Any visitInstBinOp(UIRParser::InstBinOpContext *context) = 0;

    virtual antlrcpp::Any visitInstBinOpStatus(UIRParser::InstBinOpStatusContext *context) = 0;

    virtual antlrcpp::Any visitInstCmp(UIRParser::InstCmpContext *context) = 0;

    virtual antlrcpp::Any visitInstConversion(UIRParser::InstConversionContext *context) = 0;

    virtual antlrcpp::Any visitInstSelect(UIRParser::InstSelectContext *context) = 0;

    virtual antlrcpp::Any visitInstBranch(UIRParser::InstBranchContext *context) = 0;

    virtual antlrcpp::Any visitInstBranch2(UIRParser::InstBranch2Context *context) = 0;

    virtual antlrcpp::Any visitInstSwitch(UIRParser::InstSwitchContext *context) = 0;

    virtual antlrcpp::Any visitInstCall(UIRParser::InstCallContext *context) = 0;

    virtual antlrcpp::Any visitInstTailCall(UIRParser::InstTailCallContext *context) = 0;

    virtual antlrcpp::Any visitInstRet(UIRParser::InstRetContext *context) = 0;

    virtual antlrcpp::Any visitInstThrow(UIRParser::InstThrowContext *context) = 0;

    virtual antlrcpp::Any visitInstExtractValue(UIRParser::InstExtractValueContext *context) = 0;

    virtual antlrcpp::Any visitInstInsertValue(UIRParser::InstInsertValueContext *context) = 0;

    virtual antlrcpp::Any visitInstExtractElement(UIRParser::InstExtractElementContext *context) = 0;

    virtual antlrcpp::Any visitInstInsertElement(UIRParser::InstInsertElementContext *context) = 0;

    virtual antlrcpp::Any visitInstShuffleVector(UIRParser::InstShuffleVectorContext *context) = 0;

    virtual antlrcpp::Any visitInstNew(UIRParser::InstNewContext *context) = 0;

    virtual antlrcpp::Any visitInstNewHybrid(UIRParser::InstNewHybridContext *context) = 0;

    virtual antlrcpp::Any visitInstAlloca(UIRParser::InstAllocaContext *context) = 0;

    virtual antlrcpp::Any visitInstAllocaHybrid(UIRParser::InstAllocaHybridContext *context) = 0;

    virtual antlrcpp::Any visitInstGetIRef(UIRParser::InstGetIRefContext *context) = 0;

    virtual antlrcpp::Any visitInstGetFieldIRef(UIRParser::InstGetFieldIRefContext *context) = 0;

    virtual antlrcpp::Any visitInstGetElemIRef(UIRParser::InstGetElemIRefContext *context) = 0;

    virtual antlrcpp::Any visitInstShiftIRef(UIRParser::InstShiftIRefContext *context) = 0;

    virtual antlrcpp::Any visitInstGetVarPartIRef(UIRParser::InstGetVarPartIRefContext *context) = 0;

    virtual antlrcpp::Any visitInstLoad(UIRParser::InstLoadContext *context) = 0;

    virtual antlrcpp::Any visitInstStore(UIRParser::InstStoreContext *context) = 0;

    virtual antlrcpp::Any visitInstCmpXchg(UIRParser::InstCmpXchgContext *context) = 0;

    virtual antlrcpp::Any visitInstAtomicRMW(UIRParser::InstAtomicRMWContext *context) = 0;

    virtual antlrcpp::Any visitInstFence(UIRParser::InstFenceContext *context) = 0;

    virtual antlrcpp::Any visitInstTrap(UIRParser::InstTrapContext *context) = 0;

    virtual antlrcpp::Any visitInstWatchPoint(UIRParser::InstWatchPointContext *context) = 0;

    virtual antlrcpp::Any visitInstWPBranch(UIRParser::InstWPBranchContext *context) = 0;

    virtual antlrcpp::Any visitInstCCall(UIRParser::InstCCallContext *context) = 0;

    virtual antlrcpp::Any visitInstNewThread(UIRParser::InstNewThreadContext *context) = 0;

    virtual antlrcpp::Any visitInstSwapStack(UIRParser::InstSwapStackContext *context) = 0;

    virtual antlrcpp::Any visitInstCommInst(UIRParser::InstCommInstContext *context) = 0;

    virtual antlrcpp::Any visitRetVals(UIRParser::RetValsContext *context) = 0;

    virtual antlrcpp::Any visitDestClause(UIRParser::DestClauseContext *context) = 0;

    virtual antlrcpp::Any visitBb(UIRParser::BbContext *context) = 0;

    virtual antlrcpp::Any visitTypeList(UIRParser::TypeListContext *context) = 0;

    virtual antlrcpp::Any visitExcClause(UIRParser::ExcClauseContext *context) = 0;

    virtual antlrcpp::Any visitKeepaliveClause(UIRParser::KeepaliveClauseContext *context) = 0;

    virtual antlrcpp::Any visitArgList(UIRParser::ArgListContext *context) = 0;

    virtual antlrcpp::Any visitCurStackRetWith(UIRParser::CurStackRetWithContext *context) = 0;

    virtual antlrcpp::Any visitCurStackKillOld(UIRParser::CurStackKillOldContext *context) = 0;

    virtual antlrcpp::Any visitNewStackPassValue(UIRParser::NewStackPassValueContext *context) = 0;

    virtual antlrcpp::Any visitNewStackThrowExc(UIRParser::NewStackThrowExcContext *context) = 0;

    virtual antlrcpp::Any visitBinop(UIRParser::BinopContext *context) = 0;

    virtual antlrcpp::Any visitCmpop(UIRParser::CmpopContext *context) = 0;

    virtual antlrcpp::Any visitConvop(UIRParser::ConvopContext *context) = 0;

    virtual antlrcpp::Any visitMemord(UIRParser::MemordContext *context) = 0;

    virtual antlrcpp::Any visitAtomicrmwop(UIRParser::AtomicrmwopContext *context) = 0;

    virtual antlrcpp::Any visitBinopStatus(UIRParser::BinopStatusContext *context) = 0;

    virtual antlrcpp::Any visitCallConv(UIRParser::CallConvContext *context) = 0;

    virtual antlrcpp::Any visitFlag(UIRParser::FlagContext *context) = 0;

    virtual antlrcpp::Any visitWpid(UIRParser::WpidContext *context) = 0;

    virtual antlrcpp::Any visitIntParam(UIRParser::IntParamContext *context) = 0;

    virtual antlrcpp::Any visitSizeParam(UIRParser::SizeParamContext *context) = 0;

    virtual antlrcpp::Any visitIntLiteral(UIRParser::IntLiteralContext *context) = 0;

    virtual antlrcpp::Any visitFloatNumber(UIRParser::FloatNumberContext *context) = 0;

    virtual antlrcpp::Any visitFloatInf(UIRParser::FloatInfContext *context) = 0;

    virtual antlrcpp::Any visitFloatNan(UIRParser::FloatNanContext *context) = 0;

    virtual antlrcpp::Any visitFloatBits(UIRParser::FloatBitsContext *context) = 0;

    virtual antlrcpp::Any visitDoubleNumber(UIRParser::DoubleNumberContext *context) = 0;

    virtual antlrcpp::Any visitDoubleInf(UIRParser::DoubleInfContext *context) = 0;

    virtual antlrcpp::Any visitDoubleNan(UIRParser::DoubleNanContext *context) = 0;

    virtual antlrcpp::Any visitDoubleBits(UIRParser::DoubleBitsContext *context) = 0;

    virtual antlrcpp::Any visitStringLiteral(UIRParser::StringLiteralContext *context) = 0;

    virtual antlrcpp::Any visitGlobalName(UIRParser::GlobalNameContext *context) = 0;

    virtual antlrcpp::Any visitLocalName(UIRParser::LocalNameContext *context) = 0;

    virtual antlrcpp::Any visitName(UIRParser::NameContext *context) = 0;

    virtual antlrcpp::Any visitCommInst(UIRParser::CommInstContext *context) = 0;


};

