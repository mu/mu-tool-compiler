# Copyright 2017 The Australian National University
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

all:
	$(CXX) -O3 -std=gnu++14 -Wall -I$(ANTLR_HOME)/usr/local/include/antlr4-runtime/ -L/usr/local/lib -I/usr/local/include -L$(ANTLR_HOME)/usr/local/lib/ -L$(MU_ZEBU)/target/$(ZEBU_BUILD)/deps -lantlr4-runtime -lmu -lgmpxx -lgmp main.cpp parser/*.cpp -o muc

static:
	$(CXX) -O3 -std=gnu++14 -Wall -I$(ANTLR_HOME)/usr/local/include/antlr4-runtime/ -L/usr/local/lib -I/usr/local/include $(ANTLR_HOME)/usr/local/lib/libantlr4-runtime.a $(MU_ZEBU)/target/$(ZEBU_BUILD)/deps/libmu.a -lpthread -lm -ldl -lgmpxx -lgmp main.cpp parser/*.cpp -o muc

no_mu:
	$(CXX) -O3 -std=gnu++14 -DNO_MU -Wall -I$(ANTLR_HOME)/usr/local/include/antlr4-runtime/ -L/usr/local/lib -I/usr/local/include -L$(ANTLR_HOME)/usr/local/lib/ -lantlr4-runtime -lgmpxx -lgmp main.cpp parser/*.cpp -o muc
