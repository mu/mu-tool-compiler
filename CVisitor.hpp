// Copyright 2017 The Australian National University
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once
#include "Visitor.hpp"

// A string that represents an array
struct Array_String {
    std::string value;
    std::string size;
	std::vector<std::string> content;
    Array_String(std::string v, std::string s) : value(v), size(s) { }

	template<typename T>
	Array_String(std::string type, std::vector<T> c) :
            size(std::to_string(c.size())),
			content(c) {
        value += content.size() == 0 ? "("s + type + "*)NULL"s : "("s + type + "[]){"s + list_to_string(content) + "}"s;
    }
};

namespace C
{

	// ::type contains the type that is stored in an Any<T> (a string, pair of strings, or vector of strings)
	template<typename T> struct any_type { using type = std::string; };
	template<typename T> struct any_type<std::vector<T>> { using type = Array_String; };
	template<> struct any_type<void> { using type = void; };
	template<typename T, typename U> struct any_type<std::pair<T, U>> { using type = std::pair<typename any_type<T>::type, typename any_type<U>::type>; };

	template<typename T> using Any_Type = typename any_type<T>::type;

	template<typename T> Any_Type<T> as_type(antlrcpp::Any&& a) { return a.template as<Any_Type<T>>(); }
	template<> Any_Type<void> as_type<void>(antlrcpp::Any&& a) { return; }

	std::string primordial_id {"MU_NO_ID"};
	std::map<std::string, std::string> globals {};
    std::map<std::string, int> int_sizes {}; // The size of each int type
    std::set<std::string> funcdecls {}; // The set of all function declarations
	std::vector<std::string> whitelist {}; // The set of all things to pass to the whitelist of nake boot image
	std::map<std::string, std::string> inline_decls {};
	std::size_t last_global_id = 0;

	struct Visitor : UIRVisitor {
		std::ostream& stream;
		std::ostringstream buffer {};
		std::string irbuilder;
		std::size_t last_local_id = 0;

		Visitor(std::string ctx, std::ostream& s, std::string irb) : stream(s), irbuilder(irb)
		{
			generate_bundle_name();
			write_line(irbuilder + " = "s + ctx +"->new_ir_builder(" + ctx + ");");
		}

		void write_line(std::string s = ""s) {
			buffer << "\t\t" << s << std::endl;
		}

        std::map<std::string, std::string> symbols {};

        template<typename... Args> // returns function(args...) (with args seperated by commas)
		void generate_call(std::string function, Args... args) {
			write_line(function + "("s + list_to_string(std::vector<std::string>{args...}) + ");");
		}

		std::stack<std::string> current_ids; // This is the ids of all the objects we are constructing
		std::string current_type; // The type of the thing we are trying to construct
		int inline_decl = 0; // if this is greater than 0 we are inside an inline decl

		std::string gen_local(std::string name = ""s) {
			std::string id_name = "L("s + std::to_string(last_local_id++) + (name.empty() ? ""s : ", "s + name) + ")"s;
			write_line("MuID "s + id_name + " = " + irbuilder + "->gen_sym(" + irbuilder + ", " + (name.empty() || !is_valid_mu_name(name) ? "NULL" : "\"@" + name + "\"")+ ");");
			return id_name;
		}

		std::string gen_global(std::string name = ""s) {
			std::string id = std::to_string(last_global_id++);
			std::string id_name = "G("s + id + (name.empty() ? ""s : ", "s + name) + ")"s;
			stream << "\t" << "MuID "s + id_name + ";" << std::endl;
			write_line(id_name + " = " + irbuilder + "->gen_sym(" + irbuilder + ", " + (name.empty() || !is_valid_mu_name(name) ? "NULL" : "\"@" + name + "\"")+ ");");
			return id_name;
		}

		// Get the id of the global name
		std::string get_id(std::string name, bool global) {
            std::string id;
			if (symbols.count(name) == 0) {
				if (globals.count(name) != 0) {
                    id = symbols[name] = globals[name];
                } else {
                    if (global) {
							id = symbols[name] = globals[name] = gen_global(name);
						if (name[0] != '_')
							whitelist.push_back(id);
                    } else {
                        id = symbols[name] = gen_local(name);
					}
                }
			} else {
				id = symbols[name];
			}

            if (name == primordial_name) // We found the primordial function!
                primordial_id = id;

            return id;
		}

		// Calls 'accept' if the given tree (typically a context) is non-null and casts to 'T', otherwise returns def
		template<typename T>
		Any_Type<T> try_accept(antlr4::tree::ParseTree* tree, Any_Type<T> def) {
			return tree ? as_type<T>(tree->accept(this)) : def;
		}

		// Calls 'accept' on the given tree (typically a context) and casts the result to 'T'
		template<typename T = void>
		Any_Type<T> accept(antlr4::tree::ParseTree* tree) {
			return as_type<T>(tree->accept(this));
		}

		template<typename U>
		// The first string is an expression that will evaluate to an array
		// the second will evaluate to the length of the array
		Array_String accept_list(std::string type, std::vector<U*> trees) {
			std::vector<std::string> res; //(trees.size());

			for (auto tree : trees) {
				auto a = tree->accept(this);
                if (a.isNotNull())
                    res.push_back(as_type<std::string>(std::move(a)));
			}
            return Array_String(type, res);
        }
		template<typename U>
		void accept_list(std::vector<U*> trees) {
			for (auto tree : trees)
				tree->accept(this);
		}


		// str: A (unique) string representation of the declaration ctor
		// id: were to return the id we should use for the ctor
		// returns: true if we should continue with the declaration, or false if 'id' is already declared
		// (NOTE: This will push to current_ids)
		bool declare_inline(std::string str, std::string& id) {
			if (inline_decl) {
				// Already generated an equivelent declaration
				if (inline_decls.count(str)) {
					current_ids.push(id = inline_decls[str]);
					return false;
				} else {
					current_ids.push(id = inline_decls[str] = gen_global());
					return true;
				}
			} else {
				current_ids.push(id = current_ids.top());
				return true;
			}
		}

		/*********************************** NON TERMINALS ****************************/
		virtual Any<void> visitIr(UIRParser::IrContext *context) override {
            accept_list(context->children);

            write_line();
            write_line(irbuilder + "->load(" + irbuilder + ");");

            // We've finished! write the result to the end of the file
            stream << "\t{" << std::endl;
            stream << buffer.str();
            stream << "\t}" << std::endl;

            return Any<void>();
		}

		virtual Any<void> visitBundleDef(UIRParser::BundleDefContext *context) override {
			if (context->NAME()) // Interpret as a global name
				::bundle_name = context->NAME()->getText();
			else ::bundle_name = context->GLOBAL_NAME()->getText().substr(1, std::string::npos);

			return Any<void>();
		}

		virtual Any<void> visitTypeDef(UIRParser::TypeDefContext *context) override {
            write_line();
			current_ids.push(accept<MuID>(context->nam));
			accept(context->ctor);
			current_ids.pop();
			return Any<void>();
		}
		virtual Any<MuTypeNode> visitTypeInt(UIRParser::TypeIntContext *context) override {
			auto size = accept<int>(context->length);
			auto int_size = std::stoi(size, nullptr, 0);

			std::string id;
			if (declare_inline("int<"s + std::to_string(int_size) + ">"s, id)) {
				int_sizes[id] = int_size;
				generate_call(irbuilder + "->new_type_int"s, irbuilder, id, size);
			}
			current_ids.pop();
			return id;
		}
		virtual Any<MuTypeNode> visitTypeUFuncPtr(UIRParser::TypeUFuncPtrContext *context) override {
			auto sig = accept<MuFuncSigNode>(context->funcSig());
			std::string id;
			if (declare_inline("ufuncptr<"s + sig + ">"s, id))
				generate_call(irbuilder + "->new_type_ufuncptr"s, irbuilder, id, sig);
			current_ids.pop();
			return id;
		}
		virtual Any<MuTypeNode> visitTypeFuncRef(UIRParser::TypeFuncRefContext *context) override {
			auto sig = accept<MuFuncSigNode>(context->funcSig());
			std::string id;
			if (declare_inline("funcref<"s + sig + ">"s, id))
				generate_call(irbuilder + "->new_type_funcref"s, irbuilder, id, sig);
			current_ids.pop();
			return id;
		}
		virtual Any<MuTypeNode> visitTypeStruct(UIRParser::TypeStructContext *context) override {
			auto fieldtys = accept_list("MuTypeNode", context->fieldTys);
			std::string id;
			if (declare_inline("struct<"s + list_to_string(fieldtys.content) + ">"s, id))
				generate_call(irbuilder + "->new_type_struct"s, irbuilder, id, fieldtys.value, fieldtys.size);
			current_ids.pop();
			return id;
		}
		virtual Any<MuTypeNode> visitTypeHybrid(UIRParser::TypeHybridContext *context) override {
			auto fixedtys = accept_list("MuTypeNode", context->fieldTys);
			auto varty = accept<MuTypeNode>(context->varTy);
			std::string id;
			if (declare_inline("hybrid<"s + list_to_string(fixedtys.content) + " " + varty + ">"s, id))
				generate_call(irbuilder + "->new_type_hybrid"s, irbuilder, id, fixedtys.value, fixedtys.size, varty);
			current_ids.pop();
			return id;
		}
		virtual Any<MuTypeNode> visitTypeArray(UIRParser::TypeArrayContext *context) override {
			auto ty = accept<MuTypeNode>(context->ty);
			auto len = accept<uint64_t>(context->length);
			std::string id;
			if (declare_inline("array<"s + ty + " " + std::to_string((std::uint64_t)std::stoull(len, nullptr, 0)) + ">"s, id))
				generate_call(irbuilder + "->new_type_array"s, irbuilder, id, ty, len);
			current_ids.pop();
			return id;
		}
		virtual Any<MuTypeNode> visitTypeVector(UIRParser::TypeVectorContext *context) override {
			auto ty = accept<MuTypeNode>(context->ty);
			auto len = accept<uint64_t>(context->length);
			std::string id;
			if (declare_inline("vector<"s + ty + " " + std::to_string((std::uint64_t)std::stoull(len, nullptr, 0)) + ">"s, id))
				generate_call(irbuilder + "->new_type_vector"s, irbuilder, id, ty, len);
			current_ids.pop();
			return id;
		}
		virtual Any<MuTypeNode> visitTypeUPtr(UIRParser::TypeUPtrContext *context) override {
			auto ty = accept<MuTypeNode>(context->ty);
			std::string id;
			if (declare_inline("uptr<"s + ty + ">"s, id))
				generate_call(irbuilder + "->new_type_uptr"s, irbuilder, id, ty);
			current_ids.pop();
			return id;
		}
		virtual Any<MuTypeNode> visitTypeRef(UIRParser::TypeRefContext *context) override {
			auto ty = accept<MuTypeNode>(context->ty);
			std::string id;
			if (declare_inline("ref<"s + ty + ">"s, id))
				generate_call(irbuilder + "->new_type_ref"s, irbuilder, id, ty);
			current_ids.pop();
			return id;
		}
		virtual Any<MuTypeNode> visitTypeIRef(UIRParser::TypeIRefContext *context) override {
			auto ty = accept<MuTypeNode>(context->ty);
			std::string id;
			if (declare_inline("iref<"s + ty + ">"s, id))
				generate_call(irbuilder + "->new_type_iref"s, irbuilder, id, ty);
			current_ids.pop();
			return id;
		}
		virtual Any<MuTypeNode> visitTypeWeakRef(UIRParser::TypeWeakRefContext *context) override {
			auto ty = accept<MuTypeNode>(context->ty);
			std::string id;
			if (declare_inline("weakref<"s + ty + ">"s, id))
				generate_call(irbuilder + "->new_type_weakref"s, irbuilder, id, ty);
			current_ids.pop();
			return id;
		}
		virtual Any<MuTypeNode> visitTypeTagRef64(UIRParser::TypeTagRef64Context *context) override {
			std::string id;
			if (declare_inline("tagref64"s, id))
				generate_call(irbuilder + "->new_type_tagref64"s, irbuilder, id);
			current_ids.pop();
			return id;
		}
		virtual Any<MuTypeNode> visitTypeFloat(UIRParser::TypeFloatContext *context) override {
			std::string id;
			if (declare_inline("float"s, id))
				generate_call(irbuilder + "->new_type_float"s, irbuilder, id);
			current_ids.pop();
			return id;
		}
		virtual Any<MuTypeNode> visitTypeDouble(UIRParser::TypeDoubleContext *context) override {
			std::string id;
			if (declare_inline("double"s, id))
				generate_call(irbuilder + "->new_type_double"s, irbuilder, id);
			current_ids.pop();
			return id;
		}
		virtual Any<MuTypeNode> visitTypeThreadRef(UIRParser::TypeThreadRefContext *context) override {
			std::string id;
			if (declare_inline("threadref"s, id))
				generate_call(irbuilder + "->new_type_threadref"s, irbuilder, id);
			current_ids.pop();
			return id;
		}
		virtual Any<MuTypeNode> visitTypeVoid(UIRParser::TypeVoidContext *context) override {
			std::string id;
			if (declare_inline("void"s, id))
				generate_call(irbuilder + "->new_type_void"s, irbuilder, id);
			current_ids.pop();
			return id;
		}
		virtual Any<MuTypeNode> visitTypeStackRef(UIRParser::TypeStackRefContext *context) override {
			std::string id;
			if (declare_inline("stackref"s, id))
				generate_call(irbuilder + "->new_type_stackref"s, irbuilder, id);
			current_ids.pop();
			return id;
		}
		virtual Any<MuTypeNode> visitTypeFrameCursorRef(UIRParser::TypeFrameCursorRefContext *context) override {
			std::string id;
			if (declare_inline("framecursorref"s, id))
				generate_call(irbuilder + "->new_type_framecursorref"s, irbuilder, id);
			current_ids.pop();
			return id;
		}
		virtual Any<MuTypeNode> visitTypeIRBuilderRef(UIRParser::TypeIRBuilderRefContext *context) override {
			std::string id;
			if (declare_inline("irbuilderref"s, id))
				generate_call(irbuilder + "->new_type_irbuilderref"s, irbuilder, id);
			current_ids.pop();
			return id;
		}

		virtual Any<void> visitFuncSigDef(UIRParser::FuncSigDefContext *context) override {
            write_line();
			current_ids.push(accept<MuID>(context->nam));
			accept(context->ctor);
			current_ids.pop();
			return Any<void>();
		}
		virtual Any<std::string> visitFuncSigConstructor(UIRParser::FuncSigConstructorContext *context) override {
			auto paramtys = accept_list("MuTypeNode", context->paramTys);
			auto rettys = accept_list("MuTypeNode", context->retTys);

			std::string id;
			if (declare_inline("(" + list_to_string(paramtys.content) + ")->("s + list_to_string(rettys.content) + ")"s, id))
				generate_call(irbuilder + "->new_funcsig"s, irbuilder, id, paramtys.value, paramtys.size, rettys.value, rettys.size);
			current_ids.pop();
			return id;
		}

		virtual Any<void> visitConstDef(UIRParser::ConstDefContext *context) override {
            write_line();
			current_ids.push(accept<MuID>(context->nam));
			current_type = accept<MuTypeNode>(context->ty);
			accept(context->ctor);
			current_ids.pop();
			return Any<void>();
		}
		virtual Any<std::string> visitCtorInt(UIRParser::CtorIntContext *context) override {
			auto val = accept<std::string>(context->intLiteral());
            if (!int_sizes.count(current_type))
				throw antlr4::RuntimeException("Cannot determine size of integer constant (make sure the type declaration preceds the constant) at " +
						context->getSourceInterval().toString());
			std::size_t size = (std::size_t)int_sizes[current_type];

			std::string id;
            if (size <= 64) {
                // Can use the string directly
				auto v = (uint64_t)std::stoull(val, nullptr, 0);
				if (size < 64)
					v &= ((uint64_t)1 << size) - (uint64_t)1;

				if (declare_inline("<"s + current_type + ">"s + std::to_string(v), id))
                	generate_call(irbuilder + "->new_const_int"s, irbuilder, id, current_type, std::to_string(v));
            } else {
                mpz_class c {val}; // Read the integer (will auto detect base)
				if (declare_inline("<"s + current_type + ">"s + c.get_str(10), id)) {
					// Have to split the string into parts
					// What base was used to construct the integer (we use this so we can output c code with the same base)
					int base = context->intLiteral()->INT_DEC() ? 10 : context->intLiteral()->INT_OCT() ? 8 : 16;
					std::string prefix = base == 16 ? "0x"s : base == 0 ? "0"s : ""s;

					// Set n = ceil(size/64), (i.e. how many uint64_t's we have to split c into)
					std::size_t n = (size + 64 - 1) / 64;

					std::vector<std::string> vals;
					vals.reserve((std::size_t) n);
					for (std::size_t i = 0; i < n; i++) {
						// Truncate the result to get the lowest 64-bits and convert to a string
						vals.push_back(prefix + mpz_class(c & mask_64).get_str(base));
						c >>= 64; // Shift c right by 64-bits
					}
					Array_String avals("uint64_t", vals);

					generate_call(irbuilder + "->new_const_int_ex"s, irbuilder, id, current_type, avals.value, avals.size);
				}
			}
			current_ids.pop();
			return id;
		}
		virtual Any<MuVarNode> visitCtorFloat(UIRParser::CtorFloatContext *context) override {
			auto f = accept<float>(context->floatLiteral());
			std::string id;
			if (declare_inline("<"s + current_type + ">"s + f, id))
				generate_call(irbuilder + "->new_const_float"s, irbuilder, id, current_type, f);
			current_ids.pop();
			return id;
		}
		virtual Any<MuVarNode> visitCtorDouble(UIRParser::CtorDoubleContext *context) override {
			auto d = accept<double>(context->doubleLiteral());
			std::string id;
			if (declare_inline("<"s + current_type + ">"s + d, id))
				generate_call(irbuilder + "->new_const_double"s, irbuilder, id, current_type, d);
			current_ids.pop();
			return id;
		}
		virtual Any<MuVarNode> visitCtorNull(UIRParser::CtorNullContext *context) override {
			std::string id;
			if (declare_inline("<"s + current_type + ">NULL"s, id))
				generate_call(irbuilder + "->new_const_null"s, irbuilder, id, current_type);
			current_ids.pop();
			return id;
		}
		virtual Any<MuVarNode> visitCtorList(UIRParser::CtorListContext *context) override {
			auto elems = accept_list("MuGlobalVarNode", context->globalVar());
			std::string id;
			if (declare_inline("<"s + current_type + ">{"s + list_to_string(elems.content) + "}"s, id))
				generate_call(irbuilder + "->new_const_seq"s, irbuilder, id, current_type, elems.value, elems.size);
			current_ids.pop();
			return id;
		}
		virtual Any<MuVarNode> visitCtorExtern(UIRParser::CtorExternContext *context) override {
			auto s = accept<MuCString>(context->stringLiteral());
			std::string id;
			if (declare_inline("<"s + current_type + ">"s + s + ""s, id))
				generate_call(irbuilder + "->new_const_extern"s, irbuilder, id, current_type, s);
			current_ids.pop();
			return id;
		}

		virtual Any<void> visitGlobalDef(UIRParser::GlobalDefContext *context) override {
			generate_call(irbuilder + "->new_global_cell"s, irbuilder, accept<MuGlobalVarNode>(context->nam), accept<MuID>(context->ty));
			return Any<void>();
		}

		virtual Any<void> visitFuncDecl(UIRParser::FuncDeclContext *context) override {
            write_line();
			auto id = accept<MuID>(context->nam);

            // Don't create a funcdecl if there is already one
            if (funcdecls.count(id) == 0) {
                funcdecls.insert(id);
                generate_call(irbuilder + "->new_func"s, irbuilder, id, accept<MuFuncSigNode>(context->sig));
            }

			return Any<void>();
		}

		virtual Any<void> visitFuncExpDef(UIRParser::FuncExpDefContext *context) override {
            write_line();
			generate_call(irbuilder + "->new_exp_func"s, irbuilder, accept<MuID>(context->nam), accept<MuFuncNode>(context->func()),
									accept<MuCallConv>(context->callConv()),
									accept<MuConstNode>(context->cookie));
			return Any<void>();
		}

		virtual Any<void> visitFuncDef(UIRParser::FuncDefContext *context) override {
            write_line();
			// The name of the function, function version names are relative to this
			auto func_name = accept<MuFuncNode>(context->nam);
			::parent_names.push(::last_name); // the global name of the function

            if (context->sig && funcdecls.count(func_name) == 0) {
                funcdecls.insert(func_name);
                generate_call(irbuilder + "->new_func"s, irbuilder, func_name, accept<MuFuncSigNode>(context->sig));
            }

			if (context->ver)
				current_ids.push(accept<MuID>(context->ver)); // the id of the function version we are constructing
			else
				current_ids.push(gen_local());

			::parent_names.push(::parent_names.top() + ".__"s + std::to_string(last_global_id++)); // the global name of the function version

			auto bbs = accept_list("MuBBNode", context->body);

			generate_call(irbuilder + "->new_func_ver"s, irbuilder, current_ids.top(), func_name, bbs.value, bbs.size);

			::parent_names.pop();
			::parent_names.pop();

			current_ids.pop();
			return Any<void>();
		}

		virtual Any<MuBBNode> visitBasicBlock(UIRParser::BasicBlockContext *context) override {
			auto id = accept<MuID>(context->nam);
			::parent_names.push(::last_name); // the global name of the basic block

			auto nor_param_ids = accept_list("MuID", context->params);
			auto exc_param_id = try_accept<MuID>(context->exc, "MU_NO_ID"s);
			auto nor_param_types = accept_list("MuTypeNode", context->param_tys);
			auto insts = accept_list("MuInstNode", context->inst());

			generate_call(irbuilder + "->new_bb"s, irbuilder, id,
							  nor_param_ids.value, nor_param_types.value, nor_param_ids.size,
							  exc_param_id,
							  insts.value, insts.size);

			::parent_names.pop();
			return id;
		}
		virtual Any<MuInstNode> visitInstBinOp(UIRParser::InstBinOpContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_binop"s, irbuilder, id,
								 accept<MuID>(context->instResult()),
								 accept<MuBinOptr>(context->binop()),
								 accept<MuTypeNode>(context->ty),
								 accept<MuVarNode>(context->op1),
								 accept<MuVarNode>(context->op2),
								 try_accept<MuExcClause>(context->excClause(), "MU_NO_ID"s));
			return id;
		}
		virtual Any<MuInstNode> visitInstBinOpStatus(UIRParser::InstBinOpStatusContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto results = accept<std::pair<MuID, std::vector<MuID>>>(context->pairResults());

			generate_call(irbuilder + "->new_binop_with_status"s, irbuilder, id,
								 results.first, results.second.value, results.second.size,
								 accept<MuBinOptr>(context->binop()),
								 accept<MuBinOpStatus>(context->binopStatus()),
								 accept<MuTypeNode>(context->ty),
								 accept<MuVarNode>(context->op1),
								 accept<MuVarNode>(context->op2),
								 try_accept<MuExcClause>(context->excClause(), "MU_NO_ID"s));
			return id;
		}
		virtual Any<MuInstNode> visitInstCmp(UIRParser::InstCmpContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_cmp"s, irbuilder, id,
								 accept<MuID>(context->instResult()),
								 accept<MuCmpOptr>(context->cmpop()),
								 accept<MuTypeNode>(context->ty),
								 accept<MuVarNode>(context->op1),
								 accept<MuVarNode>(context->op2));
			return id;
		}
		virtual Any<MuInstNode> visitInstConversion(UIRParser::InstConversionContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_conv"s, irbuilder, id,
							   accept<MuID>(context->instResult()),
							   accept<MuConvOptr>(context->convop()),
							   accept<MuTypeNode>(context->fromTy),
							   accept<MuTypeNode>(context->toTy),
							   accept<MuVarNode>(context->opnd));
			return id;
		}
		virtual Any<MuInstNode> visitInstSelect(UIRParser::InstSelectContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_select"s, irbuilder, id,
								accept<MuID>(context->instResult()),
								accept<MuTypeNode>(context->condTy),
								accept<MuTypeNode>(context->resTy),
								accept<MuVarNode>(context->cond),
								accept<MuVarNode>(context->ifTrue),
								accept<MuVarNode>(context->ifFalse));
			return id;
		}
		virtual Any<MuInstNode> visitInstBranch(UIRParser::InstBranchContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_branch"s, irbuilder, id, accept<MuDestClause>(context->dest));
			return id;
		}
		virtual Any<MuInstNode> visitInstBranch2(UIRParser::InstBranch2Context *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_branch2"s, irbuilder, id, accept<MuVarNode>(context->cond),
								   accept<MuDestClause>(context->ifTrue),
								   accept<MuDestClause>(context->ifFalse));
			return id;
		}
		virtual Any<MuInstNode> visitInstSwitch(UIRParser::InstSwitchContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto cases = accept_list("MuInstNode", context->caseVal);
			auto dests = accept_list("MuInstNode", context->caseDest);

			generate_call(irbuilder + "->new_switch"s, irbuilder, id,
								   accept<MuTypeNode>(context->ty),
								   accept<MuVarNode>(context->opnd),
								   accept<MuDestClause>(context->defDest),
								   cases.value, dests.value, cases.size);

			return id;
		}
		virtual Any<MuInstNode> visitInstCall(UIRParser::InstCallContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto result = accept<std::vector<MuID>>(context->instResults());
			auto args = accept<std::vector<MuVarNode>>(context->argList());
			generate_call(irbuilder + "->new_call"s, irbuilder, id,
								result.value, result.size,
								accept<MuFuncSigNode>(context->funcSig()),
								accept<MuVarNode>(context->callee),
								args.value, args.size,
								try_accept<MuExcClause>(context->excClause(), "MU_NO_ID"s),
								try_accept<MuKeepaliveClause>(context->keepaliveClause(), "MU_NO_ID"s));
			return id;
		}
		virtual Any<MuInstNode> visitInstTailCall(UIRParser::InstTailCallContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto args = accept<std::vector<MuVarNode>>(context->argList());
			generate_call(irbuilder + "->new_tailcall"s, irbuilder, id,
									accept<MuFuncSigNode>(context->funcSig()),
									accept<MuVarNode>(context->callee),
									args.value, args.size);
			return id;
		}
		virtual Any<MuInstNode> visitInstRet(UIRParser::InstRetContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto rvs = accept<std::vector<MuVarNode>>(context->retVals());;
			generate_call(irbuilder + "->new_ret"s, irbuilder, id, rvs.value, rvs.size);
			return id;
		}
		virtual Any<MuInstNode> visitInstThrow(UIRParser::InstThrowContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_throw"s, irbuilder, id, accept<MuVarNode>(context->exc));
			return id;
		}
		virtual Any<MuInstNode> visitInstExtractValue(UIRParser::InstExtractValueContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_extractvalue"s, irbuilder, id,
										accept<MuID>(context->instResult()),
										accept<MuTypeNode>(context->ty),
										accept<int>(context->intParam()),
										accept<MuVarNode>(context->opnd));
			return id;
		}
		virtual Any<MuInstNode> visitInstInsertValue(UIRParser::InstInsertValueContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_insertvalue"s, irbuilder, id,
									   accept<MuID>(context->instResult()),
									   accept<MuTypeNode>(context->ty),
									   accept<int>(context->intParam()),
									   accept<MuVarNode>(context->opnd),
									   accept<MuVarNode>(context->newVal));
			return id;
		}
		virtual Any<MuInstNode> visitInstExtractElement(UIRParser::InstExtractElementContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_extractelement"s, irbuilder, id,
										  accept<MuID>(context->instResult()),
										  accept<MuTypeNode>(context->seqTy),
										  accept<MuTypeNode>(context->indTy),
										  accept<MuVarNode>(context->opnd),
										  accept<MuVarNode>(context->index));
			return id;
		}
		virtual Any<MuInstNode> visitInstInsertElement(UIRParser::InstInsertElementContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_insertelement"s, irbuilder, id,
										 accept<MuID>(context->instResult()),
										 accept<MuTypeNode>(context->seqTy),
										 accept<MuTypeNode>(context->indTy),
										 accept<MuVarNode>(context->opnd),
										 accept<MuVarNode>(context->index),
										 accept<MuVarNode>(context->newVal));
			return id;
		}
		virtual Any<MuInstNode> visitInstShuffleVector(UIRParser::InstShuffleVectorContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_shufflevector"s, irbuilder, id,
										 accept<MuID>(context->instResult()),
										 accept<MuTypeNode>(context->vecTy),
										 accept<MuTypeNode>(context->maskTy),
										 accept<MuVarNode>(context->vec1),
										 accept<MuVarNode>(context->vec2),
										 accept<MuVarNode>(context->mask));
			return id;
		}
		virtual Any<MuInstNode> visitInstNew(UIRParser::InstNewContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_new"s, irbuilder, id,
							   accept<MuID>(context->instResult()),
							   accept<MuTypeNode>(context->allocTy),
							   try_accept<MuExcClause>(context->excClause(), "MU_NO_ID"s));
			return id;
		}
		virtual Any<MuInstNode> visitInstNewHybrid(UIRParser::InstNewHybridContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_newhybrid"s, irbuilder, id,
									 accept<MuID>(context->instResult()),
									 accept<MuTypeNode>(context->allocTy),
									 accept<MuTypeNode>(context->lenTy),
									 accept<MuVarNode>(context->length),
									 try_accept<MuExcClause>(context->excClause(), "MU_NO_ID"s));
			return id;
		}
		virtual Any<MuInstNode> visitInstAlloca(UIRParser::InstAllocaContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_alloca"s, irbuilder, id,
								  accept<MuID>(context->instResult()),
								  accept<MuTypeNode>(context->allocTy),
								  try_accept<MuExcClause>(context->excClause(), "MU_NO_ID"s));
			return id;
		}
		virtual Any<MuInstNode> visitInstAllocaHybrid(UIRParser::InstAllocaHybridContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_allocahybrid"s, irbuilder, id,
										accept<MuID>(context->instResult()),
										accept<MuTypeNode>(context->allocTy),
										accept<MuTypeNode>(context->lenTy),
										accept<MuVarNode>(context->length),
										try_accept<MuExcClause>(context->excClause(), "MU_NO_ID"s));
			return id;
		}
		virtual Any<MuInstNode> visitInstGetIRef(UIRParser::InstGetIRefContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_getiref"s, irbuilder, id,
								   accept<MuID>(context->instResult()),
								   accept<MuTypeNode>(context->refTy),
								   accept<MuVarNode>(context->opnd));
			return id;
		}
		virtual Any<MuInstNode> visitInstGetFieldIRef(UIRParser::InstGetFieldIRefContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_getfieldiref"s, irbuilder, id,
										accept<MuID>(context->instResult()),
										std::to_string(context->ptr != nullptr),
										accept<MuTypeNode>(context->refTy),
										accept<int>(context->index),
										accept<MuVarNode>(context->opnd));
			return id;
		}
		virtual Any<MuInstNode> visitInstGetElemIRef(UIRParser::InstGetElemIRefContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_getelemiref"s, irbuilder, id,
									   accept<MuID>(context->instResult()),
									   std::to_string(context->ptr != nullptr),
									   accept<MuTypeNode>(context->refTy),
									   accept<MuTypeNode>(context->indTy),
									   accept<MuVarNode>(context->opnd),
									   accept<MuVarNode>(context->index));
			return id;
		}
		virtual Any<MuInstNode> visitInstShiftIRef(UIRParser::InstShiftIRefContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_shiftiref"s, irbuilder, id,
									 accept<MuID>(context->instResult()),
									 std::to_string(context->ptr != nullptr),
									 accept<MuTypeNode>(context->refTy),
									 accept<MuTypeNode>(context->offTy),
									 accept<MuVarNode>(context->opnd),
									 accept<MuVarNode>(context->offset));
			return id;
		}
		virtual Any<MuInstNode> visitInstGetVarPartIRef(UIRParser::InstGetVarPartIRefContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_getvarpartiref"s, irbuilder, id,
										  accept<MuID>(context->instResult()),
										  std::to_string(context->ptr != nullptr),
										  accept<MuTypeNode>(context->refTy),
										  accept<MuVarNode>(context->opnd));
			return id;
		}
		virtual Any<MuInstNode> visitInstLoad(UIRParser::InstLoadContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_load"s, irbuilder, id,
								accept<MuID>(context->instResult()),
								std::to_string(context->ptr != nullptr),
								try_accept<MuMemOrd>(context->memord(), "MU_ORD_NOT_ATOMIC"s),
								accept<MuTypeNode>(context->type()),
								accept<MuVarNode>(context->loc),
								try_accept<MuExcClause>(context->excClause(), "MU_NO_ID"s));
			return id;
		}
		virtual Any<MuInstNode> visitInstStore(UIRParser::InstStoreContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_store"s, irbuilder, id,
								 std::to_string(context->ptr != nullptr),
								 try_accept<MuMemOrd>(context->memord(), "MU_ORD_NOT_ATOMIC"s),
								 accept<MuTypeNode>(context->type()),
								 accept<MuVarNode>(context->loc),
								 accept<MuVarNode>(context->newVal),
								 try_accept<MuExcClause>(context->excClause(), "MU_NO_ID"s));
			return id;
		}
		virtual Any<MuInstNode> visitInstCmpXchg(UIRParser::InstCmpXchgContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto result = accept<std::pair<MuID, MuID>>(context->pairResult());
			generate_call(irbuilder + "->new_cmpxchg"s, irbuilder, id,
								   result.first, result.second,
								   std::to_string(context->ptr != nullptr),
								   std::to_string(context->isWeak != nullptr),
								   accept<MuMemOrd>(context->ordSucc),
								   accept<MuMemOrd>(context->ordFail),
								   accept<MuTypeNode>(context->type()),
								   accept<MuVarNode>(context->loc),
								   accept<MuVarNode>(context->expected),
								   accept<MuVarNode>(context->desired),
								   try_accept<MuExcClause>(context->excClause(), "MU_NO_ID"s));
			return id;
		}
		virtual Any<MuInstNode> visitInstAtomicRMW(UIRParser::InstAtomicRMWContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_atomicrmw"s, irbuilder, id,
									 accept<MuID>(context->instResult()),
									 std::to_string(context->ptr != nullptr),
									 accept<MuMemOrd>(context->memord()),
									 accept<MuAtomicRMWOptr>(context->atomicrmwop()),
									 accept<MuTypeNode>(context->type()),
									 accept<MuVarNode>(context->loc),
									 accept<MuVarNode>(context->opnd),
									 try_accept<MuExcClause>(context->excClause(), "MU_NO_ID"s));
			return id;
		}
		virtual Any<MuInstNode> visitInstFence(UIRParser::InstFenceContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_fence"s, irbuilder, id, accept<MuMemOrd>(context->memord()));
			return id;
		}
		virtual Any<MuInstNode> visitInstTrap(UIRParser::InstTrapContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto result_ids = accept<std::vector<MuID>>(context->instResults());
			auto rettys = accept<std::vector<MuTypeNode>>(context->typeList());

			generate_call(irbuilder + "->new_trap"s, irbuilder, id,
								result_ids.value, rettys.value, result_ids.size,
								try_accept<MuExcClause>(context->excClause(), "MU_NO_ID"s),
								try_accept<MuKeepaliveClause>(context->keepaliveClause(), "MU_NO_ID"s));
			return id;
		}
		virtual Any<MuInstNode> visitInstWatchPoint(UIRParser::InstWatchPointContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto result_ids = accept<std::vector<MuID>>(context->instResults());
			auto rettys = accept<std::vector<MuTypeNode>>(context->typeList());

			generate_call(irbuilder + "->new_watchpoint"s, irbuilder, id,
									  accept<MuWPID>(context->wpid()),
									  result_ids.value, rettys.value, result_ids.size,
									  accept<MuDestClause>(context->dis),
									  accept<MuDestClause>(context->ena),
									  try_accept<MuDestClause >(context->wpExc, "MU_NO_ID"s),
									  try_accept<MuKeepaliveClause>(context->keepaliveClause(), "MU_NO_ID"s));
			return id;
		}
		virtual Any<MuInstNode> visitInstWPBranch(UIRParser::InstWPBranchContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_wpbranch"s, irbuilder, id,
									accept<MuWPID>(context->wpid()),
									accept<MuDestClause>(context->dis),
									accept<MuDestClause>(context->ena));
			return id;
		}
		virtual Any<MuInstNode> visitInstCCall(UIRParser::InstCCallContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto result = accept<std::vector<MuID>>(context->instResults());
			auto args = accept<std::vector<MuVarNode>>(context->argList());

			generate_call(irbuilder + "->new_ccall"s, irbuilder, id,
								 result.value, result.size,
								 accept<MuCallConv>(context->callConv()),
								 accept<MuTypeNode>(context->funcTy),
								 accept<MuFuncSigNode>(context->funcSig()),
								 accept<MuVarNode>(context->callee),
								 args.value, args.size,
								 try_accept<MuExcClause>(context->excClause(), "MU_NO_ID"s),
								 try_accept<MuKeepaliveClause>(context->keepaliveClause(), "MU_NO_ID"s));
			return id;
		}
		virtual Any<MuInstNode> visitInstNewThread(UIRParser::InstNewThreadContext *context) override {
			auto id = accept<MuID>(context->instName());

			generate_call(irbuilder + "->new_newthread"s, irbuilder, id,
									 accept<MuID>(context->instResult()),
									 accept<MuVarNode>(context->stack),
									 try_accept<MuVarNode>(context->threadLocal, "MU_NO_ID"s),
									 accept<MuNewStackClause>(context->newStackClause()),
									 try_accept<MuExcClause>(context->excClause(), "MU_NO_ID"s));
			return id;
		}
		virtual Any<MuInstNode> visitInstSwapStack(UIRParser::InstSwapStackContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto result_ids = accept<std::vector<MuID>>(context->instResults());
			generate_call(irbuilder + "->new_swapstack"s, irbuilder, id,
									 result_ids.value, result_ids.size,
									 accept<MuVarNode>(context->swappee),
									 accept<MuCurStackClause>(context->curStackClause()),
									 accept<MuNewStackClause>(context->newStackClause()),
									 try_accept<MuExcClause>(context->excClause(), "MU_NO_ID"s),
									 try_accept<MuKeepaliveClause>(context->keepaliveClause(), "MU_NO_ID"s));
			return id;
		}
		virtual Any<MuInstNode> visitInstCommInst(UIRParser::InstCommInstContext *context) override {
			auto id = accept<MuID>(context->instName());

			auto result_ids = accept<std::vector<MuID>>(context->instResults());

			auto flags = accept_list("MuFlag", context->flags);
			auto tys = accept_list("MuTypeNode", context->tys);
			auto sigs = accept_list("MuFuncSigNode", context->sigs);
			auto args = accept_list("MuVarNode", context->args);


			generate_call(irbuilder + "->new_comminst"s, irbuilder, id,
									result_ids.value, result_ids.size,
									accept<MuCommInst>(context->commInst()),

									flags.value, flags.size,
									tys.value, tys.size,
									sigs.value, sigs.size,
									args.value, args.size,

									try_accept<MuExcClause>(context->excClause(), "MU_NO_ID"s),
									try_accept<MuKeepaliveClause>(context->keepaliveClause(), "MU_NO_ID"s));

			return id;
		}

		/***************** Used by instructions **********************/
		virtual Any<MuID> visitInstName(UIRParser::InstNameContext *context) override {
			write_line();
			return context->nam ? accept<MuID>(context->nam) : gen_local();
		}
		virtual Any<std::vector<MuID>> visitInstResults(UIRParser::InstResultsContext *context) override {
			return accept_list("MuVarNode", context->results);

		}
		virtual Any<std::pair<MuID, MuID>> visitPairResult(UIRParser::PairResultContext *context) override {
			return std::make_pair(accept<MuID>(context->first), accept<MuID>(context->second));
		}
		virtual Any<std::pair<MuID, std::vector<MuID>>> visitPairResults(UIRParser::PairResultsContext *context) override {
			return std::make_pair(accept<MuID>(context->first), accept_list("MuID", context->second));
		}
		virtual Any<std::vector<MuTypeNode>> visitTypeList(UIRParser::TypeListContext *context) override {
			return accept_list("MuTypeNode", context->tys);
		}

		virtual Any<MuExcClause> visitExcClause(UIRParser::ExcClauseContext *context) override {
			std::string id = gen_local();

			generate_call(irbuilder + "->new_exc_clause"s, irbuilder, id, accept<MuDestClause>(context->nor), accept<MuDestClause>(context->exc));
			return id;
		}
		virtual Any<MuDestClause> visitDestClause(UIRParser::DestClauseContext *context) override {
			std::string id = gen_local();
			auto vars = accept<std::vector<MuVarNode>>(context->argList());

            std::string last_parent = ::parent_names.top();
            ::parent_names.pop();
            // local BB names are relative to the enclosing function (not the enclosing block)
            auto bb = accept<MuBBNode>(context->bb());
            ::parent_names.push(last_parent);

			generate_call(irbuilder + "->new_dest_clause"s, irbuilder, id, bb, vars.value, vars.size);
			return id;
		}
		virtual Any<MuKeepaliveClause> visitKeepaliveClause(UIRParser::KeepaliveClauseContext *context) override {
			std::string id = gen_local();
			auto vars = accept_list("MuLocalVarNode", context->value());
			generate_call(irbuilder + "->new_keepalive_clause"s, irbuilder, id, vars.value, vars.size);
			return id;
		}
		virtual Any<MuNewStackClause> visitNewStackThrowExc(UIRParser::NewStackThrowExcContext *context) override {
			std::string id = gen_local();
			generate_call(irbuilder + "->new_nsc_throw_exc"s, irbuilder, id, accept<MuVarNode>(context->exc));
			return id;
		}
		virtual Any<MuNewStackClause> visitNewStackPassValue(UIRParser::NewStackPassValueContext *context) override {
			std::string id = gen_local();
			auto tys = accept<std::vector<MuTypeNode>>(context->typeList());
			auto vars = accept<std::vector<MuVarNode>>(context->argList());
			generate_call(irbuilder + "->new_nsc_pass_values"s, irbuilder, id, tys.value, vars.value, vars.size);
			return id;
		}
		virtual Any<MuCurStackClause> visitCurStackKillOld(UIRParser::CurStackKillOldContext *context) override {
			std::string id = gen_local();
			generate_call(irbuilder + "->new_csc_kill_old"s, irbuilder, id);
			return id;
		}
		virtual Any<MuCurStackClause> visitCurStackRetWith(UIRParser::CurStackRetWithContext *context) override {
			std::string id = gen_local();
			auto rettys = accept<std::vector<MuTypeNode>>(context->typeList());
			generate_call(irbuilder + "->new_csc_ret_with"s, irbuilder, id, rettys.value, rettys.size);
			return id;
		}

		virtual Any<std::vector<MuVarNode>> visitArgList(UIRParser::ArgListContext *context) override {
			return accept_list("MuVarNode", context->value());
		}
		virtual Any<std::vector<MuVarNode>> visitRetVals(UIRParser::RetValsContext *context) override {
			return accept_list("MuVarNode", context->vals);
		}

		// Inline declarations or a name
		virtual Any<MuTypeNode> visitType(UIRParser::TypeContext *context) override {
			if (context->nam)
				return accept<MuID>(context->nam);
			else {
				inline_decl++;

				auto id = accept<MuTypeNode>(context->typeConstructor());

				inline_decl--;
				return id;
			}
		}

		virtual Any<MuFuncSigNode> visitFuncSig(UIRParser::FuncSigContext *context) override {
			if (context->nam)
				return accept<MuID>(context->nam);
			else {
				inline_decl++;

				auto id = accept<MuFuncSigNode>(context->funcSigConstructor());

				inline_decl--;
				return id;
			}
		}
		virtual Any<MuConstNode> visitConstant(UIRParser::ConstantContext *context) override {
			if (context->nam)
				return accept<MuID>(context->nam);
			else {
				std::string old_type = current_type;
				inline_decl++;

				current_type = accept<MuTypeNode>(context->ty);
				auto id = accept<MuConstNode>(context->constConstructor());

				inline_decl--;
				current_type = old_type;
				return id;
			}
		}
		// ******************************* TERMINALS *****************************

		// Names
        virtual Any<MuID> visitGlobalName(UIRParser::GlobalNameContext *context) override {
            // References a top level entity, interpret as a global name
            if (context->NAME())
                return get_id(::last_name = context->NAME()->getText(), true);
            else if (context->LOCAL_NAME())
                return get_id(::last_name = ::bundle_name + "."s + context->LOCAL_NAME()->getText().substr(1, std::string::npos), true);
            else // if (context->GLOBAL_NAME())
                return get_id(::last_name = context->GLOBAL_NAME()->getText().substr(1, std::string::npos), true);
        }
        virtual Any<MuID> visitLocalName(UIRParser::LocalNameContext *context) override {
            if (context->NAME()) // Interpret as a local name
                return get_id(::last_name = ::parent_names.top() + "."s + context->NAME()->getText(), false);
            else if (context->LOCAL_NAME())
                return get_id(::last_name = ::parent_names.top() + "."s + context->LOCAL_NAME()->getText().substr(1, std::string::npos), false);
            else // if (context->GLOBAL_NAME())
                return get_id(::last_name = context->GLOBAL_NAME()->getText().substr(1, std::string::npos), false);
        }
        virtual Any<MuID> visitName(UIRParser::NameContext *context) override {
            // Interpret as a local name if previously defined, otherwise global

            std::string suffix =
                  context->NAME()       ? context->NAME()->getText()
                : context->LOCAL_NAME() ? context->LOCAL_NAME()->getText().substr(1, std::string::npos)
                                        : context->GLOBAL_NAME()->getText().substr(1, std::string::npos);

            ::last_name = context->GLOBAL_NAME() ? suffix : ::parent_names.top() + "."s + suffix;

            if (symbols.count(::last_name) != 0)
                return get_id(::last_name, false);

            // Must be a global name
            ::last_name = context->LOCAL_NAME() ? ::bundle_name + "."s + suffix : suffix;

            return get_id(::last_name, true);
        }

		virtual Any<MuWPID> visitWpid(UIRParser::WpidContext *context) override {
			// MuWPID is an alias for uint32_t and 'unsigned long' is required to be at least 32 bits
			return accept<std::string>(context->intLiteral());
		}
		virtual Any<int> visitIntParam(UIRParser::IntParamContext *context) override {
			return accept<std::string>(context->intLiteral());
		}
		virtual Any<uint64_t> visitSizeParam(UIRParser::SizeParamContext *context) override {
			// Unsigned long long is required to be at least 64 bits
			return accept<std::string>(context->intLiteral());
		}
		virtual Any<std::string> visitIntLiteral(UIRParser::IntLiteralContext *context) override {
			return context->getText();
		}

		virtual Any<float> visitFloatNumber(UIRParser::FloatNumberContext *context) override {
			return context->FP_NUM()->getText();
		}
		virtual Any<float> visitFloatInf(UIRParser::FloatInfContext *context) override {
			if (context->INF()->getText()[0] == '-')
				return "-INFINITY"s;
			else return "INFINITY"s;
		}
		virtual Any<float> visitFloatNan(UIRParser::FloatNanContext *context) override {
			return "NAN"s;
		}
		virtual Any<float> visitFloatBits(UIRParser::FloatBitsContext *context) override {
			// Note: some floating point numbers can't be represented easily in C source code
			// (such as all the diferent NaNs), So we emit code that uses this union hack to be safe

			return "((union {uint32_t i; float f;}){"s
				   + std::to_string(std::stoul(accept<std::string>(context->intLiteral()), nullptr, 0))
				   + "}).f"s;
		}

		virtual Any<double> visitDoubleNumber(UIRParser::DoubleNumberContext *context) override {
			return context->FP_NUM()->getText();
		}
		virtual Any<double> visitDoubleInf(UIRParser::DoubleInfContext *context) override {
			if (context->INF()->getText()[0] == '-')
				return "-HUGE_VAL"s;
			else return "HUGE_VAL"s;
		}
		virtual Any<double> visitDoubleNan(UIRParser::DoubleNanContext *context) override {
			return "nan(\"\")"s; // Returns the default double nan
		}
		virtual Any<double> visitDoubleBits(UIRParser::DoubleBitsContext *context) override {
			return "((union {uint64_t i; double d;}){"s
				   + std::to_string(std::stoull(accept<std::string>(context->intLiteral()), nullptr, 0))
				   + "}).d"s;
		}

		virtual Any<MuCString> visitStringLiteral(UIRParser::StringLiteralContext *context) override {
			return context->STRING_LITERAL()->getText();
		}


		// Flags
		virtual Any<MuCallConv> visitCallConv(UIRParser::CallConvContext *context) override {
			return "MU_CC_"s + context->getText().substr(1, std::string::npos); // skip the leading '#'
		}
		virtual Any<MuBinOpStatus> visitBinopStatus(UIRParser::BinopStatusContext *context) override {
			return "MU_BOS_"s + context->getText().substr(1, std::string::npos); // skip the leading '#'
		}

		// Operators
		virtual Any<MuBinOptr> visitBinop(UIRParser::BinopContext *context) override {
			return "MU_BINOP_"s + context->getText();
		}
		virtual Any<MuCmpOptr> visitCmpop(UIRParser::CmpopContext *context) override {
			return "MU_CMP_"s + context->getText();
		}
		virtual Any<MuConvOptr> visitConvop(UIRParser::ConvopContext *context) override {
			return "MU_CONV_"s + context->getText();
		}
		virtual Any<MuMemOrd> visitMemord(UIRParser::MemordContext *context) override {
			return "MU_ORD_"s + context->getText();
		}
		virtual Any<MuAtomicRMWOptr> visitAtomicrmwop(UIRParser::AtomicrmwopContext *context) override {
			return "MU_ARMW_"s + context->getText();
		}
		virtual Any<MuCommInst> visitCommInst(UIRParser::CommInstContext *context) override {
			std::string op = context->NAME() ? context->NAME()->getText()
                                             : context->GLOBAL_NAME()->getText().substr(1, std::string::npos);
			// Use the following regex on the MU_CI... definition section in muapi.h to generate this code:
            //	search		#[^ ]* ([A-Z_0-9]*)[^@]*@([a-z0-9.-_]*)
            //	replace		: op == "$2" ? "$1"s
			return op == "uvm.new_stack" ? "MU_CI_UVM_NEW_STACK"s
				 : op == "uvm.kill_stack" ? "MU_CI_UVM_KILL_STACK"s
				 : op == "uvm.thread_exit" ? "MU_CI_UVM_THREAD_EXIT"s
				 : op == "uvm.current_stack" ? "MU_CI_UVM_CURRENT_STACK"s
				 : op == "uvm.set_threadlocal" ? "MU_CI_UVM_SET_THREADLOCAL"s
				 : op == "uvm.get_threadlocal" ? "MU_CI_UVM_GET_THREADLOCAL"s
				 : op == "uvm.tr64.is_fp" ? "MU_CI_UVM_TR64_IS_FP"s
				 : op == "uvm.tr64.is_int" ? "MU_CI_UVM_TR64_IS_INT"s
				 : op == "uvm.tr64.is_ref" ? "MU_CI_UVM_TR64_IS_REF"s
				 : op == "uvm.tr64.from_fp" ? "MU_CI_UVM_TR64_FROM_FP"s
				 : op == "uvm.tr64.from_int" ? "MU_CI_UVM_TR64_FROM_INT"s
				 : op == "uvm.tr64.from_ref" ? "MU_CI_UVM_TR64_FROM_REF"s
				 : op == "uvm.tr64.to_fp" ? "MU_CI_UVM_TR64_TO_FP"s
				 : op == "uvm.tr64.to_int" ? "MU_CI_UVM_TR64_TO_INT"s
				 : op == "uvm.tr64.to_ref" ? "MU_CI_UVM_TR64_TO_REF"s
				 : op == "uvm.tr64.to_tag" ? "MU_CI_UVM_TR64_TO_TAG"s
				 : op == "uvm.futex.wait" ? "MU_CI_UVM_FUTEX_WAIT"s
				 : op == "uvm.futex.wait_timeout" ? "MU_CI_UVM_FUTEX_WAIT_TIMEOUT"s
				 : op == "uvm.futex.wake" ? "MU_CI_UVM_FUTEX_WAKE"s
				 : op == "uvm.futex.cmp_requeue" ? "MU_CI_UVM_FUTEX_CMP_REQUEUE"s
				 : op == "uvm.kill_dependency" ? "MU_CI_UVM_KILL_DEPENDENCY"s
				 : op == "uvm.native.pin" ? "MU_CI_UVM_NATIVE_PIN"s
				 : op == "uvm.native.unpin" ? "MU_CI_UVM_NATIVE_UNPIN"s
				 : op == "uvm.native.get_addr" ? "MU_CI_UVM_NATIVE_GET_ADDR"s
				 : op == "uvm.native.expose" ? "MU_CI_UVM_NATIVE_EXPOSE"s
				 : op == "uvm.native.unexpose" ? "MU_CI_UVM_NATIVE_UNEXPOSE"s
				 : op == "uvm.native.get_cookie" ? "MU_CI_UVM_NATIVE_GET_COOKIE"s
				 : op == "uvm.meta.id_of" ? "MU_CI_UVM_META_ID_OF"s
				 : op == "uvm.meta.name_of" ? "MU_CI_UVM_META_NAME_OF"s
				 : op == "uvm.meta.load_bundle" ? "MU_CI_UVM_META_LOAD_BUNDLE"s
				 : op == "uvm.meta.load_hail" ? "MU_CI_UVM_META_LOAD_HAIL"s
				 : op == "uvm.meta.new_cursor" ? "MU_CI_UVM_META_NEW_CURSOR"s
				 : op == "uvm.meta.next_frame" ? "MU_CI_UVM_META_NEXT_FRAME"s
				 : op == "uvm.meta.copy_cursor" ? "MU_CI_UVM_META_COPY_CURSOR"s
				 : op == "uvm.meta.close_cursor" ? "MU_CI_UVM_META_CLOSE_CURSOR"s
				 : op == "uvm.meta.cur_func" ? "MU_CI_UVM_META_CUR_FUNC"s
				 : op == "uvm.meta.cur_func_Ver" ? "MU_CI_UVM_META_CUR_FUNC_VER"s
				 : op == "uvm.meta.cur_inst" ? "MU_CI_UVM_META_CUR_INST"s
				 : op == "uvm.meta.dump_keepalives" ? "MU_CI_UVM_META_DUMP_KEEPALIVES"s
				 : op == "uvm.meta.pop_frames_to" ? "MU_CI_UVM_META_POP_FRAMES_TO"s
				 : op == "uvm.meta.push_frame" ? "MU_CI_UVM_META_PUSH_FRAME"s
				 : op == "uvm.meta.enable_watchpoint" ? "MU_CI_UVM_META_ENABLE_WATCHPOINT"s
				 : op == "uvm.meta.disable_watchpoint" ? "MU_CI_UVM_META_DISABLE_WATCHPOINT"s
				 : op == "uvm.meta.set_trap_handler" ? "MU_CI_UVM_META_SET_TRAP_HANDLER"s
				 : op == "uvm.meta.constant_by_id" ? "MU_CI_UVM_META_CONSTANT_BY_ID"s
                 : op == "uvm.meta.global_by_id" ? "MU_CI_UVM_META_GLOBAL_BY_ID"s
                 : op == "uvm.meta.func_by_id" ? "MU_CI_UVM_META_FUNC_BY_ID"s
                 : op == "uvm.meta.expfunc_by_id" ? "MU_CI_UVM_META_EXPFUNC_BY_ID"s
				 : op == "uvm.irbuilder.new_ir_builder" ? "MU_CI_UVM_IRBUILDER_NEW_IR_BUILDER"s
				 : op == "uvm.irbuilder.load" ? "MU_CI_UVM_IRBUILDER_LOAD"s
				 : op == "uvm.irbuilder.abort" ? "MU_CI_UVM_IRBUILDER_ABORT"s
				 : op == "uvm.irbuilder.gen_sym" ? "MU_CI_UVM_IRBUILDER_GEN_SYM"s
				 : op == "uvm.irbuilder.new_type_int" ? "MU_CI_UVM_IRBUILDER_NEW_TYPE_INT"s
				 : op == "uvm.irbuilder.new_type_float" ? "MU_CI_UVM_IRBUILDER_NEW_TYPE_FLOAT"s
				 : op == "uvm.irbuilder.new_type_double" ? "MU_CI_UVM_IRBUILDER_NEW_TYPE_DOUBLE"s
				 : op == "uvm.irbuilder.new_type_uptr" ? "MU_CI_UVM_IRBUILDER_NEW_TYPE_UPTR"s
				 : op == "uvm.irbuilder.new_type_ufuncptr" ? "MU_CI_UVM_IRBUILDER_NEW_TYPE_UFUNCPTR"s
				 : op == "uvm.irbuilder.new_type_struct" ? "MU_CI_UVM_IRBUILDER_NEW_TYPE_STRUCT"s
				 : op == "uvm.irbuilder.new_type_hybrid" ? "MU_CI_UVM_IRBUILDER_NEW_TYPE_HYBRID"s
				 : op == "uvm.irbuilder.new_type_array" ? "MU_CI_UVM_IRBUILDER_NEW_TYPE_ARRAY"s
				 : op == "uvm.irbuilder.new_type_vector" ? "MU_CI_UVM_IRBUILDER_NEW_TYPE_VECTOR"s
				 : op == "uvm.irbuilder.new_type_void" ? "MU_CI_UVM_IRBUILDER_NEW_TYPE_VOID"s
				 : op == "uvm.irbuilder.new_type_ref" ? "MU_CI_UVM_IRBUILDER_NEW_TYPE_REF"s
				 : op == "uvm.irbuilder.new_type_iref" ? "MU_CI_UVM_IRBUILDER_NEW_TYPE_IREF"s
				 : op == "uvm.irbuilder.new_type_weakref" ? "MU_CI_UVM_IRBUILDER_NEW_TYPE_WEAKREF"s
				 : op == "uvm.irbuilder.new_type_funcref" ? "MU_CI_UVM_IRBUILDER_NEW_TYPE_FUNCREF"s
				 : op == "uvm.irbuilder.new_type_tagref64" ? "MU_CI_UVM_IRBUILDER_NEW_TYPE_TAGREF64"s
				 : op == "uvm.irbuilder.new_type_threadref" ? "MU_CI_UVM_IRBUILDER_NEW_TYPE_THREADREF"s
				 : op == "uvm.irbuilder.new_type_stackref" ? "MU_CI_UVM_IRBUILDER_NEW_TYPE_STACKREF"s
				 : op == "uvm.irbuilder.new_type_framecursorref" ? "MU_CI_UVM_IRBUILDER_NEW_TYPE_FRAMECURSORREF"s
				 : op == "uvm.irbuilder.new_type_irbuilderref" ? "MU_CI_UVM_IRBUILDER_NEW_TYPE_IRBUILDERREF"s
				 : op == "uvm.irbuilder.new_funcsig" ? "MU_CI_UVM_IRBUILDER_NEW_FUNCSIG"s
				 : op == "uvm.irbuilder.new_const_int" ? "MU_CI_UVM_IRBUILDER_NEW_CONST_INT"s
				 : op == "uvm.irbuilder.new_const_int_ex" ? "MU_CI_UVM_IRBUILDER_NEW_CONST_INT_EX"s
				 : op == "uvm.irbuilder.new_const_float" ? "MU_CI_UVM_IRBUILDER_NEW_CONST_FLOAT"s
				 : op == "uvm.irbuilder.new_const_double" ? "MU_CI_UVM_IRBUILDER_NEW_CONST_DOUBLE"s
				 : op == "uvm.irbuilder.new_const_null" ? "MU_CI_UVM_IRBUILDER_NEW_CONST_NULL"s
				 : op == "uvm.irbuilder.new_const_seq" ? "MU_CI_UVM_IRBUILDER_NEW_CONST_SEQ"s
				 : op == "uvm.irbuilder.new_const_extern" ? "MU_CI_UVM_IRBUILDER_NEW_CONST_EXTERN"s
				 : op == "uvm.irbuilder.new_global_cell" ? "MU_CI_UVM_IRBUILDER_NEW_GLOBAL_CELL"s
				 : op == "uvm.irbuilder.new_func" ? "MU_CI_UVM_IRBUILDER_NEW_FUNC"s
				 : op == "uvm.irbuilder.new_exp_func" ? "MU_CI_UVM_IRBUILDER_NEW_EXP_FUNC"s
				 : op == "uvm.irbuilder.new_func_ver" ? "MU_CI_UVM_IRBUILDER_NEW_FUNC_VER"s
				 : op == "uvm.irbuilder.new_bb" ? "MU_CI_UVM_IRBUILDER_NEW_BB"s
				 : op == "uvm.irbuilder.new_dest_clause" ? "MU_CI_UVM_IRBUILDER_NEW_DEST_CLAUSE"s
				 : op == "uvm.irbuilder.new_exc_clause" ? "MU_CI_UVM_IRBUILDER_NEW_EXC_CLAUSE"s
				 : op == "uvm.irbuilder.new_keepalive_clause" ? "MU_CI_UVM_IRBUILDER_NEW_KEEPALIVE_CLAUSE"s
				 : op == "uvm.irbuilder.new_csc_ret_with" ? "MU_CI_UVM_IRBUILDER_NEW_CSC_RET_WITH"s
				 : op == "uvm.irbuilder.new_csc_kill_old" ? "MU_CI_UVM_IRBUILDER_NEW_CSC_KILL_OLD"s
				 : op == "uvm.irbuilder.new_nsc_pass_values" ? "MU_CI_UVM_IRBUILDER_NEW_NSC_PASS_VALUES"s
				 : op == "uvm.irbuilder.new_nsc_throw_exc" ? "MU_CI_UVM_IRBUILDER_NEW_NSC_THROW_EXC"s
				 : op == "uvm.irbuilder.new_binop" ? "MU_CI_UVM_IRBUILDER_NEW_BINOP"s
				 : op == "uvm.irbuilder.new_binop_with_status" ? "MU_CI_UVM_IRBUILDER_NEW_BINOP_WITH_STATUS"s
				 : op == "uvm.irbuilder.new_cmp" ? "MU_CI_UVM_IRBUILDER_NEW_CMP"s
				 : op == "uvm.irbuilder.new_conv" ? "MU_CI_UVM_IRBUILDER_NEW_CONV"s
				 : op == "uvm.irbuilder.new_select" ? "MU_CI_UVM_IRBUILDER_NEW_SELECT"s
				 : op == "uvm.irbuilder.new_branch" ? "MU_CI_UVM_IRBUILDER_NEW_BRANCH"s
				 : op == "uvm.irbuilder.new_branch2" ? "MU_CI_UVM_IRBUILDER_NEW_BRANCH2"s
				 : op == "uvm.irbuilder.new_switch" ? "MU_CI_UVM_IRBUILDER_NEW_SWITCH"s
				 : op == "uvm.irbuilder.new_call" ? "MU_CI_UVM_IRBUILDER_NEW_CALL"s
				 : op == "uvm.irbuilder.new_tailcall" ? "MU_CI_UVM_IRBUILDER_NEW_TAILCALL"s
				 : op == "uvm.irbuilder.new_ret" ? "MU_CI_UVM_IRBUILDER_NEW_RET"s
				 : op == "uvm.irbuilder.new_throw" ? "MU_CI_UVM_IRBUILDER_NEW_THROW"s
				 : op == "uvm.irbuilder.new_extractvalue" ? "MU_CI_UVM_IRBUILDER_NEW_EXTRACTVALUE"s
				 : op == "uvm.irbuilder.new_insertvalue" ? "MU_CI_UVM_IRBUILDER_NEW_INSERTVALUE"s
				 : op == "uvm.irbuilder.new_extractelement" ? "MU_CI_UVM_IRBUILDER_NEW_EXTRACTELEMENT"s
				 : op == "uvm.irbuilder.new_insertelement" ? "MU_CI_UVM_IRBUILDER_NEW_INSERTELEMENT"s
				 : op == "uvm.irbuilder.new_shufflevector" ? "MU_CI_UVM_IRBUILDER_NEW_SHUFFLEVECTOR"s
				 : op == "uvm.irbuilder.new_new" ? "MU_CI_UVM_IRBUILDER_NEW_NEW"s
				 : op == "uvm.irbuilder.new_newhybrid" ? "MU_CI_UVM_IRBUILDER_NEW_NEWHYBRID"s
				 : op == "uvm.irbuilder.new_alloca" ? "MU_CI_UVM_IRBUILDER_NEW_ALLOCA"s
				 : op == "uvm.irbuilder.new_allocahybrid" ? "MU_CI_UVM_IRBUILDER_NEW_ALLOCAHYBRID"s
				 : op == "uvm.irbuilder.new_getiref" ? "MU_CI_UVM_IRBUILDER_NEW_GETIREF"s
				 : op == "uvm.irbuilder.new_getfieldiref" ? "MU_CI_UVM_IRBUILDER_NEW_GETFIELDIREF"s
				 : op == "uvm.irbuilder.new_getelemiref" ? "MU_CI_UVM_IRBUILDER_NEW_GETELEMIREF"s
				 : op == "uvm.irbuilder.new_shiftiref" ? "MU_CI_UVM_IRBUILDER_NEW_SHIFTIREF"s
				 : op == "uvm.irbuilder.new_getvarpartiref" ? "MU_CI_UVM_IRBUILDER_NEW_GETVARPARTIREF"s
				 : op == "uvm.irbuilder.new_load" ? "MU_CI_UVM_IRBUILDER_NEW_LOAD"s
				 : op == "uvm.irbuilder.new_store" ? "MU_CI_UVM_IRBUILDER_NEW_STORE"s
				 : op == "uvm.irbuilder.new_cmpxchg" ? "MU_CI_UVM_IRBUILDER_NEW_CMPXCHG"s
				 : op == "uvm.irbuilder.new_atomicrmw" ? "MU_CI_UVM_IRBUILDER_NEW_ATOMICRMW"s
				 : op == "uvm.irbuilder.new_fence" ? "MU_CI_UVM_IRBUILDER_NEW_FENCE"s
				 : op == "uvm.irbuilder.new_trap" ? "MU_CI_UVM_IRBUILDER_NEW_TRAP"s
				 : op == "uvm.irbuilder.new_watchpoint" ? "MU_CI_UVM_IRBUILDER_NEW_WATCHPOINT"s
				 : op == "uvm.irbuilder.new_wpbranch" ? "MU_CI_UVM_IRBUILDER_NEW_WPBRANCH"s
				 : op == "uvm.irbuilder.new_ccall" ? "MU_CI_UVM_IRBUILDER_NEW_CCALL"s
				 : op == "uvm.irbuilder.new_newthread" ? "MU_CI_UVM_IRBUILDER_NEW_NEWTHREAD"s
				 : op == "uvm.irbuilder.new_swapstack" ? "MU_CI_UVM_IRBUILDER_NEW_SWAPSTACK"s
				 : op == "uvm.irbuilder.new_comminst" ? "MU_CI_UVM_IRBUILDER_NEW_COMMINST"s
				 : throw antlr4::RuntimeException("Unrecognised common instruction \"@"s + op + "\""s);
		}

		// ******************************* Trivials, mearly accept ther child *****************************
		virtual Any<MuFuncNode> visitFunc(UIRParser::FuncContext *context) override { return accept<MuID>(context->nam); }
		virtual Any<MuGlobalVarNode> visitGlobalVar(UIRParser::GlobalVarContext *context) override { return accept<MuID>(context->nam); }
		virtual Any<MuBBNode> visitBb(UIRParser::BbContext *context) override { return accept<MuID>(context->nam); }
		virtual Any<MuID> visitInstResult(UIRParser::InstResultContext *context) override { return accept<MuID>(context->result); }
		virtual Any<MuFlag> visitFlag(UIRParser::FlagContext *context) override { return context->callConv() ? accept<MuCallConv>(context->callConv()) : accept<MuBinOpStatus>(context->binopStatus()); }
        virtual Any<MuVarNode> visitValue(UIRParser::ValueContext *context) override { return context->nam ? accept<MuID>(context->nam) : accept<MuConstNode>(context->constant()); }
	};
}


void c_compile(std::string ctx, std::string filename, std::ostream& output, std::string irbuilder) {
    std::ifstream input_file (filename);
	if (input_file.fail()) {
		throw antlr4::RuntimeException("Couldn't oppen file \""s + filename + "\""s);
	}

    antlr4::ANTLRInputStream input(input_file);

    UIRLexer lexer(&input);
    antlr4::CommonTokenStream tokens(&lexer);

    tokens.fill();
    UIRParser parser(&tokens);

	auto ir = parser.ir();
    if (lexer.getNumberOfSyntaxErrors() != 0 || parser.getNumberOfSyntaxErrors() != 0)
		::error = true;
    else
        C::Visitor(ctx, output, irbuilder).accept(ir);
}
