// Copyright 2017 The Australian National University
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <iostream>
#include <vector>
#include <libgen.h>
#include <cstring>
#include "mu-fastimpl.h"
#include "muapi.h"

#ifndef NO_MU
    #include "RuntimeVisitor.hpp"
#endif

#include "CVisitor.hpp"

using namespace std::literals;

// Get the directory name
// (this is needed as 'dirname' modifies it's argument, so were allocating and deallocating a temporary buffer to be safe)
std::string directory_name(const char* s) {
    char* buffer = std::strcpy(new char[std::strlen(s)], s);
    std::string res = dirname(buffer);
    delete[] buffer;
    return res;
}

std::string file_name(const char* s) {
    char* buffer = std::strcpy(new char[std::strlen(s)], s);
    std::string res = basename(buffer);
    delete[] buffer;
    return res;
}

std::string escape_string(std::string s)
{
	std::string output;
	for (char c : s)
	{
		switch (c)
		{
		case '\t':
			output += R"(\t)";
			break;
		case '\\':
			output += R"(\\)";
			break;
		case '\n':
			output += R"(\n)";
			break;
		case '\r':
			output += R"(\r)";
			break;
		case '"':
			output += R"(\")";
			break;
		default:
			output += c;
		}
	}
	return output;
}

int show_usage() {
    std::cerr << "usage: " << std::endl;
    std::cerr << "\tmuc -rcs [-f primordial-function] input_files... output_dir/boot_image_file [-- zebu_options...]" << std::endl;
    return -1;
}

int main(int argc, char** argv) {
    if (argc < 4)
        return show_usage();

    char** input;
    int n_input;
	int last_arg = argc-1;
	int extra_opts_count = 0;
	std::string extra_opts = "";
	for (int i = 0; i < argc; i++)
	{
		if (std::string(argv[i]) == "--")
		{
			last_arg = i - 1;
			extra_opts_count = 1;
			for (int j = i + 1; j < argc; j++)
			{
				extra_opts_count++;
				extra_opts += " "+std::string(argv[j]);
			}
			break;
		}
	}
    std::string output_file = file_name(argv[last_arg]);
    std::string output_dir = directory_name(argv[last_arg]);
    const char* primordial; // the name of the primordial function
    bool has_primordial;
    if (argv[2] == "-f"s) {
        if (argc < 6)
            return show_usage();

        primordial = argv[3];
        if (primordial[0] == '@') // Skip a leading '@'
            primordial = &primordial[1];

        input = &argv[4];
        n_input = argc - 5; //muc -c -f primordial input... output
        has_primordial = true;
    } else {
        primordial = "";
        input = &argv[2];
        n_input = argc - 3;
        has_primordial = false;
    }
	n_input -= extra_opts_count;
    ::primordial_name = primordial;
	std::string options = "init_mu --aot-emit-dir="s + output_dir + extra_opts;

	if (argv[1] == "-s"s) {
		for (int i = 0; i < n_input; i++)
		{
			std::ifstream input_file (input[i]);
			antlr4::ANTLRInputStream input_stream(input_file);

			UIRLexer lexer(&input_stream);
			antlr4::CommonTokenStream tokens(&lexer);
			tokens.fill();

			UIRParser parser(&tokens);
			parser.ir();

			auto le = lexer.getNumberOfSyntaxErrors();
			auto pe = parser.getNumberOfSyntaxErrors();

			if (le || pe)
				std::cerr << "ERROR: " << input[i] << " (lexer " << le << ", parser " << pe << ")" << std::endl;
		}

		if (::error)
			return -1;
	} else if (argv[1] == "-r"s) {
    #ifndef NO_MU

        MuVM* mvm = mu_fastimpl_new_with_opts(options.c_str());
        MuCtx* ctx = mvm->new_context(mvm);


        for (int i = 0; i < n_input; i++)
            runtime_compile(ctx, std::string(input[i]));

        if (::error) {
            std::cerr << "ERROR: input is invalid" << std::endl;
            return -1;
        }

		if (has_primordial && Runtime::primordial_id == MU_NO_ID) {
			std::cerr << "ERROR: couldn't find primordial function \"" << ::primordial_name << "\"" << std::endl;
			return -1;
		}
        MuFuncRefValue primordial_exp = has_primordial ?
             ctx->handle_from_func(ctx, Runtime::primordial_id) : nullptr;

        ctx->make_boot_image(ctx,
            &Runtime::whitelist[0], Runtime::whitelist.size(), // whitelist
            primordial_exp, nullptr, nullptr, // primordial
            nullptr, nullptr, 0, //sym
            nullptr, nullptr, 0, // reloc
            output_file.c_str());
    #else
        std::cerr << "ERROR: muc was compiled with 'NO_MU' defined" << std::endl;
        return -1;
    #endif
    } else if (argv[1] == "-c"s) {
        // Include files
        std::cout << "#include <math.h>" << std::endl
                  << "#include <stddef.h>" << std::endl
                  << "#include <stdint.h>" << std::endl
                  << "#include \"muapi.h\"" << std::endl
                  << "#include \"mu-fastimpl.h\"" << std::endl
                  << std::endl
				  << "#define G(id, ...) global_ ## id" << std::endl
				  << "#define L(id, ...)  local_ ## id" << std::endl
				  << std::endl;

        // Function header
        std::cout << "int main() {" << std::endl;

        std::string mvm = "mvm"s; // The name of the MuVM*
        std::string ctx = "ctx"s; // the name of the MuCtx*
        std::string irbuilder = "irbuilder"s; // the name of the MuIRBuilder*

        std::cout << "\tMuVM* " << mvm << " = mu_fastimpl_new_with_opts(\"" << escape_string(options) << "\");" << std::endl
                  << "\tMuCtx* " << ctx << " = " << mvm << "->new_context(" << mvm << ");" << std::endl
                  << "\tMuIRBuilder* " << irbuilder << ";" << std::endl << std::endl;

        for (int i = 0; i < n_input; i++)
            c_compile(ctx, input[i], std::cout, irbuilder);

        if (::error) {
            std::cerr << "ERROR: input is invalid" << std::endl;
            return -1;
        }

		if (has_primordial && Runtime::primordial_id == MU_NO_ID) {
			std::cerr << "ERROR: couldn't find primordial function \"" << ::primordial_name << "\"" << std::endl;
			return -1;
		}

        std::string primordial_exp = has_primordial ?
                ctx + "->handle_from_func("s + ctx + ", "s + C::primordial_id + ")"s : "NULL"s;

        Array_String ids ("MuID", C::whitelist);
        std::cout << "\t" << ctx << "->make_boot_image(" << ctx << ", " << std::endl
                  << "\t\t" << ids.value << ", " << ids.size << ", " << std::endl
                  << "\t\t" << primordial_exp << ", NULL, NULL, NULL, NULL, 0, NULL, NULL, 0," << std::endl
                  << "\t\t\"" << output_file << "\");" << std::endl;

        std::cout << "}" << std::endl;
    } else {
        return show_usage();
    }
}
